<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/
$_SESSION['module'] = 'Legal Reports';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["process"]))
	{
		$search_process = $_GET["process"];
	}
	else
	{
		$search_process = "";
	}

	if(isset($_GET["completed"]))
	{
		$search_completed = 1;
	}
	else
	{
		$search_completed = "";
	}
	// Temp data
	$alert = "";

	$file_number   = "";
	$file_type     = "";
	$survey_number = "";
	$land_owner    = "";
	$pan_number    = "";
	$extent        = "";
	$village       = "";
	$bd_project_id = "";	
	
	$search_user   = "";
	$search_reason = "";

	// Search parameters
	if(isset($_POST["file_search_submit"]))
	{
		$file_number    = $_POST["search_file_no"];
		$survey_number  = $_POST["search_survey_no"];
		$search_user    = $_POST["search_user"];
		$search_reason  = $_POST["ddl_reason"];
		$bd_project_id  = $_POST["ddl_bd_project"];
		$search_process = $_POST["ddl_process"];
	}
	
	// Get list of files
	$legal_file_list = i_get_file_list($file_number,'',$file_type,$survey_number,$land_owner,$pan_number,$extent,$village,'','','',$bd_project_id,'','',$search_process);

	if($legal_file_list["status"] == SUCCESS)
	{
		$legal_file_list_data = $legal_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_file_list["data"];
	}

	// Get list of users
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
	}

	// Get list of reasons for task delay	
	$reason_list = i_get_reason_list('','1','','','');
	if($reason_list["status"] == SUCCESS)
	{
		$reason_list_data = $reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$reason_list["data"];
	}
	
	// Get list of BD project	
	$bd_project_list = i_get_bd_project_list('','','','');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_project_list_data = $bd_project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_project_list["data"];
	}
	
	// Get list of process types
	$process_type_list = i_get_process_type_list('','','1','0');

	if($process_type_list["status"] == SUCCESS)
	{
		$process_type_list_data = $process_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$process_type_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Consolidated Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Consolidated Report&nbsp;&nbsp;&nbsp;Count: <span id="total_records"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Total Extent: <span id="total_extent_span"><i>Calculating</i></span> guntas</h3>			  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:100px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="consolidated_report.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="search_file_no" value="" placeholder="Search by file number" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="search_survey_no" value="" placeholder="Search by survey number" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_bd_project">
			  <option value="">- - Select BD Project - -</option>
			  <?php
			  for($count = 0; $count < count($bd_project_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $bd_project_list_data[$count]["bd_project_id"]; ?>" <?php if($bd_project_id == $bd_project_list_data[$count]["bd_project_id"]) { ?> selected="selected" <?php } ?>><?php echo $bd_project_list_data[$count]["bd_project_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select User - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_reason">
			  <option value="">- - Select Reason - -</option>
			  <?php
			  for($reason_count = 0; $reason_count < count($reason_list_data); $reason_count++)
			  {
			  ?>
			  <option value="<?php echo $reason_list_data[$reason_count]["reason_master_id"]; ?>" <?php if($search_reason == $reason_list_data[$reason_count]["reason_master_id"]) { ?> selected="selected" <?php } ?>><?php echo strtoupper($reason_list_data[$reason_count]["reason"]); ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($process_type_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $process_type_list_data[$process_count]["process_master_id"]; ?>" <?php if($search_process == $process_type_list_data[$process_count]["process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $process_type_list_data[$process_count]["process_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>File Number</th>
					<!-- <th>Created Date</th>
					<th>File Days</th> -->
					<th>Sy No</th>	
					<th>Land Owner</th>
					<th>Extent</th>	
					<th>Village</th>
					<th>Process</th>
					<th>Process Start</th>
					<th>Process Lead</th>
					<th>Task</th>
					<th>Start Date</th>
					<th>Days</th>
					<th>Reason</th>
					<th>Remarks</th>					
					<th>User</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<?php
					if($user == '143736679571416700')
					{
					?>
					<th>&nbsp;</th>
					<?php
					}
					?>
				</tr>
				</thead>
				<tbody>
				 <?php
				$record_count = 0;
				$total_extent = 0;
				
				if($legal_file_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($legal_file_list_data); $count++)
					{						
						$is_active_process = i_get_legal_process_plan_list('',$legal_file_list_data[$count]["file_id"],'','','','','','','','0','');
							
						if(($is_active_process['status'] == SUCCESS) || ($search_completed == 1))
						{					
							$file_handover_data = i_get_file_handover_details($legal_file_list_data[$count]["file_id"]);
							
							if($file_handover_data["status"] == SUCCESS)
							{
								//echo $file_handover_data['data'][0]['file_id'];
							}
							if($file_handover_data["status"] != SUCCESS)
							{	
								$completed_file_data = i_get_completed_process_plan_list($legal_file_list_data[$count]["file_id"],'','1');
								$is_completed = 0;
								if($completed_file_data["status"] == SUCCESS)							
								{
									$is_completed = 1;
								}
								
								$show_completed = 0;
								if($search_completed != "")
								{										
									// Check if the latest bulk process is complete
									$legal_bulk_search_data = array("file_no"=>$legal_file_list_data[$count]["file_number"]);
									$legal_bulk_result = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);
									if($legal_bulk_result['status'] == SUCCESS)
									{
										// Check if all bulk processes status is complete
										$bproc_completed = true;
										for($bcount = 0; $bcount < count($legal_bulk_result['data']); $bcount++)
										{
											if($legal_bulk_result['data'][$bcount]['legal_bulk_process_completed'] != '1')
											{
												$bproc_completed = false;
											}
										}
										
										if($bproc_completed == false)
										{
											$show_completed = 0;
										}
										else
										{
											$show_completed = 1;
										}
									}
									else
									{
										$show_completed = 1;
									}	

									$show_completed = $show_completed & $is_completed;
								}																		
								
								$process_task_plan_result = i_get_task_plan_list('','',$legal_file_list_data[$count]["file_last_process"],'','','','','slno');
								
								if($process_task_plan_result['status'] == SUCCESS)
								{																	
									$process_start_date = $process_task_plan_result['data'][0]['task_plan_actual_start_date'];
									$process_variance   = get_date_diff($process_start_date,date('Y-m-d'));
									$process_start_date = date('d-M-Y',strtotime($process_start_date));
								}
								else
								{
									$process_start_date       = '';
									$process_variance['data'] = '';
								}
								
								if((($is_completed == 0) && ($search_completed == '')) || ($show_completed == 1))
								{
						
								$task_user_data = i_get_task_plan_list($legal_file_list_data[$count]["file_last_task"],'','','','','','','');				
									
								$task_user_id   = $task_user_data["data"][0]["user_id"];							
								$task_user_name = $task_user_data["data"][0]["user_name"];
							
								if(($search_user != "") && ($search_user != $task_user_id))
								{
									// Do nothing
								}
								else
								{
								// Get file payment details
								$file_payment_list = i_get_file_payment_list($legal_file_list_data[$count]["file_id"],'');
								if($file_payment_list["status"] == SUCCESS)
								{
									$file_paid = 0;
									for($pay_count = 0; $pay_count < count($file_payment_list["data"]); $pay_count++)
									{
										$file_paid = $file_paid + $file_payment_list["data"][$pay_count]["file_payment_amount"];
									}
								}
								else
								{
									$file_paid = 0;
								}
								
								if(($search_reason == "") || ($search_reason == $task_user_data["data"][0]["task_plan_delay_reason"]))
								{
									$record_count++;								
									
									$total_extent = $total_extent + $legal_file_list_data[$count]["file_extent"];
				?>
					<tr id="row_<?php echo $count; ?>" style="color:black;">
						<td style="word-wrap:break-word;"><?php echo $legal_file_list_data[$count]["file_number"]; ?></td>		
						<!-- <td style="word-wrap:break-word;"><?php $start_date = date("d-M-Y",strtotime($legal_file_list_data[$count]["file_start_date"]));
						echo $start_date;?></td> -->
						<!-- <td style="word-wrap:break-word;"><?php $lead_time = get_date_diff(date("Y-m-d",strtotime($legal_file_list_data[$count]["file_start_date"])),date("Y-m-d"));
						echo $lead_time["data"]; ?></td> -->						
						<td style="word-wrap:break-word;"><?php echo $legal_file_list_data[$count]["file_survey_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_file_list_data[$count]["file_land_owner"]; ?></td>							
						<td style="word-wrap:break-word;"><?php $extent_array = get_acre_from_guntas($legal_file_list_data[$count]["file_extent"]);
								  echo $extent_array["acres"]." acres ".$extent_array["guntas"]." guntas"; ?></td>	
						<td style="word-wrap:break-word;"><?php if($legal_file_list_data[$count]["village_name"] != "")
						{
							echo $legal_file_list_data[$count]["village_name"]; 
						}
						else
						{
							echo $legal_file_list_data[$count]["file_village"]; 
						}?></td>											
						<td style="word-wrap:break-word;"><?php if($is_completed != 1)
						{
							if($search_completed == '')
							{
								echo $legal_file_list_data[$count]["process_name"]; 
							}
						}
						?></td>
						<td style="word-wrap:break-word;"><?php if($is_completed != 1)
						{
							if($search_completed == '')
							{
								echo $process_start_date;
							}
						}?></td>
						<td style="word-wrap:break-word;"><?php if($is_completed != 1)
						{
							if($search_completed == '')
							{
								echo $process_variance['data'];
							}
						}?></td>
						<td style="word-wrap:break-word;"><?php if($is_completed != 1)
						{
							if($search_completed == '')
							{
								echo $legal_file_list_data[$count]["task_type_name"]; 
							}
						}?></td>
						<td style="word-wrap:break-word;"><?php if($is_completed != 1)
						{
							if($search_completed == '')
							{
								if($legal_file_list_data[$count]["actual_start_date"] != '0000-00-00')
								{
									echo date("d-M-Y",strtotime($legal_file_list_data[$count]["actual_start_date"])); 
								}
								else
								{
									echo '';
								}
							}
						}?></td>
						<td style="word-wrap:break-word;"><?php if($search_completed == '')
						{
							$task_variance = get_date_diff($legal_file_list_data[$count]["actual_start_date"],date("Y-m-d"));
							if($task_variance["status"] != 2)
							{
								echo $task_variance["data"];
								
								if($task_variance["data"] == 1)
								{
									$font_color = 'black';
								}
								else if($task_variance["data"] == 1)
								{
									$font_color = 'green';
								}
								else if(($task_variance["data"] == 2) || ($task_variance["data"] == 3))
								{
									$font_color = 'blue';
								}
								else if($task_variance["data"] >= 4)
								{
									$font_color = 'red';
								}
								
								?>
								<script>
								document.getElementById('row_<?php echo $count; ?>').style.color = "<?php echo $font_color; ?>";
								</script>
								<?php
							}
							else
							{
								echo "";
							}
						}?></td>
						<td style="word-wrap:break-word;"><?php echo strtoupper($task_user_data["data"][0]["reason"]); ?></td>
						<td style="word-wrap:break-word;"><a href="#" onClick="alert('<?php echo $task_user_data["data"][0]["task_plan_delay_remarks"]; ?>')"><?php echo substr($task_user_data["data"][0]["task_plan_delay_remarks"],0,35); ?></a></td>
						<!-- <td style="word-wrap:break-word;"><?php echo get_formatted_amount($legal_file_list_data[$count]["file_deal_amount"],"INDIA"); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_amount($file_paid,"INDIA"); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_amount($legal_file_list_data[$count]["file_deal_amount"] - $file_paid,"INDIA"); ?></td> -->
						<td style="word-wrap:break-word;"><?php echo $task_user_name;?></td>
						<td style="word-wrap:break-word;"><a href="reason_list.php?task=<?php if($legal_file_list_data[$count]["file_last_task"] != '')
						{
							echo $legal_file_list_data[$count]["file_last_task"]; 
						}
						else
						{
							echo $legal_file_list_data[$count]["file_last_bulk_task"];
						}?>" target="_blank">Delay Reason List</a>						
						</td>
						<?php
						if($user == '143736679571416700')
						{
						?>
						<td style="word-wrap:break-word;"><a href="create_file_notes.php?file=<?php echo $legal_file_list_data[$count]["file_id"]; ?>" target="_blank">Notes</td>
						<?php
						}
						?>
						<td style="word-wrap:break-word;"><a href="file_process_list.php?file=<?php echo $legal_file_list_data[$count]["file_id"]; ?>" target="_blank">Processes</td>
					</tr>								
					<?php
							}
						}
						}
						}
						}
						else
						{
							// Tihs file has no processes
						}						
					}
				}
				else
				{
				?>
				<td colspan="18">No Files added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <script>
			  document.getElementById("total_records").innerHTML = <?php echo $record_count; ?>;
			  </script>
			  <script>
			  document.getElementById("total_extent_span").innerHTML = <?php echo $total_extent; ?>;
			  </script>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
