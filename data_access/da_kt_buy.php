<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
PURPOSE : To add new KT Buy Process Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Process Master ID, success or failure message
BY 		: Lakshmi
*/
function db_add_kt_buy_process_master($name,$remarks,$added_by)
{
	// Query
   $kt_buy_process_master_iquery = "insert into kt_buy_process_master (kt_buy_process_master_name,kt_buy_process_master_active,kt_buy_process_master_remarks,kt_buy_process_master_added_by,kt_buy_process_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $kt_buy_process_master_istatement = $dbConnection->prepare($kt_buy_process_master_iquery);
        
        // Data
        $kt_buy_process_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $kt_buy_process_master_istatement->execute($kt_buy_process_master_idata);
		$kt_buy_process_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $kt_buy_process_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get KT Buy Process Master list
INPUT 	: Process Master ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Buy Process Master
BY 		: Lakshmi
*/
function db_get_kt_buy_process_master($kt_buy_process_master_search_data)
{  
	if(array_key_exists("process_master_id",$kt_buy_process_master_search_data))
	{
		$process_master_id = $kt_buy_process_master_search_data["process_master_id"];
	}
	else
	{
		$process_master_id= "";
	}
	
	if(array_key_exists("name",$kt_buy_process_master_search_data))
	{
		$name = $kt_buy_process_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("process_name_check",$kt_buy_process_master_search_data))
	{
		$process_name_check = $kt_buy_process_master_search_data["process_name_check"];
	}
	else
	{
		$process_name_check = "";
	}
	
	if(array_key_exists("active",$kt_buy_process_master_search_data))
	{
		$active = $kt_buy_process_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$kt_buy_process_master_search_data))
	{
		$added_by = $kt_buy_process_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$kt_buy_process_master_search_data))
	{
		$start_date= $kt_buy_process_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$kt_buy_process_master_search_data))
	{
		$end_date= $kt_buy_process_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_kt_buy_process_master_list_squery_base = "select * from kt_buy_process_master KBPM inner join users U on U.user_id = KBPM.kt_buy_process_master_added_by";
	
	$get_kt_buy_process_master_list_squery_where = "";
	$get_kt_buy_process_master_list_squery_order_by = " order by kt_buy_process_master_name ASC";
	
	$filter_count = 0;
	
	// Data
	$get_kt_buy_process_master_list_sdata = array();
	
	if($process_master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." where kt_buy_process_master_id = :process_master_id";
		}
		else
		{
			// Query
			$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." and kt_buy_process_master_id = :process_master_id";
		}
		
		// Data
		$get_kt_buy_process_master_list_sdata[':process_master_id'] = $process_master_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($process_name_check == '1')
			{
				$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." where kt_buy_process_master_name = :name";		
				// Data
				$get_kt_buy_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." where kt_buy_process_master_name like :name";						

				// Data
				$get_kt_buy_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." where kt_buy_process_master_name like :name";						

				// Data
				$get_kt_buy_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($process_name_check == '1')
			{
				$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." and kt_buy_process_master_name = :name";	
					
				// Data
				$get_kt_buy_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." or kt_buy_process_master_name like :name";				
				// Data
				$get_kt_buy_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." and kt_buy_process_master_name like :name";
				
				// Data
				$get_kt_buy_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." where kt_buy_process_master_active = :active";
		}
		else
		{
			// Query
			$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." and kt_buy_process_master_active = :active";
		}
		
		// Data
		$get_kt_buy_process_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." where kt_buy_process_master_added_by = :added_by";
		}
		else
		{
			// Query
			$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." and kt_buy_process_master_added_by = :added_by";
		}
		
		//Data
		$get_kt_buy_process_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." where kt_buy_process_master_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." and kt_buy_process_master_added_on >= :start_date";
		}
		
		//Data
		$get_kt_buy_process_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." where kt_buy_process_master_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_kt_buy_process_master_list_squery_where = $get_kt_buy_process_master_list_squery_where." and kt_buy_process_master_added_on <= :end_date";
		}
		
		//Data
		$get_kt_buy_process_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_kt_buy_process_master_list_squery = $get_kt_buy_process_master_list_squery_base.$get_kt_buy_process_master_list_squery_where.$get_kt_buy_process_master_list_squery_order_by;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_kt_buy_process_master_list_sstatement = $dbConnection->prepare($get_kt_buy_process_master_list_squery);
		
		$get_kt_buy_process_master_list_sstatement -> execute($get_kt_buy_process_master_list_sdata);
		
		$get_kt_buy_process_master_list_sdetails = $get_kt_buy_process_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_kt_buy_process_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_kt_buy_process_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_kt_buy_process_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
 
/*
PURPOSE : To update KT Buy Process Master
INPUT 	: Process Master ID, KT Buy Process Master Update Array
OUTPUT 	: Process Master ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_kt_buy_process_master($process_master_id,$kt_buy_process_master_update_data)
{
	if(array_key_exists("name",$kt_buy_process_master_update_data))
	{	
		$name = $kt_buy_process_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$kt_buy_process_master_update_data))
	{	
		$active = $kt_buy_process_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$kt_buy_process_master_update_data))
	{	
		$remarks = $kt_buy_process_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$kt_buy_process_master_update_data))
	{	
		$added_by = $kt_buy_process_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$kt_buy_process_master_update_data))
	{	
		$added_on = $kt_buy_process_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $kt_buy_process_master_update_uquery_base = "update kt_buy_process_master set ";  
	
	$kt_buy_process_master_update_uquery_set = "";
	
	$kt_buy_process_master_update_uquery_where = " where kt_buy_process_master_id = :process_master_id";
	
	$kt_buy_process_master_update_udata = array(":process_master_id"=>$process_master_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$kt_buy_process_master_update_uquery_set = $kt_buy_process_master_update_uquery_set." kt_buy_process_master_name = :name,";
		$kt_buy_process_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$kt_buy_process_master_update_uquery_set = $kt_buy_process_master_update_uquery_set." kt_buy_process_master_active = :active,";
		$kt_buy_process_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$kt_buy_process_master_update_uquery_set = $kt_buy_process_master_update_uquery_set." kt_buy_process_master_remarks = :remarks,";
		$kt_buy_process_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$kt_buy_process_master_update_uquery_set = $kt_buy_process_master_update_uquery_set." kt_buy_process_master_added_by = :added_by,";
		$kt_buy_process_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$kt_buy_process_master_update_uquery_set = $kt_buy_process_master_update_uquery_set." kt_buy_process_master_added_on = :added_on,";
		$kt_buy_process_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$kt_buy_process_master_update_uquery_set = trim($kt_buy_process_master_update_uquery_set,',');
	}
	
	$kt_buy_process_master_update_uquery = $kt_buy_process_master_update_uquery_base.$kt_buy_process_master_update_uquery_set.$kt_buy_process_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $kt_buy_process_master_update_ustatement = $dbConnection->prepare($kt_buy_process_master_update_uquery);		
        
        $kt_buy_process_master_update_ustatement -> execute($kt_buy_process_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_master_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new KT Buy Request
INPUT 	: Project, Site No, Dimension, Extent, Request Date, Request For, Remarks, Added By
OUTPUT 	: Request ID, success or failure message
BY 		: Lakshmi
*/
function db_add_kt_buy_request($project,$site_no,$dimension,$extent,$request_date,$request_for,$remarks,$added_by)
{
	// Query
   $kt_buy_request_iquery = "insert into kt_buy_request 
   (kt_buy_request_project,kt_buy_request_site_no,kt_buy_request_dimension,kt_buy_request_extent,kt_buy_request_date,kt_buy_request_for,kt_buy_request_active,
   kt_buy_request_remarks,kt_buy_request_added_by,kt_buy_request_added_on)
   values(:project,:site_no,:dimension,:extent,:request_date,:request_for,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $kt_buy_request_istatement = $dbConnection->prepare($kt_buy_request_iquery);
        
        // Data
        $kt_buy_request_idata = array(':project'=>$project,':site_no'=>$site_no,':dimension'=>$dimension,':extent'=>$extent,':request_date'=>$request_date,
		':request_for'=>$request_for,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $kt_buy_request_istatement->execute($kt_buy_request_idata);
		$kt_buy_request_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $kt_buy_request_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get KT Buy Request List
INPUT 	: Request ID, Project, Site No, Dimension, Extent, Request Date, Request For, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Buy Request
BY 		: Lakshmi
*/
function db_get_kt_buy_request($kt_buy_request_search_data)
{  
	if(array_key_exists("request_id",$kt_buy_request_search_data))
	{
		$request_id = $kt_buy_request_search_data["request_id"];
	}
	else
	{
		$request_id= "";
	}
	
	if(array_key_exists("project",$kt_buy_request_search_data))
	{
		$project = $kt_buy_request_search_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("site_no",$kt_buy_request_search_data))
	{
		$site_no = $kt_buy_request_search_data["site_no"];
	}
	else
	{
		$site_no = "";
	}
	
	if(array_key_exists("dimension",$kt_buy_request_search_data))
	{
		$dimension = $kt_buy_request_search_data["dimension"];
	}
	else
	{
		$dimension = "";
	}
	
	if(array_key_exists("extent",$kt_buy_request_search_data))
	{
		$extent = $kt_buy_request_search_data["extent"];
	}
	else
	{
		$extent = "";
	}
	
	if(array_key_exists("request_for",$kt_buy_request_search_data))
	{
		$request_for = $kt_buy_request_search_data["request_for"];
	}
	else
	{
		$request_for = "";
	}
	
	if(array_key_exists("request_start_date",$kt_buy_request_search_data))
	{
		$request_start_date = $kt_buy_request_search_data["request_start_date"];
	}
	else
	{
		$request_start_date = "";
	}
	
	if(array_key_exists("request_end_date",$kt_buy_request_search_data))
	{
		$request_end_date = $kt_buy_request_search_data["request_end_date"];
	}
	else
	{
		$request_end_date = "";
	}
	
	if(array_key_exists("active",$kt_buy_request_search_data))
	{
		$active = $kt_buy_request_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$kt_buy_request_search_data))
	{
		$added_by = $kt_buy_request_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$kt_buy_request_search_data))
	{
		$start_date= $kt_buy_request_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$kt_buy_request_search_data))
	{
		$end_date= $kt_buy_request_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_kt_buy_request_list_squery_base = "select * from kt_buy_request KBR inner join users U on U.user_id = KBR.kt_buy_request_added_by inner join kt_buy_project_master KBPP on KBPP.kt_buy_project_master_id = KBR.kt_buy_request_project inner join bd_own_account_master AM on AM.bd_own_accunt_master_id = KBR.kt_buy_request_for";
	
	$get_kt_buy_request_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_kt_buy_request_list_sdata = array();
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_id = :request_id";				
		}
		
		// Data
		$get_kt_buy_request_list_sdata[':request_id'] = $request_id;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_project = :project";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_project = :project";				
		}
		
		// Data
		$get_kt_buy_request_list_sdata[':project'] = $project;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_project = :project";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_project = :project";				
		}
		
		// Data
		$get_kt_buy_request_list_sdata[':project'] = $project;
		
		$filter_count++;
	}
	
	if($site_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_site_no = :site_no";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_site_no = :site_no";				
		}
		
		// Data
		$get_kt_buy_request_list_sdata[':site_no'] = $site_no;
		
		$filter_count++;
	}
	
	if($dimension != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_dimension = :dimension";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_dimension = :dimension";				
		}
		
		// Data
		$get_kt_buy_request_list_sdata[':dimension'] = $dimension;
		
		$filter_count++;
	}
	
	if($extent != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_extent = :extent";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_extent = :extent";				
		}
		
		// Data
		$get_kt_buy_request_list_sdata[':extent'] = $extent;
		
		$filter_count++;
	}
	
	if($request_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_date >= :request_start_date";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_date >= :request_start_date";				
		}
		
		// Data
		$get_kt_buy_request_list_sdata[':request_start_date']  = $request_start_date;
		
		$filter_count++;
	}
	
	if($request_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_date <= :request_end_date";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_date <= :request_end_date";				
		}
		
		// Data
		$get_kt_buy_request_list_sdata[':request_end_date']  = $request_end_date;
		
		$filter_count++;
	}
	
	if($request_for != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_for = :request_for";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_for = :request_for";				
		}
		
		// Data
		$get_kt_buy_request_list_sdata[':request_for']  = $request_for;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_active = :active";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_active = :active";				
		}
		
		// Data
		$get_kt_buy_request_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_added_by = :added_by";
		}
		
		//Data
		$get_kt_buy_request_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_added_on >= :start_date";				
		}
		
		//Data
		$get_kt_buy_request_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." where kt_buy_request_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_kt_buy_request_list_squery_where = $get_kt_buy_request_list_squery_where." and kt_buy_request_added_on <= :end_date";
		}
		
		//Data
		$get_kt_buy_request_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_kt_buy_request_list_squery = $get_kt_buy_request_list_squery_base.$get_kt_buy_request_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_kt_buy_request_list_sstatement = $dbConnection->prepare($get_kt_buy_request_list_squery);
		
		$get_kt_buy_request_list_sstatement -> execute($get_kt_buy_request_list_sdata);
		
		$get_kt_buy_request_list_sdetails = $get_kt_buy_request_list_sstatement -> fetchAll();
		
		if(FALSE === $get_kt_buy_request_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_kt_buy_request_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_kt_buy_request_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update KT Buy Request
INPUT 	: Request ID, KT Buy Request Update Array
OUTPUT 	: Request ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_kt_buy_request($request_id,$kt_buy_request_update_data)
{
	if(array_key_exists("project",$kt_buy_request_update_data))
	{	
		$project = $kt_buy_request_update_data["project"];
	}
	else
	{
		$project = "";
	}

	if(array_key_exists("site_no",$kt_buy_request_update_data))
	{	
		$site_no = $kt_buy_request_update_data["site_no"];
	}
	else
	{
		$site_no = "";
	}
	
	if(array_key_exists("dimension",$kt_buy_request_update_data))
	{	
		$dimension = $kt_buy_request_update_data["dimension"];
	}
	else
	{
		$dimension = "";
	}
	
	if(array_key_exists("extent",$kt_buy_request_update_data))
	{	
		$extent = $kt_buy_request_update_data["extent"];
	}
	else
	{
		$extent = "";
	}
	
	if(array_key_exists("request_date",$kt_buy_request_update_data))
	{	
		$request_date = $kt_buy_request_update_data["request_date"];
	}
	else
	{
		$request_date = "";
	}
	
	if(array_key_exists("request_for",$kt_buy_request_update_data))
	{	
		$request_for = $kt_buy_request_update_data["request_for"];
	}
	else
	{
		$request_for = "";
	}
	
	if(array_key_exists("active",$kt_buy_request_update_data))
	{	
		$active = $kt_buy_request_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$kt_buy_request_update_data))
	{	
		$remarks = $kt_buy_request_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$kt_buy_request_update_data))
	{	
		$added_by = $kt_buy_request_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$kt_buy_request_update_data))
	{	
		$added_on = $kt_buy_request_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $kt_buy_request_update_uquery_base = "update kt_buy_request set";  
	
	$kt_buy_request_update_uquery_set = "";
	
	$kt_buy_request_update_uquery_where = " where kt_buy_request_id = :request_id";
	
	$kt_buy_request_update_udata = array(":request_id"=>$request_id);
	
	$filter_count = 0;
	
	if($project != "")
	{
		$kt_buy_request_update_uquery_set = $kt_buy_request_update_uquery_set." kt_buy_request_project = :project,";
		$kt_buy_request_update_udata[":project"] = $project;
		$filter_count++;
	}
	
	if($site_no != "")
	{
		$kt_buy_request_update_uquery_set = $kt_buy_request_update_uquery_set." kt_buy_request_site_no = :site_no,";
		$kt_buy_request_update_udata[":site_no"] = $site_no;
		$filter_count++;
	}
	
	if($dimension != "")
	{
		$kt_buy_request_update_uquery_set = $kt_buy_request_update_uquery_set." kt_buy_request_dimension = :dimension,";
		$kt_buy_request_update_udata[":dimension"] = $dimension;
		$filter_count++;
	}
	
	if($extent != "")
	{
		$kt_buy_request_update_uquery_set = $kt_buy_request_update_uquery_set." kt_buy_request_extent = :extent,";
		$kt_buy_request_update_udata[":extent"] = $extent;
		$filter_count++;
	}
	
	if($request_date != "")
	{
		$kt_buy_request_update_uquery_set = $kt_buy_request_update_uquery_set." kt_buy_request_date = :request_date,";
		$kt_buy_request_update_udata[":request_date"] = $request_date;
		$filter_count++;
	}
	
	if($request_for != "")
	{
		$kt_buy_request_update_uquery_set = $kt_buy_request_update_uquery_set." kt_buy_request_for = :request_for,";
		$kt_buy_request_update_udata[":request_for"] = $request_for;
		$filter_count++;
	}
	
	if($active != "")
	{
		$kt_buy_request_update_uquery_set = $kt_buy_request_update_uquery_set." kt_buy_request_active = :active,";
		$kt_buy_request_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$kt_buy_request_update_uquery_set = $kt_buy_request_update_uquery_set." kt_buy_request_remarks = :remarks,";
		$kt_buy_request_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$kt_buy_request_update_uquery_set = $kt_buy_request_update_uquery_set." kt_buy_request_added_by = :added_by,";
		$kt_buy_request_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$kt_buy_request_update_uquery_set = $kt_buy_request_update_uquery_set." kt_buy_request_added_on = :added_on,";
		$kt_buy_request_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$kt_buy_request_update_uquery_set = trim($kt_buy_request_update_uquery_set,',');
	}
	
	$kt_buy_request_update_uquery = $kt_buy_request_update_uquery_base.$kt_buy_request_update_uquery_set.$kt_buy_request_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $kt_buy_request_update_ustatement = $dbConnection->prepare($kt_buy_request_update_uquery);		
        
        $kt_buy_request_update_ustatement -> execute($kt_buy_request_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $request_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new KT Buy KT Process
INPUT 	: Process Master ID, Request ID, Process Start Date, Process End Date, Remarks, Added By
OUTPUT 	: Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_kt_buy_kt_process($process_master_id,$request_id,$process_start_date,$process_end_date,$remarks,$added_by)
{
	// Query
   $kt_buy_kt_process_iquery = "insert into kt_buy_kt_process 
   (kt_buy_kt_process_master_id,kt_buy_kt_process_request_id,kt_buy_kt_process_start_date,kt_buy_kt_process_end_date,kt_buy_kt_process_active,
   kt_buy_kt_process_remarks,kt_buy_kt_process_added_by,kt_buy_kt_process_added_on)
   values(:process_master_id,:request_id,:process_start_date,:process_end_date,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $kt_buy_kt_process_istatement = $dbConnection->prepare($kt_buy_kt_process_iquery);
        
        // Data
        $kt_buy_kt_process_idata = array(':process_master_id'=>$process_master_id,':request_id'=>$request_id,':process_start_date'=>$process_start_date,
		':process_end_date'=>$process_end_date,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $kt_buy_kt_process_istatement->execute($kt_buy_kt_process_idata);
		$kt_buy_kt_process_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $kt_buy_kt_process_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get KT buy KT Process List
INPUT 	: Process ID, Process Master ID, Request ID, Process Start Date, Process End Date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT buy KT Process
BY 		: Lakshmi
*/
function db_get_kt_buy_kt_process($kt_buy_kt_process_search_data)
{  
	if(array_key_exists("process_id",$kt_buy_kt_process_search_data))
	{
		$process_id = $kt_buy_kt_process_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}
	
	if(array_key_exists("process_master_id",$kt_buy_kt_process_search_data))
	{
		$process_master_id = $kt_buy_kt_process_search_data["process_master_id"];
	}
	else
	{
		$process_master_id = "";
	}
	
	if(array_key_exists("request_id",$kt_buy_kt_process_search_data))
	{
		$request_id = $kt_buy_kt_process_search_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("process_start_date",$kt_buy_kt_process_search_data))
	{
		$process_start_date = $kt_buy_kt_process_search_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$kt_buy_kt_process_search_data))
	{
		$process_end_date = $kt_buy_kt_process_search_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$kt_buy_kt_process_search_data))
	{
		$active = $kt_buy_kt_process_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$kt_buy_kt_process_search_data))
	{
		$added_by = $kt_buy_kt_process_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$kt_buy_kt_process_search_data))
	{
		$start_date= $kt_buy_kt_process_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$kt_buy_kt_process_search_data))
	{
		$end_date= $kt_buy_kt_process_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_kt_buy_kt_process_list_squery_base = "select * from kt_buy_kt_process KBKP inner join users U on U.user_id=KBKP.kt_buy_kt_process_added_by inner join kt_buy_process_master KBPM on KBPM.kt_buy_process_master_id=KBKP.kt_buy_kt_process_master_id inner join kt_buy_request KBR on KBR.kt_buy_request_id=KBKP.kt_buy_kt_process_request_id";
	
	$get_kt_buy_kt_process_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_kt_buy_kt_process_list_sdata = array();
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." where kt_buy_kt_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." and kt_buy_kt_process_id = :process_id";				
		}
		
		// Data
		$get_kt_buy_kt_process_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($process_master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." where kt_buy_kt_process_master_id = :process_master_id";								
		}
		else
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." and kt_buy_kt_process_master_id = :process_master_id";				
		}
		
		// Data
		$get_kt_buy_kt_process_list_sdata[':process_master_id'] = $process_master_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." where kt_buy_kt_process_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." and kt_buy_kt_process_request_id = :request_id";				
		}
		
		// Data
		$get_kt_buy_kt_process_list_sdata[':request_id']  = $request_id;
		
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." where kt_buy_kt_process_start_date = :process_start_date";								
		}
		else
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." and kt_buy_kt_process_start_date = :process_start_date";				
		}
		
		// Data
		$get_kt_buy_kt_process_list_sdata[':process_start_date']  = $process_start_date;
		
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." where kt_buy_kt_process_end_date = :process_end_date";								
		}
		else
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." and kt_buy_kt_process_end_date = :process_end_date";				
		}
		
		// Data
		$get_kt_buy_kt_process_list_sdata[':process_end_date']  = $process_end_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." where kt_buy_kt_process_active = :active";								
		}
		else
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." and kt_buy_kt_process_active = :active";				
		}
		
		// Data
		$get_kt_buy_kt_process_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." where kt_buy_kt_process_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." and kt_buy_kt_process_added_by = :added_by";
		}
		
		//Data
		$get_kt_buy_kt_process_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." where kt_buy_kt_process_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." and kt_buy_kt_process_added_on >= :start_date";				
		}
		
		//Data
		$get_kt_buy_kt_process_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." where kt_buy_kt_process_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_kt_buy_kt_process_list_squery_where = $get_kt_buy_kt_process_list_squery_where." and kt_buy_kt_process_added_on <= :end_date";
		}
		
		//Data
		$get_kt_buy_kt_process_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_kt_buy_kt_process_list_squery = $get_kt_buy_kt_process_list_squery_base.$get_kt_buy_kt_process_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_kt_buy_kt_process_list_sstatement = $dbConnection->prepare($get_kt_buy_kt_process_list_squery);
		
		$get_kt_buy_kt_process_list_sstatement -> execute($get_kt_buy_kt_process_list_sdata);
		
		$get_kt_buy_kt_process_list_sdetails = $get_kt_buy_kt_process_list_sstatement -> fetchAll();
		
		if(FALSE === $get_kt_buy_kt_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_kt_buy_kt_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_kt_buy_kt_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update KT buy KT Process
INPUT 	: Process ID, KT buy KT Process Update Array
OUTPUT 	: Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_kt_buy_kt_process($process_id,$kt_buy_kt_process_update_data)
{
	if(array_key_exists("process_master_id",$kt_buy_kt_process_update_data))
	{	
		$process_master_id = $kt_buy_kt_process_update_data["process_master_id"];
	}
	else
	{
		$process_master_id = "";
	}
	
	if(array_key_exists("request_id",$kt_buy_kt_process_update_data))
	{	
		$request_id = $kt_buy_kt_process_update_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("process_start_date",$kt_buy_kt_process_update_data))
	{	
		$process_start_date = $kt_buy_kt_process_update_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$kt_buy_kt_process_update_data))
	{	
		$process_end_date = $kt_buy_kt_process_update_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$kt_buy_kt_process_update_data))
	{	
		$active = $kt_buy_kt_process_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$kt_buy_kt_process_update_data))
	{	
		$remarks = $kt_buy_kt_process_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$kt_buy_kt_process_update_data))
	{	
		$added_by = $kt_buy_kt_process_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$kt_buy_kt_process_update_data))
	{	
		$added_on = $kt_buy_kt_process_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $kt_buy_kt_process_update_uquery_base = "update kt_buy_kt_process set";  
	
	$kt_buy_kt_process_update_uquery_set = "";
	
	$kt_buy_kt_process_update_uquery_where = " where kt_buy_kt_process_id = :process_id";
	
	$kt_buy_kt_process_update_udata = array(":process_id"=>$process_id);
	
	$filter_count = 0;
	
	if($process_master_id != "")
	{
		$kt_buy_kt_process_update_uquery_set = $kt_buy_kt_process_update_uquery_set." kt_buy_kt_process_master_id = :process_master_id,";
		$kt_buy_kt_process_update_udata[":process_master_id"] = $process_master_id;
		$filter_count++;
	}
	
	if($request_id != "")
	{
		$kt_buy_kt_process_update_uquery_set = $kt_buy_kt_process_update_uquery_set." kt_buy_kt_process_request_id = :request_id,";
		$kt_buy_kt_process_update_udata[":request_id"] = $request_id;
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		$kt_buy_kt_process_update_uquery_set = $kt_buy_kt_process_update_uquery_set." kt_buy_kt_process_start_date = :process_start_date,";
		$kt_buy_kt_process_update_udata[":process_start_date"] = $process_start_date;
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		$kt_buy_kt_process_update_uquery_set = $kt_buy_kt_process_update_uquery_set." kt_buy_kt_process_end_date = :process_end_date,";
		$kt_buy_kt_process_update_udata[":process_end_date"] = $process_end_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$kt_buy_kt_process_update_uquery_set = $kt_buy_kt_process_update_uquery_set." kt_buy_kt_process_active = :active,";
		$kt_buy_kt_process_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$kt_buy_kt_process_update_uquery_set = $kt_buy_kt_process_update_uquery_set." kt_buy_kt_process_remarks = :remarks,";
		$kt_buy_kt_process_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$kt_buy_kt_process_update_uquery_set = $kt_buy_kt_process_update_uquery_set." kt_buy_kt_process_added_by = :added_by,";
		$kt_buy_kt_process_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$kt_buy_kt_process_update_uquery_set = $kt_buy_kt_process_update_uquery_set." kt_buy_kt_process_added_on = :added_on,";
		$kt_buy_kt_process_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$kt_buy_kt_process_update_uquery_set = trim($kt_buy_kt_process_update_uquery_set,',');
	}
	
	$kt_buy_kt_process_update_uquery = $kt_buy_kt_process_update_uquery_base.$kt_buy_kt_process_update_uquery_set.$kt_buy_kt_process_update_uquery_where;
    
    try
    {
        $dbConnection = get_conn_handle();
        
        $kt_buy_kt_process_update_ustatement = $dbConnection->prepare($kt_buy_kt_process_update_uquery);		
        
        $kt_buy_kt_process_update_ustatement -> execute($kt_buy_kt_process_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new KT Buy Document
INPUT 	: Process ID, File Path ID, Remarks, Added By
OUTPUT 	: Document ID, success or failure message
BY 		: Lakshmi
*/
function db_add_kt_buy_document($process_id,$file_path_id,$remarks,$added_by)
{
	// Query
   $kt_buy_document_iquery = "insert into kt_buy_document
   (kt_buy_document_process_id,kt_buy_document_file_path,kt_buy_document_active,kt_buy_document_remarks,kt_buy_document_added_by,kt_buy_document_added_on) values(:process_id,:file_path_id,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $kt_buy_document_istatement = $dbConnection->prepare($kt_buy_document_iquery);
        
        // Data
        $kt_buy_document_idata = array(':process_id'=>$process_id,':file_path_id'=>$file_path_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $kt_buy_document_istatement->execute($kt_buy_document_idata);
		$kt_buy_document_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $kt_buy_document_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get KT Buy Document list
INPUT 	: Document ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Buy Document
BY 		: Lakshmi
*/
function db_get_kt_buy_document($kt_buy_document_search_data)
{  
	if(array_key_exists("document_id",$kt_buy_document_search_data))
	{
		$document_id = $kt_buy_document_search_data["document_id"];
	}
	else
	{
		$document_id = "";
	}
	
	if(array_key_exists("process_id",$kt_buy_document_search_data))
	{
		$process_id = $kt_buy_document_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("request_id",$kt_buy_document_search_data))
	{
		$request_id = $kt_buy_document_search_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("file_path_id",$kt_buy_document_search_data))
	{
		$file_path_id = $kt_buy_document_search_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("active",$kt_buy_document_search_data))
	{
		$active = $kt_buy_document_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$kt_buy_document_search_data))
	{
		$added_by = $kt_buy_document_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$kt_buy_document_search_data))
	{
		$start_date = $kt_buy_document_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$kt_buy_document_search_data))
	{
		$end_date = $kt_buy_document_search_data["end_date"];
	
	}
	else
	{
		$end_date = "";
	}

	$get_kt_buy_document_list_squery_base = "select * from kt_buy_document KBD inner join users U on U.user_id = KBD.kt_buy_document_added_by inner join kt_buy_kt_process KBKP on KBKP.kt_buy_kt_process_id=KBD.kt_buy_document_process_id inner join kt_buy_request KBR on KBR.kt_buy_request_id=KBKP.kt_buy_kt_process_request_id inner join kt_buy_process_master KBPM on KBPM.kt_buy_process_master_id = KBD.kt_buy_document_process_id";
	
	$get_kt_buy_document_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_kt_buy_document_list_sdata = array();
	
	if($document_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." where kt_buy_document_id = :document_id";								
		}
		else
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." and kt_buy_document_id = :document_id";				
		}
		
		// Data
		$get_kt_buy_document_list_sdata[':document_id'] = $document_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." where kt_buy_document_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." and kt_buy_document_process_id = :process_id";				
		}
		
		// Data
		$get_kt_buy_document_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." where KBKP.kt_buy_kt_process_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." and KBKP.kt_buy_kt_process_request_id = :request_id";				
		}
		
		// Data
		$get_kt_buy_document_list_sdata[':request_id'] = $request_id;
		
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." where kt_buy_document_file_path = :file_path_id";								
		}
		else
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." and kt_buy_document_file_path = :file_path_id";				
		}
		
		// Data
		$get_kt_buy_document_list_sdata[':file_path_id'] = $file_path_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." where kt_buy_document_active = :active";								
		}
		else
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." and kt_buy_document_active = :active";				
		}
		
		// Data
		$get_kt_buy_document_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." where kt_buy_document_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." and kt_buy_document_added_by = :added_by";
		}
		
		//Data
		$get_kt_buy_document_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." where kt_buy_document_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." and kt_buy_document_added_on >= :start_date";				
		}
		
		//Data
		$get_kt_buy_document_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." where kt_buy_document_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_kt_buy_document_list_squery_where = $get_kt_buy_document_list_squery_where." and kt_buy_document_added_on <= :end_date";
		}
		
		//Data
		$get_kt_buy_document_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_kt_buy_document_list_squery = " order by kt_buy_document_added_on DESC";
	$get_kt_buy_document_list_squery = $get_kt_buy_document_list_squery_base.$get_kt_buy_document_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_kt_buy_document_list_sstatement = $dbConnection->prepare($get_kt_buy_document_list_squery);
		
		$get_kt_buy_document_list_sstatement -> execute($get_kt_buy_document_list_sdata);
		
		$get_kt_buy_document_list_sdetails = $get_kt_buy_document_list_sstatement -> fetchAll();
		
		if(FALSE === $get_kt_buy_document_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_kt_buy_document_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_kt_buy_document_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
 /*
PURPOSE : To update KT Buy Document 
INPUT 	: Document ID, KT Buy Document Update Array
OUTPUT 	: Document ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_kt_buy_document($document_id,$kt_buy_document_update_data)
{
	
	if(array_key_exists("process_id",$kt_buy_document_update_data))
	{	
		$process_id = $kt_buy_document_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("file_path_id",$kt_buy_document_update_data))
	{	
		$file_path_id = $kt_buy_document_update_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("active",$kt_buy_document_update_data))
	{	
		$active = $kt_buy_document_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$kt_buy_document_update_data))
	{	
		$remarks = $kt_buy_document_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$kt_buy_document_update_data))
	{	
		$added_by = $kt_buy_document_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$kt_buy_document_update_data))
	{	
		$added_on = $kt_buy_document_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $kt_buy_document_update_uquery_base = "update kt_buy_document set";  
	
	$kt_buy_document_update_uquery_set = "";
	
	$kt_buy_document_update_uquery_where = " where kt_buy_document_id = :document_id";
	
	$kt_buy_document_update_udata = array(":document_id"=>$document_id);
	
	$filter_count = 0;
	
	if($process_id != "")
	{
		$kt_buy_document_update_uquery_set = $kt_buy_document_update_uquery_set." kt_buy_document_process_id = :process_id,";
		$kt_buy_document_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		$kt_buy_document_update_uquery_set = $kt_buy_document_update_uquery_set." kt_buy_document_file_path = :file_path_id,";
		$kt_buy_document_update_udata[":file_path_id"] = $file_path_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$kt_buy_document_update_uquery_set = $kt_buy_document_update_uquery_set." kt_buy_document_active = :active,";
		$kt_buy_document_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$kt_buy_document_update_uquery_set = $kt_buy_document_update_uquery_set." kt_buy_document_remarks = :remarks,";
		$kt_buy_document_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$kt_buy_document_update_uquery_set = $kt_buy_document_update_uquery_set." kt_buy_document_added_by = :added_by,";
		$kt_buy_document_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$kt_buy_document_update_uquery_set = $kt_buy_document_update_uquery_set." kt_buy_document_added_on = :added_on,";
		$kt_buy_document_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$kt_buy_document_update_uquery_set = trim($kt_buy_document_update_uquery_set,',');
	}
	
	$kt_buy_document_update_uquery = $kt_buy_document_update_uquery_base.$kt_buy_document_update_uquery_set.$kt_buy_document_update_uquery_where;
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $kt_buy_document_update_ustatement = $dbConnection->prepare($kt_buy_document_update_uquery);		
        
        $kt_buy_document_update_ustatement -> execute($kt_buy_document_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $document_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new KT Buy Project Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Project ID, success or failure message
BY 		: Lakshmi
*/
function db_add_kt_buy_project_master($name,$remarks,$added_by)
{
	// Query
   $kt_buy_project_master_iquery = "insert into kt_buy_project_master (kt_buy_project_master_name,kt_buy_project_master_active,kt_buy_project_master_remarks,kt_buy_project_master_added_by,kt_buy_project_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $kt_buy_project_master_istatement = $dbConnection->prepare($kt_buy_project_master_iquery);
        
        // Data
        $kt_buy_project_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $kt_buy_project_master_istatement->execute($kt_buy_project_master_idata);
		$kt_buy_project_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $kt_buy_project_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get KT Buy project Master list
INPUT 	: project Master ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Buy project Master
BY 		: Lakshmi
*/
function db_get_kt_buy_project_master($kt_buy_project_master_search_data)
{  
	if(array_key_exists("project_id",$kt_buy_project_master_search_data))
	{
		$project_id = $kt_buy_project_master_search_data["project_id"];
	}
	else
	{
		$project_id= "";
	}
	
	if(array_key_exists("name",$kt_buy_project_master_search_data))
	{
		$name = $kt_buy_project_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("project_name_check",$kt_buy_project_master_search_data))
	{
		$project_name_check = $kt_buy_project_master_search_data["project_name_check"];
	}
	else
	{
		$project_name_check = "";
	}
	
	if(array_key_exists("active",$kt_buy_project_master_search_data))
	{
		$active = $kt_buy_project_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$kt_buy_project_master_search_data))
	{
		$added_by = $kt_buy_project_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$kt_buy_project_master_search_data))
	{
		$start_date= $kt_buy_project_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$kt_buy_project_master_search_data))
	{
		$end_date= $kt_buy_project_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_kt_buy_project_master_list_squery_base = "select * from kt_buy_project_master KBPP inner join users U on U.user_id = KBPP.kt_buy_project_master_added_by";
	
	$get_kt_buy_project_master_list_squery_where = "";
	$get_kt_buy_project_master_list_squery_order_by = " order by kt_buy_project_master_name ASC";
	
	$filter_count = 0;
	
	// Data
	$get_kt_buy_project_master_list_sdata = array();
	
	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." where kt_buy_project_master_id = :project_id";
		}
		else
		{
			// Query
			$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." and kt_buy_project_master_id = :project_id";
		}
		
		// Data
		$get_kt_buy_project_master_list_sdata[':project_id'] = $project_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($project_name_check == '1')
			{
				$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." where kt_buy_project_master_name = :name";		
				// Data
				$get_kt_buy_project_master_list_sdata[':name']  = $name;
			}
			else if($project_name_check == '2')
			{
				$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." where kt_buy_project_master_name like :name";						

				// Data
				$get_kt_buy_project_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." where kt_buy_project_master_name like :name";						

				// Data
				$get_kt_buy_project_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($project_name_check == '1')
			{
				$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." and kt_buy_project_master_name = :name";	
					
				// Data
				$get_kt_buy_project_master_list_sdata[':name']  = $name;
			}
			else if($project_name_check == '2')
			{
				$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." or kt_buy_project_master_name like :name";				
				// Data
				$get_kt_buy_project_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." and kt_buy_project_master_name like :name";
				
				// Data
				$get_kt_buy_project_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." where kt_buy_project_master_active = :active";
		}
		else
		{
			// Query
			$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." and kt_buy_project_master_active = :active";
		}
		
		// Data
		$get_kt_buy_project_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." where kt_buy_project_master_added_by = :added_by";
		}
		else
		{
			// Query
			$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." and kt_buy_project_master_added_by = :added_by";
		}
		
		//Data
		$get_kt_buy_project_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." where kt_buy_project_master_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." and kt_buy_project_master_added_on >= :start_date";
		}
		
		//Data
		$get_kt_buy_project_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." where kt_buy_project_master_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_kt_buy_project_master_list_squery_where = $get_kt_buy_project_master_list_squery_where." and kt_buy_project_master_added_on <= :end_date";
		}
		
		//Data
		$get_kt_buy_project_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_kt_buy_project_master_list_squery = $get_kt_buy_project_master_list_squery_base.$get_kt_buy_project_master_list_squery_where.
	$get_kt_buy_project_master_list_squery_order_by;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_kt_buy_project_master_list_sstatement = $dbConnection->prepare($get_kt_buy_project_master_list_squery);
		
		$get_kt_buy_project_master_list_sstatement -> execute($get_kt_buy_project_master_list_sdata);
		
		$get_kt_buy_project_master_list_sdetails = $get_kt_buy_project_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_kt_buy_project_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_kt_buy_project_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_kt_buy_project_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
 
/*
PURPOSE : To update KT Buy Project Master
INPUT 	: project ID, KT Buy project Master Update Array
OUTPUT 	: Project ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_kt_buy_project_master($project_id,$kt_buy_project_master_update_data)
{
	if(array_key_exists("name",$kt_buy_project_master_update_data))
	{	
		$name = $kt_buy_project_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$kt_buy_project_master_update_data))
	{	
		$active = $kt_buy_project_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$kt_buy_project_master_update_data))
	{	
		$remarks = $kt_buy_project_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$kt_buy_project_master_update_data))
	{	
		$added_by = $kt_buy_project_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$kt_buy_project_master_update_data))
	{	
		$added_on = $kt_buy_project_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $kt_buy_project_master_update_uquery_base = "update kt_buy_project_master set ";  
	
	$kt_buy_project_master_update_uquery_set = "";
	
	$kt_buy_project_master_update_uquery_where = " where kt_buy_project_master_id = :project_id";
	
	$kt_buy_project_master_update_udata = array(":project_id"=>$project_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$kt_buy_project_master_update_uquery_set = $kt_buy_project_master_update_uquery_set." kt_buy_project_master_name = :name,";
		$kt_buy_project_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$kt_buy_project_master_update_uquery_set = $kt_buy_project_master_update_uquery_set." kt_buy_project_master_active = :active,";
		$kt_buy_project_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$kt_buy_project_master_update_uquery_set = $kt_buy_project_master_update_uquery_set." kt_buy_project_master_remarks = :remarks,";
		$kt_buy_project_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$kt_buy_project_master_update_uquery_set = $kt_buy_project_master_update_uquery_set." kt_buy_project_master_added_by = :added_by,";
		$kt_buy_project_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$kt_buy_project_master_update_uquery_set = $kt_buy_project_master_update_uquery_set." kt_buy_project_master_added_on = :added_on,";
		$kt_buy_project_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$kt_buy_project_master_update_uquery_set = trim($kt_buy_project_master_update_uquery_set,',');
	}
	
	$kt_buy_project_master_update_uquery = $kt_buy_project_master_update_uquery_base.$kt_buy_project_master_update_uquery_set.$kt_buy_project_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $kt_buy_project_master_update_ustatement = $dbConnection->prepare($kt_buy_project_master_update_uquery);		
        
        $kt_buy_project_master_update_ustatement -> execute($kt_buy_project_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $project_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>