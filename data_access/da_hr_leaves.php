<?php
/**
 * @author Nitin
 * @copyright 2016
 */
 
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

/*
PURPOSE : To add absence details
INPUT 	: Employee, Date of Absence, Absence Type, Absence Duration(1 if full day, 2 if half day), Absence Remarks, Added By
OUTPUT 	: Absence ID, success or failure message
BY 		: Nitin
*/
function db_add_absence($employee,$absence_date,$absence_type,$absence_duration,$remarks,$added_by)
{
	// Query
    $absence_iquery = "insert into hr_absence_details (hr_absence_employee,hr_absence_date,hr_absence_type,hr_absence_duration,hr_absence_remarks,hr_absence_approval_status,hr_absence_approval_action_by,hr_absence_approval_action_on,hr_absence_added_by,hr_absence_added_on) values (:employee,:date,:type,:duration,:remarks,:app_status,:approver,:approve_date,:added_by,:now)";    
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $absence_istatement = $dbConnection->prepare($absence_iquery);
        
        // Data		
        $absence_idata = array(':employee'=>$employee,':date'=>$absence_date,':type'=>$absence_type,':remarks'=>$remarks,':duration'=>$absence_duration,':app_status'=>'0',':approver'=>'',':approve_date'=>'',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));
    	$dbConnection->beginTransaction();
        $absence_istatement->execute($absence_idata);
		$absence_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $absence_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get absence list
INPUT 	: Absence Filter Data: Employee, Absence Type, Date, Status
OUTPUT 	: Absence List
BY 		: Nitin
*/
function db_get_absence_list($absence_filter_data)
{
	$get_absence_list_squery_base = "select *,AU.user_name as approver from hr_absence_details HAD inner join hr_employee HE on HE.hr_employee_id = HAD.hr_absence_employee inner join users U on U.user_id = HE.hr_employee_user inner join hr_attendance_type_master HATM on HATM.hr_attendance_type_id = HAD.hr_absence_type left outer join users AU on AU.user_id = HAD.hr_absence_approval_action_by";
	
	$get_absence_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_absence_list_sdata = array();
	
	if(array_key_exists("leave_id",$absence_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." where hr_absence_id=:leave_id";								
		}
		else
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." and hr_absence_id=:leave_id";				
		}
		
		// Data
		$get_absence_list_sdata[':leave_id']  = $absence_filter_data["leave_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("employee_id",$absence_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." where hr_absence_employee=:employee_id";								
		}
		else
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." and hr_absence_employee=:employee_id";				
		}
		
		// Data
		$get_absence_list_sdata[':employee_id']  = $absence_filter_data["employee_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("type",$absence_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." where hr_absence_type = :type";								
		}
		else
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." and hr_absence_type = :type";				
		}
		
		// Data
		$get_absence_list_sdata[':type']  = $absence_filter_data["type"];
		
		$filter_count++;
	}
	
	if(array_key_exists("duration",$absence_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." where hr_absence_duration = :duration";								
		}
		else
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." and hr_absence_duration = :duration";				
		}
		
		// Data
		$get_absence_list_sdata[':duration']  = $absence_filter_data["duration"];
		
		$filter_count++;
	}

	if(array_key_exists("date",$absence_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." where hr_absence_date = :date";								
		}
		else
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." and hr_absence_date = :date";				
		}
		
		// Data
		$get_absence_list_sdata[':date']  = $absence_filter_data["date"];
		
		$filter_count++;
	}
	
	if(array_key_exists("start_date",$absence_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." where hr_absence_date >= :start_date";								
		}
		else
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." and hr_absence_date >= :start_date";				
		}
		
		// Data
		$get_absence_list_sdata[':start_date']  = $absence_filter_data["start_date"];
		
		$filter_count++;
	}
	
	if(array_key_exists("end_date",$absence_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." where hr_absence_date <= :end_date";								
		}
		else
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." and hr_absence_date <= :end_date";				
		}
		
		// Data
		$get_absence_list_sdata[':end_date']  = $absence_filter_data["end_date"];
		
		$filter_count++;
	}
	
	if(array_key_exists("status",$absence_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." where hr_absence_approval_status = :status";								
		}
		else
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." and hr_absence_approval_status = :status";				
		}
		
		// Data
		$get_absence_list_sdata[':status']  = $absence_filter_data["status"];
		
		$filter_count++;
	}
	
	if(array_key_exists("manager",$absence_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." where U.user_manager = :manager";								
		}
		else
		{
			// Query
			$get_absence_list_squery_where = $get_absence_list_squery_where." and U.user_manager = :manager";				
		}
		
		// Data
		$get_absence_list_sdata[':manager']  = $absence_filter_data["manager"];
		
		$filter_count++;
	}
	
	$get_absence_list_squery = $get_absence_list_squery_base.$get_absence_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_absence_list_sstatement = $dbConnection->prepare($get_absence_list_squery);
		
		$get_absence_list_sstatement -> execute($get_absence_list_sdata);
		
		$get_absence_list_sdetails = $get_absence_list_sstatement -> fetchAll();
		
		if(FALSE === $get_absence_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_absence_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_absence_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update leave details  
INPUT 	: Leave ID, Leave Data Array
OUTPUT 	: Leave ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_leave_details($leave_id,$leave_details)
{
	// Query
    $leave_uquery_base = "update hr_absence_details set";  
	
	$leave_uquery_set = "";
	
	$leave_uquery_where = " where hr_absence_id=:leave_id";
	
	$leave_udata = array(":leave_id"=>$leave_id);
	
	$filter_count = 0;
	
	if(array_key_exists("status",$leave_details))
	{
		$leave_uquery_set = $leave_uquery_set." hr_absence_approval_status=:status,";
		$leave_udata[":status"] = $leave_details["status"];
		$filter_count++;
	}
	
	if(array_key_exists("approver",$leave_details))
	{
		$leave_uquery_set = $leave_uquery_set." hr_absence_approval_action_by=:approver,";
		$leave_udata[":approver"] = $leave_details["approver"];
		$filter_count++;
	}
	
	if(array_key_exists("approved_on",$leave_details))
	{
		$leave_uquery_set = $leave_uquery_set." hr_absence_approval_action_on=:approved_on,";
		$leave_udata[":approved_on"] = $leave_details["approved_on"];
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$leave_uquery_set = trim($leave_uquery_set,',');
	}
	
	$leave_uquery = $leave_uquery_base.$leave_uquery_set.$leave_uquery_where;	

    try
    {
        $dbConnection = get_conn_handle();
        
        $leave_ustatement = $dbConnection->prepare($leave_uquery);		
        
        $leave_ustatement -> execute($leave_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $leave_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add earned leave details
INPUT 	: Employee, Count, Year
OUTPUT 	: Earned Leave ID, success or failure message
BY 		: Nitin
*/
function db_add_earned_leaves($employee,$count,$year)
{
	// Query
    $el_iquery = "insert into hr_earned_leaves (hr_earned_leave_employee,hr_earned_leave_count,hr_earned_leave_year,hr_earned_leave_added_on) values (:employee,:count,:year,:now)";    
    try
    {
        $dbConnection = get_conn_handle();
        
        $el_istatement = $dbConnection->prepare($el_iquery);
        
        // Data		
        $el_idata = array(':employee'=>$employee,':count'=>$count,':year'=>$year,':now'=>date("Y-m-d H:i:s"));
    	$dbConnection->beginTransaction();
        $el_istatement->execute($el_idata);
		$el_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $el_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get earned leave list
INPUT 	: Earned Leave Filter Data: Employee, Year
OUTPUT 	: Earned Leave List
BY 		: Nitin
*/
function db_get_el_list($el_filter_data)
{
	$get_el_list_squery_base = "select * from hr_earned_leaves";
	
	$get_el_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_el_list_sdata = array();
	
	if(array_key_exists("employee_id",$el_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_el_list_squery_where = $get_el_list_squery_where." where hr_earned_leave_employee=:employee_id";								
		}
		else
		{
			// Query
			$get_el_list_squery_where = $get_el_list_squery_where." and hr_earned_leave_employee=:employee_id";				
		}
		
		// Data
		$get_el_list_sdata[':employee_id']  = $el_filter_data["employee_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("year",$el_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_el_list_squery_where = $get_el_list_squery_where." where hr_earned_leave_year = :year";								
		}
		else
		{
			// Query
			$get_el_list_squery_where = $get_el_list_squery_where." and hr_earned_leave_year = :year";				
		}
		
		// Data
		$get_el_list_sdata[':year']  = $el_filter_data["year"];
		
		$filter_count++;
	}
	
	$get_el_list_squery = $get_el_list_squery_base.$get_el_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_el_list_sstatement = $dbConnection->prepare($get_el_list_squery);
		
		$get_el_list_sstatement -> execute($get_el_list_sdata);
		
		$get_el_list_sdetails = $get_el_list_sstatement -> fetchAll();
		
		if(FALSE === $get_el_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_el_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_el_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add Comp off details
INPUT 	: Employee, Valid Upto, Attendance, Type
OUTPUT 	: Comp Off ID, success or failure message
BY 		: Nitin
*/
function db_add_comp_off($employee,$valid_date,$attendance,$type)
{
	// Query
    $comp_off_iquery = "insert into hr_comp_off (hr_comp_off_employee,hr_comp_off_valid_date,hr_comp_off_attendance_id,hr_comp_off_type,hr_earned_leave_added_on) values (:employee,:valid_date,:attendance,:type,:now)";    
    try
    {
        $dbConnection = get_conn_handle();
        
        $comp_off_istatement = $dbConnection->prepare($comp_off_iquery);
        
        // Data		
        $comp_off_idata = array(':employee'=>$employee,':valid_date'=>$valid_date,':attendance'=>$attendance,':type'=>$type,':now'=>date("Y-m-d H:i:s"));
    	$dbConnection->beginTransaction();
        $comp_off_istatement->execute($comp_off_idata);
		$comp_off_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $comp_off_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get comp off list
INPUT 	: Comp Off Filter Data: Employee, Year
OUTPUT 	: Comp Off List
BY 		: Nitin
*/
function db_get_comp_off_list($comp_off_filter_data)
{
	$get_comp_off_list_squery_base = "select * from hr_comp_off HCO left outer join hr_attendance HA on HA.hr_attendance_id = HCO.hr_comp_off_attendance_id inner join hr_employee HE on HE.hr_employee_id = HCO.hr_comp_off_employee";
	
	$get_comp_off_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_comp_off_list_sdata = array();
	
	if(array_key_exists("employee_id",$comp_off_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_comp_off_list_squery_where = $get_comp_off_list_squery_where." where hr_comp_off_employee=:employee_id";								
		}
		else
		{
			// Query
			$get_comp_off_list_squery_where = $get_comp_off_list_squery_where." and hr_comp_off_employee=:employee_id";				
		}
		
		// Data
		$get_comp_off_list_sdata[':employee_id']  = $comp_off_filter_data["employee_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("valid_date",$comp_off_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_comp_off_list_squery_where = $get_comp_off_list_squery_where." where hr_comp_off_valid_date >= :valid_date";								
		}
		else
		{
			// Query
			$get_comp_off_list_squery_where = $get_comp_off_list_squery_where." and hr_comp_off_valid_date >= :valid_date";				
		}
		
		// Data
		$get_comp_off_list_sdata[':valid_date']  = $comp_off_filter_data["valid_date"];
		
		$filter_count++;
	}
	
	if(array_key_exists("attendance_id",$comp_off_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_comp_off_list_squery_where = $get_comp_off_list_squery_where." where hr_comp_off_attendance_id = :attendance_id";								
		}
		else
		{
			// Query
			$get_comp_off_list_squery_where = $get_comp_off_list_squery_where." and hr_comp_off_attendance_id = :attendance_id";				
		}
		
		// Data
		$get_comp_off_list_sdata[':attendance_id']  = $comp_off_filter_data["attendance_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("type",$comp_off_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_comp_off_list_squery_where = $get_comp_off_list_squery_where." where hr_comp_off_type = :ltype";								
		}
		else
		{
			// Query
			$get_comp_off_list_squery_where = $get_comp_off_list_squery_where." and hr_comp_off_type = :ltype";				
		}
		
		// Data
		$get_comp_off_list_sdata[':ltype']  = $comp_off_filter_data["type"];
		
		$filter_count++;
	}
	
	$get_comp_off_list_squery = $get_comp_off_list_squery_base.$get_comp_off_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_comp_off_list_sstatement = $dbConnection->prepare($get_comp_off_list_squery);
		
		$get_comp_off_list_sstatement -> execute($get_comp_off_list_sdata);
		
		$get_comp_off_list_sdetails = $get_comp_off_list_sstatement -> fetchAll();
		
		if(FALSE === $get_comp_off_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_comp_off_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_comp_off_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update EL details  
INPUT 	: Employee ID, Year, EL Data Array
OUTPUT 	: Employee ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_el_details($employee_id,$year,$el_details)
{
	// Query
    $el_uquery_base = "update hr_earned_leaves set";  
	
	$el_uquery_set = "";
	
	$el_uquery_where = " where hr_earned_leave_employee=:employee and hr_earned_leave_year=:year";
	
	$el_udata = array(":employee"=>$employee_id,":year"=>$year);
	
	$filter_count = 0;
	
	if(array_key_exists("count",$el_details))
	{
		$el_uquery_set = $el_uquery_set." hr_earned_leave_count = hr_earned_leave_count + :count,";
		$el_udata[":count"] = $el_details["count"];
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$el_uquery_set = trim($el_uquery_set,',');
	}
	
	$el_uquery = $el_uquery_base.$el_uquery_set.$el_uquery_where;		

    try
    {
        $dbConnection = get_conn_handle();
        
        $el_ustatement = $dbConnection->prepare($el_uquery);		
        
        $el_ustatement -> execute($el_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $employee_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To update opening EL stock details  
INPUT 	: Employee ID, Year, EL Data Array
OUTPUT 	: Employee ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_el_details_opening($employee_id,$year,$el_details)
{
	// Query
    $el_uquery_base = "update hr_earned_leaves set";  
	
	$el_uquery_set = "";
	
	$el_uquery_where = " where hr_earned_leave_employee=:employee and hr_earned_leave_year=:year";
	
	$el_udata = array(":employee"=>$employee_id,":year"=>$year);
	
	$filter_count = 0;
	
	if(array_key_exists("count",$el_details))
	{
		$el_uquery_set = $el_uquery_set." hr_earned_leave_count = :count,";
		$el_udata[":count"] = $el_details["count"];
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$el_uquery_set = trim($el_uquery_set,',');
	}
	
	$el_uquery = $el_uquery_base.$el_uquery_set.$el_uquery_where;	

    try
    {
        $dbConnection = get_conn_handle();
        
        $el_ustatement = $dbConnection->prepare($el_uquery);		
        
        $el_ustatement -> execute($el_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $employee_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add sick leave details
INPUT 	: Employee, Count, Year
OUTPUT 	: Sick Leave ID, success or failure message
BY 		: Nitin
*/
function db_add_sick_leaves($employee,$count,$year)
{
	// Query
    $sl_iquery = "insert into hr_sick_leaves (hr_sick_leave_employee,hr_sick_leave_count,hr_sick_leave_year,hr_sick_leave_added_on) values (:employee,:count,:year,:now)";    
    try
    {
        $dbConnection = get_conn_handle();
        
        $sl_istatement = $dbConnection->prepare($sl_iquery);
        
        // Data		
        $sl_idata = array(':employee'=>$employee,':count'=>$count,':year'=>$year,':now'=>date("Y-m-d H:i:s"));
    	$dbConnection->beginTransaction();
        $sl_istatement->execute($sl_idata);
		$sl_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $sl_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get sick leave list
INPUT 	: Sick Leave Filter Data: Employee, Year
OUTPUT 	: Sick Leave List
BY 		: Nitin
*/
function db_get_sl_list($sl_filter_data)
{
	$get_sl_list_squery_base = "select * from hr_sick_leaves";
	
	$get_sl_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_sl_list_sdata = array();
	
	if(array_key_exists("employee_id",$sl_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_sl_list_squery_where = $get_sl_list_squery_where." where hr_sick_leave_employee=:employee_id";								
		}
		else
		{
			// Query
			$get_sl_list_squery_where = $get_sl_list_squery_where." and hr_sick_leave_employee=:employee_id";				
		}
		
		// Data
		$get_sl_list_sdata[':employee_id']  = $sl_filter_data["employee_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("year",$sl_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_sl_list_squery_where = $get_sl_list_squery_where." where hr_sick_leave_year = :year";								
		}
		else
		{
			// Query
			$get_sl_list_squery_where = $get_sl_list_squery_where." and hr_sick_leave_year = :year";				
		}
		
		// Data
		$get_sl_list_sdata[':year']  = $sl_filter_data["year"];
		
		$filter_count++;
	}
	
	$get_sl_list_squery = $get_sl_list_squery_base.$get_sl_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_sl_list_sstatement = $dbConnection->prepare($get_sl_list_squery);
		
		$get_sl_list_sstatement -> execute($get_sl_list_sdata);
		
		$get_sl_list_sdetails = $get_sl_list_sstatement -> fetchAll();
		
		if(FALSE === $get_sl_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_sl_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_sl_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update SL details  
INPUT 	: Employee ID, Year, SL Data Array
OUTPUT 	: Employee ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_sl_details($employee_id,$year,$sl_details)
{
	// Query
    $sl_uquery_base = "update hr_sick_leaves set";  
	
	$sl_uquery_set = "";
	
	$sl_uquery_where = " where hr_sick_leave_employee=:employee and hr_sick_leave_year=:year";
	
	$sl_udata = array(":employee"=>$employee_id,":year"=>$year);
	
	$filter_count = 0;
	
	if(array_key_exists("count",$sl_details))
	{
		$sl_uquery_set = $sl_uquery_set." hr_sick_leave_count = hr_sick_leave_count + :count,";
		$sl_udata[":count"] = $sl_details["count"];
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$sl_uquery_set = trim($sl_uquery_set,',');
	}
	
	$sl_uquery = $sl_uquery_base.$sl_uquery_set.$sl_uquery_where;	

    try
    {
        $dbConnection = get_conn_handle();
        
        $sl_ustatement = $dbConnection->prepare($sl_uquery);		
        
        $sl_ustatement -> execute($sl_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $employee_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To update opening SL stock details  
INPUT 	: Employee ID, Year, SL Data Array
OUTPUT 	: Employee ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_sl_details_opening($employee_id,$year,$sl_details)
{
	// Query
    $sl_uquery_base = "update hr_sick_leaves set";  
	
	$sl_uquery_set = "";
	
	$sl_uquery_where = " where hr_sick_leave_employee=:employee and hr_sick_leave_year=:year";
	
	$sl_udata = array(":employee"=>$employee_id,":year"=>$year);
	
	$filter_count = 0;
	
	if(array_key_exists("count",$sl_details))
	{
		$sl_uquery_set = $sl_uquery_set." hr_sick_leave_count = :count,";
		$sl_udata[":count"] = $sl_details["count"];
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$sl_uquery_set = trim($sl_uquery_set,',');
	}
	
	$sl_uquery = $sl_uquery_base.$sl_uquery_set.$sl_uquery_where;	

    try
    {
        $dbConnection = get_conn_handle();
        
        $sl_ustatement = $dbConnection->prepare($sl_uquery);		
        
        $sl_ustatement -> execute($sl_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $employee_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To update comp off details  
INPUT 	: Attendance ID, Comp Off Data Array
OUTPUT 	: Attendance ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_co_details($attendance_id,$co_details)
{
	// Query
    $co_uquery_base = "update hr_comp_off set";  
	
	$co_uquery_set = "";
	
	$co_uquery_where = " where hr_comp_off_attendance_id=:attendance_id";
	
	$co_udata = array(":attendance_id"=>$attendance_id);
	
	$filter_count = 0;
	
	if(array_key_exists("type",$co_details))
	{
		$co_uquery_set = $co_uquery_set." hr_comp_off_type = :type,";
		$co_udata[":type"] = $co_details["type"];
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$co_uquery_set = trim($co_uquery_set,',');
	}
	
	$co_uquery = $co_uquery_base.$co_uquery_set.$co_uquery_where;	

    try
    {
        $dbConnection = get_conn_handle();
        
        $co_ustatement = $dbConnection->prepare($co_uquery);		
        
        $co_ustatement -> execute($co_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $attendance_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>