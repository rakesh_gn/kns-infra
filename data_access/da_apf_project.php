<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new APF Details
INPUT 	: Project, Village, Bank, Planned Start Date, Planned End Date, Remarks, Added By
OUTPUT 	: APF ID, success or failure message
BY 		: Lakshmi
*/
function db_add_apf_details($project,$village,$bank,$planned_start_date,$planned_end_date,$remarks,$added_by)
{
	// Query
   $apf_details_iquery = "insert into apf_details
   (apf_details_project,apf_details_village,apf_details_bank,apf_details_planned_start_date,apf_details_planned_end_date,apf_details_active,apf_details_remarks,
   apf_details_added_by,apf_details_added_on)values(:project,:village,:bank,:planned_start_date,:planned_end_date,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $apf_details_istatement = $dbConnection->prepare($apf_details_iquery);
        
        // Data
        $apf_details_idata = array(':project'=>$project,':village'=>$village,':bank'=>$bank,':planned_start_date'=>$planned_start_date,':planned_end_date'=>$planned_end_date,
		':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $apf_details_istatement->execute($apf_details_idata);
		$apf_details_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_details_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get APF Details List
INPUT 	: APF ID, Project, Village, Bank, Planned Start Date, Planned End Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Details 
BY 		: Lakshmi
*/
function db_get_apf_details($apf_details_search_data)
{ 
	if(array_key_exists("apf_id",$apf_details_search_data))
	{
		$apf_id = $apf_details_search_data["apf_id"];
	}
	else
	{
		$apf_id = "";
	}
	
	if(array_key_exists("project",$apf_details_search_data))
	{
		$project = $apf_details_search_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("project_name",$apf_details_search_data))
	{
		$project_name = $apf_details_search_data["project_name"];
	}
	else
	{
		$project_name = "";
	}
	
	if(array_key_exists("bank_name",$apf_details_search_data))
	{
		$bank_name = $apf_details_search_data["bank_name"];
	}
	else
	{
		$bank_name = "";
	}
	
	if(array_key_exists("village",$apf_details_search_data))
	{
		$village = $apf_details_search_data["village"];
	}
	else
	{
		$village = "";
	}
	
	if(array_key_exists("bank",$apf_details_search_data))
	{
		$bank = $apf_details_search_data["bank"];
	}
	else
	{
		$bank = "";
	}
	
	if(array_key_exists("planned_start_date",$apf_details_search_data))
	{
		$planned_start_date = $apf_details_search_data["planned_start_date"];
	}
	else
	{
		$planned_start_date = "";
	}
	
	if(array_key_exists("planned_end_date",$apf_details_search_data))
	{
		$planned_end_date = $apf_details_search_data["planned_end_date"];
	}
	else
	{
		$planned_end_date = "";
	}
	
	if(array_key_exists("active",$apf_details_search_data))
	{
		$active = $apf_details_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$apf_details_search_data))
	{
		$added_by = $apf_details_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$apf_details_search_data))
	{
		$start_date= $apf_details_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$apf_details_search_data))
	{
		$end_date= $apf_details_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_apf_details_list_squery_base = "select * from apf_details SD inner join users U on U.user_id = SD.apf_details_added_by inner join village_master VM on VM.village_id=SD.apf_details_village inner join apf_bank_master ABM on ABM.apf_bank_master_id=SD.apf_details_bank inner join apf_project_master APM on APM.apf_project_master_id = SD.apf_details_project";
	
	$get_apf_details_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_apf_details_list_sdata = array();
	
	if($apf_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where apf_details_id = :apf_id";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and apf_details_id = :apf_id";				
		}
		
		// Data
		$get_apf_details_list_sdata[':apf_id'] = $apf_id;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where apf_details_project = :project";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and apf_details_project = :project";				
		}
		
		// Data
		$get_apf_details_list_sdata[':project'] = $project;
		
		$filter_count++;
	}
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where APM.apf_project_master_id = :project_name";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and APM.apf_project_master_id = :project_name";				
		}
		
		// Data
		$get_apf_details_list_sdata[':project_name'] = $project_name;
		
		$filter_count++;
	}
	
	if($bank_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where ABM.apf_bank_master_id = :bank_name";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and ABM.apf_bank_master_id = :bank_name";				
		}
		
		// Data
		$get_apf_details_list_sdata[':bank_name'] = $bank_name;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where apf_details_village = :village";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and apf_details_village = :village";				
		}
		
		// Data
		$get_apf_details_list_sdata[':village'] = $village;
		
		$filter_count++;
	}
	
	if($bank != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where apf_details_bank = :bank";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and apf_details_bank = :bank";				
		}
		
		// Data
		$get_apf_details_list_sdata[':bank'] = $bank;
		
		$filter_count++;
	}
	
	if($planned_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where apf_details_planned_start_date = :planned_start_date";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and apf_details_planned_start_date = :planned_start_date";				
		}
		
		// Data
		$get_apf_details_list_sdata[':planned_start_date'] = $planned_start_date;
		
		$filter_count++;
	}
	
	if($planned_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where apf_details_planned_end_date = :planned_end_date";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and apf_details_planned_end_date = :planned_end_date";				
		}
		
		// Data
		$get_apf_details_list_sdata[':planned_end_date'] = $planned_end_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where apf_details_active = :active";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and apf_details_active = :active";				
		}
		
		// Data
		$get_apf_details_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where apf_details_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and apf_details_added_by = :added_by";
		}
		
		//Data
		$get_apf_details_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where apf_details_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and apf_details_added_on >= :start_date";				
		}
		
		//Data
		$get_apf_details_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." where apf_details_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_apf_details_list_squery_where = $get_apf_details_list_squery_where." and apf_details_added_on <= :end_date";
		}
		
		//Data
		$get_apf_details_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_apf_details_list_squery = $get_apf_details_list_squery_base.$get_apf_details_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_details_list_sstatement = $dbConnection->prepare($get_apf_details_list_squery);
		
		$get_apf_details_list_sstatement -> execute($get_apf_details_list_sdata);
		
		$get_apf_details_list_sdetails = $get_apf_details_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_details_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_details_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_details_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update APF Details 
INPUT 	: APF ID, APF Details Update Array
OUTPUT 	: APF ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_apf_details($apf_id,$apf_details_update_data)
{
	
	if(array_key_exists("project",$apf_details_update_data))
	{	
		$project = $apf_details_update_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("village",$apf_details_update_data))
	{	
		$village = $apf_details_update_data["village"];
	}
	else
	{
		$village = "";
	}
	
	if(array_key_exists("bank",$apf_details_update_data))
	{	
		$bank = $apf_details_update_data["bank"];
	}
	else
	{
		$bank = "";
	}
	
	if(array_key_exists("planned_start_date",$apf_details_update_data))
	{	
		$planned_start_date = $apf_details_update_data["planned_start_date"];
	}
	else
	{
		$planned_start_date = "";
	}
	
	if(array_key_exists("planned_end_date",$apf_details_update_data))
	{	
		$planned_end_date = $apf_details_update_data["planned_end_date"];
	}
	else
	{
		$planned_end_date = "";
	}
	
	if(array_key_exists("active",$apf_details_update_data))
	{	
		$active = $apf_details_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$apf_details_update_data))
	{	
		$remarks = $apf_details_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$apf_details_update_data))
	{	
		$added_by = $apf_details_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$apf_details_update_data))
	{	
		$added_on = $apf_details_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $apf_details_update_uquery_base = "update apf_details set ";  
	
	$apf_details_update_uquery_set = "";
	
	$apf_details_update_uquery_where = " where apf_details_id = :apf_id";
	
	$apf_details_update_udata = array(":apf_id"=>$apf_id);
	
	$filter_count = 0;
	
	if($project != "")
	{
		$apf_details_update_uquery_set = $apf_details_update_uquery_set." apf_details_project = :project,";
		$apf_details_update_udata[":project"] = $project;
		$filter_count++;
	}
	
	if($village != "")
	{
		$apf_details_update_uquery_set = $apf_details_update_uquery_set." apf_details_village = :village,";
		$apf_details_update_udata[":village"] = $village;
		$filter_count++;
	}
	
	if($bank != "")
	{
		$apf_details_update_uquery_set = $apf_details_update_uquery_set." apf_details_bank = :bank,";
		$apf_details_update_udata[":bank"] = $bank;
		$filter_count++;
	}
	
	if($planned_start_date != "")
	{
		$apf_details_update_uquery_set = $apf_details_update_uquery_set." apf_details_planned_start_date = :planned_start_date,";
		$apf_details_update_udata[":planned_start_date"] = $planned_start_date;
		$filter_count++;
	}
	
	if($planned_end_date != "")
	{
		$apf_details_update_uquery_set = $apf_details_update_uquery_set." apf_details_planned_end_date = :planned_end_date,";
		$apf_details_update_udata[":planned_end_date"] = $planned_end_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$apf_details_update_uquery_set = $apf_details_update_uquery_set." apf_details_active = :active,";
		$apf_details_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$apf_details_update_uquery_set = $apf_details_update_uquery_set." apf_details_remarks = :remarks,";
		$apf_details_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$apf_details_update_uquery_set = $apf_details_update_uquery_set." apf_details_added_by = :added_by,";
		$apf_details_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$apf_details_update_uquery_set = $apf_details_update_uquery_set." apf_details_added_on = :added_on,";
		$apf_details_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$apf_details_update_uquery_set = trim($apf_details_update_uquery_set,',');
	}
	
	$apf_details_update_uquery = $apf_details_update_uquery_base.$apf_details_update_uquery_set.$apf_details_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_details_update_ustatement = $dbConnection->prepare($apf_details_update_uquery);		
        
        $apf_details_update_ustatement -> execute($apf_details_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new APF Process
INPUT 	: APF ID, Process ID, Start Date, End Date, Remarks, Added By
OUTPUT 	: APF Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_apf_process($apf_id,$process_id,$process_start_date,$process_end_date,$remarks,$added_by)
{
	// Query
   $apf_process_iquery = "insert into apf_process
   (apf_process_apf_id,process_id,apf_process_start_date,apf_process_end_date,apf_process_active,apf_process_remarks,apf_process_added_by,apf_process_added_on)
   values(:apf_id,:process_id,:process_start_date,:process_end_date,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $apf_process_istatement = $dbConnection->prepare($apf_process_iquery);
        
        // Data
        $apf_process_idata = array(':apf_id'=>$apf_id,':process_id'=>$process_id,':process_start_date'=>$process_start_date,':process_end_date'=>$process_end_date,':active'=>'1',
		':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $apf_process_istatement->execute($apf_process_idata);
		$apf_process_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_process_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get APF Process List
INPUT 	: APF Process ID, APF ID, Process ID, Start Date, End Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Process 
BY 		: Lakshmi
*/
function db_get_apf_process($apf_process_search_data)
{  
	if(array_key_exists("apf_process_id",$apf_process_search_data))
	{
		$apf_process_id = $apf_process_search_data["apf_process_id"];
	}
	else
	{
		$apf_process_id= "";
	}
	
	if(array_key_exists("apf_id",$apf_process_search_data))
	{
		$apf_id = $apf_process_search_data["apf_id"];
	}
	else
	{
		$apf_id = "";
	}
	
	if(array_key_exists("process_id",$apf_process_search_data))
	{
		$process_id = $apf_process_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("process_start_date",$apf_process_search_data))
	{
		$process_start_date = $apf_process_search_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$apf_process_search_data))
	{
		$process_end_date = $apf_process_search_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("is_process_started",$apf_process_search_data))
	{
		$is_process_started = $apf_process_search_data["is_process_started"];
	}
	else
	{
		$is_process_started = "";
	}
	
	if(array_key_exists("active",$apf_process_search_data))
	{
		$active = $apf_process_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$apf_process_search_data))
	{
		$added_by = $apf_process_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$apf_process_search_data))
	{
		$start_date= $apf_process_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$apf_process_search_data))
	{
		$end_date= $apf_process_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	if(array_key_exists("project_name",$apf_process_search_data))
	{
		$project_name= $apf_process_search_data["project_name"];
	}
	else
	{
		$project_name= "";
	}
	
	if(array_key_exists("bank_name",$apf_process_search_data))
	{
		$bank_name= $apf_process_search_data["bank_name"];
	}
	else
	{
		$bank_name= "";
	}
	
	$get_apf_process_list_squery_base = "select * from apf_process AP inner join users U on U.user_id = AP.apf_process_added_by inner join apf_process_master APM on APM.apf_process_master_id=AP.process_id inner join apf_details SD on SD.apf_details_id = AP.apf_process_apf_id inner join apf_bank_master ABM on ABM.apf_bank_master_id = SD.apf_details_bank inner join apf_project_master APPM on APPM.apf_project_master_id = SD.apf_details_project";
	
	$get_apf_process_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_apf_process_list_sdata = array();
	
	if($apf_process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where apf_process_id = :apf_process_id";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and apf_process_id = :apf_process_id";				
		}
		
		// Data
		$get_apf_process_list_sdata[':apf_process_id'] = $apf_process_id;
		
		$filter_count++;
	}
	
	if($apf_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where apf_process_apf_id = :apf_id";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and apf_process_apf_id = :apf_id";				
		}
		
		// Data
		$get_apf_process_list_sdata[':apf_id'] = $apf_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where process_id = :process_id";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and process_id = :process_id";				
		}
		
		// Data
		$get_apf_process_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where APPM.apf_project_master_id = :project_name";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and APPM.apf_project_master_id = :project_name";				
		}
		
		// Data
		$get_apf_process_list_sdata[':project_name'] = $project_name;
		
		$filter_count++;
	}
	
	if($bank_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where ABM.apf_bank_master_id = :bank_name";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and ABM.apf_bank_master_id = :bank_name";				
		}
		
		// Data
		$get_apf_process_list_sdata[':bank_name'] = $bank_name;
		
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where apf_process_start_date = :process_start_date";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and apf_process_start_date = :process_start_date";				
		}
		
		// Data
		$get_apf_process_list_sdata[':process_start_date'] = $process_start_date;
		
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where apf_process_end_date = :process_end_date";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and apf_process_end_date = :process_end_date";				
		}
		
		// Data
		$get_apf_process_list_sdata[':process_end_date'] = $process_end_date;
		
		$filter_count++;
	}
	
	if($is_process_started != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where apf_process_start_date != :not_process_start_date";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and apf_process_start_date != :not_process_start_date";				
		}
		
		// Data
		$get_apf_process_list_sdata[':not_process_start_date'] = '0000-00-00';
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where apf_process_active = :active";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and apf_process_active = :active";				
		}
		
		// Data
		$get_apf_process_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where apf_process_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and apf_process_added_by = :added_by";
		}
		
		//Data
		$get_apf_process_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where apf_process_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and apf_process_added_on >= :start_date";				
		}
		
		//Data
		$get_apf_process_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." where apf_process_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_apf_process_list_squery_where = $get_apf_process_list_squery_where." and apf_process_added_on <= :end_date";
		}
		
		//Data
		$get_apf_process_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	$get_apf_process_list_squery_order = ' order by apf_bank_master_name ASC,APM.apf_process_master_order ASC';
	
	$get_apf_process_list_squery = $get_apf_process_list_squery_base.$get_apf_process_list_squery_where.$get_apf_process_list_squery_order;
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_process_list_sstatement = $dbConnection->prepare($get_apf_process_list_squery);
		
		$get_apf_process_list_sstatement -> execute($get_apf_process_list_sdata);
		
		$get_apf_process_list_sdetails = $get_apf_process_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update APF Process 
INPUT 	: APF Process ID, APF Process Update Array
OUTPUT 	: APF Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_apf_process($apf_process_id,$apf_process_update_data)
{
	if(array_key_exists("apf_id",$apf_process_update_data))
	{	
		$apf_id = $apf_process_update_data["apf_id"];
	}
	else
	{
		$apf_id = "";
	}
	
	if(array_key_exists("process_id",$apf_process_update_data))
	{	
		$process_id = $apf_process_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("process_start_date",$apf_process_update_data))
	{	
		$process_start_date = $apf_process_update_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$apf_process_update_data))
	{	
		$process_end_date = $apf_process_update_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$apf_process_update_data))
	{	
		$active = $apf_process_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$apf_process_update_data))
	{	
		$remarks = $apf_process_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$apf_process_update_data))
	{	
		$added_by = $apf_process_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$apf_process_update_data))
	{	
		$added_on = $apf_process_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $apf_process_update_uquery_base = "update apf_process set";  
	
	$apf_process_update_uquery_set = "";
	
	$apf_process_update_uquery_where = " where apf_process_id = :apf_process_id";
	
	$apf_process_update_udata = array(":apf_process_id"=>$apf_process_id);
	
	$filter_count = 0;
	
	if($apf_id != "")
	{
		$apf_process_update_uquery_set = $apf_process_update_uquery_set." apf_process_apf_id = :apf_id,";
		$apf_process_update_udata[":apf_id"] = $apf_id;
		$filter_count++;
	}
	
	if($process_id != "")
	{
		$apf_process_update_uquery_set = $apf_process_update_uquery_set." process_id = :process_id,";
		$apf_process_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		$apf_process_update_uquery_set = $apf_process_update_uquery_set." apf_process_start_date = :process_start_date,";
		$apf_process_update_udata[":process_start_date"] = $process_start_date;
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		$apf_process_update_uquery_set = $apf_process_update_uquery_set." apf_process_end_date = :process_end_date,";
		$apf_process_update_udata[":process_end_date"] = $process_end_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$apf_process_update_uquery_set = $apf_process_update_uquery_set." apf_process_active = :active,";
		$apf_process_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$apf_process_update_uquery_set = $apf_process_update_uquery_set." apf_process_remarks = :remarks,";
		$apf_process_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$apf_process_update_uquery_set = $apf_process_update_uquery_set." apf_process_added_by = :added_by,";
		$apf_process_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$apf_process_update_uquery_set = $apf_process_update_uquery_set." apf_process_added_on = :added_on,";
		$apf_process_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$apf_process_update_uquery_set = trim($apf_process_update_uquery_set,',');
	}
	
	$apf_process_update_uquery = $apf_process_update_uquery_base.$apf_process_update_uquery_set.$apf_process_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_process_update_ustatement = $dbConnection->prepare($apf_process_update_uquery);		
        
        $apf_process_update_ustatement -> execute($apf_process_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new APF File
INPUT 	: BD File ID, APF ID, Remarks, Added By
OUTPUT 	: File ID, success or failure message
BY 		: Lakshmi
*/
function db_add_apf_file($bd_file_id,$apf_id,$remarks,$added_by)
{
	// Query
   $apf_file_iquery = "insert into apf_file
   (apf_file_bd_file_id,apf_file_apf_id,apf_file_active,apf_file_remarks,apf_file_added_by,apf_file_added_on)values(:bd_file_id,:apf_id,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $apf_file_istatement = $dbConnection->prepare($apf_file_iquery);
        
        // Data
        $apf_file_idata = array(':bd_file_id'=>$bd_file_id,':apf_id'=>$apf_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $apf_file_istatement->execute($apf_file_idata);
		$apf_file_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_file_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get APF File list
INPUT 	: File ID, BD File ID, APF ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF File
BY 		: Lakshmi
*/
function db_get_apf_file($apf_file_search_data)
{  
	if(array_key_exists("file_id",$apf_file_search_data))
	{
		$file_id = $apf_file_search_data["file_id"];
	}
	else
	{
		$file_id = "";
	}
	
	if(array_key_exists("bd_file_id",$apf_file_search_data))
	{
		$bd_file_id = $apf_file_search_data["bd_file_id"];
	}
	else
	{
		$bd_file_id = "";
	}
	
	if(array_key_exists("apf_id",$apf_file_search_data))
	{
		$apf_id = $apf_file_search_data["apf_id"];
	}
	else
	{
		$apf_id = "";
	}
	
	if(array_key_exists("active",$apf_file_search_data))
	{
		$active = $apf_file_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$apf_file_search_data))
	{
		$added_by = $apf_file_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$apf_file_search_data))
	{
		$start_date = $apf_file_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$apf_file_search_data))
	{
		$end_date = $apf_file_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	if(array_key_exists("project_name",$apf_file_search_data))
	{
		$project_name = $apf_file_search_data["project_name"];
	}
	else
	{
		$project_name = "";
	}
	
	if(array_key_exists("bank_name",$apf_file_search_data))
	{
		$bank_name = $apf_file_search_data["bank_name"];
	}
	else
	{
		$bank_name = "";
	}
	
	if(array_key_exists("process_name",$apf_file_search_data))
	{
		$process_name = $apf_file_search_data["process_name"];
	}
	else
	{
		$process_name = "";
	}
	
	if(array_key_exists("survey_no",$apf_file_search_data))
	{
		$survey_no = $apf_file_search_data["survey_no"];
	}
	else
	{
		$survey_no = "";
	}

	$get_apf_file_list_squery_base = "select * from apf_file AF inner join users U on U.user_id = AF.apf_file_added_by inner join apf_details AD on AD.apf_details_id = AF.apf_file_apf_id inner join apf_survey_master ASMM on ASMM.apf_survey_master_id = AF.apf_file_bd_file_id inner join apf_project_master APM on APM.apf_project_master_id = AD.apf_details_project inner join apf_bank_master ABM on ABM.apf_bank_master_id = AD.apf_details_bank inner join village_master VM on VM.village_id = AD.apf_details_village";
	
	$get_apf_file_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_apf_file_list_sdata = array();
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where apf_file_id = :file_id";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and apf_file_id = :file_id";				
		}
		
		// Data
		$get_apf_file_list_sdata[':file_id'] = $file_id;
		
		$filter_count++;
	}
	
	if($bd_file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where apf_file_bd_file_id = :bd_file_id";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and apf_file_bd_file_id = :bd_file_id";				
		}
		
		// Data
		$get_apf_file_list_sdata[':bd_file_id'] = $bd_file_id;
		
		$filter_count++;
	}
	
	if($apf_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where apf_file_apf_id = :apf_id";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and apf_file_apf_id = :apf_id";				
		}
		
		// Data
		$get_apf_file_list_sdata[':apf_id'] = $apf_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where apf_file_active = :active";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and apf_file_active = :active";				
		}
		
		// Data
		$get_apf_file_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where apf_file_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and apf_file_added_by = :added_by";
		}
		
		//Data
		$get_apf_file_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where apf_details_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and apf_details_added_on >= :start_date";				
		}
		
		//Data
		$get_apf_file_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where apf_details_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and apf_details_added_on <= :end_date";
		}
		
		//Data
		$get_apf_file_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where APM.apf_project_master_id = :project_name";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and APM.apf_project_master_id = :project_name";				
		}
		
		// Data
		$get_apf_file_list_sdata[':project_name'] = $project_name;
		
		$filter_count++;
	}
	
	if($process_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where APMM.apf_process_master_id = :process_name";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and APMM.apf_process_master_id = :process_name";				
		}
		
		// Data
		$get_apf_file_list_sdata[':process_name'] = $process_name;
		
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where ASMM.apf_survey_master_id = :survey_no";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and ASMM.apf_survey_master_id = :survey_no";				
		}
		
		// Data
		$get_apf_file_list_sdata[':survey_no'] = $survey_no;
		
		$filter_count++;
	}
	
	if($bank_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." where ABM.apf_bank_master_id = :bank_name";								
		}
		else
		{
			// Query
			$get_apf_file_list_squery_where = $get_apf_file_list_squery_where." and ABM.apf_bank_master_id = :bank_name";				
		}
		
		// Data
		$get_apf_file_list_sdata[':bank_name'] = $bank_name;
		
		$filter_count++;
	}
	
	
	$get_apf_file_list_squery = $get_apf_file_list_squery_base.$get_apf_file_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_file_list_sstatement = $dbConnection->prepare($get_apf_file_list_squery);
		
		$get_apf_file_list_sstatement -> execute($get_apf_file_list_sdata);
		
		$get_apf_file_list_sdetails = $get_apf_file_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_file_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_file_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_file_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update APF File 
INPUT 	: File ID, APF File Update Array
OUTPUT 	: File ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_apf_file($file_id,$apf_file_update_data)
{
	
	if(array_key_exists("bd_file_id",$apf_file_update_data))
	{	
		$bd_file_id = $apf_file_update_data["bd_file_id"];
	}
	else
	{
		$bd_file_id = "";
	}
	
	if(array_key_exists("apf_id",$apf_file_update_data))
	{	
		$apf_id = $apf_file_update_data["apf_id"];
	}
	else
	{
		$apf_id = "";
	}
	
	if(array_key_exists("active",$apf_file_update_data))
	{	
		$active = $apf_file_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$apf_file_update_data))
	{	
		$remarks = $apf_file_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$apf_file_update_data))
	{	
		$added_by = $apf_file_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$apf_file_update_data))
	{	
		$added_on = $apf_file_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $apf_file_update_uquery_base = "update apf_file set";  
	
	$apf_file_update_uquery_set = "";
	
	$apf_file_update_uquery_where = " where apf_file_id = :file_id";
	
	$apf_file_update_udata = array(":file_id"=>$file_id);
	
	$filter_count = 0;
	
	if($bd_file_id != "")
	{
		$apf_file_update_uquery_set = $apf_file_update_uquery_set." apf_file_bd_file_id = :bd_file_id,";
		$apf_file_update_udata[":bd_file_id"] = $bd_file_id;
		$filter_count++;
	}
	
	if($apf_id != "")
	{
		$apf_file_update_uquery_set = $apf_file_update_uquery_set." apf_file_apf_id = :apf_id,";
		$apf_file_update_udata[":apf_id"] = $apf_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$apf_file_update_uquery_set = $apf_file_update_uquery_set." apf_file_active = :active,";
		$apf_file_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$apf_file_update_uquery_set = $apf_file_update_uquery_set." apf_file_remarks = :remarks,";
		$apf_file_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$apf_file_update_uquery_set = $apf_file_update_uquery_set." apf_file_added_by = :added_by,";
		$apf_file_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$apf_file_update_uquery_set = $apf_file_update_uquery_set." apf_file_added_on = :added_on,";
		$apf_file_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$apf_file_update_uquery_set = trim($apf_file_update_uquery_set,',');
	}
	
	$apf_file_update_uquery = $apf_file_update_uquery_base.$apf_file_update_uquery_set.$apf_file_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_file_update_ustatement = $dbConnection->prepare($apf_file_update_uquery);		
        
        $apf_file_update_ustatement -> execute($apf_file_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new APF Document
INPUT 	: Process ID, File Path ID, Remarks, Added By
OUTPUT 	: Document ID, success or failure message
BY 		: Lakshmi
*/
function db_add_apf_document($process_id,$file_path_id,$remarks,$added_by)
{
	// Query
   $apf_document_iquery = "insert into apf_document
   (apf_document_process_id,apf_document_file_path,apf_document_active,apf_document_remarks,apf_document_added_by,apf_document_added_on) values(:process_id,:file_path_id,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $apf_document_istatement = $dbConnection->prepare($apf_document_iquery);
        
        // Data
        $apf_document_idata = array(':process_id'=>$process_id,':file_path_id'=>$file_path_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $apf_document_istatement->execute($apf_document_idata);
		$apf_document_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_document_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get APF Document list
INPUT 	: Document ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Document
BY 		: Lakshmi
*/
function db_get_apf_document($apf_document_search_data)
{  
	if(array_key_exists("document_id",$apf_document_search_data))
	{
		$document_id = $apf_document_search_data["document_id"];
	}
	else
	{
		$document_id = "";
	}
	
	if(array_key_exists("process_id",$apf_document_search_data))
	{
		$process_id = $apf_document_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("file_path_id",$apf_document_search_data))
	{
		$file_path_id = $apf_document_search_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("active",$apf_document_search_data))
	{
		$active = $apf_document_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$apf_document_search_data))
	{
		$added_by = $apf_document_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$apf_document_search_data))
	{
		$start_date = $apf_document_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$apf_document_search_data))
	{
		$end_date = $apf_document_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	if(array_key_exists("project_name",$apf_document_search_data))
	{
		$project_name = $apf_document_search_data["project_name"];
	}
	else
	{
		$project_name = "";
	}
	
	if(array_key_exists("process_name",$apf_document_search_data))
	{
		$process_name = $apf_document_search_data["process_name"];
	}
	else
	{
		$process_name = "";
	}
	
	if(array_key_exists("bank_name",$apf_document_search_data))
	{
		$bank_name = $apf_document_search_data["bank_name"];
	}
	else
	{
		$bank_name = "";
	}
	
	if(array_key_exists("apf_id",$apf_document_search_data))
	{
		$apf_id = $apf_document_search_data["apf_id"];
	}
	else
	{
		$apf_id = "";
	}

	$get_apf_document_list_squery_base = "select * from apf_document A inner join users U on U.user_id = A.apf_document_added_by inner join apf_process AP on AP.apf_process_id = A.apf_document_process_id inner join apf_process_master APM on APM.apf_process_master_id = AP.process_id inner join apf_details AD on AD.apf_details_id = AP.apf_process_apf_id inner join apf_project_master APMM on APMM.apf_project_master_id = AD.apf_details_project inner join apf_bank_master ABM on ABM.apf_bank_master_id = AD.apf_details_bank";
	
	$get_apf_document_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_apf_document_list_sdata = array();
	
	if($document_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where apf_document_id = :document_id";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and apf_document_id = :document_id";				
		}
		
		// Data
		$get_apf_document_list_sdata[':document_id'] = $document_id;
		
		$filter_count++;
	}
	
	if($apf_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where AD.apf_details_id = :apf_id";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and AD.apf_details_id = :apf_id";				
		}
		
		// Data
		$get_apf_document_list_sdata[':apf_id'] = $apf_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where apf_document_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and apf_document_process_id = :process_id";				
		}
		
		// Data
		$get_apf_document_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where apf_document_file_path = :file_path_id";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and apf_document_file_path = :file_path_id";				
		}
		
		// Data
		$get_apf_document_list_sdata[':file_path_id'] = $file_path_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where apf_document_active = :active";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and apf_document_active = :active";				
		}
		
		// Data
		$get_apf_document_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where apf_document_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and apf_document_added_by = :added_by";
		}
		
		//Data
		$get_apf_document_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where apf_document_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and apf_document_added_on >= :start_date";				
		}
		
		//Data
		$get_apf_document_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where apf_document_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and apf_document_added_on <= :end_date";
		}
		
		//Data
		$get_apf_document_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where APMM.apf_project_master_id = :project_name";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and APMM.apf_project_master_id = :project_name";				
		}
		
		// Data
		$get_apf_document_list_sdata[':project_name'] = $project_name;
		
		$filter_count++;
	}
	
	if($bank_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where ABM.apf_bank_master_id = :bank_name";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and ABM.apf_bank_master_id = :bank_name";				
		}
		
		// Data
		$get_apf_document_list_sdata[':bank_name'] = $bank_name;
		
		$filter_count++;
	}
	
	if($process_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." where APM.apf_process_master_id = :process_name";								
		}
		else
		{
			// Query
			$get_apf_document_list_squery_where = $get_apf_document_list_squery_where." and APM.apf_process_master_id = :process_name";				
		}
		
		// Data
		$get_apf_document_list_sdata[':process_name'] = $process_name;
		
		$filter_count++;
	}
	
	$get_apf_document_list_squery = " order by apf_document_added_on DESC";
	$get_apf_document_list_squery = $get_apf_document_list_squery_base.$get_apf_document_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_document_list_sstatement = $dbConnection->prepare($get_apf_document_list_squery);
		
		$get_apf_document_list_sstatement -> execute($get_apf_document_list_sdata);
		
		$get_apf_document_list_sdetails = $get_apf_document_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_document_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_document_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_document_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update APF Document 
INPUT 	: Document ID, apf Document Update Array
OUTPUT 	: Document ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_apf_document($document_id,$apf_document_update_data)
{
	
	if(array_key_exists("process_id",$apf_document_update_data))
	{	
		$process_id = $apf_document_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("file_path_id",$apf_document_update_data))
	{	
		$file_path_id = $apf_document_update_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("active",$apf_document_update_data))
	{	
		$active = $apf_document_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$apf_document_update_data))
	{	
		$remarks = $apf_document_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$apf_document_update_data))
	{	
		$added_by = $apf_document_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$apf_document_update_data))
	{	
		$added_on = $apf_document_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $apf_document_update_uquery_base = "update apf_document set";  
	
	$apf_document_update_uquery_set = "";
	
	$apf_document_update_uquery_where = " where apf_document_id = :document_id";
	
	$apf_document_update_udata = array(":document_id"=>$document_id);
	
	$filter_count = 0;
	
	if($process_id != "")
	{
		$apf_document_update_uquery_set = $apf_document_update_uquery_set." apf_document_process_id = :process_id,";
		$apf_document_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		$apf_document_update_uquery_set = $apf_document_update_uquery_set." apf_document_file_path = :file_path_id,";
		$apf_document_update_udata[":file_path_id"] = $file_path_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$apf_document_update_uquery_set = $apf_document_update_uquery_set." apf_document_active = :active,";
		$apf_document_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$apf_document_update_uquery_set = $apf_document_update_uquery_set." apf_document_remarks = :remarks,";
		$apf_document_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$apf_document_update_uquery_set = $apf_document_update_uquery_set." apf_document_added_by = :added_by,";
		$apf_document_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$apf_document_update_uquery_set = $apf_document_update_uquery_set." apf_document_added_on = :added_on,";
		$apf_document_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$apf_document_update_uquery_set = trim($apf_document_update_uquery_set,',');
	}
	
	$apf_document_update_uquery = $apf_document_update_uquery_base.$apf_document_update_uquery_set.$apf_document_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_document_update_ustatement = $dbConnection->prepare($apf_document_update_uquery);		
        
        $apf_document_update_ustatement -> execute($apf_document_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $document_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new APF Query
INPUT 	: Process ID, Query, Query By, Query Date, Remarks, Added By
OUTPUT 	: Query ID, success or failure message
BY 		: Lakshmi
*/
function db_add_apf_query($process_id,$query,$query_by,$remarks,$added_by)
{
	// Query
   $apf_query_iquery = "insert into apf_query
   (apf_query_process_id,apf_query,apf_query_by,apf_query_date,apf_query_active,apf_query_remarks,apf_query_added_by,apf_query_added_on) values(:process_id,:query,:query_by,:query_date,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $apf_query_istatement = $dbConnection->prepare($apf_query_iquery);
        
        // Data
        $apf_query_idata = array(':process_id'=>$process_id,':query'=>$query,':query_by'=>$query_by,':query_date'=>date("Y-m-d"),':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $apf_query_istatement->execute($apf_query_idata);
		$apf_query_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_query_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get APF Query list
INPUT 	: Query ID, Process ID, Query, Query By, Query Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Query
BY 		: Lakshmi
*/
function db_get_apf_query($apf_query_search_data)
{  
	if(array_key_exists("query_id",$apf_query_search_data))
	{
		$query_id = $apf_query_search_data["query_id"];
	}
	else
	{
		$query_id = "";
	}
	
	if(array_key_exists("process_id",$apf_query_search_data))
	{
		$process_id = $apf_query_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("query",$apf_query_search_data))
	{
		$query = $apf_query_search_data["query"];
	}
	else
	{
		$query = "";
	}
	
	if(array_key_exists("query_by",$apf_query_search_data))
	{
		$query_by = $apf_query_search_data["query_by"];
	}
	else
	{
		$query_by = "";
	}
	
	if(array_key_exists("query_date",$apf_query_search_data))
	{
		$query_date = $apf_query_search_data["query_date"];
	}
	else
	{
		$query_date = "";
	}
	
	if(array_key_exists("active",$apf_query_search_data))
	{
		$active = $apf_query_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$apf_query_search_data))
	{
		$added_by = $apf_query_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$apf_query_search_data))
	{
		$start_date = $apf_query_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$apf_query_search_data))
	{
		$end_date = $apf_query_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	if(array_key_exists("project_name",$apf_query_search_data))
	{
		$project_name = $apf_query_search_data["project_name"];
	}
	else
	{
		$project_name = "";
	}
	
	if(array_key_exists("process_name",$apf_query_search_data))
	{
		$process_name = $apf_query_search_data["process_name"];
	}
	else
	{
		$process_name = "";
	}
	
	if(array_key_exists("bank_name",$apf_query_search_data))
	{
		$bank_name = $apf_query_search_data["bank_name"];
	}
	else
	{
		$bank_name = "";
	}
	
	if(array_key_exists("village_name",$apf_query_search_data))
	{
		$village_name = $apf_query_search_data["village_name"];
	}
	else
	{
		$village_name = "";
	}

	$get_apf_query_list_squery_base = "select *,U.user_name as added_by from apf_query AQR inner join users U on U.user_id = AQR.apf_query_added_by inner join apf_process AP on AP.apf_process_id = AQR.apf_query_process_id inner join apf_details AD on AD.apf_details_id = AP.apf_process_apf_id inner join apf_project_master APM on APM.apf_project_master_id = AD.apf_details_project inner join apf_process_master APMM on APMM.apf_process_master_id = AP.process_id inner join apf_bank_master ABM on ABM.apf_bank_master_id = AD.apf_details_bank inner join village_master VM on VM.village_id = AD.apf_details_village";
	
	$get_apf_query_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_apf_query_list_sdata = array();
	
	if($query_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where apf_query_id = :query_id";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and apf_query_id = :query_id";				
		}
		
		// Data
		$get_apf_query_list_sdata[':query_id'] = $query_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where apf_query_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and apf_query_process_id = :process_id";				
		}
		
		// Data
		$get_apf_query_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($query != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where apf_query = :query";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and apf_query = :query";				
		}
		
		// Data
		$get_apf_query_list_sdata[':query'] = $query;
		
		$filter_count++;
	}
	
	if($query_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where apf_query_by = :query_by";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and apf_query_by = :query_by";				
		}
		
		// Data
		$get_apf_query_list_sdata[':query_by'] = $query_by;
		
		$filter_count++;
	}
	
	if($query_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where apf_query_query_date = :query_date";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and apf_query_query_date = :query_date";				
		}
		
		// Data
		$get_apf_query_list_sdata[':query_date'] = $query_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where apf_query_active = :active";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and apf_query_active = :active";				
		}
		
		// Data
		$get_apf_query_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where apf_query_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and apf_query_added_by = :added_by";
		}
		
		//Data
		$get_apf_query_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where apf_query_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and apf_query_added_on >= :start_date";				
		}
		
		//Data
		$get_apf_query_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where apf_query_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and apf_query_added_on <= :end_date";
		}
		
		//Data
		$get_apf_query_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where APM.apf_project_master_id = :project_name";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and APM.apf_project_master_id = :project_name";				
		}
		
		// Data
		$get_apf_query_list_sdata[':project_name'] = $project_name;
		
		$filter_count++;
	}
	
	if($process_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where APMM.apf_process_master_id = :process_name";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and APMM.apf_process_master_id = :process_name";				
		}
		
		// Data
		$get_apf_query_list_sdata[':process_name'] = $process_name;
		
		$filter_count++;
	}
	
	if($bank_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where ABM.apf_bank_master_id = :bank_name";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and ABM.apf_bank_master_id = :bank_name";				
		}
		
		// Data
		$get_apf_query_list_sdata[':bank_name'] = $bank_name;
		
		$filter_count++;
	}
	
	if($village_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." where VM.village_id = :village_name";								
		}
		else
		{
			// Query
			$get_apf_query_list_squery_where = $get_apf_query_list_squery_where." and VM.village_id = :village_name";				
		}
		
		// Data
		$get_apf_query_list_sdata[':village_name'] = $village_name;
		
		$filter_count++;
	}
	
	$get_apf_query_list_squery = $get_apf_query_list_squery_base.$get_apf_query_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_query_list_sstatement = $dbConnection->prepare($get_apf_query_list_squery);
		
		$get_apf_query_list_sstatement -> execute($get_apf_query_list_sdata);
		
		$get_apf_query_list_sdetails = $get_apf_query_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_query_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_query_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_query_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update APF Query 
INPUT 	: Query ID, APF Query Update Array
OUTPUT 	: Query ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_apf_query($query_id,$apf_query_update_data)
{
	
	if(array_key_exists("process_id",$apf_query_update_data))
	{	
		$process_id = $apf_query_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("query",$apf_query_update_data))
	{	
		$query = $apf_query_update_data["query"];
	}
	else
	{
		$query = "";
	}
	
	if(array_key_exists("query_by",$apf_query_update_data))
	{	
		$query_by = $apf_query_update_data["query_by"];
	}
	else
	{
		$query_by = "";
	}
	
	if(array_key_exists("query_date",$apf_query_update_data))
	{	
		$query_date = $apf_query_update_data["query_date"];
	}
	else
	{
		$query_date = "";
	}
	
	if(array_key_exists("active",$apf_query_update_data))
	{	
		$active = $apf_query_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$apf_query_update_data))
	{	
		$remarks = $apf_query_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$apf_query_update_data))
	{	
		$added_by = $apf_query_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$apf_query_update_data))
	{	
		$added_on = $apf_query_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $apf_query_update_uquery_base = "update apf_query set";  
	
	$apf_query_update_uquery_set = "";
	
	$apf_query_update_uquery_where = " where apf_query_id = :query_id";
	
	$apf_query_update_udata = array(":query_id"=>$query_id);
	
	$filter_count = 0;
	
	if($process_id != "")
	{
		$apf_query_update_uquery_set = $apf_query_update_uquery_set." apf_query_process_id = :process_id,";
		$apf_query_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($query != "")
	{
		$apf_query_update_uquery_set = $apf_query_update_uquery_set." apf_query = :query,";
		$apf_query_update_udata[":query"] = $query;
		$filter_count++;
	}
	
	if($query_by != "")
	{
		$apf_query_update_uquery_set = $apf_query_update_uquery_set." apf_query_by = :query_by,";
		$apf_query_update_udata[":query_by"] = $query_by;
		$filter_count++;
	}
	
	if($query_date != "")
	{
		$apf_query_update_uquery_set = $apf_query_update_uquery_set." apf_query_query_date = :query_date,";
		$apf_query_update_udata[":query_date"] = $query_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$apf_query_update_uquery_set = $apf_query_update_uquery_set." apf_query_active = :active,";
		$apf_query_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$apf_query_update_uquery_set = $apf_query_update_uquery_set." apf_query_remarks = :remarks,";
		$apf_query_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$apf_query_update_uquery_set = $apf_query_update_uquery_set." apf_query_added_by = :added_by,";
		$apf_query_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$apf_query_update_uquery_set = $apf_query_update_uquery_set." apf_query_added_on = :added_on,";
		$apf_query_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$apf_query_update_uquery_set = trim($apf_query_update_uquery_set,',');
	}
	
	$apf_query_update_uquery = $apf_query_update_uquery_base.$apf_query_update_uquery_set.$apf_query_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_query_update_ustatement = $dbConnection->prepare($apf_query_update_uquery);		
        
        $apf_query_update_ustatement -> execute($apf_query_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $query_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
} 

/*
PURPOSE : To add new APF Process Master
INPUT 	: Name, Order, Remarks, Added By
OUTPUT 	: Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_apf_process_master($name,$remarks,$added_by)
{
	// Query
   $apf_process_master_iquery = "insert into apf_process_master (apf_process_master_name,apf_process_master_order,apf_process_master_active,apf_process_master_remarks,apf_process_master_added_by,apf_process_master_added_on) values(:name,:order,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $apf_process_master_istatement = $dbConnection->prepare($apf_process_master_iquery);
        
        // Data
        $apf_process_master_idata = array(':name'=>$name,':order'=>'0',':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $apf_process_master_istatement->execute($apf_process_master_idata);
		$apf_process_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_process_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get apf Process Master List
INPUT 	: Process ID, Name, Order, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of apf Process Master
BY 		: Lakshmi
*/
function db_get_apf_process_master($apf_process_master_search_data)
{  
	if(array_key_exists("process_id",$apf_process_master_search_data))
	{
		$process_id = $apf_process_master_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}
	
	if(array_key_exists("name",$apf_process_master_search_data))
	{
		$name = $apf_process_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("order",$apf_process_master_search_data))
	{
		$order = $apf_process_master_search_data["order"];
	}
	else
	{
		$order = "";
	}
	
	if(array_key_exists("process_name_check",$apf_process_master_search_data))
	{
		$process_name_check = $apf_process_master_search_data["process_name_check"];
	}
	else
	{
		$process_name_check = "";
	}
	
	if(array_key_exists("active",$apf_process_master_search_data))
	{
		$active = $apf_process_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$apf_process_master_search_data))
	{
		$added_by = $apf_process_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$apf_process_master_search_data))
	{
		$start_date= $apf_process_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$apf_process_master_search_data))
	{
		$end_date= $apf_process_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_apf_process_master_list_squery_base = "select * from apf_process_master APM inner join users U on U.user_id = APM.apf_process_master_added_by";
	
	$get_apf_process_master_list_squery_where = "";
	$get_apf_process_master_list_squery_order_by = " order by apf_process_master_order ASC";
	$filter_count = 0;
	
	// Data
	$get_apf_process_master_list_sdata = array();
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." where apf_process_master_id = :process_id";								
		}
		else
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." and apf_process_master_id = :process_id";				
		}
		
		// Data
		$get_apf_process_master_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($process_name_check == '1')
			{
				$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." where apf_process_master_name = :name";		
				// Data
				$get_apf_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." where apf_process_master_name like :name";						

				// Data
				$get_apf_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." where apf_process_master_name like :name";						

				// Data
				$get_apf_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($process_name_check == '1')
			{
				$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." and apf_process_master_name = :name";	
					
				// Data
				$get_apf_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." or apf_process_master_name like :name";				
				// Data
				$get_apf_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." and apf_process_master_name like :name";
				
				// Data
				$get_apf_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	
	if($order != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." where apf_process_master_order = :order";								
		}
		else
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." and apf_process_master_order = :order";				
		}
		
		// Data
		$get_apf_process_master_list_sdata[':order']  = $order;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." where apf_process_master_active = :active";								
		}
		else
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." and apf_process_master_active = :active";				
		}
		
		// Data
		$get_apf_process_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." where apf_process_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." and apf_process_master_added_by = :added_by";
		}
		
		//Data
		$get_apf_process_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." where apf_process_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." and apf_process_master_added_on >= :start_date";				
		}
		
		//Data
		$get_apf_process_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." where apf_process_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_apf_process_master_list_squery_where = $get_apf_process_master_list_squery_where." and apf_process_master_added_on <= :end_date";
		}
		
		//Data
		$get_apf_process_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_apf_process_master_list_squery = $get_apf_process_master_list_squery_base.$get_apf_process_master_list_squery_where.$get_apf_process_master_list_squery_order_by;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_process_master_list_sstatement = $dbConnection->prepare($get_apf_process_master_list_squery);
		
		$get_apf_process_master_list_sstatement -> execute($get_apf_process_master_list_sdata);
		
		$get_apf_process_master_list_sdetails = $get_apf_process_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_process_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_process_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_process_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update APF Process Master 
INPUT 	: Process ID, APF Process Master Update Array
OUTPUT 	: Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_apf_process_master($process_id,$apf_process_master_update_data)
{
	if(array_key_exists("name",$apf_process_master_update_data))
	{	
		$name = $apf_process_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("order",$apf_process_master_update_data))
	{	
		$order = $apf_process_master_update_data["order"];
	}
	else
	{
		$order = "";
	}
	
	if(array_key_exists("active",$apf_process_master_update_data))
	{	
		$active = $apf_process_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$apf_process_master_update_data))
	{	
		$remarks = $apf_process_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$apf_process_master_update_data))
	{	
		$added_by = $apf_process_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$apf_process_master_update_data))
	{	
		$added_on = $apf_process_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $apf_process_master_update_uquery_base = "update apf_process_master set";  
	
	$apf_process_master_update_uquery_set = "";
	
	$apf_process_master_update_uquery_where = " where apf_process_master_id = :process_id";
	
	$apf_process_master_update_udata = array(":process_id"=>$process_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$apf_process_master_update_uquery_set = $apf_process_master_update_uquery_set." apf_process_master_name = :name,";
		$apf_process_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($order != "")
	{
		$apf_process_master_update_uquery_set = $apf_process_master_update_uquery_set." apf_process_master_order = :order,";
		$apf_process_master_update_udata[":order"] = $order;
		$filter_count++;
	}
	
	if($active != "")
	{
		$apf_process_master_update_uquery_set = $apf_process_master_update_uquery_set." apf_process_master_active = :active,";
		$apf_process_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$apf_process_master_update_uquery_set = $apf_process_master_update_uquery_set." apf_process_master_remarks = :remarks,";
		$apf_process_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$apf_process_master_update_uquery_set = $apf_process_master_update_uquery_set." apf_process_master_added_by = :added_by,";
		$apf_process_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$apf_process_master_update_uquery_set = $apf_process_master_update_uquery_set." apf_process_master_added_on = :added_on,";
		$apf_process_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$apf_process_master_update_uquery_set = trim($apf_process_master_update_uquery_set,',');
	}
	
	$apf_process_master_update_uquery = $apf_process_master_update_uquery_base.$apf_process_master_update_uquery_set.$apf_process_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_process_master_update_ustatement = $dbConnection->prepare($apf_process_master_update_uquery);		
        
        $apf_process_master_update_ustatement -> execute($apf_process_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new APF Bank Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Bank ID, success or failure message
BY 		: Lakshmi
*/
function db_add_apf_bank_master($name,$remarks,$added_by)
{
	// Query
   $apf_bank_master_iquery = "insert into apf_bank_master(apf_bank_master_name,apf_bank_master_active,apf_bank_master_remarks,apf_bank_master_added_by,apf_bank_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $apf_bank_master_istatement = $dbConnection->prepare($apf_bank_master_iquery);
        
        // Data
        $apf_bank_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $apf_bank_master_istatement->execute($apf_bank_master_idata);
		$apf_bank_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_bank_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get apf bank Master List
INPUT 	: Bank ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of apf bank Master
BY 		: Lakshmi
*/
function db_get_apf_bank_master($apf_bank_master_search_data)
{  
	if(array_key_exists("bank_id",$apf_bank_master_search_data))
	{
		$bank_id = $apf_bank_master_search_data["bank_id"];
	}
	else
	{
		$bank_id= "";
	}
	
	if(array_key_exists("name",$apf_bank_master_search_data))
	{
		$name = $apf_bank_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("bank_name_check",$apf_bank_master_search_data))
	{
		$bank_name_check = $apf_bank_master_search_data["bank_name_check"];
	}
	else
	{
		$bank_name_check = "";
	}
	
	if(array_key_exists("active",$apf_bank_master_search_data))
	{
		$active = $apf_bank_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$apf_bank_master_search_data))
	{
		$added_by = $apf_bank_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$apf_bank_master_search_data))
	{
		$start_date= $apf_bank_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$apf_bank_master_search_data))
	{
		$end_date= $apf_bank_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_apf_bank_master_list_squery_base = "select * from apf_bank_master ABM inner join users U on U.user_id = ABM.apf_bank_master_added_by";
	
	$get_apf_bank_master_list_squery_where = "";
	$get_apf_bank_master_list_squery_order_by = " order by apf_bank_master_name ASC";
	$filter_count = 0;
	
	// Data
	$get_apf_bank_master_list_sdata = array();
	
	if($bank_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." where apf_bank_master_id = :bank_id";								
		}
		else
		{
			// Query
			$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." and apf_bank_master_id = :bank_id";				
		}
		
		// Data
		$get_apf_bank_master_list_sdata[':bank_id'] = $bank_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($bank_name_check == '1')
			{
				$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." where apf_bank_master_name = :name";		
				// Data
				$get_apf_bank_master_list_sdata[':name']  = $name;
			}
			else if($bank_name_check == '2')
			{
				$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." where apf_bank_master_name like :name";						

				// Data
				$get_apf_bank_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." where apf_bank_master_name like :name";						

				// Data
				$get_apf_bank_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($bank_name_check == '1')
			{
				$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." and apf_bank_master_name = :name";	
					
				// Data
				$get_apf_bank_master_list_sdata[':name']  = $name;
			}
			else if($bank_name_check == '2')
			{
				$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." or apf_bank_master_name like :name";				
				// Data
				$get_apf_bank_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." and apf_bank_master_name like :name";
				
				// Data
				$get_apf_bank_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." where apf_bank_master_active = :active";								
		}
		else
		{
			// Query
			$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." and apf_bank_master_active = :active";				
		}
		
		// Data
		$get_apf_bank_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." where apf_bank_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." and apf_bank_master_added_by = :added_by";
		}
		
		//Data
		$get_apf_bank_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." where apf_bank_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." and apf_bank_master_added_on >= :start_date";				
		}
		
		//Data
		$get_apf_bank_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." where apf_bank_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_apf_bank_master_list_squery_where = $get_apf_bank_master_list_squery_where." and apf_bank_master_added_on <= :end_date";
		}
		
		//Data
		$get_apf_bank_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_apf_bank_master_list_squery = $get_apf_bank_master_list_squery_base.$get_apf_bank_master_list_squery_where.$get_apf_bank_master_list_squery_order_by;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_bank_master_list_sstatement = $dbConnection->prepare($get_apf_bank_master_list_squery);
		
		$get_apf_bank_master_list_sstatement -> execute($get_apf_bank_master_list_sdata);
		
		$get_apf_bank_master_list_sdetails = $get_apf_bank_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_bank_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_bank_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_bank_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update APF Bank Master 
INPUT 	: Bank ID, APF bank Master Update Array
OUTPUT 	: Bank ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_apf_bank_master($bank_id,$apf_bank_master_update_data)
{
	if(array_key_exists("name",$apf_bank_master_update_data))
	{	
		$name = $apf_bank_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$apf_bank_master_update_data))
	{	
		$active = $apf_bank_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$apf_bank_master_update_data))
	{	
		$remarks = $apf_bank_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$apf_bank_master_update_data))
	{	
		$added_by = $apf_bank_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$apf_bank_master_update_data))
	{	
		$added_on = $apf_bank_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $apf_bank_master_update_uquery_base = "update apf_bank_master set";  
	
	$apf_bank_master_update_uquery_set = "";
	
	$apf_bank_master_update_uquery_where = " where apf_bank_master_id = :bank_id";
	
	$apf_bank_master_update_udata = array(":bank_id"=>$bank_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$apf_bank_master_update_uquery_set = $apf_bank_master_update_uquery_set." apf_bank_master_name = :name,";
		$apf_bank_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$apf_bank_master_update_uquery_set = $apf_bank_master_update_uquery_set." apf_bank_master_active = :active,";
		$apf_bank_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$apf_bank_master_update_uquery_set = $apf_bank_master_update_uquery_set." apf_bank_master_remarks = :remarks,";
		$apf_bank_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$apf_bank_master_update_uquery_set = $apf_bank_master_update_uquery_set." apf_bank_master_added_by = :added_by,";
		$apf_bank_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$apf_bank_master_update_uquery_set = $apf_bank_master_update_uquery_set." apf_bank_master_added_on = :added_on,";
		$apf_bank_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$apf_bank_master_update_uquery_set = trim($apf_bank_master_update_uquery_set,',');
	}
	
	$apf_bank_master_update_uquery = $apf_bank_master_update_uquery_base.$apf_bank_master_update_uquery_set.$apf_bank_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_bank_master_update_ustatement = $dbConnection->prepare($apf_bank_master_update_uquery);		
        
        $apf_bank_master_update_ustatement -> execute($apf_bank_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $bank_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new APF Project Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Project ID, success or failure message
BY 		: Lakshmi
*/
function db_add_apf_project_master($name,$remarks,$added_by)
{
	// Query
   $apf_project_master_iquery = "insert into apf_project_master (apf_project_master_name,apf_project_master_active,apf_project_master_remarks,apf_project_master_added_by,apf_project_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $apf_project_master_istatement = $dbConnection->prepare($apf_project_master_iquery);
        
        // Data
        $apf_project_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $apf_project_master_istatement->execute($apf_project_master_idata);
		$apf_project_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_project_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get apf project Master List
INPUT 	: project ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of apf project Master
BY 		: Lakshmi
*/
function db_get_apf_project_master($apf_project_master_search_data)
{  
	if(array_key_exists("project_id",$apf_project_master_search_data))
	{
		$project_id = $apf_project_master_search_data["project_id"];
	}
	else
	{
		$project_id= "";
	}
	
	if(array_key_exists("name",$apf_project_master_search_data))
	{
		$name = $apf_project_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("project_name_check",$apf_project_master_search_data))
	{
		$project_name_check = $apf_project_master_search_data["project_name_check"];
	}
	else
	{
		$project_name_check = "";
	}
	
	if(array_key_exists("active",$apf_project_master_search_data))
	{
		$active = $apf_project_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$apf_project_master_search_data))
	{
		$added_by = $apf_project_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$apf_project_master_search_data))
	{
		$start_date= $apf_project_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$apf_project_master_search_data))
	{
		$end_date= $apf_project_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_apf_project_master_list_squery_base = "select * from apf_project_master APM inner join users U on U.user_id = APM.apf_project_master_added_by";
	
	$get_apf_project_master_list_squery_where = "";
	$get_apf_project_master_list_squery_order_by = " order by apf_project_master_name ASC";
	$filter_count = 0;
	
	// Data
	$get_apf_project_master_list_sdata = array();
	
	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." where apf_project_master_id = :project_id";								
		}
		else
		{
			// Query
			$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." and apf_project_master_id = :project_id";				
		}
		
		// Data
		$get_apf_project_master_list_sdata[':project_id'] = $project_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($project_name_check == '1')
			{
				$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." where apf_project_master_name = :name";		
				// Data
				$get_apf_project_master_list_sdata[':name']  = $name;
			}
			else if($project_name_check == '2')
			{
				$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." where apf_project_master_name like :name";						

				// Data
				$get_apf_project_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." where apf_project_master_name like :name";						

				// Data
				$get_apf_project_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($project_name_check == '1')
			{
				$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." and apf_project_master_name = :name";	
					
				// Data
				$get_apf_project_master_list_sdata[':name']  = $name;
			}
			else if($project_name_check == '2')
			{
				$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." or apf_project_master_name like :name";				
				// Data
				$get_apf_project_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." and apf_project_master_name like :name";
				
				// Data
				$get_apf_project_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." where apf_project_master_active = :active";								
		}
		else
		{
			// Query
			$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." and apf_project_master_active = :active";				
		}
		
		// Data
		$get_apf_project_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." where apf_project_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." and apf_project_master_added_by = :added_by";
		}
		
		//Data
		$get_apf_project_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." where apf_project_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." and apf_project_master_added_on >= :start_date";				
		}
		
		//Data
		$get_apf_project_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." where apf_project_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_apf_project_master_list_squery_where = $get_apf_project_master_list_squery_where." and apf_project_master_added_on <= :end_date";
		}
		
		//Data
		$get_apf_project_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_apf_project_master_list_squery = $get_apf_project_master_list_squery_base.$get_apf_project_master_list_squery_where.$get_apf_project_master_list_squery_order_by;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_project_master_list_sstatement = $dbConnection->prepare($get_apf_project_master_list_squery);
		
		$get_apf_project_master_list_sstatement -> execute($get_apf_project_master_list_sdata);
		
		$get_apf_project_master_list_sdetails = $get_apf_project_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_project_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_project_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_project_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update APF project Master 
INPUT 	: project ID, APF project Master Update Array
OUTPUT 	: project ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_apf_project_master($project_id,$apf_project_master_update_data)
{
	if(array_key_exists("name",$apf_project_master_update_data))
	{	
		$name = $apf_project_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$apf_project_master_update_data))
	{	
		$active = $apf_project_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$apf_project_master_update_data))
	{	
		$remarks = $apf_project_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$apf_project_master_update_data))
	{	
		$added_by = $apf_project_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$apf_project_master_update_data))
	{	
		$added_on = $apf_project_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $apf_project_master_update_uquery_base = "update apf_project_master set";  
	
	$apf_project_master_update_uquery_set = "";
	
	$apf_project_master_update_uquery_where = " where apf_project_master_id = :project_id";
	
	$apf_project_master_update_udata = array(":project_id"=>$project_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$apf_project_master_update_uquery_set = $apf_project_master_update_uquery_set." apf_project_master_name = :name,";
		$apf_project_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$apf_project_master_update_uquery_set = $apf_project_master_update_uquery_set." apf_project_master_active = :active,";
		$apf_project_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$apf_project_master_update_uquery_set = $apf_project_master_update_uquery_set." apf_project_master_remarks = :remarks,";
		$apf_project_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$apf_project_master_update_uquery_set = $apf_project_master_update_uquery_set." apf_project_master_added_by = :added_by,";
		$apf_project_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$apf_project_master_update_uquery_set = $apf_project_master_update_uquery_set." apf_project_master_added_on = :added_on,";
		$apf_project_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$apf_project_master_update_uquery_set = trim($apf_project_master_update_uquery_set,',');
	}
	
	$apf_project_master_update_uquery = $apf_project_master_update_uquery_base.$apf_project_master_update_uquery_set.$apf_project_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_project_master_update_ustatement = $dbConnection->prepare($apf_project_master_update_uquery);		
        
        $apf_project_master_update_ustatement -> execute($apf_project_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $project_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new APF Survey Master
INPUT 	: Project, Survey No, Land Owner, Extent, Remarks, Added By
OUTPUT 	: Survey ID, success or failure message
BY 		: Lakshmi
*/
function db_add_apf_survey_master($project,$survey_no,$land_owner,$extent,$remarks,$added_by)
{
	// Query
   $apf_survey_master_iquery = "insert into apf_survey_master
   (apf_survey_master_project,apf_survey_master_survey_no,apf_survey_master_land_owner,apf_survey_master_extent,apf_survey_master_active,apf_survey_master_remarks,
   apf_survey_master_added_by,apf_survey_master_added_on) values(:project,:survey_no,:land_owner,:extent,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $apf_survey_master_istatement = $dbConnection->prepare($apf_survey_master_iquery);
        
        // Data
        $apf_survey_master_idata = array(':project'=>$project,':survey_no'=>$survey_no,':land_owner'=>$land_owner,':extent'=>$extent,':active'=>'1',':remarks'=>$remarks,
		':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $apf_survey_master_istatement->execute($apf_survey_master_idata);
		$apf_survey_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_survey_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get APF Survey Master list
INPUT 	: Survey ID, Project, Survey No, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Survey Master
BY 		: Lakshmi
*/
function db_get_apf_survey_master($apf_survey_master_search_data)
{  
	if(array_key_exists("survey_id",$apf_survey_master_search_data))
	{
		$survey_id = $apf_survey_master_search_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}
	
	if(array_key_exists("project",$apf_survey_master_search_data))
	{
		$project = $apf_survey_master_search_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("survey_no",$apf_survey_master_search_data))
	{
		$survey_no = $apf_survey_master_search_data["survey_no"];
	}
	else
	{
		$survey_no = "";
	}
	
	if(array_key_exists("land_owner",$apf_survey_master_search_data))
	{
		$land_owner = $apf_survey_master_search_data["land_owner"];
	}
	else
	{
		$land_owner = "";
	}
	
	if(array_key_exists("extent",$apf_survey_master_search_data))
	{
		$extent = $apf_survey_master_search_data["extent"];
	}
	else
	{
		$extent = "";
	}
	
	if(array_key_exists("active",$apf_survey_master_search_data))
	{
		$active = $apf_survey_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$apf_survey_master_search_data))
	{
		$added_by = $apf_survey_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$apf_survey_master_search_data))
	{
		$start_date = $apf_survey_master_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$apf_survey_master_search_data))
	{
		$end_date = $apf_survey_master_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	if(array_key_exists("is_mapped",$apf_survey_master_search_data))
	{
		$is_mapped = $apf_survey_master_search_data["is_mapped"];
	}
	else
	{
		$is_mapped = "";
	}
	
	if(array_key_exists("apf_bank",$apf_survey_master_search_data))
	{
		$apf_bank = $apf_survey_master_search_data["apf_bank"];
	}
	else
	{
		$apf_bank = "";
	}

	$get_apf_survey_master_list_squery_base = "select * from apf_survey_master ASMM inner join users U on U.user_id = ASMM.apf_survey_master_added_by inner join apf_project_master APMM on APMM.apf_project_master_id = ASMM.apf_survey_master_project";
	
	$get_apf_survey_master_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_apf_survey_master_list_sdata = array();
	
	if($survey_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." where apf_survey_master_id = :survey_id";								
		}
		else
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." and apf_survey_master_id = :survey_id";				
		}
		
		// Data
		$get_apf_survey_master_list_sdata[':survey_id'] = $survey_id;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." where apf_survey_master_project = :project";								
		}
		else
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." and apf_survey_master_project = :project";				
		}
		
		// Data
		$get_apf_survey_master_list_sdata[':project'] = $project;
		
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." where apf_survey_master_survey_no = :survey_no";								
		}
		else
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." and apf_survey_master_survey_no = :survey_no";				
		}
		
		// Data
		$get_apf_survey_master_list_sdata[':survey_no'] = $survey_no;
		
		$filter_count++;
	}
	
	if($land_owner != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." where apf_survey_master_land_owner = :land_owner";								
		}
		else
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." and apf_survey_master_land_owner = :land_owner";				
		}
		
		// Data
		$get_apf_survey_master_list_sdata[':land_owner'] = $land_owner;
		
		$filter_count++;
	}
	
	if($extent != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." where apf_survey_master_extent = :extent";								
		}
		else
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." and apf_survey_master_extent = :extent";				
		}
		
		// Data
		$get_apf_survey_master_list_sdata[':extent'] = $extent;
		
		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." where apf_survey_master_active = :active";								
		}
		else
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." and apf_survey_master_active = :active";				
		}
		
		// Data
		$get_apf_survey_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." where apf_survey_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." and apf_survey_master_added_by = :added_by";
		}
		
		//Data
		$get_apf_survey_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." where apf_survey_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." and apf_survey_master_added_on >= :start_date";				
		}
		
		//Data
		$get_apf_survey_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." where apf_survey_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." and apf_survey_master_added_on <= :end_date";
		}
		
		//Data
		$get_apf_survey_master_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($is_mapped != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." where apf_survey_master_id not in (select apf_file_bd_file_id from apf_file SAF inner join apf_details SAD on SAD.apf_details_id = SAF.apf_file_apf_id where SAF.apf_file_active = 1 and SAD.apf_details_bank = :bank)";								
		}
		else
		{
			// Query
			$get_apf_survey_master_list_squery_where = $get_apf_survey_master_list_squery_where." and apf_survey_master_id not in (select apf_file_bd_file_id from apf_file SAF inner join apf_details SAD on SAD.apf_details_id = SAF.apf_file_apf_id where SAF.apf_file_active = 1 and SAD.apf_details_bank = :bank)";				
		}
		
		//Data
		$get_apf_survey_master_list_sdata[':bank']  = $apf_bank;
		
		$filter_count++;
	}
	
	$get_apf_survey_master_list_squery = $get_apf_survey_master_list_squery_base.$get_apf_survey_master_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_survey_master_list_sstatement = $dbConnection->prepare($get_apf_survey_master_list_squery);
		
		$get_apf_survey_master_list_sstatement -> execute($get_apf_survey_master_list_sdata);
		
		$get_apf_survey_master_list_sdetails = $get_apf_survey_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_survey_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_survey_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_survey_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update APF Survey Master 
INPUT 	: Survey ID, APF Survey Master Update Array
OUTPUT 	: Survey ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_apf_survey_master($survey_id,$apf_survey_master_update_data)
{
	
	if(array_key_exists("project",$apf_survey_master_update_data))
	{	
		$project = $apf_survey_master_update_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("survey_no",$apf_survey_master_update_data))
	{	
		$survey_no = $apf_survey_master_update_data["survey_no"];
	}
	else
	{
		$survey_no = "";
	}
	
	if(array_key_exists("land_owner",$apf_survey_master_update_data))
	{	
		$land_owner = $apf_survey_master_update_data["land_owner"];
	}
	else
	{
		$land_owner = "";
	}
	
	if(array_key_exists("extent",$apf_survey_master_update_data))
	{	
		$extent = $apf_survey_master_update_data["extent"];
	}
	else
	{
		$extent = "";
	}
	
	if(array_key_exists("active",$apf_survey_master_update_data))
	{	
		$active = $apf_survey_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$apf_survey_master_update_data))
	{	
		$remarks = $apf_survey_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$apf_survey_master_update_data))
	{	
		$added_by = $apf_survey_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$apf_survey_master_update_data))
	{	
		$added_on = $apf_survey_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $apf_survey_master_update_uquery_base = "update apf_survey_master set ";  
	
	$apf_survey_master_update_uquery_set = "";
	
	$apf_survey_master_update_uquery_where = " where apf_survey_master_id = :survey_id";
	
	$apf_survey_master_update_udata = array(":survey_id"=>$survey_id);
	
	$filter_count = 0;
	
	if($project != "")
	{
		$apf_survey_master_update_uquery_set = $apf_survey_master_update_uquery_set." apf_survey_master_project = :project,";
		$apf_survey_master_update_udata[":project"] = $project;
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		$apf_survey_master_update_uquery_set = $apf_survey_master_update_uquery_set." apf_survey_master_survey_no = :survey_no,";
		$apf_survey_master_update_udata[":survey_no"] = $survey_no;
		$filter_count++;
	}
	
	if($land_owner != "")
	{
		$apf_survey_master_update_uquery_set = $apf_survey_master_update_uquery_set." apf_survey_master_land_owner = :land_owner,";
		$apf_survey_master_update_udata[":land_owner"] = $land_owner;
		$filter_count++;
	}
	
	if($extent != "")
	{
		$apf_survey_master_update_uquery_set = $apf_survey_master_update_uquery_set." apf_survey_master_extent = :extent,";
		$apf_survey_master_update_udata[":extent"] = $extent;
		$filter_count++;
	}
	
	if($active != "")
	{
		$apf_survey_master_update_uquery_set = $apf_survey_master_update_uquery_set." apf_survey_master_active = :active,";
		$apf_survey_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$apf_survey_master_update_uquery_set = $apf_survey_master_update_uquery_set." apf_survey_master_remarks = :remarks,";
		$apf_survey_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$apf_survey_master_update_uquery_set = $apf_survey_master_update_uquery_set." apf_survey_master_added_by = :added_by,";
		$apf_survey_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$apf_survey_master_update_uquery_set = $apf_survey_master_update_uquery_set." apf_survey_master_added_on = :added_on,";
		$apf_survey_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$apf_survey_master_update_uquery_set = trim($apf_survey_master_update_uquery_set,',');
	}
	
	$apf_survey_master_update_uquery = $apf_survey_master_update_uquery_base.$apf_survey_master_update_uquery_set.$apf_survey_master_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_survey_master_update_ustatement = $dbConnection->prepare($apf_survey_master_update_uquery);		
        
        $apf_survey_master_update_ustatement -> execute($apf_survey_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
} 

/*
PURPOSE : To get APF Dashboard Pending list
INPUT 	: APF ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF Dashboard
BY 		: Lakshmi
*/
function db_get_apf_dashboard_pending($apf_dashboard_pending_search_data)
{  
	if(array_key_exists("apf_id",$apf_dashboard_pending_search_data))
	{
		$apf_id = $apf_dashboard_pending_search_data["apf_id"];
	}
	else
	{
		$apf_id = "";
	}
	
	if(array_key_exists("project_name",$apf_dashboard_pending_search_data))
	{
		$project_name = $apf_dashboard_pending_search_data["project_name"];
	}
	else
	{
		$project_name = "";
	}
	
	if(array_key_exists("bank_name",$apf_dashboard_pending_search_data))
	{
		$bank_name = $apf_dashboard_pending_search_data["bank_name"];
	}
	else
	{
		$bank_name = "";
	}
	
	if(array_key_exists("is_complete",$apf_dashboard_pending_search_data))
	{
		$is_complete = $apf_dashboard_pending_search_data["is_complete"];
	}
	else
	{
		$is_complete = "";
	}
	
	
	if(array_key_exists("is_pending",$apf_dashboard_pending_search_data))
	{
		$is_pending = $apf_dashboard_pending_search_data["is_pending"];
	}
	else
	{
		$is_pending = "";
	}

	$get_apf_dashboard_pending_list_squery_base = "select * from apf_details AD inner join apf_project_master APM on APM.apf_project_master_id = AD.apf_details_project inner join apf_bank_master ABM on ABM.apf_bank_master_id = AD.apf_details_bank";
	$get_apf_dashboard_pending_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_apf_dashboard_pending_list_sdata = array();
	
	if($apf_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_dashboard_pending_list_squery_where = $get_apf_dashboard_pending_list_squery_where." where apf_process_apf_id = :apf_id";								
		}
		else
		{
			// Query
			$get_apf_dashboard_pending_list_squery_where = $get_apf_dashboard_pending_list_squery_where." and apf_process_apf_id = :apf_id";				
		}
		
		// Data
		$get_apf_dashboard_pending_list_sdata[':apf_id'] = $apf_id;
		
		$filter_count++;
	}
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_dashboard_pending_list_squery_where = $get_apf_dashboard_pending_list_squery_where." where APM.apf_project_master_id = :project_name";								
		}
		else
		{
			// Query
			$get_apf_dashboard_pending_list_squery_where = $get_apf_dashboard_pending_list_squery_where." and APM.apf_project_master_id = :project_name";				
		}
		
		// Data
		$get_apf_dashboard_pending_list_sdata[':project_name'] = $project_name;
		
		$filter_count++;
	}
	
	if($bank_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_dashboard_pending_list_squery_where = $get_apf_dashboard_pending_list_squery_where." where ABM.apf_bank_master_id = :bank_name";								
		}
		else
		{
			// Query
			$get_apf_dashboard_pending_list_squery_where = $get_apf_dashboard_pending_list_squery_where." and ABM.apf_bank_master_id = :bank_name";				
		}
		
		// Data
		$get_apf_dashboard_pending_list_sdata[':bank_name'] = $bank_name;
		
		$filter_count++;
	}
	
	if($is_complete != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_dashboard_pending_list_squery_where = $get_apf_dashboard_pending_list_squery_where." where (apf_details_id not in (select apf_process_apf_id from apf_process where apf_process_end_date = 0000-00-00 and apf_process_active = 1)) and (apf_details_id in (select apf_process_apf_id from apf_process where apf_process_active = 1))";
		}
		else
		{
			// Query
			$get_apf_dashboard_pending_list_squery_where = $get_apf_dashboard_pending_list_squery_where." and (apf_details_id not in (select apf_process_apf_id from apf_process where apf_process_end_date = 0000-00-00 and apf_process_active = 1)) and (apf_details_id in (select apf_process_apf_id from apf_process where apf_process_active = 1))";				
		}
		
		$filter_count++;
	}
	
	if($is_pending != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_apf_dashboard_pending_list_squery_where = $get_apf_dashboard_pending_list_squery_where." where apf_details_id in (select apf_process_apf_id from apf_process where apf_process_end_date = :process_end_date and apf_process_active = 1)";								
		}
		else
		{
			// Query
			$get_apf_dashboard_pending_list_squery_where = $get_apf_dashboard_pending_list_squery_where." and apf_details_id in (select apf_process_apf_id from apf_process where apf_process_end_date = :process_end_date and apf_process_active = 1)";				
		}
		
		// Data
		$get_apf_dashboard_pending_list_sdata[':process_end_date']  = '0000-00-00';
		
		$filter_count++;
	}
	
	$get_apf_dashboard_pending_list_squery = $get_apf_dashboard_pending_list_squery_base.$get_apf_dashboard_pending_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_dashboard_pending_list_sstatement = $dbConnection->prepare($get_apf_dashboard_pending_list_squery);
		
		$get_apf_dashboard_pending_list_sstatement -> execute($get_apf_dashboard_pending_list_sdata);
		
		$get_apf_dashboard_pending_list_sdetails = $get_apf_dashboard_pending_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_dashboard_pending_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_dashboard_pending_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_dashboard_pending_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
 /*
PURPOSE : To add new APF Response
INPUT 	: Query ID, response, Follow up Date, Status, Remarks, Added By
OUTPUT 	: Response ID, success or failure message
BY 		: Lakshmi
*/
function db_add_apf_response($query_id,$response,$follow_up_date,$status,$remarks,$added_by)
{
	// response
   $apf_response_iresponse = "insert into apf_response
   (apf_response_query_id,apf_response,apf_response_follow_up_date,apf_response_status,apf_response_active,apf_response_remarks,apf_response_added_by,apf_response_added_on) values(:query_id,:response,:follow_up_date,:status,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $apf_response_istatement = $dbConnection->prepare($apf_response_iresponse);
        
        // Data
        $apf_response_idata = array(':query_id'=>$query_id,':response'=>$response,':follow_up_date'=>$follow_up_date,':status'=>$status,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $apf_response_istatement->execute($apf_response_idata);
		$apf_response_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_response_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get APF Response list
INPUT 	: Response ID, Query ID, response, Follow Up Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of APF response
BY 		: Lakshmi
*/
function db_get_apf_response($apf_response_search_data)
{  
	if(array_key_exists("response_id",$apf_response_search_data))
	{
		$response_id = $apf_response_search_data["response_id"];
	}
	else
	{
		$response_id = "";
	}
	
	if(array_key_exists("query_id",$apf_response_search_data))
	{
		$query_id = $apf_response_search_data["query_id"];
	}
	else
	{
		$query_id = "";
	}
	
	if(array_key_exists("response",$apf_response_search_data))
	{
		$response = $apf_response_search_data["response"];
	}
	else
	{
		$response = "";
	}
	
	if(array_key_exists("follow_up_date",$apf_response_search_data))
	{
		$follow_up_date = $apf_response_search_data["follow_up_date"];
	}
	else
	{
		$follow_up_date = "";
	}
	
	if(array_key_exists("status",$apf_response_search_data))
	{
		$status = $apf_response_search_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$apf_response_search_data))
	{
		$active = $apf_response_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$apf_response_search_data))
	{
		$added_by = $apf_response_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$apf_response_search_data))
	{
		$start_date = $apf_response_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$apf_response_search_data))
	{
		$end_date = $apf_response_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	if(array_key_exists("project_name",$apf_response_search_data))
	{
		$project_name = $apf_response_search_data["project_name"];
	}
	else
	{
		$project_name = "";
	}
	
	if(array_key_exists("process_name",$apf_response_search_data))
	{
		$process_name = $apf_response_search_data["process_name"];
	}
	else
	{
		$process_name = "";
	}
	
	if(array_key_exists("bank_name",$apf_response_search_data))
	{
		$bank_name = $apf_response_search_data["bank_name"];
	}
	else
	{
		$bank_name = "";
	}
	
	if(array_key_exists("village_name",$apf_response_search_data))
	{
		$village_name = $apf_response_search_data["village_name"];
	}
	else
	{
		$village_name = "";
	}

	$get_apf_response_list_sresponse_base = "select *,U.user_name as added_by from apf_response AQR inner join users U on U.user_id = AQR.apf_response_added_by";
	
	$get_apf_response_list_sresponse_where = "";
	$filter_count = 0;
	
	// Data
	$get_apf_response_list_sdata = array();
	
	if($response_id != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where apf_response_id = :response_id";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and apf_response_id = :response_id";				
		}
		
		// Data
		$get_apf_response_list_sdata[':response_id'] = $response_id;
		
		$filter_count++;
	}
	
	if($query_id != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where apf_response_query_id = :query_id";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and apf_response_query_id = :query_id";				
		}
		
		// Data
		$get_apf_response_list_sdata[':query_id'] = $query_id;
		
		$filter_count++;
	}
	
	if($response != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where apf_response = :response";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and apf_response = :response";				
		}
		
		// Data
		$get_apf_response_list_sdata[':response'] = $response;
		
		$filter_count++;
	}
	
	if($follow_up_date != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where apf_response_follow_up_date = :follow_up_date";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and apf_response_follow_up_date = :follow_up_date";				
		}
		
		// Data
		$get_apf_response_list_sdata[':follow_up_date'] = $follow_up_date;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where apf_response_status = :status";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and apf_response_status = :status";				
		}
		
		// Data
		$get_apf_response_list_sdata[':status'] = $status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where apf_response_active = :active";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and apf_response_active = :active";				
		}
		
		// Data
		$get_apf_response_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where apf_response_added_by = :added_by";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and apf_response_added_by = :added_by";
		}
		
		//Data
		$get_apf_response_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where apf_response_added_on >= :start_date";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and apf_response_added_on >= :start_date";				
		}
		
		//Data
		$get_apf_response_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where apf_response_added_on <= :end_date";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and apf_response_added_on <= :end_date";
		}
		
		//Data
		$get_apf_response_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where APM.apf_project_master_id = :project_name";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and APM.apf_project_master_id = :project_name";				
		}
		
		// Data
		$get_apf_response_list_sdata[':project_name'] = $project_name;
		
		$filter_count++;
	}
	
	if($process_name != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where APMM.apf_process_master_id = :process_name";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and APMM.apf_process_master_id = :process_name";				
		}
		
		// Data
		$get_apf_response_list_sdata[':process_name'] = $process_name;
		
		$filter_count++;
	}
	
	if($bank_name != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where ABM.apf_bank_master_id = :bank_name";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and ABM.apf_bank_master_id = :bank_name";				
		}
		
		// Data
		$get_apf_response_list_sdata[':bank_name'] = $bank_name;
		
		$filter_count++;
	}
	
	if($village_name != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." where VM.village_id = :village_name";								
		}
		else
		{
			// response
			$get_apf_response_list_sresponse_where = $get_apf_response_list_sresponse_where." and VM.village_id = :village_name";				
		}
		
		// Data
		$get_apf_response_list_sdata[':village_name'] = $village_name;
		
		$filter_count++;
	}
	
	$get_apf_response_list_sresponse_order = " order by apf_response_added_on desc";
	
	$get_apf_response_list_sresponse = $get_apf_response_list_sresponse_base.$get_apf_response_list_sresponse_where.$get_apf_response_list_sresponse_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_apf_response_list_sstatement = $dbConnection->prepare($get_apf_response_list_sresponse);
		
		$get_apf_response_list_sstatement -> execute($get_apf_response_list_sdata);
		
		$get_apf_response_list_sdetails = $get_apf_response_list_sstatement -> fetchAll();
		
		if(FALSE === $get_apf_response_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_apf_response_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_apf_response_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update APF Response 
INPUT 	: Response ID, APF Response Update Array
OUTPUT 	: Response ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_apf_response($response_id,$apf_response_update_data)
{
	
	if(array_key_exists("query_id",$apf_response_update_data))
	{	
		$query_id = $apf_response_update_data["query_id"];
	}
	else
	{
		$query_id = "";
	}
	
	if(array_key_exists("response",$apf_response_update_data))
	{	
		$response = $apf_response_update_data["response"];
	}
	else
	{
		$response = "";
	}
	
	if(array_key_exists("follow_up_date",$apf_response_update_data))
	{	
		$follow_up_date = $apf_response_update_data["follow_up_date"];
	}
	else
	{
		$follow_up_date = "";
	}
	
	if(array_key_exists("status",$apf_response_update_data))
	{	
		$status = $apf_response_update_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$apf_response_update_data))
	{	
		$active = $apf_response_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$apf_response_update_data))
	{	
		$remarks = $apf_response_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$apf_response_update_data))
	{	
		$added_by = $apf_response_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$apf_response_update_data))
	{	
		$added_on = $apf_response_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// response
    $apf_response_update_uresponse_base = "update apf_response set";  
	
	$apf_response_update_uresponse_set = "";
	
	$apf_response_update_uresponse_where = " where apf_response_id = :response_id";
	
	$apf_response_update_udata = array(":response_id"=>$response_id);
	
	$filter_count = 0;
	
	if($query_id != "")
	{
		$apf_response_update_uresponse_set = $apf_response_update_uresponse_set." apf_response_query_id = :query_id,";
		$apf_response_update_udata[":query_id"] = $query_id;
		$filter_count++;
	}
	
	if($response != "")
	{
		$apf_response_update_uresponse_set = $apf_response_update_uresponse_set." apf_response = :response,";
		$apf_response_update_udata[":response"] = $response;
		$filter_count++;
	}
	
	if($follow_up_date != "")
	{
		$apf_response_update_uresponse_set = $apf_response_update_uresponse_set." apf_response_follow_up_date = :follow_up_date,";
		$apf_response_update_udata[":follow_up_date"] = $follow_up_date;
		$filter_count++;
	}
	
	if($status != "")
	{
		$apf_response_update_uresponse_set = $apf_response_update_uresponse_set." apf_response_status = :status,";
		$apf_response_update_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($active != "")
	{
		$apf_response_update_uresponse_set = $apf_response_update_uresponse_set." apf_response_active = :active,";
		$apf_response_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$apf_response_update_uresponse_set = $apf_response_update_uresponse_set." apf_response_remarks = :remarks,";
		$apf_response_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$apf_response_update_uresponse_set = $apf_response_update_uresponse_set." apf_response_added_by = :added_by,";
		$apf_response_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$apf_response_update_uresponse_set = $apf_response_update_uresponse_set." apf_response_added_on = :added_on,";
		$apf_response_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$apf_response_update_uresponse_set = trim($apf_response_update_uresponse_set,',');
	}
	
	$apf_response_update_uresponse = $apf_response_update_uresponse_base.$apf_response_update_uresponse_set.$apf_response_update_uresponse_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_response_update_ustatement = $dbConnection->prepare($apf_response_update_uresponse);		
        
        $apf_response_update_ustatement -> execute($apf_response_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $response_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
} 

/*
PURPOSE : To Delete APF Process 
INPUT 	: APF Process ID, APF Process Update Array
OUTPUT 	: APF Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_delete_all_process($apf_process_update_data)
{
	if(array_key_exists("apf_id",$apf_process_update_data))
	{	
		$apf_id = $apf_process_update_data["apf_id"];
	}
	else
	{
		$apf_id = "";
	}

	// Query
    $apf_process_update_uquery_base = "update apf_process";  
	
	$apf_process_update_uquery_set = " set apf_process_active = 0";
	
	$apf_process_update_uquery_where = " where apf_process_apf_id = :apf_id";
	
	$apf_process_update_udata = array(":apf_id"=>$apf_id);
	
	$filter_count = 0;
	
	if($filter_count > 0)
	{
		$apf_process_update_uquery_set = trim($apf_process_update_uquery_set,',');
	}
	
	$apf_process_update_uquery = $apf_process_update_uquery_base.$apf_process_update_uquery_set.$apf_process_update_uquery_where;
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_process_update_ustatement = $dbConnection->prepare($apf_process_update_uquery);		
        
        $apf_process_update_ustatement -> execute($apf_process_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To Delete APF File 
INPUT 	: File ID, APF File Update Array
OUTPUT 	: File ID; Message of success or failure
BY 		: Lakshmi
*/
function db_delete_all_apf_file($apf_file_update_data)
{
	if(array_key_exists("apf_id",$apf_file_update_data))
	{	
		$apf_id = $apf_file_update_data["apf_id"];
	}
	else
	{
		$apf_id = "";
	}

	// Query
    $apf_file_update_uquery_base = "update apf_file";   
	
	$apf_file_update_uquery_set = " set apf_file_active = 0";
	
	$apf_file_update_uquery_where = " where apf_file_apf_id = :apf_id";
	
	$apf_file_update_udata = array(":apf_id"=>$apf_id);
	
	$filter_count = 0;
	
	if($filter_count > 0)
	{
		$apf_file_update_uquery_set = trim($apf_file_update_uquery_set,',');
	}
	
	$apf_file_update_uquery = $apf_file_update_uquery_base.$apf_file_update_uquery_set.$apf_file_update_uquery_where;
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $apf_file_update_ustatement = $dbConnection->prepare($apf_file_update_uquery);		
        
        $apf_file_update_ustatement -> execute($apf_file_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $apf_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>