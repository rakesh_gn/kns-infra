<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: legal_process_delete.php
CREATED ON	: 09-March-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Delete a process plan
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["file"]))
	{
		$file = $_GET["file"];
	}
	else
	{
		$file = "";
	}

	// Check whether this user is authorized to do this change
	$file_list = i_get_file_list('','','','','','','','','','',$file);
	if($file_list["status"] == SUCCESS)
	{
		if($role == "1")
		{
			// Delete the task
			$file_delete_result = i_delete_file($file);
			
			if($file_delete_result["status"] == SUCCESS)
			{
				$msg = "File was successfully deleted";
			}
			else
			{
				$msg = $file_delete_result["data"];
			}
		}
		else
		{
			$msg = "You are not authorized to delete this file!";
		}
	}
	else
	{
		$msg = "This seems to be an invalid file. Please check!";
	}

	header("location:pending_file_list.php?msg=$msg");
}
else
{
	header("location:login.php");
}	
?>