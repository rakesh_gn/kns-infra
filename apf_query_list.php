<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 05-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('APF_QUERY_RESPONSE_LIST_FUNC_ID','322');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',APF_QUERY_RESPONSE_LIST_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',APF_QUERY_RESPONSE_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',APF_QUERY_RESPONSE_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',APF_QUERY_RESPONSE_LIST_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}
	
	if(isset($_GET['query_id']))
	{
		$query_id = $_GET['query_id'];
	}
	else
	{
		$query_id = "";
	}
	
	if(isset($_REQUEST['source']))
	{
		$source = $_REQUEST['source'];
	}
	else
	{
		$source = "";
	}
	
	$search_project   = "";
	$search_process   = "";
	$search_bank      = "";
	$search_village = "";
	
	if(isset($_POST['file_search_submit']))
	{
		$search_project    = $_POST["search_project"];
		$search_process    = $_POST["search_process"];
		$search_village    = $_POST["search_village"];
		$search_bank       = $_POST["search_bank"];
		$process_id        = $_POST["hd_process_id"];
	
	}

	// Get APF Process modes already added
	$apf_process_search_data = array("active"=>'1');
	$apf_process_list = i_get_apf_process($apf_process_search_data);
	if($apf_process_list['status'] == SUCCESS)
	{
		$apf_process_list_data = $apf_process_list['data'];
	}	
	else
	{
		$alert = $apf_process_list["data"];
		$alert_type = 0;
	}
	
	// Get APF Process Master modes already added
	$apf_process_master_search_data = array("active"=>'1');
	$apf_process_master_list = i_get_apf_process_master($apf_process_master_search_data);
	if($apf_process_master_list['status'] == SUCCESS)
	{
		$apf_process_master_list_data = $apf_process_master_list['data'];
	}	

	else
	{
		$alert = $apf_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Village Master modes already added
	$village_master_list = i_get_village_list('');
	if($village_master_list['status'] == SUCCESS)
	{
		$village_master_list_data = $village_master_list["data"];
	}
	else
	{
		$alert = $village_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Master already added
	$apf_project_master_search_data = array("active"=>'1');
	$apf_project_master_list = i_get_apf_project_master($apf_project_master_search_data);
	if($apf_project_master_list['status'] == SUCCESS)
	{
		$apf_project_master_list_data = $apf_project_master_list['data'];
	}
	else
	{
		$alert = $apf_project_master_list["data"];
		$alert_type = 0;
	}
	
	
	// Get Bank Master modes already added
	$apf_bank_master_search_data = array("active"=>'1');
	$bank_master_list = i_get_apf_bank_master($apf_bank_master_search_data);
	if($bank_master_list['status'] == SUCCESS)
	{
		$bank_master_list_data = $bank_master_list["data"];
	}
	else
	{
		$alert = $bank_master_list["data"];
		$alert_type = 0;
	}
	
	
	// Get APF Query Response modes already added
	$apf_query_search_data = array("active"=>'1',"query_id"=>$query_id,"process_id"=>$process_id,"project_name"=>$search_project,"bank_name"=>$search_bank,"process_name"=>$search_process,"village_name"=>$search_village);
	$apf_query_response_list = i_get_apf_query($apf_query_search_data);
	if($apf_query_response_list['status'] == SUCCESS)
	{
		$apf_query_response_list_data = $apf_query_response_list['data'];
	}	
}
else
{
	header("location:login.php");
}	

?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>APF - Query List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>APF - Query List</h3><?php if($add_perms_list['status'] == SUCCESS){?><span style="float:right; padding-right:20px;"><?php if($source == "view") { ?><a href="apf_add_query.php?process_id=<?php echo $process_id ;?>">APF - Add Query</a><?php } ?></span><?php } ?>
	  				</div> <!-- /widget-header -->
						<div class="widget-header" style="height:50px; padding-top:10px;">		
			<form method="post" id="file_search_form" action="apf_query_list.php">
			<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
			 <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($apf_count = 0; $apf_count < count($apf_project_master_list_data); $apf_count++)
			  {
			  ?>
			  <option value="<?php echo $apf_project_master_list_data[$apf_count]["apf_project_master_id"]; ?>" <?php if($search_project == $apf_project_master_list_data[$apf_count]["apf_project_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $apf_project_master_list_data[$apf_count]["apf_project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($apf_count = 0; $apf_count < count($apf_process_master_list_data); $apf_count++)
			  {
			  ?>
			  <option value="<?php echo $apf_process_master_list_data[$apf_count]["apf_process_master_id"]; ?>" <?php if($search_process == $apf_process_master_list_data[$apf_count]["apf_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $apf_process_master_list_data[$apf_count]["apf_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_bank">
			  <option value="">- - Select Bank - -</option>
			  <?php
			  for($bank_count = 0; $bank_count < count($bank_master_list_data); $bank_count++)
			  {
			  ?>
			  <option value="<?php echo $bank_master_list_data[$bank_count]["apf_bank_master_id"]; ?>" <?php if($search_bank == $bank_master_list_data[$bank_count]["apf_bank_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $bank_master_list_data[$bank_count]["apf_bank_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>
			  </div>
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								</div>
								
							</div> 
							<?php
							if($view_perms_list['status'] == SUCCESS)
							{
							?>
							
							<table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Project</th>
					<th>Bank</th>
					<th>Query</th>
					<th>Query Date</th>
					<th>Leadtime</th>
					<th>Latest Response</th>
					<th>Follow Up Date</th>				
					<th>Added On</th>									
					<th colspan="3" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($apf_query_response_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($apf_query_response_list_data); $count++)
					{
						$sl_no++;
			
						//Get Leadtime
						$apf_response_search_data = array("query_id"=>$apf_query_response_list_data[$count]["apf_query_id"],"status"=>'Completed');
						$apf_response_list = i_get_apf_response($apf_response_search_data);
						if($apf_response_list["status"] == SUCCESS)
						{
							$apf_response_list_data = $apf_response_list["data"];
							$response_date = $apf_response_list_data[0]['apf_response_added_on'];
						}
						else
						{
							$response_date = date('Y-m-d');
						}
						 
						$query_date = $apf_query_response_list_data[$count]["apf_query_date"];
						$leadtime = get_date_diff($query_date,$response_date);
						
						// Get latest response
						$apf_latest_response_search_data = array("query_id"=>$apf_query_response_list_data[$count]["apf_query_id"],"active"=>'1');
						$apf_latest_response_list = i_get_apf_response($apf_latest_response_search_data);
						if($apf_latest_response_list["status"] == SUCCESS)
						{
							$apf_latest_response_list_data = $apf_latest_response_list["data"];
							$response_text     = $apf_latest_response_list_data[0]['apf_response'];
							if($apf_latest_response_list_data[0]['apf_response_status'] == 'Completed')
							{
								$response_fup_date = 'COMPLETED';
							}
							else
							{
								$response_fup_date = date('d-M-Y',strtotime($apf_latest_response_list_data[0]['apf_response_follow_up_date']));								
							}
						}
						else
						{
							$response_text     = 'NO RESPONSE';
							$response_fup_date = 'NO RESPONSE';
						}
						?>
						<tr>
						<td rowspan="2"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $apf_query_response_list_data[$count]["apf_project_master_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $apf_query_response_list_data[$count]["apf_bank_master_name"]; ?></td>
						<!--<td style="word-wrap:break-word;"><?php echo $apf_query_response_list_data[$count]["village_name"]; ?></td>-->
						<td style="word-wrap:break-word;"><?php echo $apf_query_response_list_data[$count]["apf_query"]; ?></td>
						<!--<td style="word-wrap:break-word;"><?php echo $apf_query_response_list_data[$count]["apf_query_by"]; ?></td>-->						
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($query_date,"d-M-Y"); ?></td>
						<td><?php echo $leadtime["data"] ;?></td>
						<td style="word-wrap:break-word;"><?php echo $response_text; ?></td>
						<td style="word-wrap:break-word;"><?php echo $response_fup_date; ?></td>
						<!--<td style="word-wrap:break-word;"><?php echo $apf_query_response_list_data[$count]["apf_query_remarks"]; ?></td>-->						
						<!--<td style="word-wrap:break-word;"><?php echo $apf_query_response_list_data[$count]["added_by"]; ?></td>-->
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($response_date,"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_edit_query_response('<?php echo $apf_query_response_list_data[$count]["apf_query_id"]; ?>','<?php echo $apf_query_response_list_data[$count]["apf_query_process_id"]; ?>');">Edit</a><?php } ?></td>
						<td style="word-wrap:break-word;"><?php if(($apf_query_response_list_data[$count]["apf_query_active"] == "1") || ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return delete_query_response('<?php echo $apf_query_response_list_data[$count]["apf_query_id"]; ?>','<?php echo $apf_query_response_list_data[$count]["apf_query_process_id"]; ?>');">Delete</a><?php } ?></td>
						<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_apf_response('<?php echo $apf_query_response_list_data[$count]["apf_query_id"]; ?>');">Responses</a><?php } ?></td>
						</tr>
						<tr>						
						<td colspan="11"><?php echo $apf_query_response_list_data[$count]["apf_process_master_name"]; ?></td>
						</tr>
						<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No APF Query condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			    <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function delete_query_response(query_id,process_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "apf_query_list.php?process_id=" +process_id;
					}
				}
			}

			xmlhttp.open("POST", "ajax/apf_delete_query.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("query_id=" + query_id + "&action=0");
		}
	}	
}
function go_to_edit_query_response(query_id,process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "apf_edit_query.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","query_id");
	hiddenField1.setAttribute("value",query_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","process_id");
	hiddenField2.setAttribute("value",process_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_apf_response(query_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "apf_add_response.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","query_id");
	hiddenField1.setAttribute("value",query_id);

	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>


  </body>

</html>
