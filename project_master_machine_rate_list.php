<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID', '250');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID, '2', '1');
    $edit_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID, '3', '1');
    $delete_perms_list  = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID, '6', '1');
    $add_perms_list    = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID,'1','1');

?>
<script>
  window.permissions = {
    view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    ok: <?php echo ($ok_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    aprove: <?php echo ($approve_perms_list['status'] == 0)? 'true' : 'false'; ?>,
  }
</script>
<?php
    // Query String Data
    // Get vendor list
  	$vendor_search_data = array('active'=>'1');
  	$vendor_sresult = i_get_project_machine_vendor_master_list($vendor_search_data);
  	if($vendor_sresult['status'] == SUCCESS)
  	{
  		$vendor_list = $vendor_sresult['data'];
  	}
  	else
  	{
  		$vendor_list = array();
  	}

    // Get machine type list
    $machine_type_search_data = array('active'=>'1');
  	$machine_type_sresult = i_get_project_machine_type_master($machine_type_search_data);
  	if($machine_type_sresult['status'] == SUCCESS)
  	{
  		$machine_type_list = $machine_type_sresult['data'];
  	}
    else{
      $machine_type_list = array();
    }
} else {
    header("location:login.php");
}
?>


<html>
  <head>
    <meta charset="utf-8">
    <title> Project Master Machine Power Rate List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="./css/style.css?<?php echo time(); ?>" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
    <style media="screen">
      table.dataTable {
        margin-top: 0px !important;
      }
    </style>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-1.7.2.min.js"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
		 <script src="datatable/project_master_machine_rate_list_datatable.js?<?php echo time(); ?>"></script>
  </head>
  <body>

    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>

    <div class="main margin-top">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6">

                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3> Project Master Machine Power Rate List</h3>
                    <?php if($add_perms_list['status'] == SUCCESS){ ?>
                    <a style="margin-right:10px;float:right;color:#19bc9c;" target="_blank" href="project_master_add_machine_rate.php"><span>Project Master Add Machine Power Rate</span></a>
                  <?php } ?>
                  </div>

                  <div class="widget-header widget-toolbar">
                    <?php
                          if ($view_perms_list['status'] == SUCCESS) {
                              ?>
                      <form class="form-inline" method="post" id="file_search_form">
                        <select id="search_vendor" class="form-control input-sm" style="max-width: 150px;">
                        <option value="">- - Select Vendor - -</option>
                        <?php
                              for ($vendor_count = 0; $vendor_count < count($vendor_list); $vendor_count++) {
                                  ?>
                        <option value="<?php echo $vendor_list[$vendor_count]["project_machine_vendor_master_id"]; ?>">
                          <?php echo $vendor_list[$vendor_count]["project_machine_vendor_master_name"]; ?></option>
                        <?php
                              } ?>
                        </select>

                        <select id="search_machine" class="form-control input-sm" style="max-width: 150px;">
                        <option value="">- - Select Machine - -</option>
                        <?php
                              for ($machine_count = 0; $machine_count < count($machine_type_list); $machine_count++) {
                                  ?>
                        <option value="<?php echo $machine_type_list[$machine_count]["project_machine_type_master_id"]; ?>">
                          <?php echo $machine_type_list[$machine_count]["project_machine_type_master_name"]; ?></option>
                        <?php
                              } ?>
                        </select>

                      <input type="date" id="start_date" name="dt_start_date" class="form-control input-sm"/>
                      <input type="date" id="end_date" name="dt_end_date" class="form-control input-sm"/>
                      <button type="button" onclick="tableDraw()" class="btn btn-primary">Submit</button>
                      </form>
                      <?php } ?>
                    </div>
                  </div>
                <div class="widget-content" style="margin-top:15px;">
                <table id="example" class="table table-striped table-bordered display nowrap">
                  <thead>
                <tr>
                <th>SL No</th>
      					<th>Machine</th>
      					<th>Machine Type</th>
      					<th>Ownership Type</th>
      					<th>Vendor</th>
      					<th>Kns Fuel(Rate/hr)</th>
      					<th>Vendor fuel(Rate/hr)</th>
      					<th>Bata(Rate/day)</th>
      					<th>Remarks</th>
      					<th>Added By</th>
      					<th>Added On</th>
      					<th>Updated By</th>
      					<th>Updated On</th>
      					<th>Delete</th>
      					<th>Edit</th>
                </tr>
                </thead>
                  </tbody>
                </table>
              </div>
            </div>
              <!-- /widget-content -->
            </div>
            <!-- /widget -->

            </div>
            <!-- /widget -->
          </div>
          <!-- /span6 -->
        </div>
        <!-- /row -->
      <!-- </div> -->
      <!-- /container -->
    <!-- </div> -->

</body>

  <div class="extra">

  	<div class="extra-inner">

  		<div class="container">

  			<div class="row">

                  </div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /extra-inner -->

  </div> <!-- /extra -->




  <div class="footer">

  	<div class="footer-inner">

  		<div class="container">

  			<div class="row">

      			<div class="span12">
      				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
      			</div> <!-- /span12 -->

      		</div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /footer-inner -->

  </div> <!-- /footer -->
</html>
