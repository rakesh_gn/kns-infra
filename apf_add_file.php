<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD: 
1. User drop down from user table
2. Session management
3. Edit form-actions css to have no background
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert = "";
	$alert_type = -1; // No alert

	// Query String
	
	if(isset($_GET['apf_id']))
	{
		$apf_id = $_GET['apf_id'];
	}
	else
	{
		$apf_id = "";
	}
	
    if(isset($_GET['project']))
	{
		$project = $_GET['project'];
	}
	else
	{
		$project = "";
	}
	
	if(isset($_GET['bank']))
	{
		$bank = $_GET['bank'];
	}
	else
	{
		$bank = "";
	}

    if(isset($_GET['village']))
	{
		$village = $_GET['village'];
	}
	else
	{
		$village = "";
	}

   if(isset($_GET['page']))
	{
		$page = $_GET['page'];
	}
	else
	{
		$page = "";
	}		
    
	// Get APF Details already added
	$apf_details_search_data = array("apf_id"=>$apf_id,"project"=>$project,"village"=>$village);
	$apf_details_list = i_get_apf_details($apf_details_search_data);
	if($apf_details_list['status'] == SUCCESS)
	{
		$project_name  = $apf_details_list["data"][0]["apf_project_master_name"];
		$village_name  = $apf_details_list["data"][0]["village_name"];
		$bank_name  = $apf_details_list["data"][0]["apf_bank_master_name"];
	}		
	else
	{
			$project_name  = "";
			$village_name  = "";
			$bank_name  = "";
	}
	
	
   // Get Survey already added
	$apf_survey_master_search_data = array("active"=>'1',"project"=>$project,"bank"=>$bank,"is_mapped"=>'1');
	$apf_survey_master_list = i_get_apf_survey_master($apf_survey_master_search_data);
	if($apf_survey_master_list['status'] == SUCCESS)
	{
		$apf_survey_master_list_data = $apf_survey_master_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$apf_survey_master_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get Project Master already added
	$apf_project_master_search_data = array("active"=>'1');
	$apf_project_master_list = i_get_apf_project_master($apf_project_master_search_data);
	if($apf_project_master_list['status'] == SUCCESS)
	{
		$apf_project_master_list_data = $apf_project_master_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$apf_project_master_list["data"];
		$alert_type = 0; // Failure
	}
	if(isset($_POST["apf_file_submit"]))
	{
		// Capture all form data
		$apf_id		      = $_POST["hd_apf_id"];
		$files_array      = $_POST["cb_files"];
		$page	          = $_POST["hd_page"];
		
		for($files_count= 0 ; $files_count < count($files_array) ; $files_count++)
		{
			$files_type = $files_array[$files_count];
			
			if($apf_id != "")
			{					
				$apf_details_result = i_add_apf_file($files_type,$apf_id,'',$user);
				
				if($apf_details_result["status"] != SUCCESS)
				{
					
					$alert = $apf_details_result["data"];
					$alert_type = 0; // Failure
				}	
				else
				{
					if($page == "file_list")
					{
						header("location:apf_file_list.php?apf_id=$apf_id");
					}
					else
					{
					header("location:apf_add_process.php?apf_id=$apf_id");
					}
					
					$alert_type = 1;
					$alert      = 'test';
				}
			}
		}
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>APF File</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Project:&nbsp;&nbsp;&nbsp;<?php echo $project_name; ?>&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;Village:&nbsp;&nbsp;&nbsp;<?php echo $village_name; ?>&nbsp;&nbsp;&nbsp;Bank:&nbsp;&nbsp;&nbsp;<?php echo $bank_name; ?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">APF File</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="apf_add_file" class="form-horizontal" method="post" action="apf_add_file.php">
								<input type="hidden" name="hd_apf_id" value="<?php echo $apf_id; ?>" />
								<input type="hidden" name="hd_bd_file_id" value="<?php echo $bd_file_id; ?>" />
								<input type="hidden" name="hd_apf_details_id" value="<?php echo $apf_details_id; ?>" />
								<input type="hidden" name="hd_page" value="<?php echo $page; ?>" />
								<fieldset>

										<div class="control-group">											
											<label class="control-label" for="cb_files">Files</label>
											<div class="controls">
												<table class="table table-bordered" style="table-layout: fixed;">
												<tr>
												<td>SL No</td>
												<td>Project</td>
												<td>Survey No</td>
												<td>Land Owner</td>
												<td>Extent</td>
												<td>&nbsp;</td>
												</tr>
												<?php 
												if($apf_survey_master_list["status"] == SUCCESS)
												{
												$sl_no = 0;
												for($count = 0; $count < count($apf_survey_master_list_data); $count++)
												{
													//$file_handover_data =  i_get_file_handover_details($bd_file_id);
							
													//if($file_handover_data["status"] != SUCCESS)
													
														$sl_no = $sl_no + 1;
														?>			
														<tr>
														<td><?php echo $sl_no; ?></td>
														<td><?php echo $apf_survey_master_list_data[$count]['apf_project_master_name']; ?></td>
														<td><?php echo $apf_survey_master_list_data[$count]['apf_survey_master_survey_no']; ?></td>
														<td><?php echo $apf_survey_master_list_data[$count]['apf_survey_master_land_owner']; ?></td>
														<td><?php echo $apf_survey_master_list_data[$count]["apf_survey_master_extent"]; ?></td>
														<td><input type="checkbox" name="cb_files[]" value="<?php echo $apf_survey_master_list_data[$count]['apf_survey_master_id']; ?>" /></td>
														</tr>														
														<?php
													
												}
												}
												else
												{?>
													<td colspan="6">No Survey Master condition added yet!</td>
												<?php
												}
												?>	
												</table>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="apf_file_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->+

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
  </body>

</html>
