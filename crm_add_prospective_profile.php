<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query String
	if(isset($_GET["enquiry"]))
	{
		$enquiry_id = $_GET["enquiry"];
	}
	else
	{
		$enquiry_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["add_prospective_submit"]))
	{
		$enquiry_id        = $_POST["hd_enquiry"];
		$occupation        = $_POST["rd_occupation"];
		$num_dependents    = $_POST["stxt_num_dependents"];
		$annual_income     = $_POST["stxt_annual_income"];
		$other_income      = $_POST["stxt_other_income"];
				
		$loan_type_string  = "";
		if(isset($_POST["cb_array_loan_type"]))
		{
			$loan_types        = $_POST["cb_array_loan_type"];
			for($count = 0; $count < count($loan_types); $count++)
			{
				$loan_type_string = $loan_type_string.$loan_types[$count].",";
			}
			$loan_type_string  = trim($loan_type_string,',');
		}
		
		$emi               = $_POST["stxt_emi"];
		
		if(isset($_POST["cb_loan_rejected"]))
		{
			$loan_reject  = 1;
		}
		else		
		{
			$loan_reject  = 0;
		}
		
		$current_living    = $_POST["rd_current_living"];
		$rent              = $_POST["stxt_rent"];
		
		$buying_interest_string = "";
		if(isset($_POST["cb_buy_interest"]))
		{
			$buying_interest   = $_POST["cb_buy_interest"];		
			for($count = 0; $count < count($buying_interest); $count++)
			{
				$buying_interest_string = $buying_interest_string.$buying_interest[$count].",";
			}
			$buying_interest_string = trim($buying_interest_string,',');
		}		
		
		$funding_source    = $_POST["rd_fund_source"];
		$loan_eligibility  = $_POST["stxt_loan_eligibility"];
		$loan_tenure       = $_POST["stxt_loan_tenure"];
		$buying_duration   = $_POST["rd_buy_duration"];
		$tentative_closure = $_POST["dt_closure_date"];
		$remarks           = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($occupation !="") && ($annual_income !="") && ($funding_source !=""))
		{
			$add_prospective_result = i_add_prospective_profile($enquiry_id,$occupation,$other_income,$num_dependents,$annual_income,$loan_type_string,$emi,$loan_reject,$current_living,$rent,$buying_interest_string,$funding_source,$loan_eligibility,$loan_tenure,$buying_duration,$tentative_closure,$remarks,$user);
			
			$alert = $add_prospective_result["data"];
			$alert_type = 1;
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Prospective Client Profile</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Prospective Client Profile</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_enquiry" class="form-horizontal" method="post" action="crm_add_prospective_profile.php">
								<input type="hidden" name="hd_enquiry" value="<?php echo $enquiry_id; ?>" />
									<fieldset>										
												
										<div class="control-group">											
											<label class="control-label" for="rd_occupation">Occupation*</label>
											<div class="controls">
												<input type="radio" name="rd_occupation" value="1" checked />&nbsp;&nbsp;&nbsp;Self-Employed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_occupation" value="2" />&nbsp;&nbsp;&nbsp;Salaried
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_num_dependents">No. of Dependants</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_num_dependents" placeholder="No. of dependants">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
                                                                                                                                                           										    <div class="control-group">											
											<label class="control-label" for="stxt_annual_income">Annual Income*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_annual_income" placeholder="Annual Income" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="stxt_other_income">Other sources of Income</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_other_income" placeholder="Ex: Spouse Salary, Rent etc.">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="cb_array_loan_type">Existing Loan</label>
											<div class="controls">
												<input type="checkbox" name="cb_array_loan_type[]" value="1" />&nbsp;&nbsp;&nbsp;Home Loan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="checkbox" name="cb_array_loan_type[]" value="2" />&nbsp;&nbsp;&nbsp;Car Loan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="checkbox" name="cb_array_loan_type[]" value="3" />&nbsp;&nbsp;&nbsp;Personal Loan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="checkbox" name="cb_array_loan_type[]" value="4" />&nbsp;&nbsp;&nbsp;Other Loan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_emi">EMI</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_emi" placeholder="Total EMI paid by the customer">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="cb_loan_reject">Loan Rejected</label>
											<div class="controls">
												<input type="checkbox" name="cb_loan_reject" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="rd_current_living">Currently Staying</label>
											<div class="controls">
												<input type="radio" name="rd_current_living" value="1" checked />&nbsp;&nbsp;&nbsp;Rent
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_current_living" value="2" />&nbsp;&nbsp;&nbsp;Own House
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_current_living" value="3" />&nbsp;&nbsp;&nbsp;On Lease
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_rent">Rent*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_rent" placeholder="Rent Paid">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="cb_buy_interest">Buying Interest</label>
											<div class="controls">
												<input type="checkbox" name="cb_buy_interest[]" value="1" />&nbsp;&nbsp;&nbsp;20x30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="checkbox" name="cb_buy_interest[]" value="2" />&nbsp;&nbsp;&nbsp;30x40&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="checkbox" name="cb_buy_interest[]" value="5" />&nbsp;&nbsp;&nbsp;30x50&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="checkbox" name="cb_buy_interest[]" value="3" />&nbsp;&nbsp;&nbsp;40x60&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="checkbox" name="cb_buy_interest[]" value="4" />&nbsp;&nbsp;&nbsp;Other&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
										
										<div class="control-group">											
											<label class="control-label" for="rd_fund_source">Funding Source*</label>
											<div class="controls">
												<input type="radio" name="rd_fund_source" value="1" checked />&nbsp;&nbsp;&nbsp;Loan
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_fund_source" value="2" />&nbsp;&nbsp;&nbsp;Self Funding
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_loan_eligibility">Loan Eligibility</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_loan_eligibility">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_loan_tenure">Loan Tenure</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_loan_tenure" placeholder="In Months">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="rd_buy_duration">Buying Duration</label>
											<div class="controls">
												<input type="radio" name="rd_buy_duration" value="1" checked />&nbsp;&nbsp;&nbsp;Immediately
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_buy_duration" value="2" />&nbsp;&nbsp;&nbsp;Next Month
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_buy_duration" value="3" />&nbsp;&nbsp;&nbsp;Later
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="dt_closure_date">Tentative Closure Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_closure_date" required>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks" class="span6"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->			
																														
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_prospective_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
