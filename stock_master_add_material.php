<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 20th Sep 2016

// LAST UPDATED BY: Lakshmi

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');

/* INCLUDES - END */



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */



	// Capture the form data

	if(isset($_POST["add_material_submit"]))

	{

		$material_code      				= $_POST["stxt_code"];

		$material_name 						= $_POST["stxt_name"];

		$material_type 						= $_POST["ddl_type"];

		$material_unit 						= $_POST["ddl_unit"];

		$material_storage 					= $_POST["ddl_storage"];

		$material_transportation_mode 		= $_POST["ddl_transportaion"];

		$material_manufacturer_part_number 	= $_POST["stxt_part_no"];

		$material_lead_time 				= $_POST["stxt_time"];

		$material_indicator 				= $_POST["ddl_indicator"];

		$material_price 					= $_POST["num_price"];

		$material_price_as_on 				= $_POST["date"];

		$material_remarks 					= $_POST["txt_remarks"];

		

		

		// Check for mandatory fields

		if(($material_code != "") && ($material_name != "") && ($material_type != "") && ($material_unit != "") && ($material_storage != "")

	    && ($material_transportation_mode != "") && ($material_indicator != ""))

		{

			$material_iresult = i_add_material($material_code,$material_name,$material_type,$material_unit,$material_storage,$material_transportation_mode,$material_manufacturer_part_number,$material_lead_time,$material_indicator,$material_price,$material_price_as_on,$material_remarks,$user);

			

			if($material_iresult["status"] == SUCCESS)

			{

				$material_id = $material_iresult["data"];

				
				$stock_project_search_data = array();
				$project_result_list = i_get_project_list($stock_project_search_data);
				if($project_result_list["status"] == SUCCESS)
				{
					for($project_count = 0 ; $project_count < count($project_result_list["data"]) ; $project_count++)
					{
						$material_stock_data = i_add_material_stock($material_id,'0',$material_unit,'',$project_result_list["data"][$project_count]["stock_project_id"],'','','','',$material_remarks,$user);

						
						
						$quote_reset_iresult = i_add_stock_quote_reset($material_id,$project_result_list["data"][$project_count]["stock_project_id"],date('Y-m-d H:i:s'),'','Opening',$user);
					}

				}

			

				$alert_type = 1;

				$alert      = 'Material Successfully added';

			}

			else

			{

				$alert_type = 0;

				$alert      = $material_iresult["data"];

			}						

		}

		else

		{

			$alert = "Please fill all the mandatory fields";

			$alert_type = 0;

		}

	}

	

	//Get Storage List

	$storage_search_data = array();

	$storage_list = i_get_storage_condition_list($storage_search_data);

	if($storage_list['status'] == SUCCESS)

	{

		$storage_list_data = $storage_list['data'];

	}

	else

	{

		$alert = $storage_list["data"];

		$alert_type = 0;

	}

	

	//Get transportation List

	$transportation_search_data = array();

	$transportation_list = i_get_transportation_list($transportation_search_data);

	if($transportation_list["status"] == SUCCESS)

	{

		$transportation_list_data = $transportation_list["data"];

	}

	else

	{

		$alert = $transportation_list["data"];

		$alert_type = 0;

	}

	

	//Get Uom List

	$stock_unit_search_data = array();

	$unit_list = i_get_stock_unit_measure_list($stock_unit_search_data);

	if($unit_list['status'] == SUCCESS)

	{

		$unit_list_data = $unit_list['data'];

	}

	else

	{

		$alert = $uom_list["data"];

		$alert_type = 0;

	}

	

	//Get Indicator List

	$stock_indicator_search_data = array();

	$indicator_list = i_get_indicator_list($stock_indicator_search_data);

	if($indicator_list["status"] == SUCCESS)

	{

		$indicator_list_data = $indicator_list["data"];

	}

	else

	{ 

		$alert = $indicator_list["data"];

		$alert_type = 0;

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Add Material - Stock Masters</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>

    

<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>    



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget ">

	      			

	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>Add Material - Masters</h3><span style="float:right; padding-right:20px;"><a href="stock_master_material_list.php">View Material List</a></span>

	  				</div> <!-- /widget-header -->

					

					<div class="widget-content">

						

						

						

						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <a href="#formcontrols" data-toggle="tab">Add Material</a>

						  </li>						  

						</ul>

						

						<br>

							<div class="control-group">												

								<div class="controls">

								<?php 

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>  

								<?php

								}

								?>

                                

								<?php 

								if($alert_type == 1) // Success

								{

								?>								

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>

								<?php

								}

								?>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">

								<form id="add_material" class="form-horizontal" method="post" action="stock_master_add_material.php">

									<fieldset>										

																				

										<div class="control-group">											

											<label class="control-label" for="stxt_code">Material Code*</label>

											<div class="controls">

												<input type="text" class="span6" name="stxt_code" placeholder="Material Code" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label" for="stxt_part_no">HSN Code</label>

											<div class="controls">

												<input type="text" class="span6" required name="stxt_part_no">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->
										

										<div class="control-group">											

											<label class="control-label" for="stxt_name">Material Name*</label>

											<div class="controls">

												<input type="text" class="span6" name="stxt_name" placeholder="Material Name" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="ddl_type">Material Type*</label>

											<div class="controls">

												<select name="ddl_type" required>

												<option value="">- - Select Material Type - -</option>

												<option value ="Returnable">Returnable</option>

												<option value ="NonReturnable"> Non Returnable</option>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="ddl_unit">Unit Of Measure*</label>

											<div class="controls">

												<select name="ddl_unit" required>

												<option value="">- - Select Unit of Measure - -</option>

												<?php

												for($count = 0; $count < count($unit_list_data); $count++)

												{

												?>

												<option value="<?php echo $unit_list_data[$count]["stock_unit_id"]; ?>"><?php echo $unit_list_data[$count]["stock_unit_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="ddl_storage">Storage Condition*</label>

											<div class="controls">

												<select name="ddl_storage" required>

												<option value="">- - Select Storage Condition - -</option>

												<?php

												for($count = 0; $count < count($storage_list_data); $count++)

												{

												?>

												<option value="<?php echo $storage_list_data[$count]["stock_storage_id"]; ?>"><?php echo $storage_list_data[$count]["stock_storage_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="ddl_transportaion">Transportation Mode*</label>

											<div class="controls">

												<select name="ddl_transportaion" required>

												<option value="">- - Select Transportation Mode - -</option>

												<?php

												for($count = 0; $count < count($transportation_list_data); $count++)

												{

												?>

												<option value="<?php echo $transportation_list_data[$count]["stock_transportation_id"]; ?>"><?php echo $transportation_list_data[$count]["stock_transportation_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="stxt_time"> Procrument Lead Time</label>

											<div class="controls">

												<input type="number" class="span6" name="stxt_time">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="ddl_indicator">Indicator*</label>

											<div class="controls">

												<select name="ddl_indicator" required>

												<option value="">- - Select Indicator - -</option>

												<?php

												for($count = 0; $count < count($indicator_list_data); $count++)

												{

												?>

												<option value="<?php echo $indicator_list_data[$count]["stock_indicator_id"]; ?>"><?php echo $indicator_list_data[$count]["stock_indicator_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="num_price">Price</label>

											<div class="controls">

												<input type="number" class="span6" name="num_price" step="0.01">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="date">Price As On</label>

											<div class="controls">

											   <input type="date" class="span6" name="date" placeholder="Lead Time" value="<?php echo date('Y-m-d'); ?>">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="txt_remarks">Remarks</label>

											<div class="controls">

												<input type="text" class="span6" name="txt_remarks">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

                                                                                                                                                               										 <br />

										

											

										<div class="form-actions">

											<input type="submit" class="btn btn-primary" name="add_material_submit" value="Submit" />

											<button type="reset" class="btn">Cancel</button>

										</div> <!-- /form-actions -->

									</fieldset>

								</form>

								</div>															

							</div>

						</div>

							

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      		      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>





  </body>



</html>

