<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: Enquiry List Report
CREATED ON	: 03-May-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Enquiry Report
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* Query String / Get form Data	*/
	// Interest Status
	if((isset($_GET["int_status"])) && ($_GET["int_status"] != "-1"))
	{
		$int_status = $_GET["int_status"];
	}
	else
	{
		$int_status = "";
	}
	
	// Project
	if((isset($_GET["project"])) && ($_GET["project"] != "-1"))
	{
		$project = $_GET["project"];
	}
	else
	{
		$project = "";
	}
	
	// Start Date
	if((isset($_GET["start_date"])) && ($_GET["start_date"] != "-1"))
	{
		$start_date = $_GET["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	// End Date
	if((isset($_GET["end_date"])) && ($_GET["end_date"] != "-1"))
	{
		$end_date = $_GET["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	// Start Date Entry
	if((isset($_GET["start_date_entry"])) && ($_GET["start_date_entry"] != "-1"))
	{
		$start_date_entry = $_GET["start_date_entry"];
	}
	else
	{
		$start_date_entry = "";
	}
	
	// End Date
	if((isset($_GET["end_date_entry"])) && ($_GET["end_date_entry"] != "-1"))
	{
		$end_date_entry = $_GET["end_date_entry"];
	}
	else
	{
		$end_date_entry = "";
	}
	
	// Source
	if((isset($_GET["source"])) && ($_GET["source"] != "-1"))
	{
		$source = $_GET["source"];
	}
	else
	{
		$source = "";
	}
	
	// Assigner
	if((isset($_GET["assigner"])) && ($_GET["assigner"] != "-1"))
	{
		$assigner = $_GET["assigner"];
	}
	else
	{
		$assigner = "";
	}
	
	// Enquiry No
	if((isset($_GET["enquiry_num"])) && ($_GET["enquiry_num"] != "-1"))
	{
		$enquiry_number = $_GET["enquiry_num"];
	}
	else
	{
		$enquiry_number = "";
	}
	
	// Customer Mob No
	if((isset($_GET["cell"])) && ($_GET["cell"] != "-1"))
	{
		$cell_number = $_GET["cell"];
	}
	else
	{
		$cell_number = "";
	}
	
	// Assigned To
	if((isset($_GET["assignee"])) && ($_GET["assignee"] != "-1"))
	{
		$assignee = $_GET["assignee"];
	}
	else
	{
		$assignee = "";
	}																			

	// Temp data
	$alert = "";
	
	if($role == "6")
	{
		$order = "entry_date_desc";
		$enquiry_list = i_get_enquiry_list('',$enquiry_number,$project,'','',$cell_number,'','',$source,'','',$assignee,$assigner,$start_date_entry,$end_date_entry,$start_date,$end_date,$order,'','');
	}
	else if(($role == "7") || ($role == "8"))
	{
		$order = "assign_date_desc";
		$enquiry_list = i_get_enquiry_list('',$enquiry_number,$project,'','',$cell_number,'','',$source,'','',$assignee,$assigner,$start_date_entry,$end_date_entry,$start_date,$end_date,$order,'','');
	}
	else
	{
		$user_details = i_get_user_list($assignee,'','','');		
		if($user_details["data"][0]["user_role"] == "6")
		{
			$order = "entry_date_desc";
			$enquiry_list = i_get_enquiry_list('',$enquiry_number,$project,'','',$cell_number,'','',$source,'','',$assignee,$assigner,$start_date_entry,$end_date_entry,$start_date,$end_date,$order,'','');
		}
		else		
		{
			$order = "assign_date_desc";
			$enquiry_list = i_get_enquiry_list('',$enquiry_number,$project,'','',$cell_number,'','',$source,'','',$assignee,$assigner,$start_date_entry,$end_date_entry,$start_date,$end_date,$order,'','');
		}
	}
	
	if($enquiry_list["status"] == SUCCESS)
	{
		$enquiry_list_data = $enquiry_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_list["data"];
	}
	
	/* Create excel sheet and write the column headers - START */
	// Instantiate a new PHPExcel object
	$objPHPExcel = new PHPExcel(); 
	// Set the active Excel worksheet to sheet 0
	$objPHPExcel->setActiveSheetIndex(0); 
	// Initialise the Excel row number
	$row_count = 1; 
	// Excel column identifier array
	$column_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U');
	// Set the serial no
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "SL NO"); 
	// Set the enquiry no
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "ENQUIRY NO"); 
	// Set the project
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "PROJECT"); 
	$objPHPExcel->getActiveSheet()->getStyle($column_array[2].$row_count)->getAlignment()->setWrapText(true);
	// Set the customer name
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "NAME"); 
	// Set the customer mobile
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, "MOBILE"); 
	// Set the customer email
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, "EMAIL"); 
	// Set the company
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "COMPANY"); 
	// Set the enquiry source
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, "SOURCE"); 
	// Set the enquiry walk in
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, "WALK IN"); 
	// Set the latest status
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "LATEST STATUS");
	// Set the sold status
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, "SOLD STATUS");
	// Set the booking date
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, "BOOKING DATE");
	// Set the enquiry date
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, "ENQUIRY DATE");
	// Set the enquiry assigned date
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, "ENQUIRY ASSIGNED DATE");
	// Set the follow up count
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[14].$row_count, "FOLLOW UP COUNT");
	// Set the follow up remarks
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[15].$row_count, "FOLLOW UP REMARKS");
	// Set the next follow up
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[16].$row_count, "NEXT FOLLOW UP");
	// Set the site visit count
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[17].$row_count, "SITE VISIT COUNT");
	// Set the site visit date
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[18].$row_count, "SITE VISIT DATE");
	// Set the assignee
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[19].$row_count, "ASSIGNEE");

	// Increment the Excel row counter
	$row_count++; 	 
	/* Create excel sheet and write the column headers - END */
	
	if($enquiry_list["status"] == SUCCESS)
	{
		$sl_no 				  = 0;
		$unq_count            = 0;
		$int_filter_out_count = 0;
		for($count = 0; $count < count($enquiry_list_data); $count++)
		{						
			$latest_follow_up_data = i_get_enquiry_fup_list($enquiry_list_data[$count]["enquiry_id"],'','','desc','0','1');	
			if($latest_follow_up_data['status'] == SUCCESS)
			{
				$fup_remarks = $latest_follow_up_data['data'][0]['enquiry_follow_up_remarks'];
			}
			else
			{
				$fup_remarks = '';
			}
			
			$follow_up_data = i_get_enquiry_fup_list($enquiry_list_data[$count]["enquiry_id"],'','','','','');
			if($follow_up_data["status"] == SUCCESS)
			{
				$fup_count = count($follow_up_data["data"]);
			}
			else
			{
				$fup_count = 0;
			}
		
			// Site Visit Data
			$site_visit_data = i_get_site_travel_plan_list('','',$enquiry_list_data[$count]["enquiry_id"],'','','','2','','');
			if($site_visit_data["status"] == SUCCESS)
			{
				$sv_count = count($site_visit_data["data"]);
				$sv_date  = date('d-M-Y',strtotime($site_visit_data['data'][($sv_count - 1)]['crm_site_travel_plan_date']));
			}
			else
			{
				$sv_count = 0;
				$sv_date  = '';
			}
			
			// Check if there is an active booking for this enquiry
			$sales_result = i_get_site_booking('','','','','','1','','','','','','',$enquiry_list_data[$count]["enquiry_id"],'','','','');
			if($sales_result["status"] == SUCCESS)
			{
				$sold_status = 'SOLD';
				if($sales_result['data'][0]['crm_booking_date'] != '0000-00-00')
				{
					$booking_date = date('d-M-Y',strtotime($sales_result['data'][0]['crm_booking_date']));
				}
				else
				{
					$booking_date = 'NA. Approved Date: '.date('d-M-Y',strtotime($sales_result['data'][0]['crm_booking_added_on']));
				}
			}
			else
			{
				$sold_status  = 'UNSOLD';
				$booking_date = '';
			}
		
			// Is the enquiry unqualified
			$unqualified_data = i_get_unqualified_leads('',$enquiry_list_data[$count]["enquiry_id"],'','','','','');
			if($unqualified_data["status"] != SUCCESS)
			{
				if(($int_status != "") && ($latest_follow_up_data["data"][0]["crm_cust_interest_status_id"] != $int_status))
				{
					$int_filter_out_count++;
				}
				else
				{
					$sl_no++;
					$row_count++;
					// Set the first column
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $sl_no); 
					// Set the second column - Enquiry No
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $enquiry_list_data[$count]["enquiry_number"]); 
					// Set the third column - Project Name
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $enquiry_list_data[$count]["project_name"]); 
					// Set the fourth column - Name
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $enquiry_list_data[$count]["name"]); 
					// Set the fifth column - Mobile
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, $enquiry_list_data[$count]["cell"]); 
					// Set the sixth column - Email
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, $enquiry_list_data[$count]["email"]); 
					// Set the seventh column - Company
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $enquiry_list_data[$count]["company"]); 
					// Set the eight column - Source
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, $enquiry_list_data[$count]["enquiry_source_master_name"]); 
					// Set the ninth column - Walk In
					if($enquiry_list_data[$count]["walk_in"] == '1')
					{
						$walk_in_value = "Yes";
					}
					else
					{
						$walk_in_value = "No";
					}
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, $walk_in_value); 
					// Set the tenth column - Latest Status
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $latest_follow_up_data["data"][0]["crm_cust_interest_status_name"]); 
					// Set the eleventh column - Sold Status
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, $sold_status);
					// Set the twelfth column - Booking Date
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, $booking_date);
					// Set the thirteenth column - Enquiry Date
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, date("d-M-Y",strtotime($enquiry_list_data[$count]["added_on"]))); 
					// Set the fourteenth column - Enquiry Assigned Date
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, date("d-M-Y",strtotime($enquiry_list_data[$count]["assigned_on"]))); 
					// Set the fifteenth column - Follow Up Count
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[14].$row_count, $fup_count); 
					// Set the sixteenth column - Follow Up Remarks
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[15].$row_count, $fup_remarks); 
					// Set the seventeenth column - Next Follow Up
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[16].$row_count, date("d-M-Y H:i",strtotime($latest_follow_up_data["data"][0]["enquiry_follow_up_date"]))); 
					// Set the eighteenth column - Site Visit Count
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[17].$row_count, $sv_count); 
					// Set the nineteenth column - Site Visit Date
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[18].$row_count, $sv_date); 
					// Set the twentieth column - Executive
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[19].$row_count, $enquiry_list_data[$count]["user_name"]);
				}
			}
		}	
	}
			
	header('Content-Type: application/vnd.ms-excel'); 
	header('Content-Disposition: attachment;filename="enquiry_report_'.date("d-M-Y").'.xls"'); 
	header('Cache-Control: max-age=0'); 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
	$objWriter->save('php://output');
}
else
{
	header("location:login.php");
}	
?>