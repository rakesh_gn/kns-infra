<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th May 2017
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Role dropdown should be from database
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	/* DATA INITIALIZATION - END */

	$search_project   	 = "";

	if(isset($_POST["project_search_submit"]))
	{
		$search_project   = $_POST["search_project"];
	}

	// Get Project List
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user, "user_id"=>$user);
	$project_management_master_filter_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_filter_list["status"] == SUCCESS)
	{
		$project_management_master_filter_list_data = $project_management_master_filter_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_filter_list["data"];
	}

	// Get Project details
	$project_management_master_search_data = array("active"=>'1',"project_id"=>$search_project, "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}

	$material = i_get_actual_material_value($search_project,'1');
	var_dump($material);
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Dashboard - Projectwise Financials</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Wise Finance Dashboard</h3>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:50px; padding-top:10px;">
			  <form method="post" id="project_search_form" action="dashboard_projectmgmnt.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_filter_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_filter_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_filter_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_filter_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="project_search_submit" />
			  </form>
            </div>
            <div class="widget-content">
              <table class="table table-bordered">
                <thead>
					<tr>
						<th>Project</th>
						<th colspan="3">Material</th>
						<th colspan="3">Machine</th>
						<th colspan="3">Manpower</th>
						<th colspan="3">Contract</th>
						<th colspan="2">Budget</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Variance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Variance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Variance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Variance</th>
						<th>Budgeted</th>
						<th>Variance</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$total_material_plan     = 0;
					$total_machine_plan      = 0;
					$total_manpower_plan     = 0;
					$total_contract_plan     = 0;
					$total_material_actuals  = 0;
					$total_machine_actuals   = 0;
					$total_manpower_actuals  = 0;
					$total_contract_actuals  = 0;
					$total_material_variance = 0;
					$total_machine_variance  = 0;
					$total_manpower_variance = 0;
					$total_contract_variance = 0;
					$total_budgeted_value	 = 0;
					$total_budget_variance	 = 0;

					if($project_management_master_list['status'] == SUCCESS)
					{
						for($count = 0; $count < count($project_management_master_list['data']); $count++)
						{
							$project_id = $project_management_master_list['data'][$count]['project_management_master_id'];

							$project_finance_filter = array('project_id'=>$project_id);
							$project_finance_sresult = i_get_project_finance_dashboard($project_finance_filter);

							if($project_finance_sresult['status'] == SUCCESS)
							{
								$project_finance_data = $project_finance_sresult['data'];

								/* Project Material Data - Start */
								$material_plan_value   = $project_finance_data[count($project_finance_data) - 1]['project_finance_material_planned_value'];
								$material_actual_value = $project_finance_data[count($project_finance_data) - 1]['project_finance_material_actual_value'];
								$material_variance     = $project_finance_data[count($project_finance_data) - 1]['project_finance_material_variance_value'];

								$total_material_plan     = $total_material_plan + $material_plan_value;
								$total_material_actuals  = $total_material_actuals + $material_actual_value;
								$total_material_variance = $total_material_variance + $material_variance;
								/* Project Material Data - End */

								/* Project Machine Data - Start */
								$machine_plan_value   = $project_finance_data[count($project_finance_data) - 1]['project_finance_machine_planned_value'];
								$machine_actual_value = $project_finance_data[count($project_finance_data) - 1]['project_finance_machine_actual_value'];
								$machine_variance     = $project_finance_data[count($project_finance_data) - 1]['project_finance_machine_variance_value'];

								$total_machine_plan     = $total_machine_plan + $machine_plan_value;
								$total_machine_actuals  = $total_machine_actuals + $machine_actual_value;
								$total_machine_variance = $total_machine_variance + $machine_variance;
								/* Project Machine Data - End */

								/* Project Manpower Data - Start */
								$manpower_plan_value   = $project_finance_data[count($project_finance_data) - 1]['project_finance_manpower_planned_value'];
								$manpower_actual_value = $project_finance_data[count($project_finance_data) - 1]['project_finance_manpower_actual_value'];
								$manpower_variance     = $project_finance_data[count($project_finance_data) - 1]['project_finance_manpower_variance_value'];

								$total_manpower_plan     = $total_manpower_plan + $manpower_plan_value;
								$total_manpower_actuals  = $total_manpower_actuals + $manpower_actual_value;
								$total_manpower_variance = $total_manpower_variance + $manpower_variance;
								/* Project Manpower Data - End */

								/* Project Contract Data - Start */
								$contract_plan_value   = $project_finance_data[count($project_finance_data) - 1]['project_finance_contract_planned_value'];
								$contract_actual_value = $project_finance_data[count($project_finance_data) - 1]['project_finance_contract_actual_value'];
								$contract_variance     = $project_finance_data[count($project_finance_data) - 1]['project_finance_contract_variance_value'];

								$total_contract_plan     = $total_contract_plan + $contract_plan_value;
								$total_contract_actuals  = $total_contract_actuals + $contract_actual_value;
								$total_contract_variance = $total_contract_variance + $contract_variance;
								/* Project Contract Data - End */
							}

							/* Budget - Start */
							$project_budget_search_data = array("active"=>'1',"project_id"=>$project_id);
							$project_budget_list = i_get_project_budget($project_budget_search_data);
							if($project_budget_list["status"] == SUCCESS)
							{
								$project_budget_list_data = $project_budget_list["data"];
								$budgeted_value = round($project_budget_list_data[0]['project_budget_value']);
							}
							else
							{
								$budgeted_value = 0;
							}

							$budget_variance = $budgeted_value - ($material_actual_value + $machine_actual_value + $manpower_actual_value + $contract_actual_value);
							$total_budgeted_value  = $total_budgeted_value + $budgeted_value;
							$total_budget_variance = $total_budget_variance + $budget_variance;
							/* Budget - End */
							?>
							<tr>
								<td><?php echo $project_management_master_list['data'][$count]['project_master_name']; ?></td>
								<td><?php echo $material_plan_value; ?></td>
								<td><?php echo $material_actual_value; ?></td>
								<td <?php if($material_variance > 0){ ?> style="color:red;" <?php } ?>><?php echo $material_variance; ?></td>
								<td><?php echo $machine_plan_value; ?></td>
								<td><?php echo $machine_actual_value; ?></td>
								<td <?php if($machine_variance > 0){ ?> style="color:red;" <?php } ?>><?php echo $machine_variance; ?></td>
								<td><?php echo $manpower_plan_value; ?></td>
								<td><?php echo $manpower_actual_value; ?></td>
								<td <?php if($manpower_variance > 0){ ?> style="color:red;" <?php } ?>><?php echo $manpower_variance; ?></td>
								<td><?php echo $contract_plan_value; ?></td>
								<td><?php echo $contract_actual_value; ?></td>
								<td <?php if($contract_variance > 0){ ?> style="color:red;" <?php } ?>><?php echo $contract_variance; ?></td>
								<td><?php echo $budgeted_value; ?></td>
								<td <?php if($budget_variance > 0){ ?> style="color:red;" <?php } ?>><?php echo $budget_variance; ?></td>
							</tr>
							<?php
						}
						?>
							<tr>
								<td>TOTAL</td>
								<td><?php echo $total_material_plan; ?></td>
								<td><?php echo $total_material_actuals; ?></td>
								<td <?php if($total_material_variance > 0){ ?> style="color:red;" <?php } ?>><?php echo $total_material_variance; ?></td>
								<td><?php echo $total_machine_plan; ?></td>
								<td><?php echo $total_machine_actuals; ?></td>
								<td <?php if($total_machine_variance > 0){ ?> style="color:red;" <?php } ?>><?php echo $total_machine_variance; ?></td>
								<td><?php echo $total_manpower_plan; ?></td>
								<td><?php echo $total_manpower_actuals; ?></td>
								<td <?php if($total_manpower_variance > 0){ ?> style="color:red;" <?php } ?>><?php echo $total_manpower_variance; ?></td>
								<td><?php echo $total_contract_plan; ?></td>
								<td><?php echo $total_contract_actuals; ?></td>
								<td <?php if($total_contract_variance > 0){ ?> style="color:red;" <?php } ?>><?php echo $total_contract_variance; ?></td>
								<td><?php echo $total_budgeted_value; ?></td>
								<td <?php if($total_budget_variance > 0){ ?> style="color:red;" <?php } ?>><?php echo $total_budget_variance; ?></td>
							</tr>
						<?php
					}
					else
					{
						?>
						<tr>
						<td colspan="15">No project added yet</td>
						</tr>
						<?php
					}
					?>
                </tbody>
              </table>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
