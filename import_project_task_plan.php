<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID', '253');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
require_once dirname(__FILE__) . '/../Legal/utilities/PHPExcel-1.8/Classes/PHPExcel.php';
if (isset($_REQUEST["hd_project_id"])) {
    $project_id   = $_REQUEST["hd_project_id"];
// set here
} else {
    $project_id = " ";
}

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

// upload file
    $uploaddir = dirname(__FILE__).'/temp_uploads/';
    $filename = $uploaddir .'task_plan_import_file_'.time().'.xlsx';
    echo '<pre>';
    if (move_uploaded_file($_FILES['file']['tmp_name'], $filename)) {


  	$type = PHPExcel_IOFactory::identify($filename);
  	$objReader = PHPExcel_IOFactory::createReader($type);
  	$objPHPExcel = $objReader->load($filename);

  	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
  	   $rows = $worksheet->toArray();
        break;
  	}

    $header = array();
    foreach ($rows[0] as $index => $value) {
  	   $header[$value] = $index;
  	}

    echo "<pre>";
    $tmp_row = array();
    $executed_query_count = 0 ;
    $ignored_query_count = 0 ;
    for($i=1; $i<count($rows); $i++){
       $row = $rows[$i];
  	   $tmp_row['planned_project_id'] = $row[$header['planned_project_id']];
  	   $tmp_row['planned_process_id'] = $row[$header['planned_process_id']];
  	   $tmp_row['task_id'] = $row[$header['task_id']];
  	   $tmp_row['task_planning_id'] = $row[$header['task_planning_id']];
  	   $tmp_row['planned_road_id'] = $row[$header['planned_road_id']];
  	   $tmp_row['UOM'] = $row[$header['UOM']];
  	   $tmp_row['Resource_type'] = $row[$header['Resource_type']];
  	   $tmp_row['Resource_id'] = $row[$header['Resource_id']];
       $tmp_row["Resource_name"] = str_replace('–', '-', $row[$header['Resource_name']]);
  	   $tmp_row['Resource_mc_id'] = $row[$header['Resource_mc_id']];
  	   $tmp_row['Qty'] = $row[$header['Qty']];
  	   $tmp_row['Start_date'] = $row[$header['Start_date']];
  	   $tmp_row['End_date'] = $row[$header['End_date']];
  	   $tmp_row['Planning ID'] = $row[$header['Planning ID']];
  	   $tmp_row['Per_day_output'] = $row[$header['Per_day_output']];
       $total_days=0;
       $no_of_objects=0;
       $project_object_output_search_data = array("active"=>'1',"object_type"=>$tmp_row['Resource_type'],"task_id"=>$tmp_row['task_id']);
       $project_object_output_cw_master_list = i_get_project_object_output_task($project_object_output_search_data);
       $value=0;
       $object_id="";
       $object_name="";
       $formatted_start_date = date_create($tmp_row['Start_date']);
       $formatted_start_date = date_format($formatted_start_date,"Y-m-d");
       $formatted_end_date = date_create($tmp_row['End_date']);
       $formatted_end_date = date_format($formatted_end_date,"Y-m-d");
       $total_actual_mp_cost = 0 ;
      $project_budget_manpower_search_data = array("task_id"=>$tmp_row['task_planning_id'],"road_id"=>$tmp_row['planned_road_id']);
      $budget_manpower_list =  db_get_project_budget_manpower($project_budget_manpower_search_data);
      if($budget_manpower_list["status"] == DB_RECORD_ALREADY_EXISTS)
      {
          $total_actual_mp_cost = $total_actual_mp_cost + $budget_manpower_list["data"][0]["total_msmrt"];
      }
      else {
        $total_actual_mp_cost = 0;
      }
      $total_actual_mc_cost = 0 ;
      $project_budget_machine_search_data = array("task_id"=>$tmp_row['task_planning_id'],"road_id"=>$tmp_row['planned_road_id']);
      $budget_machine_list =  db_get_project_budget_machine($project_budget_machine_search_data);
      if($budget_machine_list["status"] == DB_RECORD_ALREADY_EXISTS)
      {
          $budget_machine_list_data = $budget_machine_list["data"];
            $total_actual_mc_cost = $total_actual_mc_cost + $budget_machine_list["data"][0]["total_msmrt"];
      }
      else {
          $total_actual_mc_cost = 0;
      }

      //Contract
      $total_actual_cw_cost = 0 ;
      $project_budget_contract_search_data = array("task_id"=>$tmp_row['task_planning_id'],"road_id"=>$tmp_row['planned_road_id']);
      $budget_cw_list =  db_get_project_budget_contract($project_budget_contract_search_data);
      if($budget_cw_list["status"] == DB_RECORD_ALREADY_EXISTS)
      {
            $total_actual_cw_cost = $total_actual_cw_cost + $budget_cw_list["data"][0]["total_msmrt"];
      }
      else {
        $total_actual_cw_cost = 0;
      }
      $total_actual_measurment = $total_actual_mp_cost + $total_actual_mc_cost + $total_actual_cw_cost;

       if ($project_object_output_cw_master_list['status'] == SUCCESS) {
         $project_object_output_master_list_data = $project_object_output_cw_master_list["data"];
         foreach ($project_object_output_master_list_data as $item) {
           $item["project_cw_master_name"] = str_replace('–', '-', $item["project_cw_master_name"]);
           if($tmp_row['Resource_type']=='CW'){
             $object_name=$item["project_cw_master_name"];
           }
          else {
            $object_name=$item["project_machine_type_master_name"];
          }
           if($tmp_row["Resource_name"]==$object_name){
             $value=1;
               if($tmp_row['Resource_type']=='CW'){
                 $object_id=$item["project_cw_master_id"];
                 $per_day_output=$item["project_object_output_obejct_per_hr"];
               }
              else {
                $object_id=$item["project_machine_type_master_id"];
                $per_day_output=$item["project_object_output_obejct_per_hr"];
              }
           }
         }
       }

       if(($value > 0) || !empty($object_id)){
         if(($tmp_row['End_date'] != "30-11--0001" && $tmp_row['Start_date'] != "30-11--0001" ) && ($tmp_row['Qty']!=0 ))
         {
           $start_date_time = strtotime($formatted_start_date);
           $end_date_time = strtotime($formatted_end_date);
           $date_diff = $end_date_time - $start_date_time;
           $total_days=($date_diff / (60 * 60 * 24))+1;
           $no_of_objects=abs(ceil(($tmp_row['Qty']/$per_day_output)/$total_days));
           $get_planned_data = i_get_project_plan_data("project_task_planning_id",$tmp_row['Planning ID']);
           if($get_planned_data["status"] == DB_RECORD_ALREADY_EXISTS)
           {
             if($total_actual_measurment==0){
               $project_update_data = array("measurment"=>$tmp_row['Qty'],
               "no_of_object"=>$no_of_objects,"object_type"=>$tmp_row['Resource_type'],"machine_type"=>$object_id,"plan_start_date"=>$formatted_start_date,
               "plan_end_date"=>$formatted_end_date,"added_by"=>$user,"uom"=>$tmp_row['UOM'],"total_days"=>$total_days);
             }
             else{
               $project_update_data = array("measurment"=>$tmp_row['Qty'],
               "no_of_object"=>$no_of_objects,"object_type"=>$tmp_row['Resource_type'],"machine_type"=>$object_id,
               "plan_end_date"=>$formatted_end_date,"added_by"=>$user,"uom"=>$tmp_row['UOM'],"total_days"=>$total_days);
             }
           $update_planned_data = i_update_project_task_planning($tmp_row['Planning ID'],'',$project_update_data);
              if($update_planned_data["data"] != 0)
              {
                $executed_query_count ++ ;
              }
              else {
                $ignored_query_count ++ ;
              }
              if (($formatted_start_date != $old_start_date) || ($old_end_date!=$formatted_end_date)) {
                $task_plan_history_iresults = db_add_project_task_planning_shadow($tmp_row['Planning ID'],$project_id,$tmp_row['task_id'], $tmp_row['planned_road_id'],$old_end_date, $formatted_end_date, $old_start_date, $formatted_start_date, $user);
              }
         }
      }
    }
    else{
      if($total_actual_measurment>0){
        $project_update_data = array("measurment"=>"0.00",
      "no_of_object"=>"0","object_type"=>"empty",
    "plan_end_date"=>"0000-00-00","added_by"=>$user,"total_days"=>"0");
    }
      else{
        $project_update_data = array("measurment"=>"0.00",
      "no_of_object"=>"0","object_type"=>"empty","plan_start_date"=>"0000-00-00",
    "plan_end_date"=>"0000-00-00","added_by"=>$user,"total_days"=>"0");
      }
   $update_planned_data = i_update_project_task_planning($tmp_row['Planning ID'],'',$project_update_data);
    }
  	}
    $total_records = (count($rows) - 1);
    echo '<script>window.parent.redrawTable();alert("Import success");</script>';
    } else {
        echo '<script>window.parent.resetUploadForm();alert("Possible file upload attack!");</script>';
    }
} else {
    header("location:login.php");
}
?>
