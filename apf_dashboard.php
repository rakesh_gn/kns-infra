<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 18th April 2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('APF_DASHBOARD_FUNC_ID','325');
/* DEFINES - END */

/* TBD - START */
// 1. Role dropdown should be from database
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	$view_perms_list   = i_get_user_perms($user,'',APF_DASHBOARD_FUNC_ID,'2','1');

	/* DATA INITIALIZATION - START */
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Get Apf Details already added
	$apf_details_search_data = array("active"=>'1');
	$apf_details_list = i_get_apf_details($apf_details_search_data);
	if($apf_details_list['status'] == SUCCESS)
	{
		$apf_details_list_data = $apf_details_list['data'];
	}
	else
	{
		$alert = $apf_details_list["data"];
		$alert_type = 0;
	}

	/* Get file count for each process */
	// Get process type list
	$apf_bank_master_search_data = array("active"=>'1');
	$bank_list = i_get_apf_bank_master($apf_bank_master_search_data);
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Process Level Dashboard</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
		 
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>File List</h3>
            </div>
            <!-- /widget-header -->
			
            <div class="widget-content">
			<?php 
			if($view_perms_list["status"] == SUCCESS)
			{
		    ?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>Bank</th>
					<th>Number of files</th>
				</tr>
				</thead>
				<tbody>
				<?php
				if($bank_list["status"] == SUCCESS)
				{
					for($count = 0; $count < count($bank_list["data"]); $count++)
					{ 	
				        $apf_details_search_data = array("bank"=>$bank_list["data"][$count]["apf_bank_master_id"]);
						$file_list = i_get_apf_details($apf_details_search_data);
						if($file_list["status"] == SUCCESS)
						{
							//get apf process list
							$apf_process_search_data = array("active"=>'1',"process_end_date"=>'0000-00-00');
							$apf_process_list = i_get_apf_process($apf_process_search_data);
							if($apf_process_list["status"] == SUCCESS)
							{
								
								$pending_files_count = 0;
								for($file_count = 0; $file_count < count($apf_process_list["data"]); $file_count++)
								{
									if($apf_process_list["data"][$file_count]["apf_process_id"] == $apf_process_list["data"][$count]["apf_process_id"])
									{
										
										$pending_files_count++;
									}
								}
									
							}
							
						}
						else if($file_list["status"] == FAILURE)
						{
							$pending_files_count = 0;
						}
						else
						{
							$pending_files_count = 0;
						}
					?>
					<tr>
						<td><?php echo $bank_list["data"][$count]["apf_bank_master_name"]; ?></td>
						<td><?php if($view_perms_list['status'] == SUCCESS){?><a href="apf_details_list.php?bank_id=<?php echo $bank_list["data"][$count]["apf_bank_master_id"]; ?>"><?php echo $pending_files_count; ?></a><?php } ?></td>
						
					</tr>
					<?php 
					}
				}
				?>	

                </tbody>
              </table>
			  <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
