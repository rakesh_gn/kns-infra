<?php
session_start();

define('ADD_USER_FUNC_ID', '1');

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_user.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {

    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];


    $view_perms_list = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '2', '1');

    $add_perms_list  = i_get_user_perms($user, '', ADD_USER_FUNC_ID, '1', '1');

    $alert = "";
    $alert_type = -1;

    if (isset($_POST["add_pli"])) {

        $persona_department_id   = $_POST["ddl_department_id"];
        $persona_user_id         = $_POST["sel_user"];
        $persona_amount          = $_POST["amount"];
        $persona_date            = $_POST["date"];
        $persona_remarks         = $_POST["remarks"];


        if(($persona_department_id != "0") && ($persona_user_id != "0")  &&( $persona_amount != "0") && ($persona_date != "")) {


          $isExists = db_pli_exists($persona_user_id);

          if($isExists["status"] == -103) {

            ?>
            <script>
              alert('PLI record already exists');
            </script>
            <?php
          } else if($isExists["status"] == -104) {

                      $response = db_add_pli($persona_department_id, $persona_user_id, $persona_amount, $persona_date, $persona_remarks);

                       var_dump($response);
                      if($response["status"] == 'SUCCESS') {
                        $_POST = array();
                        ?>
                        <script>
                        alert("PLI saved successfully ");
                         console.log('values ', <?= json_encode($_POST); ?>);
                        </script>
                        <?php
                      }
          }

        }
        else {
          $alert = "Please fill all the mandatory fields";
          $alert_type = 0;

        ?>
          <script>
          alert("Fill all feilds");
            console.log('values ', <?= json_encode($_POST); ?>);
          </script>
          <?php
        }
    }



    $department_list = i_get_department_list('', '');

    if ($department_list["status"] == SUCCESS) {
        $department_list_data = $department_list["data"];
    } else {
        $alert = $alert."Alert: ".$department_list["data"];
      }
    
    $designation_list = db_get_designation_list();
    if($designation_list['status'] == 'DB_RECORD_ALREADY_EXISTS') {
      $designation_list_data = $designation_list['data'];
    }

    // Get Location List

    // $stock_location_master_search_data = array('location_id'=>$location_id);
    //
    // $location_list =  i_get_stock_location_master_list($stock_location_master_search_data);
    //
    // if ($location_list['status'] == SUCCESS) {
    //     $location_list_data = $location_list['data'];
    // } else {
    //     $alert = $location_list["data"];
    //
    //     $alert_type = 0;
    // }
} else {
    header("location:login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Add PLI</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->
    <!-- Custom styles -->
    <link href="assets/multistepform/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
  <?php

  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

  ?>
<!-- MultiStep Form -->
<div class="row">
    <div class="col-md-6 col-md-offset-3" style="margin-top:-50px; margin-bottom:50px">
        <form id="msform" method="post">
            <!-- progressbar -->
            <!-- <ul id="progressbar">
                <li class="active">Performance Linked Incentives</li>
                <li>Address Details</li>
                <li>Amenities</li>
            </ul> -->
            <!-- fieldsets -->
            <fieldset>
                <h2 class="fs-title">Performance Linked Incentives</h2>
                <h3 class="fs-subtitle">Enter PLI Bonus Amount</h3>
                <div class="">
                  <div class="form-group">
                    <!-- <label for="exampleSelect1">Gender</label> -->
                    <select class="form-control" id="ddl_department_id" name="ddl_department_id">
                      <option value='0'>- - Select Department - -</option>
                      <?php
                        for ($count = 0; $count < count($department_list_data); $count++) {
                      ?>
                      <option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>"><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <select class="form-control" id="sel_user" name="sel_user">
                    <option value='0'>- - Select User - -</option>
                    </select>
                  </div>

                  <input type="text" id="amount" name="amount" placeholder="Amount"/>
                  <input type="date" name="date" id="date" placeholder="Effective date"/>
                  <input type="text" name="remarks" id="remarks" placeholder="Remarks"/>
                <!-- <input type="text" name="fname" placeholder="First Name" required/>
                <input type="text" name="lname" placeholder="Last Name"/> -->
                <!-- <input type="text" maxlength="10" id="user_phone" name="user_phone" placeholder="Phone"/> -->
              </div>
              <input type="submit" id="add_pli" name="add_pli" class="submit action-button" value="Submit"/>
                <br/>
            </fieldset>


        </form>
    </div>
</div>
<!-- /.MultiStep Form -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/multistepform/js/msform.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

<script>

$(document).ready(function() {
  $("#ddl_department_id").change(function() {
		var department_id = $(this).val();
		console.log('department_id ', department_id);
		if(department_id == 0) {
			$("#sel_user").empty();
      $("#sel_user").append("  <option value='0'>- - Select User - -</option>");
			return false;
		}

		$.ajax({
			url: 'ajax/getUsers.php',
			data: {department_id: department_id},
			dataType: 'json',
			success: function(response) {
				console.log('hope i got all users ', response);

				$("#sel_user").empty();
        $("#sel_user").append("  <option value='0'>- - Select User - -</option>");
				for(var i=0; i< response.length; i++) {
					var id = response[i]['user_id'];
					var name = response[i]['user_name'];
          // var class = 'list-group-item';

					$("#sel_user").append("<option value='"+id+"'>"+name+"</option>");
					// $("#sel_user").append("<div id='"+id+"'>"+name+"</div>");
				}
			}
		})
	})
})

</script>
</body>
</html>
