<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 02-May-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["add_survey_master_submit"]))
	{
		$parent_survey_id  = $_POST["hd_parent_survey_id"];
		$village           = $_POST["ddl_village"];
		$survey_no         = $_POST["stxt_survey_no"];
		$land_owner        = $_POST["stxt_owner"];
		$extent            = $_POST["stxt_extent"];
		$remarks           = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($village != "") && ($survey_no != "") && ($land_owner != "") && ($extent != ""))
		{
			$survey_master_iresult =  i_add_survey_master('',$parent_survey_id,$village,$survey_no,$land_owner,$extent,$remarks,$user);

			if($survey_master_iresult["status"] == SUCCESS)				
			{	
				$alert_type = "1";
			}
			
			$alert = $survey_master_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	if(isset($_POST["hd_parent_survey_id"]))
	{
		$search_parent_survey_id = $_POST["hd_parent_survey_id"];		
	}
	else
	{
		$search_parent_survey_id = "";		
	}
	
    // Get Village Master already added
	$village_master_list = i_get_village_list('');
	if($village_master_list['status'] == SUCCESS)
	{
		$village_master_list_data = $village_master_list["data"];
	}
	else
	{
		$alert = $village_master_list["data"];
		$alert_type = 0;
	}
    
	// Get Survey already added
	$survey_master_search_data = array("active"=>'1',"parent_survey_id"=>$search_parent_survey_id);
	$survey_master_list = i_get_survey_master($survey_master_search_data);
	if($survey_master_list['status'] == SUCCESS)
	{
		$survey_master_list_data = $survey_master_list['data'];
	}
}
else
{
	header("location:login.php");
}	
	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey Master - Add Survey</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Survey</h3><span style="float:right; padding-right:20px;"><a href="survey_master_survey_list.php">Survey List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Survey</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="survey_master_add_survey_form" class="form-horizontal" method="post" action="survey_master_add_survey.php">
									<fieldset>
									  

										<div class="control-group">											
											<label class="control-label" for="ddl_village">Village*</label>
											<div class="controls">
												<select class="span6" name="ddl_village" id="ddl_village" required>
												<option value="">- - Select Village - -</option>
												<?php
												for($count = 0; $count < count($village_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $village_master_list_data[$count]["village_id"]; ?>"><?php echo $village_master_list_data[$count]["village_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
																
											<div class="control-group">											
											<label class="control-label" for="stxt_survey_no">Survey Number *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_survey_no" placeholder="Dont use spaces. Use only / symbol" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																														
										<div class="control-group">											
											<label class="control-label" for="stxt_owner">Land Owner*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_owner" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_extent">Extent*</label>
											<div class="controls">
												<input type="number" class="span6" name="stxt_extent" required="required" min="0" step="0.01">
												<p class="help-block">Enter in guntas</p>
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->
										
										<div class="control-group">		
									    <input type="hidden" name="hd_parent_survey_id" id="hd_parent_survey_id" value="" />	
                                        <label class="control-label" for="hd_parent_survey_id">Parent Survey No</label>									  
									    <span style="padding-left:20px; padding-right:20px;">										
									    <input type="text" name="stxt_parent_survey_no" class="span6" autocomplete="off" id="stxt_parent_survey_no" onkeyup="return get_survey_list();" placeholder="Search By Survey No" />
									    <div id="search_results" style="padding-left:160px; padding-right:20px;" class="dropdown-content" style="display:none;"></div>
									    </span>
									    </div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_survey_master_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>

function get_survey_list()
{ 
	var searchstring = document.getElementById('stxt_parent_survey_no').value;
	var village = document.getElementById('ddl_village').value;

	if(searchstring.length >= 1)
	{
		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}

		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{		                  
				if(xmlhttp.responseText != 'FAILURE')

				{
					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}

			}

		}

		xmlhttp.open("POST", "ajax/survey_get_survey_no.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring + "&ddl_village=" + village);				
	}

	else	

	{
		document.getElementById('search_results').style.display = 'none';
	}
}

function select_survey_no(survey_id,search_survey_no,land_owner)
{
	document.getElementById('hd_parent_survey_id').value = survey_id;
	var name_code = search_survey_no.concat('-',land_owner);
	document.getElementById('stxt_parent_survey_no').value = name_code;
	document.getElementById('search_results').style.display = 'none';
}

</script>
  </body>

</html>
