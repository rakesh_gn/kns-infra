<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_method_planning_list.php
CREATED ON	: 14-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID','283');
/* DEFINES - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing

	if(isset($_REQUEST['project_id']))
	{
		$project_id = $_REQUEST['project_id'];
	}
	else
	{
		$project_id = '-1';
	}

	$process_id= "";
	if(isset($_REQUEST["process_id"]))
	{
		$process_id = $_REQUEST["process_id"];
	}

	 // Get Users modes already added
	$user_list = i_get_user_list('','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}

	// Get Project Process Master modes already added
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}

	// Get Project Task Master modes already added
	$project_task_master_search_data = array("active"=>'1');
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list['status'] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list["data"];
	}
	else
	{
		$alert = $project_task_master_list["data"];
		$alert_type = 0;
	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}

	//Get Task Mehod Planning List
	$project_task_method_planning_search_data = array("active"=>'1',"process_id"=>$process_id,"project_id"=>$project_id);
	$project_task_method_planning_list = i_get_project_task_method_planning($project_task_method_planning_search_data);
	if($project_task_method_planning_list["status"] == SUCCESS)
	{
		$project_task_method_planning_list_data = $project_task_method_planning_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_method_planning_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>
<div class="modal-header">
	<?php
	if(!empty($project_task_method_planning_list_data[0]))
	{
		?>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<div>
	<h4  class="modal-title">Process : <?php echo $project_task_method_planning_list_data[0]["project_process_master_name"];?> </h4>
	<h4 class="modal-title">Project : <?php echo $project_task_method_planning_list_data[0]["project_master_name"];?> </h4>
 </div>
</div>
<div class="modal-body">
            <div class="widget-content">
			<?php
			if($view_perms_list["status"] == SUCCESS)
			{
		    ?>
          <table class="table table-striped table-bordered display nowrap">
          <thead>
          <tr>
					<th>#</th>
					<th>Document</th>
					<th>Document Type</th>
					<th>Remarks</th>
					<th>Added By</th>
					<th>Added On</th>
				</tr>
				</thead>
				<tbody>
				<?php
					if($project_task_method_planning_list["status"] == SUCCESS)
					{
					$sl_no = 0;
					for($count = 0; $count < count($project_task_method_planning_list_data); $count++)
					{

						$sl_no++;
						$doc = $project_task_method_planning_list_data[$count]["project_task_method_planning_document"];
						$plan_type = $project_task_method_planning_list_data[$count]["project_task_method_planning_type"];
						if($plan_type == 1)
						{
							$plan_type = "Study Document";
						}
						elseif($plan_type == 2)
						{
							$plan_type = "Check List";
						}
						else
						{
							$plan_type = "Cad Drawings";
						}
					?>
					<?php
					if(($edit_perms_list['status'] == SUCCESS) || ($project_task_method_planning_list_data[$count]["project_task_method_planning_added_by"] == $user))
					{
					 ?>
					<tr>
					<td><?php echo $sl_no;?></td>
					<td><a href="documents/<?php echo $project_task_method_planning_list_data[$count]["project_task_method_planning_document"]; ?>" target="_blank"><?php echo end(explode("_",$project_task_method_planning_list_data[$count]["project_task_method_planning_document"])); ?></a></td>
					<td><?php echo $plan_type; ?></td>
					<td><a href="#" onclick="alert('<?php echo $project_task_method_planning_list_data[$count]["project_task_method_planning_remarks"]; ?>');"><?php echo substr($project_task_method_planning_list_data[$count]["project_task_method_planning_remarks"],0,30); ?></a></td>
					<td><?php echo $project_task_method_planning_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:normal !important;"><?php echo date("d-M-Y h:i:s a",strtotime($project_task_method_planning_list_data[$count]["project_task_method_planning_added_on"])); ?></td>
					</tr>
					<?php
					}
					}
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				<?php
				}
			 }
			 else
			 {
			 ?>
			 <h4 class="modal-title">No Files Found for this Process</h4>
			 <?php
			 }
			 ?>
        </tbody>
      </table>
      </div>
		</div>
