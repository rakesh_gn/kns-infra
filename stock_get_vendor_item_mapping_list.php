<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_method_planning_list.php
CREATED ON	: 14-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID','283');
/* DEFINES - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing

	if(isset($_REQUEST['material_id']))
	{
		$material_id = $_REQUEST['material_id'];
	}
	else
	{
		$material_id = '';
	}
  //Get Venor Type of Service List
	$stock_vendor_item_mapping_search_data = array("item_id"=>$material_id);
	$vendor_item_list = i_get_stock_vendor_item_mapping($stock_vendor_item_mapping_search_data);
	if($vendor_item_list["status"] == SUCCESS)
	{
		$vendor_item_list_data = $vendor_item_list["data"];
	}
	else
	{
		$alert = $vendor_item_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}
?>
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<div>
	<h4  class="modal-title">Vendor List </h4>
 </div>
</div>
<div class="modal-body">
        <div class="widget-content">
      <table class="table table-striped table-bordered display nowrap" style="width:100%">
      <thead>
      <tr>
        <th>SL No</th>
        <th>Vendor Name</th>
        <th>Item</th>
        <th>Item Code</th>
        <th>Contact Person</th>
        <th>Contact Number</th>
        <th>Email Id</th>
        <th>Address</th>
		</tr>
		</thead>
		<tbody>
    <?php
		if($vendor_item_list["status"] == SUCCESS)
		{
        $sl_no = 0;
				for($count = 0; $count < count($vendor_item_list_data); $count++)
				{
					$sl_no++;
				?>
				<tr>
				<td><?php echo $sl_no; ?></td>
				<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_vendor_name"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_material_name"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_material_code"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_vendor_contact_person"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_vendor_contact_number"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_vendor_email_id"]; ?></td>
        <td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_vendor_address"]; ?></td>
				</tr>
				<?php
				}
			}
			else
			{
				?>
				<td colspan="5">No Vendor data added yet!</td>
        <?php
      }
     ?>
    </tbody>
  </table>
  </div>
</div>
