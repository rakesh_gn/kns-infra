<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_stock_indent.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'transaction_config.php');



/*

PURPOSE : To add Stock Indent

INPUT 	: Indent No, Project, Requested_By, Requested_On, Department, Location, Required Date, Remarks, Status, Added By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_add_stock_indent($project,$requested_by,$requested_on,$department,$location,$required_date,$remarks,$added_by)

{

	$indent_no =  p_generate_indent_no();

	$indent_iresult = db_add_stock_indent($indent_no,$project,$requested_by,$requested_on,$department,$location,$required_date,$remarks,$added_by);

	

	if($indent_iresult['status'] == SUCCESS)

	{

		$return["data"]   = $indent_iresult['data'];

		$return["status"] = SUCCESS;	

	}

	else

	{
		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}

	



/*

PURPOSE : To get stock Indent list

INPUT 	: Indent Project, Active

OUTPUT 	: Indent List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_indent_list($stock_indent_search_data)

{

	$indent_sresult = db_get_stock_indent_list($stock_indent_search_data);

	

	if($indent_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$indent_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No indent added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Indent

INPUT 	: Indent ID, Indent Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/



function i_update_indent($indent_id,$indent_update_data)

{

		$indent_sresult = db_update_indent($indent_id,$indent_update_data);

		

		if($indent_sresult['status'] == SUCCESS)

		{

			$return["data"]   = "indent Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

		

	return $return;

}



/*

PURPOSE : To add Stock Indent Items

INPUT 	: Material Id, Indent Id, Quantity, Uom, Required Date, Requested Date, Remarks, Approved by, Approved on, Added by

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_add_stock_indent_items($material_id,$indent_id,$quantity,$uom,$required_date,$requested_date,$remarks,$approved_by,$approved_on,$added_by)

{

	$stock_indent_items_search_data = array("material_id"=>$material_id,"indent_id"=>$indent_id,"active"=>'1');

	$indent_items_sresult = db_get_stock_indent_items_list($stock_indent_items_search_data);

	if($indent_items_sresult["status"] == DB_NO_RECORD)

	{

		$indent_items_iresult = db_add_stock_indent_items($material_id,$indent_id,$quantity,$uom,$required_date,$requested_date,$remarks,$approved_by,$approved_on,$added_by);

		if($indent_items_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "Indent items Successfully Added";

			$return["status"] = SUCCESS;	

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		$return["data"]   = "This item already exists for this indent";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get Indent Item list

INPUT 	: Item_Id, Material_Id, Indent_Id, Quantity, Uom, Active, Status, Required Date, Requested Date, Approved by, Approved on, Added by, 

          Start Date(for added on), End Date(for added on)

OUTPUT 	: Indent Item List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_indent_sum_of_items_list($stock_indent_items_search_data)

{

	$indent_items_sresult = db_get_stock_indent_sum_of_items_list($stock_indent_items_search_data);

	

	if($indent_items_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $indent_items_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No items added. Please contact the system admin"; 

    }

	

	return $return;

}

/*

PURPOSE : To get Indent Item list

INPUT 	: Item_Id, Material_Id, Indent_Id, Quantity, Uom, Active, Status, Required Date, Requested Date, Approved by, Approved on, Added by, 

          Start Date(for added on), End Date(for added on)

OUTPUT 	: Indent Item List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_indent_items_list($stock_indent_items_search_data)

{

	$indent_items_sresult = db_get_stock_indent_items_list($stock_indent_items_search_data);

	

	if($indent_items_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $indent_items_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No indent item added"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Indent Items

INPUT 	: Indent Item ID, Indent Items Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/



function i_update_indent_items($item_id,$indent_id,$indent_items_update_data)

{

		$indent_items_sresult = db_update_indent_items($item_id,$indent_id,$indent_items_update_data);

		

		if($indent_items_sresult['status'] == SUCCESS)

		{

			$return["data"]   = "indent items Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

		

	return $return;

}

/* PRIVATE FUNCTIONS - START */

function p_generate_indent_no()

{

	// Get the last invoice no

	$stock_indent_search_data = array("sort"=>'1');

	$indent_no_data = db_get_stock_indent_list($stock_indent_search_data);

	

	if($indent_no_data["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$invoice_numeric_value_data = $indent_no_data["data"][0]["stock_indent_no"];

		$invoice_numeric_value_array = explode(INV_NO_DELIMITER,$invoice_numeric_value_data);

		

		$invoice_numeric_value = ($invoice_numeric_value_array[2]) + 1;

	}

	else

	{

		$invoice_numeric_value = 1;

	}

	

	return INV_NO_ROOT.INV_NO_DELIMITER.INV_NO_IND.INV_NO_DELIMITER.$invoice_numeric_value;

}

/* PRIVATE FUNCTIONS - END */

?>