<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: kns_project_list.php
CREATED ON	: 06-Oct-2016
CREATED BY	: Lakshmi
PURPOSE     : List of Project for customer withdrawals
*/

/*
TBD: 
*/

/* DEFINES - START */
define('PROJECT_MATER_FUNC_ID','');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_MATER_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_MATER_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_MATER_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_MATER_FUNC_ID,'1','1');

	// Query String Data
	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}
	
	//Get Project Process Task Manpower
	$total_completion_percentage = 0;
	$total_machine_completion_percentage = 0;
	$total_contract_completion_percentage = 0;
	$avg_count = 0;
	$project_process_task_search_data = array("process_id"=>$process_id,"active"=>'1');
	$project_process_task_list = i_get_project_process_task($project_process_task_search_data);
	if($project_process_task_list["status"] == SUCCESS)
	{
		$project_process_task_list_data = $project_process_task_list["data"];
		$no_of_task = count($project_process_task_list_data);
		for($task_count = 0 ; $task_count < count($project_process_task_list_data) ; $task_count++)
		{
			$task_id = $project_process_task_list_data[0]["project_process_task_id"];
			$total_completion_percentage = $total_completion_percentage + $project_process_task_list_data[$task_count]["project_process_task_completion"];
			
			$total_machine_completion_percentage = $total_machine_completion_percentage + $project_process_task_list_data[$task_count]["project_process_machine_task_completion"];
			
			$total_contract_completion_percentage = $total_contract_completion_percentage + $project_process_task_list_data[$task_count]["project_process_contract_task_completion"];
		}
		
		// Actual Manpower data
		$man_power_search_data = array("active"=>'1',"task_id"=>$task_id,"work_type"=>"Regular");
		$man_power_list = i_get_man_power_list($man_power_search_data);
		if($man_power_list["status"] == SUCCESS)
		{
			$avg_count = $avg_count + 1;
		}
		else
		{
			// Get Project Man Power Estimate modes already added
			$project_man_power_estimate_search_data = array("active"=>'1',"task_id"=>$task_id);
			$project_man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
			if($project_man_power_estimate_list['status'] == SUCCESS)
			{
				$avg_count = $avg_count + 1;
			}
			else
			{
				//Do not Inncrement
			}
		}
		
		// Machine Planning data
		$actual_machine_plan_search_data = array("active"=>'1',"task_id"=>$task_id,"work_type"=>"Regular");
		$actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
		if($actual_machine_plan_list["status"] == SUCCESS)
		{
			$avg_count = $avg_count + 1;
		}
		else
		{
			//Machine Planning
			$project_machine_planning_search_data = array("active"=>'1',"task_id"=>$task_id);
			$project_machine_planning_list = i_get_project_machine_planning($project_machine_planning_search_data);
			if($project_machine_planning_list["status"] == SUCCESS)
			{
				$avg_count = $avg_count + 1;
			}
			else
			{
				//Do not Increment
			}
		}
		
		// Actual Contract  Data
		$project_task_boq_actual_search_data = array("active"=>'1',"task_id"=>$task_id,"work_type"=>"Regular");
		$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
		if($project_task_boq_actual_list['status'] == SUCCESS)
		{
			$avg_count = $avg_count + 1;
		}
		else
		{
			$project_task_boq_search_data = array("active"=>'1',"task_id"=>$task_id);
			$project_task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
			if($project_task_boq_list['status'] == SUCCESS)
			{
				$avg_count = $avg_count + 1;
			}
			else
			{
				///Do not Increment
			}
		}
		
		$avg_completion_percentage = $total_completion_percentage/$no_of_task;
		$pending_percentage = 100 - $avg_completion_percentage;
		
		$avg_machine_completion_percentage = $total_machine_completion_percentage/$no_of_task;
		$pending_percentage = 100 - $avg_machine_completion_percentage;
		
		$avg_contract_completion_percentage = $total_contract_completion_percentage/$no_of_task;
		$contract_pending_percentage = 100 - $avg_contract_completion_percentage;
	}
	else
	{
		$avg_completion_percentage = 0;
		$pending_percentage = 100;
		$avg_machine_completion_percentage = 0;
		$pending_percentage = 100;
		$avg_contract_completion_percentage = 0;
		$contract_pending_percentage = 100;
	}
	
	$overall_percentage = $avg_completion_percentage + $avg_contract_completion_percentage +$avg_machine_completion_percentage ;
	if($avg_count != 0)
	{
		$avg_overall_percentage = $overall_percentage/$avg_count;
	}
	else
	{
		$avg_overall_percentage = 0;
	}
	$overall_pending_percentage = 100 - $avg_overall_percentage;
	
	
	// Get Project Task BOQ modes already added
	$project_task_boq_actual_search_data = array("active"=>'1',"process_id"=>$process_id,"work_type"=>"Regular");
	$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
	if($project_task_boq_actual_list['status'] == SUCCESS)
	{
		$project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
		$project_task_contract_start_date = get_formatted_date($project_task_boq_actual_list_data[0]["project_task_actual_boq_date"],"d-M-Y");
	}
	else
	{
		$project_task_contract_start_date = "";
	}
	
	// Temp data
	$man_power_search_data = array("active"=>'1',"process_id"=>$process_id,"work_type"=>"Regular");
	$man_power_actual_list = i_get_man_power_list($man_power_search_data);
	if($man_power_actual_list["status"] == SUCCESS)
	{
		$man_power_actual_list_data = $man_power_actual_list["data"];
		$man_power_start_date = date("d-M-Y",strtotime($man_power_actual_list_data[0]["project_task_actual_manpower_date"]));
	}
	else
	{
		$man_power_start_date = "";
	}
	
	// Machine Planning data
	$actual_machine_plan_search_data = array("active"=>'1',"process_id"=>$process_id,"work_type"=>"Regular");
	$actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
	if($actual_machine_plan_list["status"] == SUCCESS)
	{
		$actual_machine_plan_list_data = $actual_machine_plan_list["data"];
		$actual_machine_start_date = date("d-M-Y H:i:s",strtotime($actual_machine_plan_list_data[0]["project_task_actual_machine_plan_start_date_time"]));
	}
	else
	{
		$actual_machine_start_date = "";
	}
}
else
{
	header("location:login.php");
}	
?>


<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Master List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
            </div>
            <!-- /widget-header -->
								<table class="table table-bordered" style="width:75%; ">
								<thead>
								  <tr style="height:200px;">
									<th style="vertical-align:middle;font-size: large;">Man Power : <?php echo round($avg_completion_percentage,2) ;?>%<br/> Start Date: <?php echo $man_power_start_date;?><br/>
									<br/><a style="padding-right:10px" href="project_pending_task_list.php?process_id=<?php echo $process_id ;?>&source=<?php echo "view"; ?>" target="_blank">View Details</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </th>
									
									<th style="vertical-align:middle ; font-size: large;">Machine : <?php echo round($avg_machine_completion_percentage,2) ; ?>%<br/>Start Date : <?php echo $actual_machine_start_date;?><br/>
									<br/><a style="padding-right:10px" href="project_pending_task_list.php?process_id=<?php echo $process_id ;?>&source=<?php echo "view"; ?>" target="_blank"> View Details</a> &nbsp;&nbsp;&nbsp; </th>
									
									<th style="vertical-align:middle;font-size: large;"
									>Contract Work : <?php echo round($avg_contract_completion_percentage,2) ; ?>%<br/> Start Date : <?php echo $project_task_contract_start_date ;?><br/>
									<br/><a style="padding-right:10px" href="project_pending_task_list.php?process_id=<?php echo $process_id ;?>&source=<?php echo "view"; ?>" target="_blank">View Details</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </th>
								</tr>
								</thead>
							  </table>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function go_to_project_task_actual(process_id)
{	
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_actual_manpower_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
function go_to_project_task_actual_machine_planning(process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_actual_machine_planning_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
function go_to_project_task_boq_actuals(process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_boq_actuals_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>


  </body>

</html>