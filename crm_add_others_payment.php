<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 24th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data	
	if(isset($_POST["add_other_payment_submit"]))
	{
		$booking_id  = $_POST["hd_booking_id"];
		$amount      = $_POST["num_amount"];
		$mode        = $_POST["ddl_mode"];
		$type        = $_POST["ddl_type"];
		$instru_date = $_POST["dt_instru_date"];
		$bank        = $_POST["ddl_bank"];
		$branch      = $_POST["stxt_branch"];
		$rec_date    = $_POST["dt_rec_date"];
		$remarks     = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($booking_id !="") && ($amount !="") && ($mode !=""))
		{
			if((($mode != "4") && ($bank != "") && ($branch != "")) || ($mode == "4"))
			{
				// Check how much other payment is already done					
				switch($type)
				{
				case "1": // Maintenance Charges
				$current_pending = $_POST["hd_maint_charges"];
				break;
				
				case "2": // Water Charges
				$current_pending = $_POST["hd_water_charges"];
				break;
				
				case "3": // Club House Charges
				$current_pending = $_POST["hd_club_house_charges"];
				break;
				
				case "4": // Documentation Charges
				$current_pending = $_POST["hd_documentation_charges"];
				break;
				
				case "5": // Other Charges
				$current_pending = $_POST["hd_other_charges"];
				break;				
				}
			
				if($amount <= $current_pending)
				{
					$add_other_pay_result = i_add_other_payment($booking_id,$amount,$type,$mode,$instru_date,$bank,$branch,$rec_date,$remarks,$user);
					
					if($add_other_pay_result["status"] == SUCCESS)
					{
						$alert_type = 1;
						$alert      = $add_other_pay_result["data"];
					}
					else
					{
						$alert_type = 0;
						$alert      = $add_other_pay_result["data"];
					}
				}
				else
				{
					$alert_type = 0;
					$alert      = "Amount entered is greater than pending amount. Please check";
				}
			}
			else			
			{
				$alert_type = 0;
				$alert      = "Please fill bank and branch details";
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
	
	// Get booking details
	$booking_list = i_get_site_booking($booking_id,'','','','','','','','','','','');
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$booking_list["data"];
	}
	
	// Get payment mode list
	$payment_mode_list = i_get_payment_mode_list('','1');
	if($payment_mode_list["status"] == SUCCESS)
	{
		$payment_mode_list_data = $payment_mode_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$payment_mode_list["data"];
	}
	
	// Get bank list
	$bank_list = i_get_bank_list('','1');
	if($bank_list["status"] == SUCCESS)
	{
		$bank_list_data = $bank_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bank_list["data"];
	}
	
	// Get payment terms list
	$payment_terms_list = i_get_payment_terms($booking_id);
	if($payment_terms_list["status"] == SUCCESS)
	{
		$payment_terms_list_data = $payment_terms_list["data"];
		
		$maintenance_cost   =  $payment_terms_list_data[0]["crm_pt_layout_maintenance_charges"];
		$water_cost         =  $payment_terms_list_data[0]["crm_pt_water_charges"];
		$club_house_cost    =  $payment_terms_list_data[0]["crm_pt_club_house_charges"];
		$documentation_cost =  $payment_terms_list_data[0]["crm_pt_documentation_charges"];
		$other_costs        =  $payment_terms_list_data[0]["crm_pt_other_charges"];
		
		$maint_result = i_get_other_payment_total($booking_id,'1');
		$maint_pending = $maintenance_cost - $maint_result["data"];
		
		$water_result = i_get_other_payment_total($booking_id,'2');
		$water_pending = $water_cost - $water_result["data"];
		
		$club_house_result = i_get_other_payment_total($booking_id,'3');
		$club_house_pending = $club_house_cost - $club_house_result["data"];
		
		$documentation_result = i_get_other_payment_total($booking_id,'2');
		$documentation_pending = $documentation_cost - $documentation_result["data"];
		
		$other_result = i_get_other_payment_total($booking_id,'3');
		$other_pending = $other_costs - $other_result["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$payment_terms_list["data"];
		
		$maintenance_cost   =  "0";
		$water_cost         =  "0";
		$club_house_cost    =  "0";
		$documentation_cost =  "0";
		$other_costs        =  "0";
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Other Payment</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
						<?php 
						if($cust_details["status"] == SUCCESS)
						{
						?>
						<h3>Project: <?php echo $cust_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $cust_details_list[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $cust_details_list[0]["crm_customer_name_one"]." (".$cust_details_list[0]["crm_customer_contact_no_one"].")"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}
						else
						{
							$alert_type = 0;
							$alert      = "Customer Profile not added!";?>
	      				<h3>Project: <?php echo $booking_list_data[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $booking_list_data[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $booking_list_data[0]["name"]." (".$booking_list_data[0]["cell"].")"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}?>					

						<span style="float:right; padding-right:10px;"><a href="crm_customer_transactions.php?booking=<?php echo $booking_list_data[0]["crm_booking_id"]; ?>">Back to customer profile</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<h3>
						Maintenance Charges: <?php echo $maint_pending; ?> (out of <?php echo $maintenance_cost; ?>)&nbsp;&nbsp;&nbsp;
						Water Charges: <?php echo $water_pending; ?> (out of <?php echo $water_cost; ?>)&nbsp;&nbsp;&nbsp;
						Club House Charges: <?php echo $club_house_pending; ?> (out of <?php echo $club_house_cost; ?>)&nbsp;&nbsp;&nbsp;
						Documentation Charges: <?php echo $documentation_pending; ?> (out of <?php echo $documentation_cost; ?>)&nbsp;&nbsp;&nbsp;
						Other Charges: <?php echo $other_pending; ?> (out of <?php echo $other_costs; ?>)&nbsp;&nbsp;&nbsp;
						</h3>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_other_payment" class="form-horizontal" method="post" action="crm_add_others_payment.php">
								<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />	
								<input type="hidden" name="hd_maint_charges" value="<?php echo $maint_pending; ?>" />
								<input type="hidden" name="hd_water_charges" value="<?php echo $water_pending; ?>" />
								<input type="hidden" name="hd_club_house_charges" value="<?php echo $club_house_pending; ?>" />
								<input type="hidden" name="hd_documentation_charges" value="<?php echo $documentation_pending; ?>" />
								<input type="hidden" name="hd_other_charges" value="<?php echo $other_pending; ?>" />
									<fieldset>										
												
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount*</label>
											<div class="controls">
												<input type="number" name="num_amount" min="1" step="0.01" class="span6" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="num_tds_amount">TDS</label>
											<div class="controls">
												1% TDS if amount greater than INR 50,00,000
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="num_tds_amount">Payment Mode*</label>
											<div class="controls">
												<select class="span6" name="ddl_mode">
												<option value="">- - Select payment Mode - -</option>
												<?php
												for($count = 0; $count < count($payment_mode_list_data); $count++)
												{
												?>
												<option value="<?php echo $payment_mode_list_data[$count]["payment_mode_id"]; ?>"><?php echo $payment_mode_list_data[$count]["payment_mode_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="ddl_type">Payment Reason*</label>
											<div class="controls">
												<select class="span6" name="ddl_type">
												<option value="">- - Select payment reason - -</option>
												<option value="1">Maintenance Charges</option>
												<option value="2">Water Charges</option>
												<option value="3">Club House Charges</option>
												<option value="4">Documentation Charges</option>
												<option value="5">other Charges</option>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="dt_instru_date">Instrument Date*</label>
											<div class="controls">
												<input type="date" name="dt_instru_date" class="span6" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="ddl_bank">Bank</label>
											<div class="controls">
												<select class="span6" name="ddl_bank">
												<option value="">- - Select Bank - -</option>
												<?php
												for($count = 0; $count < count($bank_list_data); $count++)
												{
												?>
												<option value="<?php echo $bank_list_data[$count]["crm_bank_master_id"]; ?>"><?php echo $bank_list_data[$count]["crm_bank_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_branch">Branch</label>
											<div class="controls">
												<input type="text" name="stxt_branch" class="span6" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="dt_rec_date">Receipt Date</label>
											<div class="controls">
												<input type="date" name="dt_rec_date" class="span6" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_other_payment_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
