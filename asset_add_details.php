<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 23-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'asset'.DIRECTORY_SEPARATOR.'asset_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["add_asset_details_submit"]))
	{
		$asset_master_id        = $_POST["ddl_asset_master_id"];
		$asset_type_id          = $_POST["ddl_asset_type_id"];
		$vendor_id              = $_POST["ddl_vendor_id"];
		$location               = $_POST["ddl_location"];
		$invoice_no             = $_POST["invoice_no"];
		$invoice_amount         = $_POST["invoice_amount"];
		$invoice_date           = $_POST["invoice_date"];
		$other_charges          = $_POST["other_charges"];
		$serial_no              = $_POST["serial_no"];
		$units                  = $_POST["units"];
		$on_date                = $_POST["on_date"];
		$life_of_asset          = $_POST["life_of_asset"];
		$asset_used_till        = $_POST["asset_used_till"];
		$life_form              = $_POST["life_form"];
		$depreciation_rate      = $_POST["depreciation_rate"];
		$depreciation_amount    = $_POST["depreciation_amount"];
		$wdv_as_on              = $_POST["wdv_as_on"];
		$remarks                = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($asset_master_id != "") && ($asset_type_id != "") && ($vendor_id != "") && ($location != ""))
		{
			$asset_details_iresult =  i_add_asset_details($asset_master_id,$asset_type_id,$vendor_id,$invoice_no,$invoice_amount,$invoice_date,$other_charges,$location,$serial_no,$units,$on_date,$life_of_asset,$asset_used_till,$life_form,$depreciation_rate,$depreciation_amount,$wdv_as_on,'',$remarks,$user);
			
			if($asset_details_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
			}
			
			$alert = $asset_details_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get Asset Master modes already added
	$asset_master_search_data = array("active"=>'1');
	$asset_master_list = i_get_asset_master($asset_master_search_data);
	if($asset_master_list['status'] == SUCCESS)
	{
		$asset_master_list_data = $asset_master_list['data'];
	}	
	else
	{
		$alert = $asset_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Asset Type Master modes already added
	$asset_type_master_search_data = array("active"=>'1');
	$asset_type_master_list = i_get_asset_type_master($asset_type_master_search_data);
	if($asset_type_master_list['status'] == SUCCESS)
	{
		$asset_type_master_list_data = $asset_type_master_list['data'];
	}	
	else
	{
		$alert = $asset_type_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Stock Location Master modes already added
	$stock_location_master_search_data = array("active"=>'1');
	$stock_location_master_list = i_get_stock_location_master_list($stock_location_master_search_data);
	if($stock_location_master_list['status'] == SUCCESS)
	{
		$stock_location_master_list_data = $stock_location_master_list['data'];
	}	
	else
	{
		$alert = $stock_location_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Vendor Master modes already added
	$stock_vendor_master_search_data = array("active"=>'1');
	$stock_vendor_master_list = i_get_stock_vendor_master_list($stock_vendor_master_search_data);
	if($stock_vendor_master_list['status'] == SUCCESS)
	{
		$stock_vendor_master_list_data = $stock_vendor_master_list['data'];
	}	
}
else
{
	header("location:login.php");
}	

?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Asset Add Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Asset Add Details</h3><span style="float:right; padding-right:20px;"><a href="asset_details_list.php">Asset Details List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Asset Add Details</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="asset_add_details_form" class="form-horizontal" method="post" action="asset_add_details.php">
									<fieldset>	
                                        
										
										<div class="control-group">											
											<label class="control-label" for="ddl_asset_master_id">Material*</label>
											<div class="controls">
												<select name="ddl_asset_master_id" required>
												<option>- - Select Asset - -</option>
												<?php
												for($count = 0; $count < count($asset_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $asset_master_list_data[$count]["asset_master_id"]; ?>"><?php echo $asset_master_list_data[$count]["stock_material_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_asset_type_id">Asset Type*</label>
											<div class="controls">
												<select name="ddl_asset_type_id" required>
												<option>- - Select Asset Type - -</option>
												<?php
												for($count = 0; $count < count($asset_type_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $asset_type_master_list_data[$count]["asset_type_master_id"]; ?>"><?php echo $asset_type_master_list_data[$count]["asset_type_master_asset_type"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="ddl_vendor_id">Vendor*</label>
											<div class="controls">
												<select name="ddl_vendor_id" required>
												<option>- - Select Vendor - -</option>
												<?php
												for($count = 0; $count < count($stock_vendor_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $stock_vendor_master_list_data[$count]["stock_vendor_id"]; ?>"><?php echo $stock_vendor_master_list_data[$count]["stock_vendor_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="ddl_location">Location*</label>
											<div class="controls">
												<select name="ddl_location" required>
												<option>- - Select Location - -</option>
												<?php
												for($count = 0; $count < count($stock_location_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $stock_location_master_list_data[$count]["stock_location_id"]; ?>"><?php echo $stock_location_master_list_data[$count]["stock_location_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
																
											<div class="control-group">											
											<label class="control-label" for="invoice_no">Invoice No*</label>
											<div class="controls">
												<input type="number" class="span6" name="invoice_no" placeholder="Number">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="invoice_amount">Invoice Amount*</label>
											<div class="controls">
												<input type="number" class="span6" min="0.01" step="0.01" name="invoice_amount" placeholder="Number">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="invoice_date">Invoice Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="invoice_date" placeholder="Date">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="other_charges">Other Charges*</label>
											<div class="controls">
												<input type="number" class="span6" min="0.01" step="0.01" name="other_charges" placeholder="Number">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="serial_no">Serial No*</label>
											<div class="controls">
												<input type="number" class="span6" name="serial_no" placeholder="Serial No">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="units">Unit*</label>
											<div class="controls">
												<input type="number" class="span6" min="0" step="1" name="units" placeholder="Units">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="on_date">On Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="on_date" placeholder="Date">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="life_of_asset">Life Of Asset*</label>
											<div class="controls">
												<input type="number" class="span6" name="life_of_asset" placeholder="Asset No">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="asset_used_till">Asset Used Till*</label>
											<div class="controls">
												<input type="date" class="span6" name="asset_used_till" placeholder="Asset Date">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="life_form">Remaining Life Form*</label>
											<div class="controls">
												<input type="date" class="span6" name="life_form" placeholder="Life Form">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="depreciation_rate">Depreciation Rate*</label>
											<div class="controls">
												<input type="number" class="span6" min="0.01" step="0.01" name="depreciation_rate" placeholder="Rate">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="depreciation_amount">Depreciation Amount*</label>
											<div class="controls">
												<input type="number" class="span6" min="0.01" step="0.01" name="depreciation_amount" placeholder="Amount">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="wdv_as_on">Wdv as On*</label>
											<div class="controls">
												<input type="number" class="span6" min="0.01" step="0.01" name="wdv_as_on" placeholder="Number">
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_asset_details_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
