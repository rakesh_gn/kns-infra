<?php
//ini_set('memory_limit',$ '-1');
/**
 * @author Perumal
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_hr_employee.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_config.php');

/*
PURPOSE : To add new employee
INPUT 	: User ID of the employee, Employee Name, Employee Postal Address, Employee Permanent Address, Father's Name, DOB, Employment Date, Employee Code, Remarks, Added By
OUTPUT 	: Employee ID, success or failure message
BY 		: Nitin
*/
function i_add_employee($employee_user,$employee_name,$employee_postal_address,$employee_permanent_address,$employee_father_name,$employee_dob,$employee_employment_date,$employee_code,$employee_week_off,$remarks,$added_by)
{
	// Check if this employee is already added
	$employee_filter_data = array("employee_code"=>$employee_code);
	$employee_sresult = db_get_employee_list($employee_filter_data);
	
	if($employee_sresult["status"] != DB_RECORD_ALREADY_EXISTS)
	{
		$employee_iresult = db_add_employee($employee_user,$employee_name,$employee_postal_address,$employee_permanent_address,$employee_father_name,$employee_dob,$employee_employment_date,$employee_code,$employee_week_off,$remarks,$added_by);
		
		if($employee_iresult['status'] == SUCCESS)
		{
			$return["data"]   = $employee_iresult["data"];
			$return["status"] = SUCCESS;			
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This employee already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

function i_get_employee_list($employee_filter_data)
{
	$employee_sresult = db_get_employee_list($employee_filter_data);
	
	if($employee_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
        $return["data"]   = $employee_sresult["data"]; 
		$return["status"] = SUCCESS;
    }
    else
    {
		$return["data"]   = "No employee with this ID";
	    $return["status"] = FAILURE;
    }
	
	return $return;
}

/*
PURPOSE : To terminate an employee
INPUT 	: Employee ID
OUTPUT 	: NONE
BY 		: Nitin
*/
function i_terminate_employee($employee_id)
{
	$employee_details = array('emp_status'=>'0');
	$employee_uresult = db_update_employee_details($employee_id,$employee_details);
		
	if($employee_uresult["status"] == SUCCESS)
	{
		$return["data"]   = "Employee Termination Successfully Updated";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}		
		
	return $return;
}

/*
PURPOSE : To edit employee details
INPUT 	: Employee ID, Employee Data
OUTPUT 	: Message, success or failure message
BY 		: Nitin
*/
function i_edit_employee_details($employee_id,$employee_details)
{
    $employee_uresult = db_update_employee_details($employee_id,$employee_details);
	
	if($employee_uresult['status'] == SUCCESS)
	{
		$return["data"]   = "Employee Details Successfully Updated";
		$return["status"] = SUCCESS;		
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

function i_delete_employee($employee_id)
{
	$employee_uresult = db_update_employee_status($employee_id,'2');
		
	if($employee_uresult["status"] == SUCCESS)
	{
		$return["data"]   = "Employee Deleted!";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "There was an internal error. Please try again later!";
		$return["status"] = FAILURE;
	}	
}

function i_rejoin_employee($employee_id)
{
	$employee_uresult = db_update_employee_status($employee_id,'1');
		
	if($employee_uresult["status"] == SUCCESS)
	{
		$return["data"]   = "Employee Re-joining Details successfully updated!";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "There was an internal error. Please try again later!";
		$return["status"] = FAILURE;
	}	
}
?>