<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 13th Jan 2017
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_marketing'.DIRECTORY_SEPARATOR.'crm_marketing_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_marketing_sales_planning.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert          = "";
	$today          = 0;
	$this_week      = 0;
	$this_month     = 0;
	$this_quarter   = 0;
	/* DATA INITIALIZATION - END */
	
	$total_num_leads   = 0;
	$total_num_sales   = 0;
	
	// Get Current FY
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>CRM Variance - Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
	
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    <div class="span6">                        
                    </div>
                    <!-- /span6 -->
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Sales cs CRM Variance</h3>
                            </div>
                            <!-- /widget-header -->							
                            <div class="widget-content">
                                <!-- Tables to go here -->
								<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th rowspan="2">Month</th>
									<th colspan="4">Sales (in sq. ft)</th>
									<th colspan="4">Collection in INR (in Crores)</th>									
								  </tr>
								  <tr>
								  <th style="word-wrap:break-word;">Budgeted Sales</th>
								  <th style="word-wrap:break-word;">Actual Sales</th>
								  <th style="word-wrap:break-word;">Sales Variance</th>
								  <th style="word-wrap:break-word;">%</th>
								  <th style="word-wrap:break-word;">Budgeted Collection</th>
								  <th style="word-wrap:break-word;">Actual Collection</th>
								  <th style="word-wrap:break-word;">Collection Variance</th>
								  <th style="word-wrap:break-word;">%</th>
								  </tr>
								</thead>
								<tbody>
								<?php 
								$fy_start_date = get_financial_year_start(date('Y-m-d'));
								for($count = 0; $count < 12; $count++)
								{
									$month_start_date = get_month_start_date($fy_start_date,(0 - $count));
									$no_of_days_in_month = cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($month_start_date)),date('Y',strtotime($month_start_date)));
									$month_end_date = date('Y',strtotime($month_start_date)).'-'.date('m',strtotime($month_start_date)).'-'.$no_of_days_in_month;
									
									// Get budget for the month
									$sales_target_data = array('month'=>date('m',strtotime($month_start_date)),'year'=>date('Y',strtotime($month_start_date)));
									$sales_target_result = i_get_sales_target($sales_target_data);									
									if($sales_target_result['status'] == SUCCESS)
									{
										$sales_target = $sales_target_result['data'][0]['crm_sales_target_area'];
									}
									else
									{
										$sales_target = 0;
									}
									
									// No of bookings done this month
									$month_booking_filter_data = array('booking_start_date'=>$month_start_date,'booking_end_date'=>$month_end_date,'status'=>'1');
									$month_sales_result = i_get_booking_summary_data($month_booking_filter_data);	
									$month_sales_count = $month_sales_result['data']['salecount'];
									$month_sales_area  = $month_sales_result['data']['salearea'];
									
									$sales_mktg_variance = $month_sales_area - $sales_target;
									if($sales_target != 0)
									{
										$percentage = round(($month_sales_area/$sales_target),2)*100;
									}
									else
									{
										$percentage = 'N.A';
									}
									
									// Scheduled payment collection
									$scheduled_collection_result = i_get_pay_schedule('',$month_start_date,$month_end_date,'','','');
									if($scheduled_collection_result['status'] == SUCCESS)
									{
										$scheduled_collection = 0;
										for($sch_count = 0; $sch_count < count($scheduled_collection_result['data']); $sch_count++)
										{
											$scheduled_collection = $scheduled_collection + $scheduled_collection_result['data'][$sch_count]['crm_payment_schedule_amount'];
										}
									}
									else
									{
										$scheduled_collection = 0;
									}
									
									// Actual payment collection
									$payment_collection_result = i_get_payment('','','',$month_start_date,$month_end_date);
									if($payment_collection_result['status'] == SUCCESS)
									{
										$payment_collection = 0;
										for($pay_count = 0; $pay_count < count($payment_collection_result['data']); $pay_count++)
										{
											$payment_collection = $payment_collection + $payment_collection_result['data'][$pay_count]['crm_payment_amount'];
										}
									}
									else
									{
										$payment_collection = 0;
									}
									
									$pay_collection_variance = $payment_collection - $scheduled_collection;
									if($scheduled_collection != 0)
									{
										$col_percentage = round(($payment_collection/$scheduled_collection),2)*100;
									}
									else
									{
										$col_percentage = 'N.A';
									}
									
									if($sales_mktg_variance < 0)
									{
										$color = 'red';
									}
									else
									{
										$color = 'black';
									}
									
									if($pay_collection_variance < 0)
									{
										$col_color = 'red';
									}
									else
									{
										$col_color = 'black';
									}
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo date('M-Y',strtotime($month_start_date)); ?></td>
									<td style="word-wrap:break-word; color: <?php echo $color; ?>;"><?php echo $sales_target; ?></td>
									<td style="word-wrap:break-word; color: <?php echo $color; ?>;"><?php echo $month_sales_area; ?></td>
									<td style="word-wrap:break-word; color: <?php echo $color; ?>;"><?php echo $sales_mktg_variance; ?></td>
									<td style="word-wrap:break-word; color: <?php echo $color; ?>;"><?php echo $percentage; ?>%</td>
									<td style="word-wrap:break-word; color: <?php echo $col_color; ?>;"><?php echo $scheduled_collection; ?></td>
									<td style="word-wrap:break-word; color: <?php echo $col_color; ?>;"><?php echo $payment_collection; ?></td>
									<td style="word-wrap:break-word; color: <?php echo $col_color; ?>;"><?php echo $pay_collection_variance; ?></td>
									<td style="word-wrap:break-word; color: <?php echo $col_color; ?>;"><?php echo $col_percentage; ?></td>
									</tr>
									<?php
								}
								?>
								</tbody>
								</table>
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
    <div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2016
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/excanvas.min.js"></script>
    <script src="js/chart.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/base.js"></script>
<script>
function go_to_enquiry_list(start_date,end_date)
{	
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "crm_enquiry_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","dt_start_date_entry_form");
	hiddenField1.setAttribute("value",start_date);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","dt_end_date_entry_form");
	hiddenField2.setAttribute("value",end_date);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","enquiry_search_submit");
	hiddenField3.setAttribute("value",'submit');
	
    form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_booking_list(start_date,end_date)
{	
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "crm_overall_report.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","dt_start_date");
	hiddenField1.setAttribute("value",start_date);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","dt_end_date");
	hiddenField2.setAttribute("value",end_date);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","search_report_submit");
	hiddenField3.setAttribute("value",'submit');
	
    form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	document.body.appendChild(form);
    form.submit();
}
</script>
</body>
</html>