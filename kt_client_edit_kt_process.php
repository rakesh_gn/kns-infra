<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 10-Feb-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'katha_transfer'.DIRECTORY_SEPARATOR.'kt_client_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}
	
	if(isset($_GET['request_id']))
	{
		$request_id = $_GET['request_id'];
	}
	else
	{
		$request_id = "";
	}

	// Capture the form data
	if(isset($_POST["edit_kt_client_process_submit"]))
	{
		$process_id                = $_POST["hd_process_id"];
		$process_master_id         = $_POST["ddl_process_master"];
		$request_id                = $_POST["hd_request_id"];
		$process_start_date        = $_POST["process_start_date"];
		$process_end_date          = $_POST["process_end_date"];
		$remarks                   = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($process_master_id != "") &&($request_id != "") && ($process_start_date != "") && ($process_end_date != ""))
		{
			if(((strtotime($process_start_date)) >= (strtotime(date('Y-m-d')))) && ((strtotime($process_end_date)) >= (strtotime($process_start_date))))
			{
				$kt_client_kt_process_update_data = array("process_master_id"=>$process_master_id,"process_start_date"=>$process_start_date,"process_end_date"=>$process_end_date,"remarks"=>$remarks);
				$kt_client_kt_process_iresult = i_update_kt_client_kt_process($process_id,$kt_client_kt_process_update_data);
				
				if($kt_client_kt_process_iresult["status"] == SUCCESS)
				{	
			
					$alert_type = 1;
					header("location:kt_client_kt_process_list.php");

				}
				
				$alert = $kt_client_kt_process_iresult["data"];
			}
			else
			{
				$alert_type = 0;
				$alert      = 'Start Date and End Date cannot be earlier than today. End Date cannot be earlier than start date';
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
    // Get kt Client Process Master modes already added
	$kt_client_process_master_search_data = array("active"=>'1');
	$kt_client_process_master_list = i_get_kt_client_process_master($kt_client_process_master_search_data);
	if($kt_client_process_master_list['status'] == SUCCESS)
	{
		$kt_client_process_master_list_data = $kt_client_process_master_list['data'];
		
    }
     else
    {
		$alert = $kt_client_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Get kt Process already added
	$kt_client_kt_process_search_data = array("active"=>'1',"process_id"=>$process_id,"request_id"=>$request_id);
	$kt_client_kt_process_list = i_get_kt_client_kt_process($kt_client_kt_process_search_data);
	if($kt_client_kt_process_list['status'] == SUCCESS)
	{
		$kt_client_kt_process_list_data = $kt_client_kt_process_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$kt_client_kt_process_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>KT Client - Edit KT Process</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>KT Client - Edit KT Process</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">KT Client Edit Process</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="kt_client_edit_kt_process_form" class="form-horizontal" method="post" action="kt_client_edit_kt_process.php">
								<input type="hidden" name="hd_request_id" value="<?php echo $request_id; ?>" />
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
									<fieldset>

										<div class="control-group">											
												<label class="control-label" for="ddl_process_master">Process*</label>
												<div class="controls">
													<select name="ddl_process_master" required>
													<option value="">- - Select Process - -</option>
													<?php
													for($count = 0; $count < count($kt_client_process_master_list_data); $count++)
													{
													?>
													<option value="<?php echo $kt_client_process_master_list_data[$count]["kt_client_process_master_id"]; ?>" <?php if($kt_client_process_master_list_data[$count]["kt_client_process_master_id"] == $kt_client_kt_process_list_data[0]["kt_client_kt_process_master_id"]){ ?> selected="selected" <?php } ?>><?php echo $kt_client_process_master_list_data[$count]["kt_client_process_master_name"]; ?></option>
													<?php
													}
													?>
													</select>
												</div> <!-- /controls -->					
											</div> <!-- /control-group -->
																
											<div class="control-group">											
											<label class="control-label" for="process_start_date">Start Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="process_start_date" placeholder="Date" value="<?php echo $kt_client_kt_process_list_data[0]["kt_client_kt_process_start_date"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="process_end_date">End Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="process_end_date" placeholder="Date" value="<?php echo $kt_client_kt_process_list_data[0]["kt_client_kt_process_end_date"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks" value="<?php echo $kt_client_kt_process_list_data[0]["kt_client_kt_process_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_kt_client_process_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
