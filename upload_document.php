<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD: 
1. Session management
2. Calendar Object
3. Document Upload
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET['task']))
	{
		$task = $_GET['task'];
	}	

	// Temp data
	$alert = "";

	if(isset($_POST["upload_document_submit"]))
	{
		// Capture all form data	
		$planned_end_date      = date("Y-m-d",strtotime($_POST["planned_end_date"]));
		$forecasted_end_date   = date("Y-m-d",strtotime($_POST["forecasted_end_date"]));
		$actual_end_date       = date("Y-m-d",strtotime($_POST["actual_end_date"]));
		$reason                = $_POST["reason"];
		$document              = upload("document",$user);
		$task                  = $_POST["task_plan"];	// Hidden field
		$applicable            = $_POST["applicable"];	// Hidden field
		$approved_on           = $_POST["approved_on"];	// Hidden field
		
		if($document != "")
		{	
			$task_plan_update_result = i_update_task_plan($task,$planned_end_date,$forecasted_end_date,$actual_end_date,$reason,$document,$applicable,$approved_on);
			
			if($task_plan_update_result["status"] != SUCCESS)
			{
				$disp_msg = $task_plan_update_result["data"];
				$disp_class = "";					
			}
			else
			{
				$disp_msg = $task_plan_update_result["data"];
				$disp_class = "";
				
				header("location:pending_task_list.php");
			}
		}
		else	
		{
			$disp_msg = "Please fill all the mandatory fields. Mandatory fields are marked with *";
			$disp_class = "";
		}
	}

	// Get task details
	$task_data = i_get_task_plan_list($task,'','','','','','',''); // Get task plan for this task plan ID
	if($task_data["status"] == SUCCESS)
	{
		$task_data_details = $task_data["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$task_data["data"];
	}
}
else
{
	header("location:login.php");
}	

// Functions
function upload($file_id,$user_id) 
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "documents/".$_FILES[$file_id]["name"]);							  
		}
	}
	
	return $_FILES[$file_id]["name"];
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Upload Document</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Upload Document</a>
						  </li>						  
						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="upload_document_form" class="form-horizontal" method="post" action="upload_document.php" enctype="multipart/form-data">
									<fieldset>
										<input type="hidden" name="task_plan" value="<?php echo $task_data_details[0]["task_plan_legal_id"]; ?>" />
										<input type="hidden" name="applicable" value="<?php echo $task_data_details[0]["task_plan_applicable"]; ?>" />
										<input type="hidden" name="approved_on" value="<?php echo $task_data_details[0]["task_plan_approved_on"]; ?>" />
										<input type="hidden" name="planned_end_date" value="<?php echo $task_data_details[0]["task_plan_planned_end_date"]; ?>" />
										<input type="hidden" name="reason" value="<?php echo $task_data_details[0]["task_plan_delay_reason"]; ?>" />
										<input type="hidden" name="forecasted_end_date" value="<?php echo $task_data_details[0]["task_plan_forecasted_end_date"]; ?>" />
										<input type="hidden" name="actual_end_date" value="<?php echo $task_data_details[0]["task_plan_actual_end_date"]; ?>" />
										
										<div class="control-group">											
											<label class="control-label" for="document">Document</label>
											<div class="controls">
												<input type="file" class="span6" name="document">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="upload_document_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2013 <a href="http://www.egrappler.com/">Bootstrap Responsive Admin Template</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
