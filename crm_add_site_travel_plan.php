<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'CRM Transactions';

/* DEFINES - START */
define('CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID', '108');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */
if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list    = i_get_user_perms($user, '', CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID, '2', '1');
    $edit_perms_list    = i_get_user_perms($user, '', CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID, '3', '1');
    $delete_perms_list  = i_get_user_perms($user, '', CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID, '6', '1');

?>
<script>
  window.permissions = {
    view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    ok: <?php echo ($ok_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    aprove: <?php echo ($approve_perms_list['status'] == 0)? 'true' : 'false'; ?>,
  }
</script>
<?php
    // Query String Data
    // Nothing

    $alert_type = -1;
    $alert = "";

    $search_project = "";

    if (isset($_REQUEST["project_id"])) {
        $search_project   = $_REQUEST["project_id"];
    // set here
    } else {
        $search_project = "";
    }

    $start_date = "";
    if (isset($_REQUEST["start_date"])) {
        $start_date = $_REQUEST["start_date"];
    }
    $end_date = "";
    if (isset($_REQUEST["end_date"])) {
        $end_date = $_REQUEST["end_date"];
    }

    $crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');
  	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);
  	if($project_list["status"] == SUCCESS)
  	{
  		$project_list_data = $project_list["data"];
  	}
  	else
  	{
  		$alert = $alert."Alert: ".$project_list["data"];
  		$alert_type = 0; // Failure
  	}

    $cab_list = i_get_cab_list('','');
  	if($cab_list["status"] == SUCCESS)
  	{
  		$cab_list_data = $cab_list["data"];
  	}
    else{
      echo $cab_list;
    }
} else {
    header("location:login.php");
}
?>


<html>
  <head>
    <meta charset="utf-8">
    <title>Add Site Travel Plan</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">

    <style media="screen">
      table.dataTable {
        margin-top: 0px !important;
      }
    </style>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js?22062018"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.js?21062018"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
		 <script src="datatable/crm_add_site_travel_plan_datatable.js?<?php echo time(); ?>"></script>
  </head>
  <body>

    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Site Visit Plan</h4>
          </div>
            <div class="widget-header" style="height:auto;">
              <div style="border-bottom: 1px solid #C0C0C0;">
                <span class="header-label">Project Name: </span><span id="project"></span>
              </div>
              <div style="border-bottom: 1px solid #C0C0C0;">
                <span class="header-label">Enquiry Number: </span><span id="enquiry"></span>
              </div>
              <div style="border-bottom: 1px solid #C0C0C0;">
                <span class="header-label">Name: </span><span id="name"></span>
              </div>
              <div style="border-bottom: 1px solid #C0C0C0;">
                <span class="header-label">Phone Number: </span><span id="phone"></span>
              </div>

           </div>
           <div class="modal-body">
           <form id="site_travel_plan" class="form-group">
             <input  class="form-control" type="hidden" id="project_id" name="project_id"/>
             <input  class="form-control" type="hidden" id="plan_id" name="plan_id"/>
             <div class="form-group">
               <label class="control-label" for="date">select Date*</label>
               <input class="form-control" type="date" id="date"/>
            </div>
             <div class="form-group">
               <label class="control-label" for="ddl_cab">Cab*</label>
                 <select id="ddl_cab" name="ddl_cab" class="form-control">
                 <option value="">- - Select Cab - -</option>
                 <?php
                 foreach ($cab_list_data as $item) {?>
                 <option value="<?php echo  $item["crm_cab_id"]; ?>"><?php echo $item["crm_cab_travels"]; ?></option>
                 <?php
                 }
                 ?>
                 </select>
               </div>
           </form>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           <button type="button" class="btn btn-primary" onclick="submitPlan()">Submit</button>
         </div>
         </div>
        </div>
      </div>
    <div class="main margin-top">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6">

                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3>Add Site Travel Plan</h3>
                  </div>

                  <div class="widget-header widget-toolbar">
                    <?php
                          if ($view_perms_list['status'] == SUCCESS) {
                              ?>
                      <form class="form-inline" method="post" id="file_search_form">
                        <select class="form-control input-sm" id="ddl_project" name="ddl_project">
                        <option value="">- - Select Project - -</option>
                        <?php
                        for($count = 0; $count < count($project_list_data); $count++)
                        {
                        ?>
                        <option value="<?php echo $project_list_data[$count]["project_id"]; ?>">
                        <?php echo $project_list_data[$count]["project_name"]; ?></option>
                        <?php
                        }?>
                        </select>
                      <input type="date" id="start_date" name="dt_start_date" value="<?php echo $start_date; ?>"  class="form-control input-sm"/>
                      <input type="date" id="end_date" name="dt_end_date" value="<?php echo $end_date; ?>"  class="form-control input-sm"/>
                      <button type="button" onclick="tableDraw()" class="btn btn-primary">Submit</button>
                      </form>
                      <?php } ?>
                    </div>
                  </div>
                <div class="widget-content" style="margin-top:15px;">
                <table id="example" class="table table-striped table-bordered display nowrap">
                  <thead>
                <tr>
                  <th>SL No</th>
                  <th>Site Visit Plan Date</th>
                  <th>Pickup Time</th>
                  <th>Enquiry Number</th>
									<th>Project</th>
                  <th>Name</th>
                  <th>Mobile</th>
                  <th>STM</th>
                  <th>Pickup Location</th>
                  <th>Confirmation Status</th>
                  <th>Drive Type</th>
									<th>Site Visit Planned By</th>
                  <th>Site Visit</th>
                </tr>
                </thead>
                  </tbody>
                </table>
              </div>
            </div>
              <!-- /widget-content -->
            </div>
            <!-- /widget -->

            </div>
            <!-- /widget -->
          </div>
          <!-- /span6 -->
        </div>
        <!-- /row -->
      <!-- </div> -->
      <!-- /container -->
    <!-- </div> -->

</body>

  <div class="extra">

  	<div class="extra-inner">

  		<div class="container">

  			<div class="row">

                  </div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /extra-inner -->

  </div> <!-- /extra -->




  <div class="footer">

  	<div class="footer-inner">

  		<div class="container">

  			<div class="row">

      			<div class="span12">
      				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
      			</div> <!-- /span12 -->

      		</div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /footer-inner -->

  </div> <!-- /footer -->
</html>
