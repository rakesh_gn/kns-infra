<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
$dbConnection = get_conn_handle();

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Easy set variables
     */

    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
  $aColumns = array(
    'project_management_master_id',
    'project_master_name'
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "project_management_master_id";

   /* DB table to use */
   $sTable = 'project_list';

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP server-side, there is
     * no need to edit below this line
     */

    /*
     * Local functions
     */
    function fatal_error($sErrorMessage = '')
    {
        header($_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error');
        die($sErrorMessage);
    }

    /*
     * Paging
     */
    $sLimit = "";
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = "LIMIT ".intval($_GET['iDisplayStart']).", ".
            intval($_GET['iDisplayLength']);
    }

    /*
     * Ordering
     */
    $sOrder = "";
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = "ORDER BY";
        for ($i=0 ; $i<intval($_GET['iSortingCols']) ; $i++) {
            if ($_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true") {
                $sOrder .= "`".$aColumns[ intval($_GET['iSortCol_'.$i]) ]."` ".
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
        }

        $sOrder = substr_replace($sOrder, "", -2);
        if ($sOrder == "ORDER BY") {
            $sOrder = "";
        }
    }

    /*
     * Filtering
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here, but concerned about efficiency
     * on very large tables, and MySQL's regex functionality is very limited
     */
   $sWhere = "";
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
        $sWhere = "WHERE (";
        for ($i=0 ; $i<count($aColumns) ; $i++) {
            $sWhere .= "`".$aColumns[$i]."` LIKE '%".($_GET['sSearch'])."%' OR ";
        }
        $sWhere = substr_replace($sWhere, "", -3);
        $sWhere .= ")";
    }

    $sQuery = "
		SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
		FROM   $sTable
		$sWhere
    $sOrder
		$sLimit
		";
  $statement = $dbConnection->prepare($sQuery);
  $statement -> execute();
  $rResult = $statement -> fetchAll();

    /* Data set length after filtering */
    $sQuery = "SELECT FOUND_ROWS() as iFilteredTotal";
  $statement = $dbConnection->prepare($sQuery);
  $statement -> execute();
  $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];

    /* Total data set length */
    $sQuery = "
		SELECT COUNT(`".$sIndexColumn."`) as iTotal
		FROM   $sTable
	";
  $statement = $dbConnection->prepare($sQuery);
  $statement -> execute();
  $iTotal = $statement -> fetch()['iTotal'];

    /*
     * Output
     */
    $output = array(
        "draw" => intval($_GET['draw']),
        "iTotalRecords" => $iTotal,
        "iTotalDisplayRecords" => $iFilteredTotal,
        "aaData" => $rResult,
    "where" => $dbConnection
    );
    echo json_encode($output);
