//globalization variable for data rendaring
var table;
//data render function
function redrawTable() {
  table.fnDraw();
}

function exportData() {
  var project_id = $("#project_id").val();
  if (project_id == "") {
    alert("select project to continue");
  } else {
    $('#hd_project_id').val(project_id);
    $('#formImport')[0].action = 'budget_planning_history_excel.php';
    $('#formImport')[0].submit();
  }
}

function colorizeCell() {
  if (arguments[4] < 11) {
    $(arguments[0]).addClass('blue');
  } else if (arguments[4] > 10) {
    $(arguments[0]).addClass('red');
  }
}

$(document).ready(function() {
  get_process($('#project_id').val());
  get_process_task($('#project_id').val(), $('#ddl_task_id').val());

  function createToolTip(str, length) {
    var substr = (str.length <= length) ? str : str.substr(0, length) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 2
    },
    scrollX: true,
    ajax: 'datatable/project_budget_planning_history_datatable.php',
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.search_process = $('#search_process').val();
      aoData.search_task = $('#search_task').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    scrollY: 600,
    scrollCollapse: true,
    fixedHeader: true,
    processing: true,
    searchDelay: 1500,
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'project_master_name'
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.project_process_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.project_task_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if (data['budget_history_road_id'] == "0") {
            return 'No Roads';
          }
          return data['project_site_location_mapping_master_name'];
        }
      },
      {
        orderable: false,
        data: 'budget_history_manpower',
        createdCell: colorizeCell
      },
      {
        orderable: false,
        data: 'budget_history_machine',
        createdCell: colorizeCell
      },
      {
        orderable: false,
        data: 'budget_history_contract',
        createdCell: colorizeCell
      },
      {
        orderable: false,
        data: 'budget_history_material',
        createdCell: colorizeCell
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.budget_history_remarks, 20);
        }
      },
      {
        orderable: false,
        data: function(data) {
          return `<a target=_blank href=documents/${data.budget_history_file}>${data.budget_history_file}</a>`
        }
      },
      {
        orderable: false,
        data: 'user_name'
      },
      {
        orderable: true,
        data: function(data) {
          return moment(data.budget_history_changed_on).format("DD/MM/YYYY");
        }
      },
    ]
  });
  $("#project_id").change(function() {
    var project_id = $(this).val();
    get_process(project_id);
  })
  $("#search_process").change(function() {
    var project_id = $("#project_id").val();
    var process_id = $(this).val();
    get_process_task(project_id, process_id);
  })
});

function get_process(project_id) {
  $.ajax({
    url: 'ajax/project_get_process.php',
    data: {
      project_id: project_id
    },
    dataType: 'json',
    success: function(response) {
      $("#search_process").empty();
      $("#search_process").append("<option value=''>Select Process</option>");
      for (var i = 0; i < response.length; i++) {
        var id = response[i]['process_master_id'];
        var name = response[i]['process_name'];
        $("#search_process").append("<option value='" + id + "'>" + name + "</option>");
        var selectedVal = $('#ddl_process_id').val();
        $('#search_process option').map(function() {
          if ($(this).val() == selectedVal) return this;
        }).attr('selected', 'selected');
      }
    }
  })
}

function get_process_task(project_id, process_id) {
  $.ajax({
    url: 'ajax/project_get_task_data.php',
    data: {
      project_id: project_id,
      process_id: process_id
    },
    dataType: 'json',
    success: function(response) {
      $("#search_task").empty();
      $("#search_task").append("<option value=''>Select Task</option>");
      for (var i = 0; i < response.length; i++) {
        if (i == response.length - 1) {
          var id = response[i]['project_task_master_id'];
          var name = response[i]['project_task_master_name'];
        } else {
          if (response[i]['project_task_master_name'] == response[i + 1]['project_task_master_name'])
            continue;
        }
        var id = response[i]['project_task_master_id'];
        var name = response[i]['project_task_master_name'];
        $("#search_task").append("<option value='" + id + "'>" + name + "</option>");
        var selectedVal = $('#ddl_task_id').val();
        $('#search_task option').map(function() {
          if ($(this).val() == selectedVal) return this;
        }).attr('selected', 'selected');
      }
    }
  })
}