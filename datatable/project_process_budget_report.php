<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
     session_start();
   $base = $_SERVER['DOCUMENT_ROOT'];
   include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
   $dbConnection = get_conn_handle();
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
        $user = $_SESSION["loggedin_user"];
  $aColumns = array(
    'project_master_name',
    'project_id',
    'project_process_master_name',
    'project_process_master_id',
    'project_plan_process_id',
    'project_process_master_order'
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "project_id";

   /* DB table to use */
   $sTable = $_GET['table'];

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */

	/*
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	/*
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}


	/*
	 * Ordering
	 */
   $sOrder = " order by `project_process_master_order` asc";


	/*
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
            $sWhere = "";
            if($sWhere == "") {
              $sWhere .= "Where `project_plan_process_active`=1 AND `project_id` IN (select `project_user_mapping_project_id` from `project_user_mapping`
              where `project_user_mapping_user_id`= $user  and `project_user_mapping_active` = 1)";
            }
               if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
             	{
             		$sWhere = "WHERE (";
             		for ( $i=0 ; $i<count($aColumns) ; $i++ )
             		{
             			$sWhere .= "`".$aColumns[$i]."` LIKE '%".( $_GET['sSearch'] )."%' OR ";
             		}
             		$sWhere = substr_replace( $sWhere, "", -3 );
             		$sWhere .= ") AND `project_plan_process_active`=1 AND `project_id` IN (select `project_user_mapping_project_id` from `project_user_mapping` where `project_user_mapping_user_id`= $user  and `project_user_mapping_active` = 1)";
             	}

             	/* Individual column filtering */
             	for ( $i=0 ; $i<count($aColumns) ; $i++ )
             	{
             		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
             		{
             			if ( $sWhere == "" )
             			{
             				$sWhere = "WHERE ";
             			}
             			else
             			{
             				$sWhere .= " AND ";
             			}
             			$sWhere .= "`".$aColumns[$i]."` LIKE '%".($_GET['sSearch_'.$i])."%' ";
             		}
             	}
              if(isset($_GET['project_id']) && $_GET['project_id'] != '') {
                $sWhere .= " AND `project_id` = ". $_GET['project_id'];
              }
	/*
	 * SQL queries
	 * Get data to display
	 */
   $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
    FROM $sTable
    $sWhere
    $sOrder
    $sLimit
    ";
   $statement = $dbConnection->prepare($sQuery);
   $statement -> execute();
   $rResult = $statement -> fetchAll();

 // get 'iFilteredTotal'
   $statement = $dbConnection->prepare("SELECT FOUND_ROWS() as iFilteredTotal");
   $statement -> execute();
   $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];

 // get 'iTotal' Total data set length
   $statement = $dbConnection->prepare("SELECT COUNT(`".$sIndexColumn."`) as iTotal FROM $sTable");
   $statement -> execute();
   $iTotal = $statement -> fetch()['iTotal'];

  /*
   * Output
   */
  $output = array(
    // print_r($iFilteredTotal) exit;
    "sEcho" => $sQuery,
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
  );

   foreach($rResult as $aRow){
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      if ( $aColumns[$i] == "version" )
      {
        /* Special output formatting for 'version' column */
        $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
      }
      else if ( $aColumns[$i] != ' ' )
      {
        /* General output */
        $row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
      }
    }
     $row[0] = 'index';
    $output['aaData'][] = $row;
     $output['where'] = $sWhere;
  }
  echo json_encode( $output );
 ?>
