<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
     session_start();
   $base = $_SERVER['DOCUMENT_ROOT'];
   include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
   $dbConnection = get_conn_handle();
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
  $user = $_SESSION["loggedin_user"];
  $role = $_SESSION["loggedin_role"];
  $aColumns = array(
    'name',
    'crm_site_visit_plan_date',
    'cell',
    'project_id',
    'project_name',
    'enquiry_number',
    'crm_site_visit_plan_id',
    'crm_site_visit_plan_time',
    'assignee',
    'assignee_id',
    'assigner',
    'assigner_id',
    'crm_site_visit_pickup_location',
    'crm_site_visit_plan_confirmation',
    'crm_site_visit_plan_status',
    'crm_site_visit_status',
    'crm_site_visit_plan_drive',
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "crm_site_visit_plan_id";

   /* DB table to use */
   $sTable = $_GET['table'];

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */

	/*
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	/*
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}


	/*
	 * Ordering
	 */
	$sOrder = "";
	if ( isset( $_GET['order'][0]["column"] ) )
	{
		$sOrder = "ORDER BY ";
		for ( $i=0 ; $i<count($_GET['order']) ; $i++ )
		{
				$sOrder .= "`".$aColumns[ intval( $_GET['order'][$i]["column"] ) ]."` ".
				($_GET['order'][$i]["dir"]=='asc' ? 'asc' : 'desc') .", ";
		}

		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	/*
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
  if($sWhere == "" && ($role!=1 && $role!=5)) {
      $sWhere .= "WHERE `assigner_id`= $user AND `crm_site_visit_plan_status`=1 AND `crm_site_visit_plan_confirmation`= 1 AND `crm_site_visit_status`!='Planned'";
               }
               else{
                 $sWhere .= "WHERE `crm_site_visit_plan_status`=1 AND `crm_site_visit_plan_confirmation`= 1 AND `crm_site_visit_status`!='Planned'";
               }

               if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
             	{
             		$sWhere = "WHERE (";
             		for ( $i=0 ; $i<count($aColumns) ; $i++ )
             		{
             			$sWhere .= "`".$aColumns[$i]."` LIKE '%".( $_GET['sSearch'] )."%' OR ";
             		}
                $sWhere = substr_replace( $sWhere, "", -3 );
                if($role!=1 && $role!=5){
                  $sWhere .= ") AND `assigner_id`= $user AND `crm_site_visit_plan_status`=1 AND `crm_site_visit_plan_confirmation`= 1 AND `crm_site_visit_status`!='Planned'";
                }
                else{
                $sWhere .= ") AND `crm_site_visit_plan_status`=1 AND `crm_site_visit_plan_confirmation`= 1 AND `crm_site_visit_status`!='Planned'";
              }
             	}

             	/* Individual column filtering */
             	for ( $i=0 ; $i<count($aColumns) ; $i++ )
             	{
             		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
             		{
             			if ( $sWhere == "" )
             			{
             				$sWhere = "WHERE ";
             			}
             			else
             			{
             				$sWhere .= " AND ";
             			}
             			$sWhere .= "`".$aColumns[$i]."` LIKE '%".($_GET['sSearch_'.$i])."%' ";
             		}
             	}



    if(isset($_GET['project_id']) && $_GET['project_id'] != '') {
      $sWhere .= " AND `project_id` = ". $_GET['project_id'];
    }

    if(isset($_GET['start_date']) && $_GET['start_date'] != '') {
      $sWhere .= " AND `crm_site_visit_plan_date` >= '". $_GET['start_date']."'";
    }

    if(isset($_GET['end_date']) && $_GET['end_date'] != '') {
      $sWhere .= " AND `crm_site_visit_plan_date` <= '". $_GET['end_date']."'";
    }
	/*
	 * SQL queries
	 * Get data to display
	 */
   $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
    FROM $sTable
    $sWhere
    $sOrder
    $sLimit
    ";
    // print_r($sQuery); exit;
   $statement = $dbConnection->prepare($sQuery);
   $statement -> execute();
   $rResult = $statement -> fetchAll();
   $statement = $dbConnection->prepare("SELECT FOUND_ROWS() as iFilteredTotal");
   $statement -> execute();
   $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];

  /*
   * Output
   */
  $output = array(
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array(),
    "query"=>$sQuery,
    "role"=>$role
  );
   foreach($rResult as $aRow){
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      if ( $aColumns[$i] == "version" )
      {
        /* Special output formatting for 'version' column */
        $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
      }
      else if ( $aColumns[$i] != ' ' )
      {
        /* General output */
        $row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
      }
    }
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
 ?>
