var table;
var columnsMapping = {
  '2': 'project',
  '3': 'machine',
  '4': 'machine_name',
  '5': 'payment_mode',
  '6': 'instrument_details',
  '7': 'remarks',
  '8': 'payment_added_on',
  '9': 'payment_added_by',
}

function tableDraw() {
  table.draw(false);
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
  }
  return '<span class="' + classname + '">Loading..</span>';
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue
  }
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      width: "1%",
      "data": "project_machine_vendor_master_name"
    },
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        return data.project_payment_machine_bata_bill_no;
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.stock_company_master_name;
        }
        if (data.stock_company_master_name == null) {
          return '';
        }
        return createToolTip(data.stock_company_master_name);
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_payment_machine_bata;
        }
        return `₹ ${data.project_payment_machine_bata}`
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_payment_machine_from_date).format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_payment_machine_to_date).format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a target = _blank href=project_machine_bata_print.php?machine_payment_id=${data.project_payment_machine_id}>
          <span class="glyphicon glyphicon-Print"></span></a>`;
        }
        return '***';
      }
    },
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_issued_machine_bata_payment_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "weekly_machine_payment_list";
      aoData.search_vendor = $('#search_vendor').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, data, index) {
      if ($('#search_vendor').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $.ajax({
        url: 'ajax/get_machine_details.php',
        data: "payment_id=" + data.project_payment_machine_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'project' + '_' + index).html(createToolTip(response.project));
          $('span.' + 'machine' + '_' + index).html(createToolTip(response.machine));
          $('span.' + 'machine_name' + '_' + index).html(createToolTip(response.machine_name));
        }
      });

      $.ajax({
        url: 'ajax/get_machine_bata_payment_details.php',
        data: "payment_id=" + data.project_payment_machine_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'payment_mode' + '_' + index).html(createToolTip(response.payment_mode));
          $('span.' + 'instrument_details' + '_' + index).html(createToolTip(response.instrument_details));
          $('span.' + 'remarks' + '_' + index).html(createToolTip(response.remarks));
          $('span.' + 'payment_added_on' + '_' + index).html(moment(response.payment_added_on).format("DD/MM/YYYY"));
          $('span.' + 'payment_added_by' + '_' + index).html(createToolTip(response.payment_added_by));
        }
      });
    },
    fixedColumns: {
      leftColumns: 2,
      rightColumns: 4
    },
    scrollX: true,
    "columns": columns,
  });
});