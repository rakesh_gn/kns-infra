var table;

function tableDraw() {
  table.fnDraw();
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function project_delete_man_power_rate_master(rate_id) {
  var ok = confirm("Are you sure you want to Delete?");
  if (ok) {

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          tableDraw();
        }
      }
    }

    xmlhttp.open("POST", "project_master_delete_man_power_rate.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("rate_id=" + rate_id + "&action=0");
  }
}

var columns = [{
    className: 'noVis',
    "orderable": false,
    "width": "1%",
    "data": function() {
      return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
    }
  },
  {
    "orderable": false,
    "data": "project_manpower_agency_name"
  },
  {
    "orderable": false,
    data: function(data, type) {
      if (data.project_man_power_type_id == '1') {
        return 'Male';
      } else if (data.project_man_power_type_id == '2') {
        return 'Female';
      }
      return 'Mason';
    }
  },
  {
    "orderable": false,
    "data": "project_man_power_rate_cost_per_hours"
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return data.project_man_power_rate_remarks;
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return data.added_by;
    }
  },
  {
    "orderable": false,
    data: function(data, type) {
      return moment(data.project_man_power_rate_added_on).
      format('DD-MM-YYYY');
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return data.updated_by;
    }
  },
  {
    "orderable": false,
    data: function(data, type) {
      if (data.project_man_power_rate_updated_on == '0000-00-00 00:00:00') {
        return '';
      }
      return moment(data.project_man_power_rate_updated_on).
      format('DD-MM-YYYY');
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (window.permissions.delete) {
        return `<a style="color:red" href="#" onclick=project_delete_man_power_rate_master(${data.project_man_power_rate_master_id})>
        <span class="red glyphicon glyphicon-trash"></span></a>`;
      }
      return '***';
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (window.permissions.edit) {
        return `<a target = _blank href=project_master_edit_man_power_rate.php?rate_id=${data.project_man_power_rate_master_id}>
        <span class="glyphicon glyphicon-pencil"></span></a>`;
      }
      return '***';
    }
  },
];

$(document).ready(function() {
  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_master_man_power_rate_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_manpower_vendor_rate";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    scrollX: true,
    "columns": columns,
  });
});