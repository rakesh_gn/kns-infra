var table;
var columnsMapping = {
  '2': 'project',
  '9': 'issued_amount',
  '10': 'balance_amount'
}

function tableDraw() {
  table.draw();
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function contract_deposit_payment(vendor_id) {
  $('#ajax_loading').show();
  $.ajax({
    url: "tools/contract_payment_list/getVenderPaymentDepositDetails.php?search_vendor=" + vendor_id,
    success: function(result) {
      $('#ajax_loading').hide();
      $("#details").html(result);
      $("#myModal").modal();
      $('#example').DataTable({
        dom: 'Bfrtip',
        buttons: [
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
        ]
      });
    }
  });
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    if (columnsMapping[meta.col] == 'project') {
      return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
    }
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue
  }
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_manpower_agency_name"
    },
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        return data.project_actual_contract_payment_deposit_bill_no;
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type == 'export') {
          return data.stock_company_master_name;
        }
        return createToolTip(data.stock_company_master_name);
      }
    },

    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_actual_contract_payment_from_date).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_actual_contract_payment_to_date).
        format('DD-MM-YYYY');
      }
    },
    {
      orderable: false,
      data: `project_actual_contract_payment_amount`
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_actual_contract_deposit_amount;
        }
        return `₹ ${data.project_actual_contract_deposit_amount}`
      }
    },
    commonCellDefinition,
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_actual_contract_payment_remarks;
        }
        return createToolTip(data.project_actual_contract_payment_remarks);
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.approved_by;
        }
        return createToolTip(data.approved_by);
      }
    },
    {
      "orderable": true,
      data: function(data, type) {
        return moment(data.project_actual_contract_payment_approved_on).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a target = _blank href=project_contract_weekly_print.php?contract_payment_id=${data.project_actual_contract_payment_id}&deposit_status=${data.project_actual_contract_payment_deposit_status}>
          <span class="glyphicon glyphicon-Print"></span></a>`;
        }
        return '***';
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a href=# onclick=contract_deposit_payment(${data.project_manpower_agency_id})>
          <span style=font-size:16px>₹</span></a>`;
        }
        return '***';
      }
    }
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_accept_contract_deposit_payment_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "weekly_contract_payment_list";
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, full, index) {
      if ($('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $.ajax({
        url: 'ajax/get_contract_deposit_details.php',
        data: "payment_id=" + full.project_actual_contract_payment_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'project' + '_' + index).html(createToolTip(response.project));
          $('span.' + 'issued_amount' + '_' + index).html(response.issued_amount.toFixed(2));
          $('span.' + 'balance_amount' + '_' + index).html((full.project_actual_contract_deposit_amount - response.issued_amount).toFixed(2));
        }
      });
    },
    fixedColumns: {
      leftColumns: 2,
      rightColumns: 2
    },
    scrollX: true,
    "columns": columns,
  });
});