$(document).ready(function() {

  var table = $('#example').dataTable({
    "sScrollX": "100%",
    "sScrollXInner": "110%",
    "bScrollCollapse": true,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": 'datatable/persona_list_datatable.php',
    "iDisplayLength": 10,
    "sScrollY": "50%",
    "bInfo": true,
    "iTabIndex": 1,
    "lengthChange": false,
    dom: 'Bfrtip',
  buttons: [
    {
      extend: 'excelHtml5',
      text: '<i class="fa fa-file-excel-o"></i> Excel',
      titleAttr: 'Export to Excel',
      title: 'User Basic Details List',
      exportOptions: { orthogonal: 'export' }
    },
  ],
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search...",
      "infoFiltered": ""
    },
    "fnServerParams": function(aoData) {
      aoData.push({
        "name": "table",
        "value": "persona_list"
      });
      aoData.push({
        "name": "search_department",
        "value": getVars('search_department')
      });
    },
    "aaSorting": [
      // [1, 'desc']
    ],
    "fnDrawCallback": function(oSettings) {
      /* Need to redo the counters if filtered or sorted */
      if (oSettings.bSorted || oSettings.bFiltered || oSettings.iDraw > 1) {
        for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
          $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + oSettings._iDisplayStart + 1);
        }
      }
    },
    "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": [0]
      },
      {
        "bSortable": false,
        "aTargets": [1],
        "mData": `employee_code`
      },
      {
        "bSortable": false,
        "aTargets": [2],
        "mData": 'user_name'
      },
      {
        "bSortable": false,
        "aTargets": [3],
        "mData": `general_task_department_name`
      },
      {
        "bSortable": false,
        "aTargets": [4],
        "mData": `designation_name`
      },
      {
        "bSortable": false,
        "aTargets": [5],
        "mRender": function(data, type, full) {
          return moment(full['dob']).format('DD-MM-YYYY');
        }
      },
      {
        "bSortable": false,
        "aTargets": [6],
        "mData": `uan`
      },
      {
        "bSortable": false,
        "aTargets": [7],
        "mData": `persona_pf_number`
      },
      {
        "bSortable": false,
        "aTargets": [8],
        "mData": `esi_number`
      },
      {
        "bSortable": false,
        "aTargets": [9],
        "mData": `persona_pan_number`
      },
      {
        "bSortable": false,
        "aTargets": [10],
        "mData": `persona_aadhar_number`
      },
      {
        "bSortable": false,
        "aTargets": [11],
        "mData": `persona_user_phone`
      },
      {
        "bSortable": false,
        "aTargets": [12],
        "mRender": function(data, type, full) {
          return full['persona_gender'];
        }
      },
      {
        "bSortable": false,
        "aTargets": [13],
        "mData": `persona_maritial_status`
      },
      {
        "bSortable": false,
        "aTargets": [14],
        "mRender": function(data, type, full) {
          return full['persona_blood_group']
        }
      },
      {
        "bSortable": false,
        "aTargets": [15],
        "mData": `persona_present_address`
      },
      {
        "bSortable": false,
        "aTargets": [16],
        "mData": `persona_present_city`
      },
      {
        "bSortable": false,
        "aTargets": [17],
        "mData": `persona_present_state`
      },
      {
          "bSortable": false,
          "aTargets": [18],
          "mData": `persona_present_pincode`
        },
      {
        "bSortable": false,
        "aTargets": [19],
        "mData": `persona_permanent_address`
      },
      {
        "bSortable": false,
        "aTargets": [20],
        "mData": `persona_permanent_city`
      },
      {
        "bSortable": false,
        "aTargets": [21],
        "mData": `persona_permanent_state`
      },
      {
        "bSortable": false,
        "aTargets": [22],
        "mData": `persona_permanent_pincode`
      },
      {
        "bSortable": false,
        "aTargets": [23],
        "mData": `persona_company_phone`
      },
      {
        "bSortable": false,
        "aTargets": [24],
        "mData": `persona_voice_limit`
      },
      {
        "bSortable": false,
        "aTargets": [25],
        "mData": `persona_sim_number`
      },
      {
        "bSortable": false,
        "aTargets": [26],
        "mData": `persona_data_limit`
      },
      {
        "bSortable": false,
        "aTargets": [27],
        "mData": `persona_insurance_name`
      },
      {
        "bSortable": false,
        "aTargets": [28],
        "mData": `persona_insurance_number`
      },
      {
        "bSortable": false,
        "aTargets": [29],
        "mData": `persona_insurance_amount`
      },
      {
        "bSortable": false,
        "aTargets": [30],
        "mRender": function(data, type, full) {
          return '<a href="#" id="edit">Edit</a>';
        }
      },
    ]
  });

  $('#example tbody').on('click', 'tr', function(event) {

    if (event.target.tagName == 'A' && event.target.id == 'edit') {

      var rowData = table.api().row(this).data();
      go_to_persona_edit_list(rowData['persona_user_id']);
    }
    // else if (event.target.tagName == 'A' && event.target.id == 'delete') {
    //   project_delete_task_actual_manpower(table, table.api().row(this).data()['project_task_actual_manpower_id']);
    //
    // } else if (event.target.tagName == 'A' && event.target.id == 'ok') {
    //
    //   var rowData = table.api().row(this).data();
    //   project_check_task_actual_manpower(table, rowData['project_task_actual_manpower_id'], rowData['project_task_actual_manpower_task_id']);
    //
    // } else if (event.target.tagName == 'A' && event.target.id == 'approve') {
    //   var rowData = table.api().row(this).data();
    //   project_approve_task_actual_manpower(table, rowData['project_task_actual_manpower_id'], rowData['project_task_actual_manpower_task_id']);
    // }

  });
});

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  console.log('getVars ',  key, element);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}

// set search filters' as hidden value
function setVars(department_id) {
console.log('setVars ', department_id);

  if(department_id) {
    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("id", 'search_department');
    hiddenField1.setAttribute("value", department_id);
    document.body.appendChild(hiddenField1);
  }
}

function go_to_persona_edit_list(user_id) {
  var form = document.createElement("form");
  form.setAttribute("method", "get");
  form.setAttribute("action", "edit_persona.php");
  form.setAttribute("target", "blank")

  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "persona_user_id");
  hiddenField1.setAttribute("value", user_id);

  form.appendChild(hiddenField1);

  document.body.appendChild(form);
  form.submit();
}
