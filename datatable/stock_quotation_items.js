var table;
// function toggleSelection(ele) {
//   $("input.input-checkbox").prop("checked", ele.checked);
// }

function redrawTable() {
  table.fnDraw(false);
}

$(document).ready(function() {
  function createToolTip(str, length) {
    var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    ajax: 'datatable/stock_quotation_items_approval_datatable.php',
    fnRowCallback: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      // get the stock qty
      $.ajax({
        url: 'ajax/stock_get_material_stock.php',
        data: "project_id=" + full['project_id'] + "&material_id=" + full['material_id'],
        dataType: 'json',
        success: function(stock_response) {
          $('span#stock_qty_' + index).html(stock_response);
        }
      });
    },
    fnServerParams: function(aoData) {
      aoData.search_status = $('#search_status').val();
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'system_quote_no'
      },
      {
        orderable: false,
        data: 'stock_material_name'
      },
      {
        orderable: false,
        data: 'stock_material_code'
      },
      {
        orderable: false,
        data: 'quote_qty'
      },
      {
        orderable: false,
        data: 'user_name'
      },
      {
        orderable: true,
        data: function(data) {
          return moment(data.added_on).format('DD/MM/YYYY');
        }
      },
      {
      orderable: false,
      data : function(data, type, full) {
          return '<a href="stock_master_vendor_item_mapping_list.php?indent_id='+data.indent_id+'&indent_item_id='+data.material_id+' " target=_blank> <span class="glyphicon glyphicon-eye-open"></span></a>';
    }
    },
      {
        orderable: false,
        data: function(data, type, full) {
          if ($("#search_status").val() == 'Approved' || !window.permissions.edit) {
            return "***";
          }
          return `<a href="#"><span id="approve" class="glyphicon glyphicon-ok"></span></a>`;
        }
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if ($("#search_status").val() == 'Rejected' || !window.permissions.edit) {
            return "***";
          }
          return `<a style=color:red href="#"><span id=reject class="glyphicon glyphicon-remove-circle"></span></a>`;
        }
      },
    ]
  });
  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.api().row(this).data();
    if (event.target.tagName == 'SPAN' && event.target.id == 'approve') {
      approve_or_reject_quotation(rowData.quote_id,rowData.quote_project,'Approved');
    }
    else {
      if (event.target.tagName == 'SPAN' && event.target.id == 'reject')
        approve_or_reject_quotation(rowData.quote_id,rowData.quote_project,'Rejected');
    }
  });
});

function approve_or_reject_quotation(quotation_id,projectid,status)
{if(status == "Approved")
{
  var display_status = " Approve"
}
else if(status == "Rejected")
{
  display_status = " Reject";
}
	var ok = confirm("Are you sure you want to" + display_status+ "?")
	{
		if (ok)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 redrawTable();
					}
				}
			}
			xmlhttp.open("POST", "stock_approve_quotation.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("quotation_id=" + quotation_id + "&projectid=" + projectid + "&action="+ status);
		}
	}
}
