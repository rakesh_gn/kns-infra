<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: kns_grn_engineer_inspection_list.php
CREATED ON	: 30-Sep-2016
CREATED BY	: Lakshmi
PURPOSE     : List of grn engineer inspection for customer withdrawals
*/

/*
TBD: 
*/

/* DEFINES - START */
define('STOCK_MOVED_PAYMENT_LIST_FUNC_ID','208');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',STOCK_MOVED_PAYMENT_LIST_FUNC_ID,'2','1');

	// Query String Data
	if(isset($_REQUEST["grn_item_id"]))
	{
		$grn_item_id = $_REQUEST["grn_item_id"];
	}
	else
	{
		$grn_item_id = "-1";
	}
	
	if(isset($_POST['eng_ins_search_submit']))
	{
		$search_status = $_POST['search_status'];
	}
	else
	{
		$search_status = 'Pending';
	}
	
	// Temp data
	// Check if moved to payments
	$stock_grn_accounts_search_data = array();
	$moved_payment_list = i_get_stock_sum_grn_accounts($stock_grn_accounts_search_data);	
	if($moved_payment_list['status'] == SUCCESS)
	
	{	
		$moved_payment_list_data = $moved_payment_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Accounts Approval </title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Accounts Approval</h3>
            </div>
			<!--<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="eng_ins_search_form" action="stock_move_to_payments_list.php">			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_status">			  
			  <option value="Pending" <?php if($search_status == "Pending"){?> selected <?php } ?>>Pending</option>
			  <option value="Completed" <?php if($search_status == "Completed"){?> selected <?php } ?>>Completed</option>			  
			  </select>			  
			  </span>
			  <input type="submit" name="eng_ins_search_submit" />
			  </form>			  
            </div>-->
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				  
				    <th>SL No</th>
					<th>PO No</th>
					<th>PO Date</th>
					<th>value</th>
					<th>Additional Cost</th>
					<th>Total</th>
					<th style="text-align:center;" colspan='2'>Action</th>		
				</tr>
				</thead>
				<tbody>							
				<?php
				if($moved_payment_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$additional_amt = 0;
					for($count = 0; $count < count($moved_payment_list_data); $count++)
					{				
						$sl_no++;
						$additional_amt = $moved_payment_list_data[$count]["total_additional_amt"];
						var_dump($additional_amt);
						// Check if moved to payments
						$stock_grn_accounts_search_data = array("order_id"=>$moved_payment_list_data[$count]["stock_purchase_order_id"]);
						$account_approved_list = i_get_stock_grn_accounts($stock_grn_accounts_search_data);
						if($account_approved_list['status'] == SUCCESS)
						{	
							$total = 0;
							$account_approved_list_data = $account_approved_list["data"];
							$additional_amt = 0;
							for($acount = 0 ; $acount < count($account_approved_list_data) ; $acount++)
							{
								$additional_amt	 = $additional_amt + $account_approved_list_data[$acount]["stock_grn_accounts_approval_additional_amount"];
								$stock_purchase_order_items_search_data = array('item'=>$account_approved_list_data[$acount]['stock_grn_item'],
								'order_id'=>$account_approved_list_data[$acount]["stock_purchase_order_id"]);
								$po_sresult = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);
								if($po_sresult["status"] == SUCCESS)
								{
									for($pcount =0 ;$pcount < count ($po_sresult["data"]) ; $pcount++)
									{	
										$item_cost   = $po_sresult['data'][$pcount]["stock_purchase_order_item_cost"];
										$stock_grn_engineer_inspection_search_data = array("grn_item_id"=>$account_approved_list_data[$acount]["stock_grn_accounts_approval_item_id"]);
										$grn_inspection_list =  i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);
										if($grn_inspection_list["status"] == SUCCESS)
										{
											$accepted_qty = $grn_inspection_list["data"][0]["stock_grn_engineer_inspection_approved_quantity"];
											$trans_charge = $grn_inspection_list["data"][0]["stock_purchase_order_transportation_charges"];
										}
										$item_value   = $item_cost*$accepted_qty;
										$tax_rate     = $po_sresult['data'][$pcount]["stock_tax_type_master_value"];
										$tax_value    = ($po_sresult['data'][$pcount]["stock_tax_type_master_value"] * $item_value)/100;
										$excise_value = ($po_sresult['data'][$pcount]["stock_purchase_order_item_excise_duty"] * $item_value)/100;
										$total_value  = $item_value + $tax_value + $excise_value + $trans_charge;
										$total = $total + $total_value;
										
									}
								}
							}
						}
						// Check if moved to payments
						$stock_management_payment_search_data = array("po_id"=>$moved_payment_list_data[$count]["stock_purchase_order_id"]);
						$stock_paymemt_managment_list = i_get_stock_management_payment($stock_management_payment_search_data);
						if($stock_paymemt_managment_list["status"] != SUCCESS)
						{
								
						?>
						<tr>
						<td><?php echo $sl_no; ?></td>
						<td><?php echo $moved_payment_list_data[$count]["stock_purchase_order_number"]; ?></td>
						<td><?php echo date("d-M-Y",strtotime($moved_payment_list_data[$count]["stock_purchase_order_added_on"])); ?></td>
						<!--<td><?php echo $moved_payment_list_data[$count]["stock_unit_name"]; ?></td>
						<td><?php echo $moved_payment_list_data[$count]["stock_vendor_name"]; ?></td>-->
						<!--<td><?php echo $moved_payment_list_data[$count]["stock_grn_no"]; ?></td>
						<td><?php echo date("d-M-Y",strtotime($moved_payment_list_data[$count]["stock_grn_added_on"])); ?></td>
						<!--<td><?php echo $moved_payment_list_data[$count]["stock_grn_item_inward_quantity"]; ?></td>
						<td><?php echo $accepted_qty; ?></td>-->
						<td><?php echo $total; ?></td>
						<td><?php echo $additional_amt; ?></td>
						<td><?php echo ($total + $additional_amt); ?></td>
						<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px"
						href="stock_move_to_management_list.php?po_id=<?php echo $moved_payment_list_data[$count]["stock_purchase_order_id"]; ?>">Move to Management</a><?php } ?></td>
						</tr>
						<?php
						}					
					}
				}
				else
				{
				?>
				<td colspan="15">No grn engineer inspection added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>	
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function delete_grn_engineer_inspection(inspection_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "stock_grn_engineer_inspection_list.php";
					}
				}
			}

			xmlhttp.open("POST", "stock_delete_grn_engineer_inspection.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("inspection_id=" + inspection_id + "&action=0");
		}
	}	
}
function go_to_edit_grn_engineer_inspection(inspection_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "stock_edit_grn_engineer_inspection.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","inspection_id");
	hiddenField1.setAttribute("value",inspection_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>