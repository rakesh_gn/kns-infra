<?php

/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID', '413');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '3', '1');
    $delete_perms_list 	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '6', '1');
?>

    <script>
      window.permissions = {
        view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
        ok: <?php echo ($ok_perms_list['status'] == 0)? 'true' : 'false'; ?>,
        delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
        aprove: <?php echo ($approve_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      }
    </script>

       <?php
    $alert_type = -1;
    $alert = "";

    $search_project = "";

    if (isset($_POST["search_project"])) {
        $search_project   = $_POST["search_project"];
    // set here
    } else {
        $search_project = "";
    }

  	// if($project_list["status"] == SUCCESS)
  	// {
  	// 	$project_list_data = $project_list["data"];
  	// }
if($approve_perms_list["status"]==SUCCESS){
  $stock_project_search_data = array("active"=>"1");
  $project_list = i_get_project_list($stock_project_search_data);
}
else{
  $stock_project_search_data = array("active"=>"1","user_id"=>$user);
  $project_list = i_get_project_mapping_list($stock_project_search_data);
}
if($project_list["status"] == SUCCESS)
  	{
  		$project_list_data = $project_list["data"];
  	}
  } else {
  	header("location:login.php");
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Stock Project User Mapping List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
    <script type="text/javascript" src="datatable/project_purchase_user_mapping_list_datatable.js?<?php echo time(); ?>"></script>
    <link href="./css/style.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
  </head>
  <body>
    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>
    <div class="main margin-top">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6" style="width:100%;">

                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3>Stock Project User Mapping List</h3>
                    <a style="margin-right:10px;float:right;color:#19bc9c;" target="_blank" href="project_add_project_purchase_user_mapping.php"><span>Stock Project Add User Mapping List</span></a>
                  </div>

                  <div class="widget-header widget-toolbar">
                    <?php
                          if ($view_perms_list['status'] == SUCCESS) {
                              ?>
                      <form class="form-inline" method="post" id="file_search_form">
                        <input type="hidden" id="view_perm" value="<?= $view_perms_list['status'];?>">
                        <input type="hidden" id="edit_perm" value="<?= $edit_perms_list['status'];?>">
                      <select id="search_project" name="search_project" class="form-control">
                        <option value="">- - Select Project - -</option>
      								  <?php
                                        for ($project_count = 0; $project_count < count($project_list_data); $project_count++) {
                                            ?>
      								  <option value="<?php echo $project_list_data[$project_count]["stock_project_id"]; ?>" <?php if ($search_project == $project_list_data[$project_count]["stock_project_id"]) {
                                                ?> selected="selected" <?php
                                            } ?>><?php echo $project_list_data[$project_count]["stock_project_name"]; ?></option>
      								  <?php
                                        } ?>
      								  </select>
                      <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="widget-content">
                    <table id="example" width=100% class="table table-striped table-bordered display nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Project</th>
                          <th>UserName</th>
                          <th>Remarks</th>
                          <th>Added On</th>
                          <th>Added By</th>
                    </thead>
                    </tbody>
                  </table>
                </div>
                <!-- /widget-content -->

                </div>
                <!-- /widget -->

                </div>
                <!-- /widget -->
              </div>
              <!-- /span6 -->
            </div>
            <!-- /row -->
          </div>
          <!-- /container -->
</body>
</html>
