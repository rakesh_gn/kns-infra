<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_stock_history.php');



/*

PURPOSE : To add Stock History

INPUT 	: Material ID,Transaction Type,Location,Quantity,Remarks,Added By

OUTPUT 	: Message, success or failure message

BY 		: Sonakshi D

*/
function i_add_stock_history($material_id,$transaction_type,$project,$qty,$remarks,$added_by,$reference="")
{
	$stock_history_iresult =  db_add_stock_history($material_id,$transaction_type,$project,$qty,$reference,$remarks,$added_by);

	if($stock_history_iresult['status'] == SUCCESS)

	{

		$return["data"]   = "Stock History Successfully Added";

		$return["status"] = SUCCESS;	

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

	

	return $return;

}

	

/*

PURPOSE : To get Stock History list

INPUT 	: Stock history array

OUTPUT 	: Stock History List or Error Details, success or failure message

BY 		: Sonakshi D

*/

function i_get_stock_history($stock_history_search_data)

{

	$stock_history_sresult = db_get_stock_history_list($stock_history_search_data);

	

	if($stock_history_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $stock_history_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No material stock added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To get Sum Of Stock History list

INPUT 	: Stock history array

OUTPUT 	: Stock History List or Error Details, success or failure message

BY 		: Sonakshi D

*/

function i_get_sum_stock_history($stock_history_search_data)

{

	$stock_sum_history_sresult =  db_get_sum_stock_history_list($stock_history_search_data);

	

	if($stock_sum_history_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $stock_sum_history_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No material stock added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Material Master List

INPUT 	: Material ID, Material Master update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_update_stock_history($material_id,$stock_history_search_data)

{

	$stock_history_sresult = db_update_material_list($material_id,$stock_history_search_data);

	

	if($stock_history_sresult['status'] == SUCCESS)

	{

		$return["data"]   = "Stock History Successfully Updated";

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}

/*

PURPOSE : To get Stock History list

INPUT 	: Stock history array

OUTPUT 	: Stock History List or Error Details, success or failure message

BY 		: Sonakshi D

*/

function i_get_stock_closing_stock($material_id,$project,$start_date,$end_date)
{
	/* OPENING STOCK - START */
	$stock_history_search_data = array("material_id"=>$material_id,"transaction_type"=>"Opening","project"=>$project,"start_date"=>$start_date,"end_date"=>$end_date);
	$stock_history_sresult = db_get_closing_stock($stock_history_search_data);

	if($stock_history_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$opening_stock =  $stock_history_sresult['data'][0]['total_qty'];
    }
    else
    {
	    $opening_stock = 0;
    }
	/* OPENING STOCK - END */
	
	/* ISSUED STOCK - START */
	$stock_history_search_data = array("material_id"=>$material_id,"transaction_type"=>"Issue","project"=>$project,"start_date"=>$start_date,"end_date"=>$end_date);
	$stock_history_sresult = db_get_closing_stock($stock_history_search_data);

	if($stock_history_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$issued_stock =  $stock_history_sresult['data'][0]['total_qty'];
    }
    else
    {
	    $issued_stock = 0;
    }
	/* ISSUE STOCK - END */
	
	/* PURCHASE STOCK - START */
	$stock_history_search_data = array("material_id"=>$material_id,"transaction_type"=>"Purchase","project"=>$project,"start_date"=>$start_date,"end_date"=>$end_date);
	$stock_history_sresult = db_get_closing_stock($stock_history_search_data);

	if($stock_history_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$purchased_qty =  $stock_history_sresult['data'][0]['total_qty'];
    }
    else
    {
	    $purchased_qty = 0;
    }
	/* PURCHASE STOCK - END */
	
	/* TRANSFER IN STOCK - START */
	$stock_history_search_data = array("material_id"=>$material_id,"transaction_type"=>"Transfer In","project"=>$project,"start_date"=>$start_date,"end_date"=>$end_date);
	$stock_history_sresult = db_get_closing_stock($stock_history_search_data);

	if($stock_history_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$transfered_in_stock =  $stock_history_sresult['data'][0]['total_qty'];
    }
    else
    {
	    $transfered_in_stock = 0;
    }
	/* TRANSFER IN STOCK - END */
	
	/* TRANSFER OUT STOCK - START */
	$stock_history_search_data = array("material_id"=>$material_id,"transaction_type"=>"Transfer Out","project"=>$project,"start_date"=>$start_date,"end_date"=>$end_date);
	$stock_history_sresult = db_get_closing_stock($stock_history_search_data);

	if($stock_history_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$transfered_out_stock =  $stock_history_sresult['data'][0]['total_qty'];
    }
    else
    {
	    $transfered_out_stock = 0;
    }
	/* TRANSFER OUT STOCK - END */
	$closing_stock = $opening_stock + $purchased_qty + $transfered_in_stock - $issued_stock - $transfered_out_stock ;
	
	return $closing_stock;

}