-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 21, 2019 at 04:26 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.1.16-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_legal`
--

-- --------------------------------------------------------

--
-- Structure for view `po_status_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `po_status_list`  AS  select `spo`.`stock_purchase_order_id` AS `stock_purchase_order_id`,`spo`.`stock_purchase_order_number` AS `purchase_order_number`,`spoi`.`stock_purchase_order_item_id` AS `stock_poitems_id`,`spoi`.`stock_purchase_order_item` AS `stock_po_material_id`,`sg`.`stock_grn_no` AS `grn_no`,`spoi`.`stock_purchase_order_item_quantity` AS `po_qty`,`sgi`.`stock_grn_item_inward_quantity` AS `inward_quantity`,`sgi`.`stock_grn_item` AS `grn_item`,`sgei`.`stock_grn_engineer_inspection_approved_quantity` AS `approved_quantity`,`sgei`.`stock_grn_engineer_inspection_rejected_quantity` AS `rejected_quantity`,`sgei`.`stock_grn_engineer_inspection_additional_quantity` AS `additional_quantity` from (((((((`stock_purchase_order_items` `spoi` left join `users` `u` on((`u`.`user_id` = `spoi`.`stock_purchase_order_item_approved_by`))) join `stock_purchase_order` `spo` on((`spo`.`stock_purchase_order_id` = `spoi`.`stock_purchase_order_id`))) join `stock_material_master` `smm` on((`spoi`.`stock_purchase_order_item` = `smm`.`stock_material_id`))) left join `stock_project` `sp` on((`sp`.`stock_project_id` = `spo`.`stock_purchase_order_project`))) left join `stock_grn` `sg` on((`sg`.`stock_grn_purchase_order_id` = `spoi`.`stock_purchase_order_id`))) left join `stock_grn_items` `sgi` on((`sgi`.`stock_grn_id` = `sg`.`stock_grn_id`))) left join `stock_grn_engineer_inspection` `sgei` on((`sgei`.`stock_grn_engineer_inspection_grn_item_id` = `sgi`.`stock_grn_item_id`))) ;

--
-- VIEW  `po_status_list`
-- Data: None
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
