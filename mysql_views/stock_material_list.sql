-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 21, 2019 at 04:25 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.1.16-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_legal`
--

-- --------------------------------------------------------

--
-- Structure for view `stock_material_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock_material_list`  AS  select `smm`.`stock_material_code` AS `material_code`,`smm`.`stock_material_name` AS `material_name`,`smm`.`stock_material_price` AS `material_price`,`ms`.`material_id` AS `material_id`,`ms`.`material_stock_project` AS `material_stock_project_id`,`ms`.`material_stock_quantity` AS `material_stock_quantity` from (`material_stock` `ms` left join `stock_material_master` `smm` on((`smm`.`stock_material_id` = `ms`.`material_id`))) ;

--
-- VIEW  `stock_material_list`
-- Data: None
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
