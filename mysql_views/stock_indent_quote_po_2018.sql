-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 23, 2019 at 04:57 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.1.16-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_new`
--

-- --------------------------------------------------------

--
-- Structure for view `stock_indent_quote_po_2018`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock_indent_quote_po_2018`  AS  select `sii`.`stock_indent_item_id` AS `stock_indent_item_id`,`sii`.`stock_indent_item_material_id` AS `stock_indent_item_material_id`,`sii`.`stock_indent_id` AS `stock_indent_id`,`si`.`stock_indent_no` AS `indent_no`,`si`.`stock_indent_project` AS `indent_project_id`,`sii`.`stock_indent_item_quantity` AS `stock_indent_item_quantity`,`sii`.`stock_indent_item_uom` AS `stock_indent_item_uom`,`sii`.`stock_indent_item_active` AS `stock_indent_item_active`,`sii`.`stock_indent_item_status` AS `stock_indent_item_status`,`sii`.`stock_indent_item_quote_status` AS `stock_indent_item_quote_status`,`sii`.`stock_indent_item_required_date` AS `stock_indent_item_required_date`,`sii`.`stock_indent_item_requested_date` AS `stock_indent_item_requested_date`,`sii`.`stock_indent_item_remarks` AS `stock_indent_item_remarks`,`sii`.`stock_indent_item_approved_by` AS `stock_indent_item_approved_by`,`sii`.`stock_indent_item_approved_on` AS `stock_indent_item_approved_on`,`sii`.`stock_indent_item_added_by` AS `stock_indent_item_added_by`,`sii`.`stock_indent_item_added_on` AS `stock_indent_item_added_on`,`smm`.`stock_material_id` AS `stock_material_id`,`smm`.`stock_material_code` AS `stock_material_code`,`smm`.`stock_material_name` AS `stock_material_name`,`smm`.`stock_material_unit_of_measure` AS `stock_material_unit_of_measure`,`smm`.`stock_material_price` AS `stock_material_price`,`smm`.`stock_material_price_as_on` AS `stock_material_price_as_on`,`summ`.`stock_unit_id` AS `stock_unit_id`,`summ`.`stock_unit_name` AS `stock_unit_name`,`sqc`.`stock_quotation_id` AS `stock_quotation_id`,`sqc`.`stock_quote_no` AS `stock_quote_no`,`sqc`.`stock_quotation_indent_id` AS `stock_quotation_indent_id`,`sqc`.`stock_quotation_amount` AS `stock_quotation_amount`,`sqc`.`stock_quotation_no` AS `stock_quotation_no`,`sqc`.`stock_quotation_vendor` AS `stock_quotation_vendor`,`sqc`.`stock_quotation_quantity` AS `stock_quotation_quantity`,`sqc`.`stock_quotation_po_qty` AS `stock_quotation_po_qty`,`sqc`.`stock_quotation_location` AS `stock_quotation_location`,`sqc`.`stock_quotation_project` AS `stock_quotation_project`,`sqc`.`stock_quotation_received_date` AS `stock_quotation_received_date`,`sqc`.`stock_quotation_status` AS `stock_quotation_status`,`sqc`.`stock_quotation_doc` AS `stock_quotation_doc`,`sqc`.`stock_quotation_remarks` AS `stock_quotation_remarks`,`sqc`.`stock_quotation_active` AS `stock_quotation_active`,`sqc`.`stock_quotation_approved_on` AS `stock_quotation_approved_on`,`sqc`.`stock_quotation_added_on` AS `stock_quotation_added_on`,`u`.`user_id` AS `user_id`,`u`.`user_name` AS `indent_added_by`,`au`.`user_name` AS `indent_approved_by`,`spo`.`stock_purchase_order_number` AS `po_number`,`spo`.`stock_purchase_order_added_on` AS `po_date`,`svm`.`stock_vendor_name` AS `vendor_name`,`spo`.`stock_purchase_order_id` AS `po_id`,`sp`.`stock_project_name` AS `indent_project_name`,`spoi`.`stock_purchase_order_item_status` AS `item_status`,`spoi`.`stock_purchase_order_item_id` AS `po_items_id`,`spoi`.`stock_purchase_order_total_amount` AS `po_total_value`,`spoi`.`stock_grn_qty` AS `stock_grn_qty`,`spoi`.`stock_grn_remaning_qty` AS `stock_grn_remaning_qty` from ((((((((((`stock_indent_items` `sii` join `stock_indent` `si` on((`si`.`stock_indent_id` = `sii`.`stock_indent_id`))) left join `stock_quotation_compare` `sqc` on((`sqc`.`stock_quotation_indent_id` = `sii`.`stock_indent_id`))) join `users` `u` on((`sii`.`stock_indent_item_added_by` = `u`.`user_id`))) join `stock_material_master` `smm` on((`sii`.`stock_indent_item_material_id` = `smm`.`stock_material_id`))) join `stock_unit_measure_master` `summ` on((`summ`.`stock_unit_id` = `smm`.`stock_material_unit_of_measure`))) left join `users` `au` on((`au`.`user_id` = `sii`.`stock_indent_item_approved_by`))) left join `stock_purchase_order` `spo` on((`spo`.`stock_indent_id` = `sii`.`stock_indent_id`))) left join `stock_vendor_master` `svm` on((`svm`.`stock_vendor_id` = `spo`.`stock_purchase_order_vendor`))) left join `stock_project` `sp` on((`sp`.`stock_project_id` = `si`.`stock_indent_project`))) join `stock_purchase_order_items` `spoi` on(((`spo`.`stock_purchase_order_id` = `spoi`.`stock_purchase_order_id`) and (`spoi`.`stock_purchase_order_item` = `smm`.`stock_material_id`)))) where (`sii`.`stock_indent_item_added_on` <= '2018-12-30') ;

--
-- VIEW  `stock_indent_quote_po_2018`
-- Data: None
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
