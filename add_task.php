<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert = "";
	$alert_type = -1; // No alert	

	// Query String
	if(isset($_GET["process"]))
	{
		$process_id = $_GET["process"];		
	}	
	else
	{
		$process_id = "";
	}
	
	if(isset($_GET["bprocess"]))
	{
		$bprocess_id = $_GET["bprocess"];		
	}	
	else
	{
		$bprocess_id = "";
	}	

	if(isset($_POST["add_tasks_submit"]))
	{
		// Capture all form data
		$process_id  = $_POST["hd_process_id"];
		$bprocess_id = $_POST["hd_bprocess_id"];
		$start_date  = $_POST["hd_start_date"];
		$file_id     = $_POST["hd_file_id"];
		$assigned_to = $_POST["hd_assigned_to"];
		
		$task_type_array = $_POST["cb_task_type"];
		
		if($process_id != "")
		{
			// Process Data
			$process_sresult = i_get_legal_process_plan_details($process_id);
			if($process_sresult["status"] == SUCCESS)
			{
				$assignee = $process_sresult["data"]["process_plan_legal_assigned_to"];
			}
			else
			{
				$alert_type = 0;
				$alert      = "Invalid User for process";
				$assignee   = "";
			}
		}
		else if($bprocess_id != "")
		{
			// Process Data
			$legal_bulk_search_data = array("bulk_process_id"=>$bprocess_id);
			$process_sresult = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);
			if($process_sresult["status"] == SUCCESS)
			{
				$assignee = $process_sresult["data"][0]["legal_process_assigned_to"];
			}
			else
			{
				$alert_type = 0;
				$alert      = "Invalid User for process";
				$assignee   = "";
			}
		}		
		
		$previous_end_date = $start_date;
		for($task_type_count = 0; $task_type_count < count($task_type_array); $task_type_count++)
		{
			$task_type = $task_type_array[$task_type_count];
			
			$task_type_list = i_get_task_type_list('','','',$task_type);
			
			// Planned End Date
			$planned_end_date = date("Y-m-d",strtotime($previous_end_date.' + '.$task_type_list["data"][0]["task_type_maximum_expected_duration"].' days'));
			// Planned Start Date
			$planned_start_date  = $previous_end_date;
			
			// Add task			
			$task_iresult = i_add_task_plan_legal($task_type,$process_id,$bprocess_id,$planned_end_date,$planned_start_date,$assignee);
			$previous_end_date = $planned_end_date;
		}
		
		if($task_iresult["status"] == SUCCESS)
		{
			$user_data = i_get_user_list($assigned_to,'','','');
			$manager_data = i_get_user_list($user_data["data"][0]["user_manager"],'','','');
		
			// Configure mail properties
			$to = $user_data["data"][0]["user_email_id"]."||".$manager_data["data"][0]["user_email_id"];
			$name = $user_data["data"][0]["user_name"];
			$cc = "venkataramanaiah@knsgroup.in";
			$subject = "KNS Legal - New task assigned to you";
			$message = "Hello $name <br /><br /> You have been assigned a project.<br /><br />
			Review your tasks at http://iweavesolutions.com/kns/Legal/pending_task_list.php?process=".$process_id;
			$attachment = "";
			$mail_result = send_email($to,$cc,$subject,$message,$attachment);					
		}
		
		if($process_id != "")
		{
			// If there are previous processes on this file, don't do anything. Else, update file details
			$process_plan_list = i_get_legal_process_plan_list('',$file_id,'','','','','','','','0','');		
			
			if($process_plan_list["status"] == SUCCESS)
			{
				if(count($process_plan_list["data"]) > 1)
				{
					// Do nothing
				}
				else
				{
					$first_task_sresult = i_get_task_plan_list('','',$process_id,'','','','','asc');				
					
					if($first_task_sresult["status"] == SUCCESS)
					{				
						$file_update = db_update_file($file_id,$process_id,'',$first_task_sresult["data"][0]["task_plan_legal_id"],'');
					}
					else
					{
						// Do nothing
					}
				}
			}
			else		
			{
				// Do nothing
			}
		}
		else if($bprocess_id != '')
		{
			// Update file status in a loop
			$legal_bulk_files_search_data = array("legal_bulk_process_files_process_id"=>$bprocess_id);
			$bulk_process_files = i_get_bulk_legal_process_plan_files($legal_bulk_files_search_data);
			
			if($bulk_process_files['status'] == SUCCESS)
			{
				for($bf_count= 0; $bf_count < count($bulk_process_files['data']); $bf_count++)
				{
					// If there are previous processes on this file, don't do anything. Else, update file details
					$legal_bulk_files_search_data = array("process_status"=>'0',"legal_bulk_process_files_id"=>$bulk_process_files['data'][$bf_count]['legal_bulk_process_file_id']);
					$bulk_process_file_details = i_get_bulk_legal_process_plan_files($legal_bulk_files_search_data);	
					
					if($bulk_process_file_details["status"] == SUCCESS)
					{
						if(count($bulk_process_file_details["data"]) > 1)
						{
							// Do nothing
						}
						else
						{
							$first_task_sresult = i_get_task_plan_list('','','','','','','','asc',$bprocess_id);											
							if($first_task_sresult["status"] == SUCCESS)
							{				
								$file_update = db_update_file_for_bulk($bulk_process_files['data'][$bf_count]['legal_bulk_process_file_id'],$bprocess_id,'',$first_task_sresult["data"][0]["task_plan_legal_id"],'');
							}
							else
							{
								// Do nothing
							}
						}
					}
					else		
					{
						// Do nothing
					}
				}
			}
			else
			{
				$alert_type = 0;
				$alert      = 'There were no files for this process';
			}
		}
		
		header("location:pending_project_list.php");
	}
	
	if($process_id != '')
	{
		// Get project details
		$project_list = i_get_legal_process_plan_list($process_id,'','','','','','','','','','');
		if($project_list["status"] == SUCCESS)
		{
			$project_list_data    = $project_list["data"];
			$project_process_type = $project_list_data[0]["process_plan_type"];
			$project_process_name = $project_list_data[0]["process_name"];
			$project_file_number  = $project_list_data[0]["file_number"];
			$project_start_date   = $project_list_data[0]["process_plan_legal_start_date"];
			$project_file_id      = $project_list_data[0]["process_plan_legal_file_id"];
			$project_assigned_to  = $project_list_data[0]["process_plan_legal_assigned_to"];
		}
		else
		{
			$alert = $alert."Alert: ".$project_list["data"];
			$alert_type = 0; // Failure
		}
	}
	
	if($bprocess_id != '')
	{
		// Get project details
		$legal_bulk_search_data = array("bulk_process_id"=>$bprocess_id);
		$project_list = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);		
		if($project_list["status"] == SUCCESS)
		{
			$project_list_data    = $project_list["data"];
			$project_process_type = $project_list_data[0]["legal_bulk_process_type"];
			$project_process_name = $project_list_data[0]["process_name"];
			
			// Get associated files
			$project_file_number = '';
			$legal_bulk_files_search_data = array("legal_bulk_process_files_process_id"=>$bprocess_id);
			$process_files_result = i_get_bulk_legal_process_plan_files($legal_bulk_files_search_data);			
			for($count = 0; $count < count($process_files_result['data']); $count++)
			{
				$project_file_number = $project_file_number.$process_files_result['data'][$count]["file_number"].',';
			}			
			$project_file_number  = trim($project_file_number,',');
			$project_start_date   = $project_list_data[0]["legal_process_start_date"];
			$project_file_id      = '';
			$project_assigned_to  = $project_list_data[0]["legal_process_assigned_to"];
		}
		else
		{
			$alert = $alert."Alert: ".$project_list["data"];
			$alert_type = 0; // Failure
		}
	}

	// Get list of tasks for this process type	
	$task_type_list = i_get_task_type_list('',$project_process_type,'');
	if($task_type_list["status"] == SUCCESS)
	{
		$task_type_list_data = $task_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$task_type_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Task</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Process: <?php echo $project_process_name; ?>&nbsp;&nbsp;&nbsp;&nbsp;File No: <?php echo $project_file_number; ?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add tasks to this process</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_tasks" class="form-horizontal" method="post" action="add_task.php">
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
								<input type="hidden" name="hd_bprocess_id" value="<?php echo $bprocess_id; ?>" />
								<input type="hidden" name="hd_start_date" value="<?php echo $project_start_date; ?>" />
								<input type="hidden" name="hd_file_id" value="<?php echo $project_file_id; ?>" />
								<input type="hidden" name="hd_assigned_to" value="<?php echo $project_assigned_to; ?>" />
									<fieldset>
										
										<div class="control-group">											
											<label class="control-label" for="process_type">Choose Tasks</label>
											<div class="controls">
												<?php 
												for($count = 0; $count < count($task_type_list_data); $count++)
												{?>
												<input type="checkbox" name="cb_task_type[]" value="<?php echo $task_type_list_data[$count]["task_type_id"]; ?>" />&nbsp;&nbsp;&nbsp;<?php echo $task_type_list_data[$count]["task_type_name"]; ?>&nbsp;&nbsp;&nbsp;<br /><br />
												<?php
												}?>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->                                                                                                                       <br />										
											
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="add_tasks_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
