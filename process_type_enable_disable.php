<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 8th July 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
/* INCLUDES - END */

// Session Data
$user = $_SESSION["loggedin_user"];
$role = $_SESSION["loggedin_role"];

/* QUERY STRING DATA - START */
$type   = $_GET["process_type"];
$action = $_GET["action"];
/* QUERY STRING DATA - END */

// Enable or disable user according to the command
$process_update_result = i_enable_disable_process_type($type,$action);

header("location:process_type_list.php");
?>