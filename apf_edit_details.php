<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 10-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('APF_EDIT_DETAILS_FUNC_ID','334');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$delete_perms_list = i_get_user_perms($user,'',APF_EDIT_DETAILS_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET["apf_id"]))
	{
		$apf_id = $_GET["apf_id"];
	}
	else
	{
		$apf_id = "";
	}


	// Capture the form data
	if(isset($_POST["edit_apf_details_submit"]))
	{
		$apf_id                 = $_POST["hd_apf_id"];
		$planned_start_date     = $_POST["planned_start_date"];
		$planned_end_date       = $_POST["planned_end_date"];
		$remarks                = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($planned_start_date != "") && ($planned_end_date != ""))
		{
			$apf_details_update_data = array("planned_start_date"=>$planned_start_date,"planned_end_date"=>$planned_end_date);
			$apf_details_iresult = i_update_apf_details($apf_id,$apf_details_update_data);
			
			if($apf_details_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
		    
				header("location:apf_details_list.php?apf_id=$apf_id");
			}
			else
			{
				$alert 		= $apf_details_iresult["data"];
				$alert_type = 0;	
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
		
  // Get Village Master modes already added
	$village_master_list = i_get_village_list('');
	if($village_master_list['status'] == SUCCESS)
	{
		$village_master_list_data = $village_master_list["data"];
	}
	else
	{
		$alert = $village_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Bank Master modes already added
	$apf_bank_master_search_data = array();
	$bank_master_list = i_get_apf_bank_master($apf_bank_master_search_data);
	if($bank_master_list['status'] == SUCCESS)
	{
		$bank_master_list_data = $bank_master_list["data"];
	}
	else
	{
		$alert = $bank_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Master already added
	$apf_project_master_search_data = array("active"=>'1');
	$apf_project_master_list = i_get_apf_project_master($apf_project_master_search_data);
	if($apf_project_master_list['status'] == SUCCESS)
	{
		$apf_project_master_list_data = $apf_project_master_list['data'];
    }
	else
	{
		$alert = $apf_project_master_list["data"];
		$alert_type = 0;
	}
	
	// Get APF Details already added
	$apf_details_search_data = array("active"=>'1',"apf_id"=>$apf_id);
	$apf_details_list = i_get_apf_details($apf_details_search_data);
	if($apf_details_list['status'] == SUCCESS)
	{
		$apf_details_list_data = $apf_details_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>APF - Edit Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>APF - Edit Details</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">APF Edit Details</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="survey_edit_details_form" class="form-horizontal" method="post" action="apf_edit_details.php">
								<input type="hidden" name="hd_apf_id" value="<?php echo $apf_id; ?>" />
									<fieldset>										
																
											<div class="control-group">											
											<label class="control-label" for="ddl_project">Project*</label>
											<div class="controls">
												<select name="ddl_project" required disabled class="span6">
												<option value="">- - Select Project - -</option>
												<?php
												for($count = 0; $count < count($apf_project_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $apf_project_master_list_data[$count]["apf_project_master_id"]; ?>" <?php if($apf_project_master_list_data[$count]["apf_project_master_id"] == $apf_details_list_data[0]["apf_details_project"]){ ?> selected="selected" <?php } ?>><?php echo $apf_project_master_list_data[$count]["apf_project_master_name"]; ?></option>
												<?php
												}
												?>	
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="ddl_village">Village*</label>
											<div class="controls">
												<select name="ddl_village" required disabled class="span6">
												<option value="">- - Select Village - -</option>
												<?php
												for($count = 0; $count < count($village_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $village_master_list_data[$count]["village_id"]; ?>" <?php if($village_master_list_data[$count]["village_id"] == $apf_details_list_data[0]["apf_details_village"]){ ?> selected="selected" <?php } ?>><?php echo $village_master_list_data[$count]["village_name"]; ?></option>
												<?php
												}
												?>	
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_bank">Bank*</label>
											<div class="controls">
												<select name="ddl_bank" required disabled class="span6">
												<option value="">- - Select Bank - -</option>
												<?php
												for($count = 0; $count < count($bank_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $bank_master_list_data[$count]["apf_bank_master_id"]; ?>" <?php if($bank_master_list_data[$count]["apf_bank_master_id"] == $apf_details_list_data[0]["apf_details_bank"]){ ?> selected="selected" <?php } ?>><?php echo $bank_master_list_data[$count]["apf_bank_master_name"]; ?></option>
												<?php
												}
												?>	
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="planned_start_date">Start Date*</label>
											<div class="controls">
											<?php if($apf_details_list_data[0]["apf_details_planned_start_date"] != '0000-00-00')
											{ 
												if($delete_perms_list['status'] == SUCCESS)
												{
													$disabled = '';
												}
												else
												{
													$disabled = ' disabled';
												}
											}
											else
											{
												$disabled = '';
											}
											?>
											<input type="date" class="span6" name="planned_start_date" placeholder="Date" value="<?php echo $apf_details_list_data[0]["apf_details_planned_start_date"]; ?>"<?php echo $disabled; ?>>											
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="planned_end_date">End Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="planned_end_date" placeholder="Date" value="<?php echo $apf_details_list_data[0]["apf_details_planned_end_date"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks" value="<?php echo $apf_details_list_data[0]["apf_details_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_apf_details_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
