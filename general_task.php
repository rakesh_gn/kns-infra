<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 30 Oct 2017
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Role dropdown should be from database
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	/* DATA INITIALIZATION - END */	
	
	// Get General Tasks List
	$gen_task_plan_list = i_get_gen_task_plan_list('','','','','2017-10-03','','','','','','3');
	
	if($gen_task_plan_list["status"] == SUCCESS)
	{	
		for($count = 0; $count < count($gen_task_plan_list['data']); $count++)
		{			
			$delete_status = $gen_task_plan_list['data'][$count]["general_task_delete_status"];
			if($delete_status != 1)
			{
				$task_details = $gen_task_plan_list['data'][$count]["general_task_details"];
				var_dump($task_details.'<br><br>');
			}
		}
	}
}
else
{
	header("location:login.php");
}

function task_mail_get_content($assigned_to,$assigned_by,$planned_end_date)
{
	$subject = 'Task Assigned By '.$assigned_by.'. Deadline: <strong>TODAY</strong> ('.date('d-M-Y',strtotime($planned_end_date)).')';
	$message = 'Dear '.$assigned_to.',<br /><br />Today is the last day to complete the task assigned to you by '.$assigned_by.'. The details are as follows:<br /><br />Task Details: '.$details.'<br />Planned End Date: '.date('d-M-Y',strtotime($planned_end_date)).'<br />From: '.$assigned_by.'<br />To: '.$name;
	$attachment = '';
	
	$return = array('subject'=>$subject,'message'=>$message,'attachment'=>$attachment);
	return $return;
}

function task_mail_get_recipients($assignee_email,$assigner_email,$assignee_manager)
{
	/* TO - START */
	$to = $assignee_email;
	/* TO - END */
	
	/* CC - START */
	$cc = array();
	$cc_count = 0;
	$assigner_perms = i_get_user_email_perms('','','1','2','1');
	
	if($assigner_perms['status'] == SUCCESS)
	{
		$cc[$cc_count] = $assigner_email;
		$cc_count++;
	}			
	$manager_perms = i_get_user_email_perms('','','1','3','1');
	if($manager_perms['status'] == SUCCESS)
	{
		$manager_sresult = i_get_user_list($assignee_manager,'','','','1','');
		$cc[$cc_count] = $manager_sresult['data'][0]['user_email_id'];
		$cc_count++;
	}
	$admin_perms = i_get_user_email_perms('','','1','4','1');
	if($admin_perms['status'] == SUCCESS)
	{
		for($ucount = 0; $ucount < count($admin_perms['data']); $ucount++)
		{
			$admin_sresult = i_get_user_list($admin_perms['data'][$ucount]['user_id'],'','','','1','');
			$cc[$cc_count] = $admin_sresult['data'][0]['user_email_id'];
			$cc_count++;
		}
	}
	/* CC - END */
	
	$return['to'] = $to;
	$return['cc'] = $cc;
	
	return $return;
}
?>