<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 16-Oct-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET["survey_id"]))
	{
		$survey_id = $_GET["survey_id"];
	}
	else
	{
		$survey_id = "";
	}
	
	if(isset($_GET["village"]))
	{
		$village = $_GET["village"];
	}
	else
	{
		$village = "";
	}

	// Capture the form data
	if(isset($_POST["survey_assign_project_submit"]))
	{
		$survey_id            = $_POST["hd_survey_id"];
		$village              = $_POST["hd_village"];
		$project              = $_POST["ddl_project"];
		
		// Check for mandatory fields
		if(($project != ""))
		{
			$survey_details_update_data = array("project"=>$project,"village"=>$village);
			$survey_details_iresult = i_update_survey_details($survey_id,$survey_details_update_data);
			
			if($survey_details_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
		    
				header("location:survey_details_list.php?survey_id=$survey_id&village=$village");
			}
			else
			{
				$alert 		= $survey_details_iresult["data"];
				$alert_type = 0;	
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get Survey Project already added
	$survey_project_master_search_data = array("active"=>'1');
	$survey_project_master_list = i_get_survey_project_master($survey_project_master_search_data);
	if($survey_project_master_list['status'] == SUCCESS)
	{
		$survey_project_master_list_data = $survey_project_master_list['data'];
	}
	 else 
	{
		$alert = $survey_project_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Survey Details already added
	$survey_details_search_data = array("active"=>'1',"survey_id"=>$survey_id,"village"=>$village);
	$survey_details_list = i_get_survey_details($survey_details_search_data);
	if($survey_details_list['status'] == SUCCESS)
	{
		$survey_details_list_data = $survey_details_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey - Assign Projecct</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Survey - Assign Project</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Survey - Assign Project</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="survey_assign_project_form" class="form-horizontal" method="post" action="survey_assign_project.php">
								<input type="hidden" name="hd_survey_id" value="<?php echo $survey_id; ?>" />
								<input type="hidden" name="hd_village" value="<?php echo $village; ?>" />
									<fieldset>										
																
											<div class="control-group">											
											<label class="control-label" for="ddl_project">Project*</label>
											<div class="controls">
												<select class="span6" name="ddl_project" required>
												<option value="">- - Select Project - -</option>
												<?php
												for($count = 0; $count < count($survey_project_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $survey_project_master_list_data[$count]["survey_project_master_id"]; ?>"><?php echo $survey_project_master_list_data[$count]["survey_project_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="survey_assign_project_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
