<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_approved_booking_list.php
CREATED ON	: 04-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Approved Booking List
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["project"]))
	{
		$project = $_GET["project"];
	}
	else
	{
		$project = "";
	}
	
	if(isset($_GET["reason"]))
	{
		$reason = $_GET["reason"];
	}
	else
	{
		$reason = "";
	}
	
	if(isset($_GET["status"]))
	{
		$status = $_GET["status"];
	}
	else
	{
		$status = "";
	}
	
	if(isset($_GET["dt_start_date"]))
	{
		$start_date = $_GET["dt_start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(isset($_GET["dt_end_date"]))
	{
		$end_date = $_GET["dt_end_date"];
	}
	else
	{
		$end_date = "";
	}

	
	// Temp data
	$alert      = "";
	$alert_type = -1;
	if($reason == '0')
	{
		$delayed_payment_list = i_get_pending_nondelayed_payment_list($project,$status,'');
	}
	else if($reason != '')
	{
		$delayed_payment_list = i_get_delayed_payment_list($project,$status,$reason,'');
	}
	else
	{
		$reason_list = i_get_delayed_payment_list($project,$status,$reason,'');
		
		$collection_list      = i_get_pending_nondelayed_payment_list($project,$status,'');
		if($reason_list['status'] == SUCCESS)
		{
			if($collection_list['status'] == SUCCESS)
			{
				$delayed_payment_list['data'] = array_merge($collection_list['data'],$reason_list['data']);
			}
			else
			{
				$delayed_payment_list['data'] = $reason_list['data'];
			}
			
			$delayed_payment_list['status'] = SUCCESS;
		}
		else
		{
			$delayed_payment_list = $collection_list;
		}

		array_sort_conditional($delayed_payment_list['data'],"sort_on_site_no");
	}	
		
	if($delayed_payment_list["status"] == SUCCESS)
	{
		$delayed_payment_list_data = $delayed_payment_list["data"];
	}
	
	// Project List
	$project_list = i_get_project_list('','1');
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Schedule Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:50px;">
              <h3>Collection Schedule Report&nbsp;&nbsp;&nbsp;Pending Amount: <span id="total_pending_amount"><i>Calculating</i></span></h3><span style="float:right; padding-right:25px;"><a href="crm_collections_details.php?project=<?php echo $project; ?>&reason=<?php echo $reason; ?>&status=<?php echo $status; ?>" target="_blank"><b>Collection Details</b></a></span>
            </div>			
            <!-- /widget-header -->
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="get" id="schedule_filter" action="crm_schedule_report.php">
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_start_date" value="<?php echo $start_date; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_end_date" value="<?php echo $end_date; ?>" />
			  </span>			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="project">
			  <option value="">- - Select Project - -</option>
			  <?php
				for($count = 0; $count < count($project_list_data); $count++)
				{
					?>
					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php 
					if($project == $project_list_data[$count]["project_id"])
					{
					?>					
					selected="selected"
					<?php
					}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="search_schedule_submit" />
			  </span>
			  </form>			  
            </div>
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>										
					<th>SL No</th>										<th>Project</th>
					<th>Site No.</th>										
					<th>Client Name</th>
					<th>Client No</th>
					<th>Booking Date</th>
					<th>Lead Time</th>			
					<th>Latest Status</th>
					<th>Tentative Registered Date</th>					
					<th>Delay Reason</th>						
					<th>Follow Up</th>
					<th>Follow Up Date</th>
					<th>Pending Amount</th>					
					<th>Loan</th>			
					<th>Details</th>					
				</tr>
				</thead>
				<tbody>							
				<?php			
				$total_area                = 0;
				$sum_amount                = 0;
				$sum_received_amount       = 0;
				$sum_pending_amount        = 0;
				$sum_other_amount          = 0;
				$sum_other_received_amount = 0;
				$sum_other_pending_amount  = 0;
				$total_pending_amount      = 0;
				if($delayed_payment_list["status"] == SUCCESS)
				{										
					$sl_no = 0;					
					$cancelled_count = 0;
					$milestone = '';
					for($count = 0; $count < count($delayed_payment_list_data); $count++)
					{		
						// Get customer profile details, if entered
						$cust_details = i_get_cust_profile_list('',$delayed_payment_list_data[$count]["crm_booking_id"],'','','','','','','','');
						if($cust_details["status"] == SUCCESS)
						{
							if($cust_details["data"][0]["crm_customer_funding_type"] == '2')
							{
								$bank_loan = $cust_details["data"][0]["crm_bank_name"];
							}
							else if($cust_details["data"][0]["crm_customer_funding_type"] == '3')
							{
								$bank_loan = "Waiting for info";
							}
							else
							{
								$bank_loan = "Self Funding";
							}
						}
						else
						{
							$bank_loan = "";
						}
						
						// Customer Follow Up Data
						$customer_fup = i_get_payment_fup_list('',$delayed_payment_list_data[$count]["crm_booking_id"],'','','','','');
						if($customer_fup['status'] == SUCCESS)
						{
							if($customer_fup['data'][0]['crm_payment_follow_up_cust_remarks'] != '')
							{
								$fup_remarks = $customer_fup['data'][0]['crm_payment_follow_up_cust_remarks'];
							}
							else
							{
								if(count($customer_fup['data']) > 1)
								{
									$fup_remarks = $customer_fup['data'][1]['crm_payment_follow_up_cust_remarks'];
								}
								else
								{
									$fup_remarks = '';
								}
							}
							$fup_date    = date('d-M-Y',strtotime($customer_fup['data'][0]['crm_payment_follow_up_date_time']));
						}
						else
						{
							$fup_remarks = '';
							$fup_date    = '';
						}
					
						$current_status = 'Booking';
						// Current Status Data										
						if($delayed_payment_list_data[$count]["crm_booking_date"] != "0000-00-00")
						{
							$booking_date = date("d-M-Y",strtotime($delayed_payment_list_data[$count]["crm_booking_date"])); 
							$current_status = 'Booked';
							$milestone = 'REGISTRATION';
							
							$lead_data = get_date_diff($delayed_payment_list_data[$count]["crm_booking_date"],date('Y-m-d'));
						}
						else
						{
							$booking_date = "NA. Approved Date: ".date("d-M-Y",strtotime($delayed_payment_list_data[$count]["crm_booking_approved_on"])); 
							$milestone = 'REGISTRATION';
							
							$lead_data = get_date_diff($delayed_payment_list_data[$count]["crm_booking_approved_on"],date('Y-m-d'));
						}												
												
						$agreement_list = i_get_agreement_list('',$delayed_payment_list_data[$count]["crm_booking_id"],'','','','','','','','','');
						if($agreement_list["status"] == SUCCESS)
						{							
							$current_status = 'Agreement';
							$milestone = 'REGISTRATION';
						}
						else
						{							
							// Do nothing
						}											
												
						$registration_list = i_get_registration_list('',$delayed_payment_list_data[$count]["crm_booking_id"],'','','','','','','','','');
						if($registration_list["status"] == SUCCESS)
						{							
							$current_status = 'Registration';
							$milestone = 'KHATHA TRANSFER';
						}
						else
						{														
							// Do nothing
						}				
						
						// Payment Data
						$total_payment_done = 0;
						$payment_details = i_get_payment('',$delayed_payment_list_data[$count]["crm_booking_id"],'','','');
						if($payment_details["status"] == SUCCESS)
						{
							for($pay_count = 0; $pay_count < count($payment_details["data"]); $pay_count++)
							{
								$total_payment_done = $total_payment_done + $payment_details["data"][$pay_count]["crm_payment_amount"];
							}
						}
						else						
						{
							$total_payment_done = 0;
						}
						
						// Delay Reason
						$delay_reason_list = i_get_delay_reason_list($delayed_payment_list_data[$count]["crm_booking_id"],'','1');
						if($delay_reason_list['status'] == SUCCESS)
						{
							$delay_reason  = $delay_reason_list['data'][0]['crm_delay_reason_master_name'];
							$delay_remarks = $delay_reason_list['data'][0]['crm_delay_reason_remarks'];
						}
						else
						{
							$delay_reason  = 'NO REASON';
							$delay_remarks = '';
						}
						
						// Get payment schedule
						$pay_schedule_list = i_get_pay_schedule($delayed_payment_list_data[$count]["crm_booking_id"],'','','',$milestone,'date_desc');
						if($pay_schedule_list['status'] == SUCCESS)
						{
							$next_stage_date = date('d-M-Y',strtotime($pay_schedule_list['data'][0]['crm_payment_schedule_date']));
							if((strtotime($next_stage_date)) >= (strtotime(date('Y-m-d'))))
							{
								$color = 'black';
							}
							else
							{
								$color = 'red';
							}
						}						
						else
						{
							$next_stage_date = 'NO SCHEDULE';
							$color = 'red';
						}	
					
						if($delayed_payment_list_data[$count]["crm_booking_status"] == "1")
						{							
							if((($start_date == '') && ($end_date == '')) || ((strtotime($start_date) <= strtotime($next_stage_date)) && (strtotime($next_stage_date) <= strtotime($end_date))))
							{
							$total_area = $total_area + $delayed_payment_list_data[$count]["crm_site_area"];

							if($delayed_payment_list_data[$count]["crm_booking_consideration_area"] != "0")
							{
								$consideration_area = $delayed_payment_list_data[$count]["crm_booking_consideration_area"];
							}
							else
							{
								$consideration_area = $delayed_payment_list_data[$count]["crm_site_area"];
							}
							$total_payment_to_be_done = ($consideration_area * $delayed_payment_list_data[$count]["crm_booking_rate_per_sq_ft"]);
							$pending_payment = round(($total_payment_to_be_done - $total_payment_done));
							
							if($pending_payment > 10)
							{								$sl_no++;
							?>
							<tr style="color:<?php echo $color; ?>;">														
							<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>														<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["project_name"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["crm_site_no"]; ?> (<?php echo $delayed_payment_list_data[$count]["crm_site_area"].' sq. ft'; ?>)</td>
							<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["name"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["cell"]; ?></td>							
							<td style="word-wrap:break-word;"><?php echo $booking_date; ?></td>
							<td style="word-wrap:break-word;"><?php echo $lead_data['data']; ?></td>
							<td style="word-wrap:break-word;"><?php echo $current_status; ?></td>
							<td style="word-wrap:break-word;"><?php echo $next_stage_date; ?></td>
							<td style="word-wrap:break-word;"><?php echo $delay_reason; ?><?php if($delay_remarks != ''){ ?><br /><a href="#" onClick="alert('<?php echo $delay_remarks; ?>');">Details</a><?php } ?></td>							
							<td style="word-wrap:break-word;"><a href="crm_add_payment_fup.php?booking=<?php echo $delayed_payment_list_data[$count]["crm_booking_id"]; ?>#fup_table" target="_blank"><?php echo substr($fup_remarks,0,35); ?></a></td>
							<td style="word-wrap:break-word;"><?php echo $fup_date; ?></td>
							<td style="word-wrap:break-word;"><?php echo $pending_payment;							
							$total_pending_amount = $total_pending_amount + $pending_payment;
							?></td>							
							<td style="word-wrap:break-word;"><?php echo $bank_loan; ?></td>
							<td><a href="crm_customer_transactions.php?booking=<?php echo $delayed_payment_list_data[$count]["crm_booking_id"]; ?>" target="_blank">Details</a></td>							
							</tr>
							<?php
							}
							}							
						}
						else
						{
							$cancelled_count++;
						}
					}
				}
				else
				{
				?>
				<td colspan="14">No site booked yet!</td>
				<?php
				}
				?>	
				<script>				
				document.getElementById("total_pending_amount").innerHTML = '<?php echo "Rs. ".$total_pending_amount; ?>';				
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>