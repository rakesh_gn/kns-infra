<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_man_power_estimate_list.php
CREATED ON	: 07-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_REQUEST['process_id']))
	{
		$process_id = $_REQUEST['process_id'];
	}
	else
	{
		$process_id = '';
	}
	if(isset($_REQUEST['process_name']))
	{
		$process_name = $_REQUEST['process_name'];
	}
	else
	{
		$process_name = '';
	}
	if(isset($_REQUEST['project']))
	{
		$project = $_REQUEST['project'];
	}
	else
	{
		$project = '';
	}
	// Nothing

	// Get Project Delay Reason
	$delay_reason_search_data = array("process_id"=>$process_id);
	$project_delay_reason_list = i_get_project_delay_reason($delay_reason_search_data);
	if($project_delay_reason_list["status"] == SUCCESS)
	{
		$project_delay_reason_list_data = $project_delay_reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_delay_reason_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Process Delay Reason List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Process Delay Reason List</h3> <strong>Project :<?php echo $project ;?></strong>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
             <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Process</th>
					<th>Task</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Days</th>
					<th>Reason</th>
					<th>Remarks</th>					
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_delay_reason_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_delay_reason_list_data); $count++)
					{
							$sl_no++;
							$pause_date = date("d-M-Y H:i:s", strtotime($project_delay_reason_list_data[$count]["project_task_delay_reason_start_date"]));
							$end_date = $project_delay_reason_list_data[$count]["project_task_delay_reason_end_date"];
							$today = date("Y-m-d");
							if($end_date != '0000-00-00 00:00:00')
							{
								$end_date = date("d-M-Y H:i:s",strtotime($end_date));
								$pause_days = get_date_diff($pause_date,$end_date);
							}
							else
							{
								$end_date = '';
								$pause_days = get_date_diff($pause_date,$today);
							}
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $process_name; ?></td>
					<td><?php echo $project_delay_reason_list_data[$count]["project_task_master_name"]; ?></td>	
					<td><?php echo date("d-M-Y H:i:s",strtotime($project_delay_reason_list_data[$count]["project_task_delay_reason_start_date"])); ?></td>
					<td><?php echo $end_date; ?></td>
					<td><?php echo $pause_days["data"]; ?></td>
					<td><?php echo $project_delay_reason_list_data[$count]["project_reason_master_name"]; ?></td>
					<td><?php echo $project_delay_reason_list_data[$count]["project_task_delay_reason_remarks"]; ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_man_power_estimate(estimate_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_man_power_estimate_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_man_power_estimate.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("estimate_id=" + estimate_id + "&action=0");
		}
	}	
}
function go_to_project_edit_man_power_estimate(estimate_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_man_power_estimate.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","estimate_id");
	hiddenField1.setAttribute("value",estimate_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>