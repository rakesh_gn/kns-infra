<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
function get_conn_handle()
{
	$db     = 'kns_erp_live';
	$dbhost = 'localhost';
	$dbuser = 'root';
	$dbpass = '';
	$error = "";

	$db_conn_handle = new mysqli($dbhost, $dbuser, $dbpass, $db);
    if ($db_conn_handle->connect_errno) {
    //echo "Failed to connect to MySQL: (" . $db_conn_handle->connect_errno . ") " . $db_conn_handle->connect_error;
		$db_conn_handle = DB_CONN_FAILURE;
		$error = $db_conn_handle->connect_error;
	}

	return $db_conn_handle;
}

?>

<!DOCTYPE html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>DHTMLX Gantt Chart - dhtmlxGantt demo with baselines in the timeline area
	</title>
	<meta name="description"
		  content="You can add any additional custom element to the timeline area in Gantt Chart, including baselines, deadlines, and others."/>
	<meta name="keywords"
		  content="dhtmlx, ajax, javascript, library, framework, dhtmlx, web, components, gantt chart, gantt, baselines, deadlines, demo, sample, project"/>


	<script src="assets/dhtmlxgantt.js?v=4.0" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="assets/dhtmlxgantt_meadow.css?v=4.0" type="text/css" media="screen" title="no title" charset="utf-8">

	<style type="text/css" media="screen">
		html, body {
			margin: 0px;
			padding: 0px;
			height: 100%;
			overflow: hidden;
		}

		.baseline {
			position: absolute;
			border-radius: 2px;
			opacity: 0.6;
			margin-top: -7px;
			height: 12px;
			background: #ffd180;
			border: 1px solid rgb(255, 153, 0);
		}

			/* move task lines upper */
		.gantt_task_line, .gantt_line_wrapper {
			margin-top: -9px;
		}

		.gantt_side_content {
			margin-bottom: 7px;
		}

		.gantt_task_link .gantt_link_arrow {
			margin-top: -10px
		}

		.gantt_side_content.gantt_right {
			bottom: 0;
		}
		.custom_row{
			background:rgb(245, 248, 245);
		}


	</style>
</head>
<body onresize="modSampleHeight()" onload="modSampleHeight()">
<!--[if lte IE 7]>
<style type="text/css">div {
	display: none;
}</style>
<h4 style='text-align:center; font-family:Arial; margin-top:50px;'>Unfortunately dhtmlxGantt 2.0 doesn't support IE6 and
	IE7 browsers.<br>Please open these demos in different browser or in IE8+.</h3>
<![endif]-->
<script>
	function modSampleHeight() {
		var headHeight = 100;
		var sch = document.getElementById("gantt_here");
		sch.style.height = (parseInt(document.body.offsetHeight) - headHeight) + "px";


		gantt.setSizes();
	}
</script>


<div id="gantt_here" style='width:100%; height:100%;'></div>
<script type="text/javascript">

	gantt.config.task_height = 16;
	gantt.config.row_width = 6;
	gantt.config.row_height = 40;
	gantt.locale.labels.baseline_enable_button = 'Set';
	gantt.locale.labels.baseline_disable_button = 'Remove';

	gantt.config.columns = [
		{name: "text", label: "Task name", width: "*", tree: true, resize: true },
		{name: "start_time", label: "Start time", template: function (obj) {
			return gantt.templates.date_grid(obj.start_date);
		}, align: "center", width: 60, resize: true },
		{name: "duration", label: "Duration", align: "center", width: 60, resize: true},
		{name: "planned_start", label: "Planned Start", template: function (obj) {
			if (obj.planned_start)
				return gantt.templates.date_grid(obj.planned_start);
			return "";
		}, align: "center", width: 60, resize: true /*, hide: true */ },
		{name: "planned_end", label: "Planned End", template: function (obj) {
			if (obj.planned_start)
				return gantt.templates.date_grid(obj.planned_end);
			return "";
		}, align: "center", width: 60, resize: true /*, hide: true */}
	];


	gantt.config.lightbox.sections = [
		{name: "description", height: 70, map_to: "text", type: "textarea", focus: true},
		{name: "time", map_to: "auto", type: "duration"},
		{name: "baseline", map_to: { start_date: "planned_start", end_date: "planned_end"}, button: true, type: "duration_optional"}
	];
	gantt.config.lightbox.project_sections = [
		{name: "description", height: 70, map_to: "text", type: "textarea", focus: true},
		{name: "time", map_to: "auto", type: "duration", readonly: true},
		{name: "baseline", map_to: { start_date: "planned_start", end_date: "planned_end"}, button: true, type: "duration_optional"}
	];
	gantt.config.lightbox.milestone_sections = [
		{name: "description", height: 70, map_to: "text", type: "textarea", focus: true},
		{name: "time", map_to: "auto", type: "duration", single_date:true},
		{name: "baseline", single_date:true,map_to: { start_date: "planned_start", end_date: "planned_end"}, button: true, type: "duration_optional"}
	];

	gantt.locale.labels.section_baseline = "Planned";


	// adding baseline display
	gantt.addTaskLayer(function draw_planned(task) {
		if (task.planned_start && task.planned_end) {
			var sizes = gantt.getTaskPosition(task, task.planned_start, task.planned_end);
			var el = document.createElement('div');
			el.className = 'baseline';
			el.style.left = sizes.left + 'px';
			el.style.width = sizes.width + 'px';
			el.style.top = sizes.top + gantt.config.task_height + 13 + 'px';
			return el;
		}
		return false;
	});

	gantt.templates.task_class = function (start, end, task) {
		if (task.planned_end) {
			var classes = ['has-baseline'];
			if (end.getTime() > task.planned_end.getTime()) {
				classes.push('overdue');
			}
			return classes.join(' ');
		}
	};

	/* var myEvent = gantt.attachEvent("onTaskClick", function(id, e) {
    alert("You've just clicked an item with id="+id);
}); */


	gantt.templates.rightside_text = function (start, end, task) {
		if (task.planned_end) {
			if (end.getTime() > task.planned_end.getTime()) {
				var overdue = Math.ceil(Math.abs((end.getTime() - task.planned_end.getTime()) / (24 * 60 * 60 * 1000)));
				var text = "<b>Overdue: " + overdue + " days</b>";
				return text;
			}
		}
	};

	//defines the text inside the tak bars
		gantt.templates.task_text=function(start,end,task){
			return "";
		};
		//defines the style of task bars
		gantt.templates.grid_row_class = gantt.templates.task_row_class = function(start, end, task){
			return "";
		};


	gantt.templates.progress_text = function(start, end, task){
		return "<span>"+Math.round(task.progress*100)+ "% </span>";
	};
		gantt.config.grid_resize = true;
	gantt.config.grid_width = 480;
	gantt.config.date_grid = "%F %d";
	 var weekScaleTemplate = function(date){
					var dateToStr = gantt.date.date_to_str("%d %M");
					var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
					return dateToStr(date) + " - " + dateToStr(endDate);
				};
	gantt.config.scale_unit = "week";
				gantt.config.step = 1;
				gantt.templates.date_scale = weekScaleTemplate;
				gantt.config.subscales = [
					{unit:"day", step:1, date:"%D" }
				];
				gantt.config.scale_height = 50;

	gantt.config.scale_unit = "week";
	gantt.config.subscales = [
					{unit:"day", step:1, date:"%d" }
				];
	gantt.attachEvent("onTaskLoading", function (task) {
		task.planned_start = gantt.date.parseDate(task.planned_start, "xml_date");
		task.planned_end = gantt.date.parseDate(task.planned_end, "xml_date");
		return true;
	});

	gantt.config.date_grid = "%d-%m-%Y";

	gantt.init("gantt_here");
/*
	var demo_tasks = {"data": [
		{"id": 1, "text": "Office itinerancy", "type": "project",   "progress": 0.4, "open": true, "start_date": "06-04-2013 00:00", "duration": <?php echo $duration;?>, "end_date": "19-04-2013 00:00", "parent": 0, "planned_start": "02-04-2013 00:00", "planned_end": "19-04-2013 00:00"},
		{"id": 2, "text": "Office facing", "type": "project", "start_date": "05-04-2013 00:00", "duration": 8,   "progress": 0.6, "parent": "1", "open": true, "end_date": "10-04-2013 00:00", "planned_start": "02-04-2013 00:00", "planned_end": "10-04-2013 00:00"},
		{"id": 5, "text": "Interior office", "start_date": "06-04-2013 00:00", "duration": 7, "order": "3", "parent": "2", "progress": 0.6, "open": true, "end_date": "09-04-2013 00:00", "planned_start": "03-04-2013 00:00", "planned_end": "10-04-2013 00:00"},
		{"id": 6, "text": "Air conditioners check", "start_date": "07-04-2013 00:00", "duration": 7, "order": "3", "parent": "2", "progress": 0.6, "open": true, "end_date": "10-04-2013 00:00", "planned_start": "03-04-2013 00:00", "planned_end": "09-04-2013 00:00"},
		{"id": 3, "text": "Furniture installation", "type": "project", "start_date": "11-04-2013 00:00", "duration": 8, "order": "20", "parent": "1", "progress": 0.6, "open": true, "end_date": "19-04-2013 00:00", "planned_start": "11-04-2013 00:00", "planned_end": "19-04-2013 00:00"},
		{"id": 7, "text": "Workplaces preparation", "start_date": "11-04-2013 00:00", "duration": 8, "order": "3", "parent": "3", "progress": 0.6, "open": true, "end_date": "19-04-2013 00:00", "planned_start": "11-04-2013 00:00", "planned_end": "20-04-2013 00:00"},
		{"id": 4, "text": "The employee relocation", "type": "project", "start_date": "14-04-2013 00:00", "duration": 5, "order": "30", "parent": "1", "progress": 0.5, "open": true, "end_date": "19-04-2013 00:00", "planned_start": "14-04-2013 00:00", "planned_end": "19-04-2013 00:00"},
		{"id": 8, "text": "Preparing workplaces", "start_date": "14-04-2013 00:00", "duration": 5, "order": "3", "parent": "4", "progress": 0.5, "open": true, "end_date": "19-04-2013 00:00", "planned_start": "15-04-2013 00:00", "planned_end": "20-04-2013 00:00"},
		{"id": 9, "text": "Workplaces importation", "start_date": "14-04-2013 00:00", "duration": 4, "order": "3", "parent": "4", "progress": 0.5, "open": true, "end_date": "18-04-2013 00:00", "planned_start": "13-04-2013 00:00", "planned_end": "18-04-2013 00:00"},
		{"id": 10, "text": "Workplaces exportation", "start_date": "14-04-2013 00:00", "duration": 3, "order": "3", "parent": "4", "progress": 0.5, "open": true, "end_date": "17-04-2013 00:00", "planned_start": "14-04-2013 00:00", "planned_end": "17-04-2013 00:00"},
		{"id": 11, "text": "Product launch", "type": "project", "order": "5", "progress": 0.6, "open": true, "start_date": "02-04-2013 00:00", "duration": 13, "end_date": "15-04-2013 00:00", "parent": 0, "planned_start": "03-04-2013 00:00", "planned_end": "16-04-2013 00:00"},
		{"id": 12, "text": "Perform Initial testing", "start_date": "03-04-2013 00:00", "duration": 5, "order": "3", "parent": "11", "progress": 1, "open": true, "end_date": "08-04-2013 00:00", "planned_start": "05-04-2013 00:00", "planned_end": "10-04-2013 00:00"},
		{"id": 13, "text": "Development", "type": "project", "start_date": "03-04-2013 00:00", "duration": 11, "order": "3", "parent": "11", "progress": 0.5, "open": true, "end_date": "14-04-2013 00:00", "planned_start": "02-04-2013 00:00", "planned_end": "13-04-2013 00:00"},
		{"id": 17, "text": "Develop System", "start_date": "03-04-2013 00:00", "duration": 2, "order": "3", "parent": "13", "progress": 1, "open": true, "end_date": "05-04-2013 00:00", "planned_start": "03-04-2013 00:00", "planned_end": "05-04-2013 00:00"},
		{"id": 25, "text": "Beta Release", "start_date": "06-04-2013 00:00", "order": "3", "type": "milestone", "parent": "13", "progress": 0, "open": true, "end_date": "06-04-2013 00:00", "duration": 0},
		{"id": 18, "text": "Integrate System", "start_date": "08-04-2013 00:00", "duration": 2, "order": "3", "parent": "13", "progress": 0.8, "open": true, "end_date": "10-04-2013 00:00", "planned_start": "08-04-2013 00:00", "planned_end": "10-04-2013 00:00"},
		{"id": 19, "text": "Test", "start_date": "10-04-2013 00:00", "duration": 4, "order": "3", "parent": "13", "progress": 0.2, "open": true, "end_date": "14-04-2013 00:00", "planned_start": "10-04-2013 00:00", "planned_end": "14-04-2013 00:00"},
		{"id": 20, "text": "Marketing", "start_date": "10-04-2013 00:00", "duration": 4, "order": "3", "parent": "13", "progress": 0, "open": true, "end_date": "14-04-2013 00:00", "planned_start": "10-04-2013 00:00", "planned_end": "14-04-2013 00:00"},
		{"id": 14, "text": "Analysis", "start_date": "02-04-2013 00:00", "duration": 6, "order": "3", "parent": "11", "progress": 0.8, "open": true, "end_date": "08-04-2013 00:00"},
		{"id": 15, "text": "Design", "type": "project", "start_date": "03-04-2013 00:00", "duration": 5, "order": "3", "parent": "11", "progress": 0.2, "open": true, "end_date": "08-04-2013 00:00", "planned_start": "03-04-2013 00:00", "planned_end": "08-04-2013 00:00"},
		{"id": 21, "text": "Design database", "start_date": "03-04-2013 00:00", "duration": 4, "order": "3", "parent": "15", "progress": 0.5, "open": true, "end_date": "07-04-2013 00:00", "planned_start": "03-04-2013 00:00", "planned_end": "07-04-2013 00:00"},
		{"id": 22, "text": "Software design", "start_date": "03-04-2013 00:00", "duration": 4, "order": "3", "parent": "15", "progress": 0.1, "open": true, "end_date": "07-04-2013 00:00", "planned_start": "06-04-2013 00:00", "planned_end": "10-04-2013 00:00"},
		{"id": 23, "text": "Interface setup", "start_date": "03-04-2013 00:00", "duration": 5, "order": "3", "parent": "15", "progress": 0, "open": true, "end_date": "08-04-2013 00:00", "planned_start": "02-04-2013 00:00", "planned_end": "08-04-2013 00:00"},
		{"id": 16, "text": "Documentation creation", "start_date": "02-04-2013 00:00", "duration": 7, "order": "3", "parent": "11", "progress": 0, "open": true, "end_date": "09-04-2013 00:00", "planned_start": "02-04-2013 00:00", "planned_end": "09-04-2013 00:00"},
		{"id": 24, "text": "Release v1.0", "start_date": "15-04-2013 00:00", "order": "3", "type": "milestone", "parent": "11", "progress": 0, "open": true, "end_date": "15-04-2013 00:00", "duration": 0}
	], "links": [
		{"id": "1", "source": "1", "target": "2", "type": "1"},
		{"id": "2", "source": "2", "target": "3", "type": "0"},
		{"id": "3", "source": "3", "target": "4", "type": "0"},
		{"id": "4", "source": "2", "target": "5", "type": "2"},
		{"id": "5", "source": "2", "target": "6", "type": "2"},
		{"id": "6", "source": "3", "target": "7", "type": "2"},
		{"id": "7", "source": "4", "target": "8", "type": "2"},
		{"id": "8", "source": "4", "target": "9", "type": "2"},
		{"id": "9", "source": "4", "target": "10", "type": "2"},
		{"id": "10", "source": "11", "target": "12", "type": "1"},
		{"id": "11", "source": "11", "target": "13", "type": "1"},
		{"id": "12", "source": "11", "target": "14", "type": "1"},
		{"id": "13", "source": "11", "target": "15", "type": "1"},
		{"id": "14", "source": "11", "target": "16", "type": "1"},
		{"id": "15", "source": "13", "target": "17", "type": "1"},
		{"id": "16", "source": "17", "target": "25", "type": "0"},
		{"id": "17", "source": "18", "target": "19", "type": "0"},
		{"id": "18", "source": "19", "target": "20", "type": "0"},
		{"id": "19", "source": "15", "target": "21", "type": "2"},
		{"id": "20", "source": "15", "target": "22", "type": "2"},
		{"id": "21", "source": "15", "target": "23", "type": "2"},
		{"id": "22", "source": "13", "target": "24", "type": "0"},
		{"id": "23", "source": "25", "target": "18", "type": "0"}
	]}; */

	var demo_tasks = {"data": [
	<?php
	$mysqli = get_conn_handle();
	$sqlQueryForProject = 'SELECT project_management_master_id as project_id, project_master_name as project_name, project_master_start_date as start_date
							FROM project_management_project_master WHERE project_master_active="1" AND project_management_master_id <> "19" ';

	//echo $sqlQueryForProject;
	$resultProjects = $mysqli->query( $sqlQueryForProject );
	//print_r( $resultProjects );
	while ( $rowProjects = $resultProjects->fetch_assoc() ) {
		 $projectId = $rowProjects['project_id'];
		 $start_date = $rowProjects['start_date'];
		 $end_date = "2017-12-23";

		 $start_date = date("d-m-Y H:i:s", strtotime("2017-12-01"));
		 $end_date = date("d-m-Y H:i:s", strtotime("2017-12-23"));

		 $planned_start_date = date("d-m-Y H:i:s", strtotime("2017-12-01"));
		 $planned_end_date = date("d-m-Y H:i:s", strtotime("2017-12-19"));
		 $duration  = 20;
	?>
		//{"id": 1, "text": "Office itinerancy", "type": "project",   "progress": 0.4, "open": true, "start_date": "06-04-2013 00:00", "duration": <?php echo $duration;?>, "end_date": "19-04-2013 00:00", "parent": 0, "planned_start": "02-04-2013 00:00", "planned_end": "19-04-2013 00:00"},
		{"id": <?php echo $projectId ;?> ,"text": '<?php echo $rowProjects['project_name'] ;?> ',"type": "project",  "progress": 0.4, "open": true, "start_date": "06-12-2017 00:00","duration": 10,  "parent": 0 , "planned_start": "<?php echo $planned_start_date;?>", "planned_end": "<?php echo $planned_end_date;?>"},
   <?php

		$sqlQueryForPrjPlan = 'SELECT pp.project_plan_id as project_id, pp.project_plan_project_id as pppid, project_plan_start_date as start_date, project_plan_end_date as end_date
								FROM project_plan as pp WHERE project_plan_active="1" and pp.project_plan_project_id = ' . $rowProjects['project_id'];

		//echo $sqlQueryForPrjPlan;
		$resultPrjPlan = $mysqli->query( $sqlQueryForPrjPlan );

		//print_r( $resultPrjPlan );
		$counter = 0;
		while ( $rowPrjPlan = $resultPrjPlan->fetch_assoc() ) {
			$projectPlanId = sprintf("%0d0%04d", $projectId, $rowPrjPlan['project_id']);
			$counter++;

			/*$date = new DateTime($rowProcess['start_date']);
			$start_date = $date->format('d-m-Y H:i:s');
			$date = new DateTime($rowProcess['end_date']);
			$end_date = $date->format('d-m-Y H:i:s'); */
			$start_date = $rowPrjPlan['start_date'];
		 $start_date = date("d-m-Y H:i:s", strtotime("2017-12-01"));
		 $end_date = date("d-m-Y H:i:s", strtotime("2017-12-23"));


	?>
		{"id": <?php echo $projectPlanId ;?> ,"text": '<?php echo "PLAN " . $counter ;?> ',"type": "project",  "progress": 0.4, "open": true, "start_date": "06-12-2017 00:00","duration": 8,  "parent": <?php echo $projectId ;?> , "planned_start": "<?php echo $planned_start_date;?>", "planned_end": "<?php echo $planned_end_date;?>"},

	<?php
			//print_r( $rowPrjPlan );
			$sqlQueryForProcess = 'SELECT project_plan_process_id as process_id, project_plan_process_plan_id, project_process_master_name as process_name,
											project_plan_process_start_date as start_date, project_plan_process_end_date as end_date
									FROM project_plan_process
									INNER JOIN project_process_master
									ON project_plan_process_name = project_process_master_id
									WHERE project_plan_process_active = "1" and project_process_master_active ="1" and project_plan_process_plan_id = ' . $rowPrjPlan['project_id'];

			//echo $sqlQueryForProcess;
			$resultProcess = $mysqli->query( $sqlQueryForProcess );
			//print_r( $resultProcess );

			while ( $rowProcess = $resultProcess->fetch_assoc() ) {
				$processId = sprintf("%0d0%04d", $projectPlanId, $rowProcess['process_id']);

				/* $start_date = (strcmp($rowProcess['start_date'], '0000-00-00') !== 0)? $rowProcess['start_date']: '2017-01-01';
				$end_date = (strcmp($rowProcess['end_date'], '0000-00-00') !== 0)? $rowProcess['end_date']: '2018-01-01'; */
				/* $date = new DateTime($rowProcess['start_date']);
				$start_date = $date->format('d-m-Y H:i:s');
				$date = new DateTime($rowProcess['end_date']);
				$end_date = $date->format('d-m-Y H:i:s'); */


				$start_date = $rowProcess['start_date'];
		 $start_date = date("d-m-Y H:i:s", strtotime("2017-12-04"));
		 $end_date = date("d-m-Y H:i:s", strtotime("2017-12-23"));

	?>
		{"id": <?php echo $processId ;?> ,"text": "<?php echo $rowProcess['process_name'];?>","type": "project",  "progress": 0.4, "open": true, "start_date": "06-12-2017 00:00","duration": 13,  "parent": <?php echo $projectPlanId ;?> , "planned_start": "<?php echo $planned_start_date;?>", "planned_end": "<?php echo $planned_end_date;?>"},
	<?php


			$sqlQueryForTask = 'SELECT project_process_task_id as task_id, project_process_id, project_task_master_name as task_name,
										project_process_actual_start_date as start_date, project_process_actual_end_date as end_date,
										project_process_task_completion as man_completion, project_process_machine_task_completion as machine_completion,
										project_process_contract_task_completion as contract_completion
									FROM project_plan_process_task
									INNER JOIN project_task_master
									ON 	project_task_master_id = project_process_task_type
									WHERE project_process_task_active = "1" and project_task_master_active ="1" and project_process_id = ' . $rowProcess['process_id'];

			//echo $sqlQueryForTask;
			$resultTasks = $mysqli->query( $sqlQueryForTask );
			//print_r( $resultProcess );

			while ( $rowTasks = $resultTasks->fetch_assoc() ) {
				//print_r( $rowTasks );
				$taskId = sprintf("%0d0%04d", $processId, $rowTasks['task_id']);
				/* $start_date = (strcmp($rowTasks['start_date'], '0000-00-00') !== 0)? $rowTasks['start_date']: '2017-01-01';
				$end_date = (strcmp($rowTasks['end_date'], '0000-00-00') !== 0)? $rowTasks['end_date']: '2018-01-01';
				*/
				/* $date = new DateTime($rowProcess['start_date']);
				$start_date = $date->format('d-m-Y H:i:s');
				$date = new DateTime($rowProcess['end_date']);
				$end_date = $date->format('d-m-Y H:i:s'); */

				$start_date = $rowTasks['start_date'];
		 $start_date = date("d-m-Y H:i:s", strtotime("2017-12-04"));
		 $end_date = date("d-m-Y H:i:s", strtotime("2017-12-23"));

				$completionPer = 0;
				$completionTasksAvg =0;
				$sqlQuery = ' SELECT project_task_actual_manpower_task_id FROM project_task_actual_manpower WHERE project_task_actual_manpower_task_id = ' . $rowTasks['task_id'];
				$result = $mysqli->query( $sqlQuery );
				//echo "### : " . $result->num_rows . PHP_EOL;
				if( $result->num_rows > 0 ) {
					$completionPer = $completionPer + $rowTasks ['man_completion'];
					$completionTasksAvg++;
				}

				$sqlQuery = ' SELECT project_task_actual_machine_plan_task_id FROM project_task_actual_machine_plan WHERE project_task_actual_machine_plan_task_id = ' . $rowTasks['task_id'];
				$result = $mysqli->query( $sqlQuery );
				if( $result->num_rows > 0 ) {
					$completionPer = $completionPer + $rowTasks ['machine_completion'];
					$completionTasksAvg++;
				}
				$sqlQuery = ' SELECT project_boq_actual_task_id FROM project_task_boq_actuals WHERE project_boq_actual_task_id = ' . $rowTasks['task_id'];
				$result = $mysqli->query( $sqlQuery );
				if( $result->num_rows > 0 ) {
					$completionPer = $completionPer + $rowTasks ['contract_completion'];
					$completionTasksAvg++;
				}
				$completionPer = $completionPer / $completionTasksAvg;
				//echo "@@@ : " . $completionPer . PHP_EOL;

	?>
		{"id": <?php echo $taskId ;?> ,"text": "<?php echo $rowTasks['task_name'];?>","type": "project",  "progress": 0.8, "open": true, "start_date": "06-12-2017 00:00","duration": 14,  "parent": <?php echo $processId ;?> , "planned_start": "<?php echo $planned_start_date;?>", "planned_end": "<?php echo $planned_end_date;?>"},
	<?php
				}
			}
		}
		break;
	}
   ?>

		/* {"id": 1, "text": "Office itinerancy", "type": "project",   "progress": 0.4, "open": true, "start_date": "06-04-2013 00:00", "duration": <?php echo $duration;?>, "end_date": "19-04-2013 00:00", "parent": 0, "planned_start": "02-04-2013 00:00", "planned_end": "19-04-2013 00:00"},
		{"id": 2, "text": "Office facing", "type": "project", "start_date": "05-04-2013 00:00", "duration": 8,   "progress": 0.6, "parent": "1", "open": true, "end_date": "10-04-2013 00:00", "planned_start": "02-04-2013 00:00", "planned_end": "10-04-2013 00:00"},
		{"id": 5, "text": "Interior office", "start_date": "06-04-2013 00:00", "duration": 7, "order": "3", "parent": "2", "progress": 0.6, "open": true, "end_date": "09-04-2013 00:00", "planned_start": "03-04-2013 00:00", "planned_end": "10-04-2013 00:00"},
		{"id": 6, "text": "Air conditioners check", "start_date": "07-04-2013 00:00", "duration": 7, "order": "3", "parent": "2", "progress": 0.6, "open": true, "end_date": "10-04-2013 00:00", "planned_start": "03-04-2013 00:00", "planned_end": "09-04-2013 00:00"},
		{"id": 3, "text": "Furniture installation", "type": "project", "start_date": "11-04-2013 00:00", "duration": 8, "order": "20", "parent": "1", "progress": 0.6, "open": true, "end_date": "19-04-2013 00:00", "planned_start": "11-04-2013 00:00", "planned_end": "19-04-2013 00:00"},
		{"id": 7, "text": "Workplaces preparation", "start_date": "11-04-2013 00:00", "duration": 8, "order": "3", "parent": "3", "progress": 0.6, "open": true, "end_date": "19-04-2013 00:00", "planned_start": "11-04-2013 00:00", "planned_end": "20-04-2013 00:00"},
		{"id": 4, "text": "The employee relocation", "type": "project", "start_date": "14-04-2013 00:00", "duration": 5, "order": "30", "parent": "1", "progress": 0.5, "open": true, "end_date": "19-04-2013 00:00", "planned_start": "14-04-2013 00:00", "planned_end": "19-04-2013 00:00"},
		{"id": 8, "text": "Preparing workplaces", "start_date": "14-04-2013 00:00", "duration": 5, "order": "3", "parent": "4", "progress": 0.5, "open": true, "end_date": "19-04-2013 00:00", "planned_start": "15-04-2013 00:00", "planned_end": "20-04-2013 00:00"},
		{"id": 9, "text": "Workplaces importation", "start_date": "14-04-2013 00:00", "duration": 4, "order": "3", "parent": "4", "progress": 0.5, "open": true, "end_date": "18-04-2013 00:00", "planned_start": "13-04-2013 00:00", "planned_end": "18-04-2013 00:00"},
		{"id": 10, "text": "Workplaces exportation", "start_date": "14-04-2013 00:00", "duration": 3, "order": "3", "parent": "4", "progress": 0.5, "open": true, "end_date": "17-04-2013 00:00", "planned_start": "14-04-2013 00:00", "planned_end": "17-04-2013 00:00"},
		{"id": 11, "text": "Product launch", "type": "project", "order": "5", "progress": 0.6, "open": true, "start_date": "02-04-2013 00:00", "duration": 13, "end_date": "15-04-2013 00:00", "parent": 0, "planned_start": "03-04-2013 00:00", "planned_end": "16-04-2013 00:00"},
		{"id": 12, "text": "Perform Initial testing", "start_date": "03-04-2013 00:00", "duration": 5, "order": "3", "parent": "11", "progress": 1, "open": true, "end_date": "08-04-2013 00:00", "planned_start": "05-04-2013 00:00", "planned_end": "10-04-2013 00:00"},
		{"id": 13, "text": "Development", "type": "project", "start_date": "03-04-2013 00:00", "duration": 11, "order": "3", "parent": "11", "progress": 0.5, "open": true, "end_date": "14-04-2013 00:00", "planned_start": "02-04-2013 00:00", "planned_end": "13-04-2013 00:00"},
		{"id": 17, "text": "Develop System", "start_date": "03-04-2013 00:00", "duration": 2, "order": "3", "parent": "13", "progress": 1, "open": true, "end_date": "05-04-2013 00:00", "planned_start": "03-04-2013 00:00", "planned_end": "05-04-2013 00:00"},
		{"id": 25, "text": "Beta Release", "start_date": "06-04-2013 00:00", "order": "3", "type": "milestone", "parent": "13", "progress": 0, "open": true, "end_date": "06-04-2013 00:00", "duration": 0},
		{"id": 18, "text": "Integrate System", "start_date": "08-04-2013 00:00", "duration": 2, "order": "3", "parent": "13", "progress": 0.8, "open": true, "end_date": "10-04-2013 00:00", "planned_start": "08-04-2013 00:00", "planned_end": "10-04-2013 00:00"},
		{"id": 19, "text": "Test", "start_date": "10-04-2013 00:00", "duration": 4, "order": "3", "parent": "13", "progress": 0.2, "open": true, "end_date": "14-04-2013 00:00", "planned_start": "10-04-2013 00:00", "planned_end": "14-04-2013 00:00"},
		{"id": 20, "text": "Marketing", "start_date": "10-04-2013 00:00", "duration": 4, "order": "3", "parent": "13", "progress": 0, "open": true, "end_date": "14-04-2013 00:00", "planned_start": "10-04-2013 00:00", "planned_end": "14-04-2013 00:00"},
		{"id": 14, "text": "Analysis", "start_date": "02-04-2013 00:00", "duration": 6, "order": "3", "parent": "11", "progress": 0.8, "open": true, "end_date": "08-04-2013 00:00"},
		{"id": 15, "text": "Design", "type": "project", "start_date": "03-04-2013 00:00", "duration": 5, "order": "3", "parent": "11", "progress": 0.2, "open": true, "end_date": "08-04-2013 00:00", "planned_start": "03-04-2013 00:00", "planned_end": "08-04-2013 00:00"},
		{"id": 21, "text": "Design database", "start_date": "03-04-2013 00:00", "duration": 4, "order": "3", "parent": "15", "progress": 0.5, "open": true, "end_date": "07-04-2013 00:00", "planned_start": "03-04-2013 00:00", "planned_end": "07-04-2013 00:00"},
		{"id": 22, "text": "Software design", "start_date": "03-04-2013 00:00", "duration": 4, "order": "3", "parent": "15", "progress": 0.1, "open": true, "end_date": "07-04-2013 00:00", "planned_start": "06-04-2013 00:00", "planned_end": "10-04-2013 00:00"},
		{"id": 23, "text": "Interface setup", "start_date": "03-04-2013 00:00", "duration": 5, "order": "3", "parent": "15", "progress": 0, "open": true, "end_date": "08-04-2013 00:00", "planned_start": "02-04-2013 00:00", "planned_end": "08-04-2013 00:00"},
		{"id": 16, "text": "Documentation creation", "start_date": "02-04-2013 00:00", "duration": 7, "order": "3", "parent": "11", "progress": 0, "open": true, "end_date": "09-04-2013 00:00", "planned_start": "02-04-2013 00:00", "planned_end": "09-04-2013 00:00"},
		{"id": 24, "text": "Release v1.0", "start_date": "15-04-2013 00:00", "order": "3", "type": "milestone", "parent": "11", "progress": 0, "open": true, "end_date": "15-04-2013 00:00", "duration": 0}
	*/ ]};
	gantt.parse(demo_tasks, "json");


</script>


</body>
