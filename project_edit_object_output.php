<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_REQUEST["object_output_id"]))
	{
		$object_output_id = $_REQUEST["object_output_id"];
	}
	else
	{
		$object_output_id = "";
	}
	
	if(isset($_REQUEST["search_process"]))
	{
		$search_process = $_REQUEST["search_process"];
	}
	else
	{
		$search_process	 = "";
	}

	// Get Already added Object Output
	$project_object_output_search_data = array("active"=>'1',"output_id"=>$object_output_id);
	$project_object_output_master_list = i_get_project_object_output_task($project_object_output_search_data);
	if($project_object_output_master_list["status"] == SUCCESS)
	{
		$project_object_output_master_list_data = $project_object_output_master_list["data"];
		$uom = $project_object_output_master_list_data[0]["project_object_output_uom"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_object_output_master_list["data"];
	}
	// Capture the form data
	if(isset($_POST["edit_project_object_output_submit"]))
	{
		
        $object_output_id    = $_POST["hd_object_output_id"];
        $search_process      = $_POST["hd_search_process"];
		$object_uom          = $_POST["ddl_unit"];
		$object_type 	   	 = $_POST["ddl_object_type"];
		if($object_type == "MC")
		{
			$reference_id 	   	 = $_POST["ddl_mc_reference_id"];
		}
		elseif($object_type == "CW")
		{
			$reference_id 	   	 = $_POST["ddl_cw_reference_id"];
		}
		$obeject_per_hour 	 = $_POST["object_per_hr"];
		$obeject_remarks 	 = $_POST["txt_remarks"];
		
		
		// Check for mandatory fields
		if(($object_output_id != ""))
		{
			$project_object_output_update_data = array("uom"=>$object_uom,"object_type"=>$object_type,"reference_id"=>$reference_id,"object_per_hr"=>$obeject_per_hour,"remarks"=>$obeject_remarks);
			$project_object_output_iresult = i_update_project_object_output($object_output_id,$project_object_output_update_data);
			
			if($project_object_output_iresult["status"] == SUCCESS)
				
			{	
				$alert_type = 1;
		    
				header("location:project_object_output_master_list.php?search_process=$search_process");
			}
			else
			{
			   $alert = "";
			  $alert_type = 0;	
			}
			
			$alert = $project_object_output_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	
	if(isset($_POST['ddl_process']))
	{
		$process_id = $_POST['ddl_process'];
		
		//Get Project Task Master modes already added
		$project_task_master_search_data = array("active"=>'1',"process"=>$process_id);
		$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
		if($project_task_master_list['status'] == SUCCESS)
		{
			$project_task_master_list_data = $project_task_master_list['data'];
		}
		else
		{
			
		}
	}
	else
	{
		$process_id = "";
	}
	
	// Get Project Task Master modes already added
	$project_task_master_search_data = array("active"=>'1');
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list['status'] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list['data'];
	}

	else
	{
		$alert = $project_task_master_list["data"];
		$alert_type = 0;
	}
	// Get Project Process Master modes already added
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Users modes already added
	$user_list = i_get_user_list('','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}
	
	// Get Stock master uom modes already added
	$project_uom_master_search_data = array("active"=>'1');
	$stock_unit_measure_list = i_get_project_uom_master($project_uom_master_search_data);
	if($stock_unit_measure_list['status'] == SUCCESS)
	{
		$stock_unit_measure_list_data = $stock_unit_measure_list['data'];
		
    }
     else
    {
		$alert = $stock_unit_measure_list["data"];
		$alert_type = 0;
	}
	
	// Get Project machine types already added
	$project_machine_type_master_search_data = array("active"=>'1');
	$project_machine_type_master_list = i_get_project_machine_type_master($project_machine_type_master_search_data);
	if($project_machine_type_master_list['status'] == SUCCESS)
	{
		$project_machine_type_master_list_data = $project_machine_type_master_list['data'];
		
    }
     else
    {
		$alert = $project_machine_type_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Project CW Master List
	$project_cw_master_search_data = array("active"=>'1');
	$project_cw_master_list = i_get_project_cw_master($project_cw_master_search_data);
	if($project_cw_master_list['status'] == SUCCESS)
	{
		$project_cw_master_list_data = $project_cw_master_list['data'];
	}
	else
	{
		//
	}
		

}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Project Task User Mapping</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Project Object Output Mapping</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Project Object Output Mapping</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_add_object_output_form" class="form-horizontal" method="post" action="project_edit_object_output.php">
								<input type="hidden" name="hd_object_output_id" value="<?php echo $object_output_id; ?>" />
								<input type="hidden" name="hd_search_process" value="<?php echo $search_process; ?>" />
									<fieldset>										
																
										<div class="control-group">											
											<label class="control-label" for="ddl_process">Process*</label>
											<div class="controls">
												<select name="ddl_process" disabled  onchange="javascript: submit()" required>
											  <option><?php echo $project_object_output_master_list_data[0]["project_process_master_name"]; ?></option>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_task_id">Task*</label>
											<div class="controls">
												<select name="ddl_task_id" disabled required>
												<option>- - Select Task - -</option>
												<?php
												for($count = 0; $count < count($project_task_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_task_master_list_data[$count]["project_task_master_id"]; ?>"<?php if($project_task_master_list_data[$count]["project_task_master_id"] == $project_object_output_master_list_data[0]["project_object_output_task_id"]){ ?> selected="selected" <?php } ?>><?php echo $project_task_master_list_data[$count]["project_task_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_unit">UOM*</label>
											<div class="controls">
												<select name="ddl_unit" required>
												<option value="">- - Select UOM - -</option>
												<?php
												for($count = 0; $count < count($stock_unit_measure_list_data); $count++)
												{
												?>
												<option value="<?php echo $stock_unit_measure_list_data[$count]["project_uom_id"]; ?>"<?php if($stock_unit_measure_list_data[$count]["project_uom_id"] == $project_object_output_master_list_data[0]["project_object_output_uom"]){ ?> selected="selected" <?php } ?>><?php echo $stock_unit_measure_list_data[$count]["project_uom_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
											</div> <!-- /control-group -->
											
											<div class="control-group">											
												<label class="control-label" for="ddl_object_type">Object Type</label>
												<div class="controls">
													<select name="ddl_object_type" id="ddl_object_type" required onchange="return get_object_type_reference()">
													<option value="">- - Select Object Type - -</option>
													<option value ="MC" <?php if($project_object_output_master_list_data[0]["project_object_output_object_type"] == 'MC') {?> selected="selected" <?php }?>>Machine</option>
													<option value ="CW" <?php if($project_object_output_master_list_data[0]["project_object_output_object_type"] == 'CW') {?> selected="selected" <?php }?>>Contract Work</option>
													</select>
												</div> <!-- /controls -->					
											</div> <!-- /control-group -->
											
											<div class="control-group">											
											<label class="control-label" for="ddl_mc_reference_id">Machine Type*</label>
											<div class="controls">
												<select name="ddl_mc_reference_id" <?php if($project_object_output_master_list_data[0]["project_object_output_object_type"] == 'CW') {?> disabled <?php } ?> id="ddl_mc_reference_id" requiredonchange="return get_object_type_reference()">
												<option value="">- - Select Machine Type - -</option>
												<?php
												for($count = 0; $count < count($project_machine_type_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_machine_type_master_list_data[$count]["project_machine_type_master_id"]; ?>"<?php if($project_machine_type_master_list_data[$count]["project_machine_type_master_id"] == $project_object_output_master_list_data[0]["project_object_output_object_type_reference_id"]){ ?> selected="selected" <?php } ?>><?php echo $project_machine_type_master_list_data[$count]["project_machine_type_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
											<div class="control-group">											
											<label class="control-label" for="ddl_cw_reference_id">CW Type*</label>
											<div class="controls">
												<select name="ddl_cw_reference_id" <?php if($project_object_output_master_list_data[0]["project_object_output_object_type"] == 'MC') {?> disabled <?php } ?> id="ddl_cw_reference_id" required>
												<option value="">- - Select CW Type - -</option>
												<?php
												for($count = 0; $count < count($project_cw_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_cw_master_list_data[$count]["project_cw_master_id"]; ?>" <?php if($project_cw_master_list_data[$count]["project_cw_master_id"] == $project_object_output_master_list_data[0]["project_object_output_object_type_reference_id"]){ ?> selected="selected" <?php } ?>><?php echo $project_cw_master_list_data[$count]["project_cw_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="object_per_hr">Object Output</label>
											<div class="controls">
												<input type="number" min="0.01" step="0.01" name="object_per_hr" placeholder="Object Output" value="<?php echo $project_object_output_master_list_data[0]["project_object_output_obejct_per_hr"] ;?>" >
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" name="txt_remarks" placeholder="Remarks" value="<?php echo $project_object_output_master_list_data[0]["project_object_output_remarks"];?>" >
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_project_object_output_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
<script>
 function get_object_type_reference()
{
	var object_type = document.getElementById("ddl_object_type").value;
	if(object_type == "MC")
	{
		document.getElementById("ddl_cw_reference_id").disabled = true;
		document.getElementById("ddl_mc_reference_id").disabled = false;
	}
	if(object_type == "CW")
	{
		document.getElementById("ddl_mc_reference_id").disabled = true;
		document.getElementById("ddl_cw_reference_id").disabled = false;
	}
}
</script> 


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
