<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_master_machine_rate_list.php
CREATED ON	: 02-Jan-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_MASTER_PROJECT_MACHINE_RATE_FUNC_ID','250');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_RATE_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_RATE_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_RATE_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_RATE_FUNC_ID,'1','1');

	// Query String Data
	// Nothing

	$search_vendor 		 = '';
	$search_machine_type = '';	
	if(isset($_POST["rate_search_submit"]))
	{
		$search_vendor 		 = $_POST['search_vendor'];
		$search_machine_type = $_POST['search_machine_type'];	
	}	
	
	// Get vendor list
	$vendor_search_data = array('active'=>'1');
	$vendor_sresult = i_get_project_machine_vendor_master_list($vendor_search_data);
	if($vendor_sresult['status'] == SUCCESS)
	{
		$vendor_list = $vendor_sresult['data'];
	}
	else
	{
		$vendor_list = array();
	}
		
	// Get machine type list
	$machine_type_search_data = array('active'=>'1');
	$machine_type_sresult = i_get_project_machine_type_master($machine_type_search_data);
	if($machine_type_sresult['status'] == SUCCESS)
	{
		$machine_type_list = $machine_type_sresult['data'];
	}
	else
	{
		$machine_type_list = array();
	}			
	
	// Get Project Machine Rate Master already added
	$project_machine_rate_master_search_data = array("active"=>'1','machine_type'=>$search_machine_type,'vendor'=>$search_vendor);
	$project_machine_rate_master_list = i_get_project_machine_rate_master($project_machine_rate_master_search_data);
	if($project_machine_rate_master_list['status'] == SUCCESS)
	{
		$project_machine_rate_master_list_data = $project_machine_rate_master_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Master Machine Rate List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Master Machine Rate List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="project_master_add_machine_rate.php">Project Master Add  Machine Rate</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="machine_rate_search_form" action="project_master_machine_Rate_list.php">
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_vendor">
			  <option value="">- - Select Vendor - -</option>
			  <?php
			  for($vendor_count = 0; $vendor_count < count($vendor_list); $vendor_count++)
			  {
			  ?>
			  <option value="<?php echo $vendor_list[$vendor_count]["project_machine_vendor_master_id"]; ?>" <?php if($search_vendor == $vendor_list[$vendor_count]["project_machine_vendor_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $vendor_list[$vendor_count]["project_machine_vendor_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  <select name="search_machine_type">
			  <option value="">- - Select Machine Type - -</option>
			  <?php
			  for($machine_count = 0; $machine_count < count($machine_type_list); $machine_count++)
			  {
			  ?>
			  <option value="<?php echo $machine_type_list[$machine_count]["project_machine_type_master_id"]; ?>" <?php if($search_machine_type == $machine_type_list[$machine_count]["project_machine_type_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $machine_type_list[$machine_count]["project_machine_type_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>			  
			  </select>
			  </span>
			 
			  <input type="submit" name="rate_search_submit" />
			  </form>			  
            </div>
            <div class="widget-content">
			<?php if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Machine</th>
					<th>Machine Type</th>
					<th>Ownership Type</th>
					<th>Vendor</th>
					<th>Kns Fuel(Rate/hr)</th>
					<th>Vendor fuel(Rate/hr)</th>
					<th>Bata(Rate/day)</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="2" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_machine_rate_master_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_machine_rate_master_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_machine_rate_master_list_data[$count]["project_machine_master_name"]; ?>-<?php echo $project_machine_rate_master_list_data[$count]["project_machine_master_id_number"]; ?></td>
					<td><?php echo $project_machine_rate_master_list_data[$count]["project_machine_type_master_name"]; ?></td>
					<td><?php echo $project_machine_rate_master_list_data[$count]["project_machine_type"]; ?></td>
					<td><?php echo $project_machine_rate_master_list_data[$count]["project_machine_vendor_master_name"]; ?></td>
					<td><?php echo $project_machine_rate_master_list_data[$count]["project_machine_kns_fuel"]; ?></td>
					<td><?php echo $project_machine_rate_master_list_data[$count]["project_machine_vendor_fuel"]; ?></td>
					<td><?php echo $project_machine_rate_master_list_data[$count]["project_machine_kns_bata"]; ?></td>
					<td><?php echo $project_machine_rate_master_list_data[$count]["project_machine_rate_master_remarks"]; ?></td>
					<td><?php echo $project_machine_rate_master_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_machine_rate_master_list_data[$count][
					"project_machine_rate_master_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_master_machine_rate('<?php echo $project_machine_rate_master_list_data[$count]["project_machine_rate_master_id"]; ?>');">Edit </a></div><?php } ?></td>
					<td><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($project_machine_rate_master_list_data[$count]["project_machine_rate_master_active"] == "1")){?><a href="#" onclick="return project_delete_master_machine_rate(<?php echo $project_machine_rate_master_list_data[$count]["project_machine_rate_master_id"]; ?>);">Delete</a><?php } ?><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="13">No Machine Rate added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
	     <?php 
		   } 
		   ?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_master_machine_rate(machine_rate_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_master_machine_rate_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_master_delete_machine_rate.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("machine_rate_id=" + machine_rate_id + "&action=0");
		}
	}	
}
function go_to_project_edit_master_machine_rate(machine_rate_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_master_edit_machine_rate.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","machine_rate_id");
	hiddenField1.setAttribute("value",machine_rate_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>