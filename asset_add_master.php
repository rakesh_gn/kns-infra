<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 20-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'asset'.DIRECTORY_SEPARATOR.'asset_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert = "";
	$alert_type = -1; // No alert	

	// Get Stock Master details
	$stock_material_search_data = array("material_type"=>'Returnable');
	$stock_material_master_list = i_get_stock_material_master_list($stock_material_search_data);
	if($stock_material_master_list['status'] == SUCCESS)
	{
		
		$stock_material_master_list_data = $stock_material_master_list['data'];
	}	
	else
	{
		$alert = $stock_material_master_list["data"];
		$alert_type = 0;
	}
	
	if(isset($_POST["add_asset_submit"]))
	{
		// Capture all form data
		$asset_type_array = $_POST["cb_asset_type"];
		for($asset_type_count = 0; $asset_type_count < count($asset_type_array); $asset_type_count++)
		{
			$asset_type = $asset_type_array[$asset_type_count];
			// Add task
			$asset_iresult = i_add_asset_master($asset_type,'',$user);
			if($asset_iresult["status"] == SUCCESS)
			{
				$alert =  "Asset Successfully Added";
				$alert_type = 1;
			}
		}
	}

	// Get Asset Master List	
	$asset_master_search_data = array();
	$asset_master_list = i_get_asset_master($asset_master_search_data);
	if($asset_master_list["status"] == SUCCESS)
	{
		$asset_master_list_data = $asset_master_list["data"];
	}
	else
	{
		
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Asset</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
						 <h3>Asset Master List</h3><span style="float:right; padding-right:20px;"><a href="asset_master_list.php">Asset Master List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Material to this Asset</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
							
								<div class="tab-pane active" id="formcontrols">
								<form id="add_asset" class="form-horizontal" method="post" action="asset_add_master.php">
									<fieldset>
										
										<div class="control-group">											
											<label class="control-label" for="asset_type">Material</label>
											<div class="controls">
												<?php 
												for($count = 0; $count < count($stock_material_master_list_data); $count++)
												{
											   // Get already added Material
												$asset_master_search_data = array("material_id"=>$stock_material_master_list_data[$count]["stock_material_id"]);
												$asset_master = i_get_asset_master($asset_master_search_data);
												if($asset_master['status'] != SUCCESS)
												
												{?>
												<input type="checkbox" name="cb_asset_type[]" value="<?php echo $stock_material_master_list_data[$count]["stock_material_id"]; ?>" />&nbsp;&nbsp;&nbsp;<?php echo $stock_material_master_list_data[$count]["stock_material_name"]; ?>&nbsp;&nbsp;&nbsp;<br /><br />
												<?php
												 }
												}?>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->                                                                                                                       <br />										
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="add_asset_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
                                								
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
