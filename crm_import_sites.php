<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 14th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* DEFINES - START */

define('IMPORT_SITES_FUNC_ID',88);

/* DEFINES - END */
$_SESSION['module'] = 'CRM Projects';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page

	$add_perms_list    = i_get_user_perms($user,'',IMPORT_SITES_FUNC_ID,'1','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["import_submit"]))
	{
		$import_file = upload("site_file",$user);

		// Check for mandatory fields
		if($import_file != "")
		{
			ini_set("max_execution_time", 0);

			$row = 1;
			if (($handle = fopen($import_file, "r")) !== FALSE)
			{
				while (($project_data = fgetcsv($handle, 0, ",")) !== FALSE)
				{
					if($row > 1)
					{
						// Status
						$status = 1;

						// Field 1 - Project
						$project_name     = $project_data[0];
						$project_sdetails = i_get_project_list($project_name,'');
						if($project_sdetails["status"] == SUCCESS)
						{
							$project = $project_sdetails["data"][0]["project_id"];
						}
						else
						{
							echo "Import field at row no. ".$row." due to invalid project";
							$status = ($status&0);
						}

						// Field 2 - Site No
						$site_no = $project_data[1];

						// Field 3 - Block
						$block = $project_data[2];

						// Field 4 - Wing
						$wing = $project_data[3];

						// Field 5 - Dimension
						$dimension_name = $project_data[4];
						$dimension_sdetails = i_get_dimension_list($dimension_name,'','','','');
						if($dimension_sdetails["status"] == SUCCESS)
						{
							$dimension = $dimension_sdetails["data"][0]["crm_dimension_id"];
						}
						else
						{
							echo "Import field at row no. ".$row." due to invalid dimension";
							$status = ($status&0);
						}

						// Field 6 - Area
						$area = $project_data[5];

						// Field 7 - Site Type
						$site_type_name = $project_data[6];
						$site_type_sdetails = i_get_site_type_list($site_type_name,'');
						if($site_type_sdetails["status"] == SUCCESS)
						{
							$site_type = $site_type_sdetails["data"][0]["crm_site_type_id"];
						}
						else
						{
							echo "Import field at row no. ".$row." due to invalid site type";
							$status = ($status&0);
						}

						// Field 8 - Remarks
						$remarks = $project_data[7];

						// Field 9 - Site Status
						$site_status_name = $project_data[8];
						$site_status_sdetails = i_get_site_status_list($site_status_name,'');
						if($site_status_sdetails["status"] == SUCCESS)
						{
							$site_status = $site_status_sdetails["data"][0]["status_id"];
						}
						else
						{
							echo "Import field at row no. ".$row." due to invalid site status";
							$status = ($status&0);
						}

						// Field 10 - Site Release Status
						$site_rel_status_name = $project_data[9];
						$site_rel_status_sdetails = i_get_release_status($site_rel_status_name,'');
						if($site_rel_status_sdetails["status"] == SUCCESS)
						{
							$site_rel_status = $site_rel_status_sdetails["data"][0]["release_status_id"];
						}
						else
						{
							echo "Import field at row no. ".$row." due to invalid site release status";
							$status = ($status&0);
						}

						// Field 11 - Mortgage Bank
						$mortgage_bank_name = $project_data[10];
						$mortgage_bank_sdetails = i_get_bank_list($mortgage_bank_name,'');
						if($mortgage_bank_sdetails["status"] == SUCCESS)
						{
							$mortgage_bank = $mortgage_bank_sdetails["data"][0]["crm_bank_master_id"];
						}
						else
						{
							echo "Import field at row no. ".$row." due to invalid mortgage bank";
							$status = ($status&0);
						}

						// Field 12 - Survey No. 1
						$survey_one_name = $project_data[11];
						if(trim($survey_one_name) != "")
						{
							$survey_one_sdetails = i_get_survey_number_list($survey_one_name,$project,'');
							if($survey_one_sdetails["status"] == SUCCESS)
							{
								$survey_one = $survey_one_sdetails["data"][0]["crm_survey_master_id"];
							}
							else
							{
								echo "Import field at row no. ".$row." due to invalid survey no. 1";
								$status = ($status&0);
							}
						}
						else
						{
							$survey_one = "";
						}

						// Field 13 - Survey No. 2
						$survey_two_name = $project_data[12];
						if(trim($survey_two_name) != "")
						{
							$survey_two_sdetails = i_get_survey_number_list($survey_two_name,$project,'');
							if($survey_two_sdetails["status"] == SUCCESS)
							{
								$survey_two = $survey_two_sdetails["data"][0]["crm_survey_master_id"];
							}
							else
							{
								echo "Import field at row no. ".$row." due to invalid survey no. 2";
								$status = ($status&0);
							}
						}
						else
						{
							$survey_two = "";
						}

						// Field 14 - Survey No. 3
						$survey_three_name = $project_data[13];
						if(trim($survey_three_name) != "")
						{
							$survey_three_sdetails = i_get_survey_number_list($survey_three_name,$project,'');
							if($survey_three_sdetails["status"] == SUCCESS)
							{
								$survey_three = $survey_three_sdetails["data"][0]["crm_survey_master_id"];
							}
							else
							{
								echo "Import field at row no. ".$row." due to invalid survey no. 3";
								$status = ($status&0);
							}
						}
						else
						{
							$survey_three = "";
						}

						// Field 15 - Survey No. 4
						$survey_four_name = $project_data[14];
						if(trim($survey_four_name) != "")
						{
							$survey_four_sdetails = i_get_survey_number_list($survey_four_name,$project,'');
							if($survey_four_sdetails["status"] == SUCCESS)
							{
								$survey_four = $survey_four_sdetails["data"][0]["crm_survey_master_id"];
							}
							else
							{
								echo "Import field at row no. ".$row." due to invalid survey no. 4";
								$status = ($status&0);
							}
						}
						else
						{
							$survey_four = "";
						}

						if($status != 0)
						{
							$added_by = $user;
							$site_project_iresult = i_add_site_to_project($project,$site_no,$block,$wing,$dimension,$area,'',$site_type,$remarks,$site_status,$site_rel_status,$mortgage_bank,$added_by);
							if($site_project_iresult["status"] == SUCCESS)
							{
								$alert = "Import Successful!";
								$alert_type = 1;

								// Survey Number 1
								if($survey_one != "")
								{
									$site_survey_mapping = i_add_survey_to_site($survey_one,$site_project_iresult["data"],$added_by);
								}

								// Survey Number 2
								if($survey_two != "")
								{
									$site_survey_mapping = i_add_survey_to_site($survey_two,$site_project_iresult["data"],$added_by);
								}

								// Survey Number 3
								if($survey_three != "")
								{
									$site_survey_mapping = i_add_survey_to_site($survey_three,$site_project_iresult["data"],$added_by);
								}

								// Survey Number 4
								if($survey_four != "")
								{
									$site_survey_mapping = i_add_survey_to_site($survey_four,$site_project_iresult["data"],$added_by);
								}
							}
						}
					}

					$row++;
				}
				fclose($handle);
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
}
else
{
	header("location:login.php");
}

// Functions
function upload($file_id,$user_id)
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], $_FILES[$file_id]["name"]);
		}
	}

	return $_FILES[$file_id]["name"];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Import Enquiries</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Import Enquiries</a>
						  </li>
						</ul>

						<br>
							<div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="import_sites" class="form-horizontal" method="post" action="crm_import_sites.php" enctype="multipart/form-data">
									<fieldset>

										<div class="control-group">
											<label class="control-label" for="site_file">Site File</label>
											<div class="controls">
												<input type="file" class="span6" name="site_file" required="required">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />

											<?php

										if($add_perms_list['status'] == SUCCESS)

										{

										?>
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="import_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
										<?php

										}

										else

										{

											?>

											<div class="form-actions">

												You are not authorized to Import Sites

											</div> <!-- /form-actions -->

											<?php

										}

										?>
									</fieldset>
								</form>
								</div>

							</div>


						</div>





					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
