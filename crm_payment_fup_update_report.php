<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_enquiry_fup_list.php
CREATED ON	: 07-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Enquiries
*/

/*
TBD: 
*/$_SESSION['module'] = 'CRM Reports';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String / Get form Data
	// Nothing here
	
	$day = "";
	if(isset($_POST["crm_payment_fup_search_submit"]))
	{	
		if($_POST["start_date"] != "")
		{
			$entry_date_start_form = $_POST["start_date"];
			$entry_date_start      = $entry_date_start_form." 00:00:00";
		}
		else
		{
			$entry_date_start_form = "";
			$entry_date_start      = "";
		}
		
		if($_POST["end_date"] != "")
		{
			$entry_date_end_form = $_POST["end_date"];
			$entry_date_end      = $entry_date_end_form." 23:59:59";
		}
		else
		{
			$entry_date_end_form = "";
			$entry_date_end      = "";
		}
					
		$assigned_to      = $_POST["ddl_user"];
	}
	else
	{
		$entry_date_start = "";
		$entry_date_end   = "";
		$assigned_to      = "-1";
	}

	// Temp data
	$alert = "";

	if(($role == 1) || ($role == 5))
	{
		if($assigned_to != "")
		{	
			$added_by = $assigned_to;
		}
		else		
		{
			$added_by = "";
		}
	}
	else
	{
		$added_by = $user;
	}
	
	$payment_fup_list = i_get_payment_fup_list('','','','','','','',$entry_date_start,$entry_date_end,$added_by);
	if($payment_fup_list["status"] == SUCCESS)
	{
		$payment_fup_list_data = $payment_fup_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$payment_fup_list["data"];
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Payment Follow Up Update Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Payment Follow Up Update List&nbsp;&nbsp;&nbsp;Total Follow Up tobe done: <span id="total_count">0</span>&nbsp;&nbsp;&nbsp;Not Followed Up: <span id="nfu_count">0</span>&nbsp;&nbsp;&nbsp;Followed Up: <span id="fu_count">0</span></h3>
            </div>
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="payment_fup_search" action="crm_payment_fup_update_report.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="start_date" value="<?php echo $entry_date_start_form; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="end_date" value="<?php echo $entry_date_end_form; ?>" />
			  </span>
			  <?php
			  if(($role == "1") || ($role == "10"))
			  {
			  ?>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_user">
			  <option value="">- - Select Executive - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($assigned_to == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php
					
				}
      		  ?>
			  </select>
			  </span>
			  <?php
			  }
			  ?>
			  <input type="submit" name="crm_payment_fup_search_submit" />
			  </form>			  
            </div>
			
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>														
					<th>Project</th>				
					<th>Site No</th>	
					<th>Name</th>
					<th>Mobile</th>										
					<th>Updated Date</th>					
					<th>Updated By</th>
					<th>Remarks</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($payment_fup_list["status"] == SUCCESS)
				{
					$sl_no       = 0;
					$not_updated = 0;					
					for($count = 0; $count < count($payment_fup_list_data); $count++)
					{
						$sl_no++;
						
						if($payment_fup_list_data[$count]["crm_payment_follow_up_updated_by"] != '')
						{
							$updated_time = date('d-M-Y H:i:s',strtotime($payment_fup_list_data[$count]["crm_payment_follow_up_updated_on"]));
							$updated_by   = $payment_fup_list_data[$count]["updater"];
							$font_color   = 'black';
						}
						else
						{
							$updated_time = 'NOT UPDATED';
							$updated_by   = 'NOT UPDATED';
							$font_color   = 'red';
							$not_updated++;
						}
					?>
					<tr style="color:<?php echo $font_color; ?>;">
					<td><?php echo $sl_no; ?></td>										
					<td><?php echo $payment_fup_list_data[$count]["project_name"]; ?></td>					
					<td><?php echo $payment_fup_list_data[$count]["crm_site_no"]; ?></td>
					<td><?php echo $payment_fup_list_data[$count]["name"]; ?></td>
					<td><?php echo $payment_fup_list_data[$count]["cell"]; ?></td>										
					<td><?php echo $updated_time; ?></td>					
					<td><?php echo $updated_by; ?></td>
					<td><a href="#" onClick="alert('<?php echo $payment_fup_list_data[$count]["crm_payment_follow_up_cust_remarks"]; ?>');"><?php echo substr($payment_fup_list_data[$count]["crm_payment_follow_up_cust_remarks"],0,35); ?></a></td>
					<td><a href="crm_add_payment_fup.php?booking=<?php echo $payment_fup_list_data[$count]["crm_booking_id"]; ?>">Follow Up Details</a></td>
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="9">No payment follow up updates!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <script>
			  document.getElementById('total_count').innerHTML = <?php echo $sl_no; ?>;
			  document.getElementById('nfu_count').innerHTML = <?php echo $not_updated; ?>;
			  document.getElementById('fu_count').innerHTML = <?php echo $sl_no - $not_updated; ?>;
			  </script>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>