<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: kns_location_list.php
CREATED ON	: 05-Oct-2016
CREATED BY	: Lakshmi
PURPOSE     : List of Location for customer withdrawals
*/

/*
TBD: 
*/

/* DEFINES - START */
define('COMPANY_MASTER_FUNC_ID','176');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',COMPANY_MASTER_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',COMPANY_MASTER_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',COMPANY_MASTER_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',COMPANY_MASTER_FUNC_ID,'1','1');

	// Query String Data
	// Nothing
		
	// Get companies already added
	$stock_company_master_search_data = array("active"=>'1');
	$comapany_list = i_get_company_list($stock_company_master_search_data);
	if($comapany_list['status'] == SUCCESS)
	{
		$comapany_list_data = $comapany_list['data'];
	}
}
else
{
	header("location:login.php");
}	
?>


<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Company Master List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Company Master List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="stock_master_add_company.php">Add Company</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              					<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>SL No</th>
									<th>Company Name</th>
									<th>Contact person</th>
									<th>Company Address</th>
									<th>Tin no</th>									
									<th>Last Updated By</th>		
									<th>Last Updated On</th>
                                    <th>Action</th>							
								</tr>
								</thead>
								<tbody>							
								<?php
								if($comapany_list["status"] == SUCCESS)
								{			
									$sl_no = 0;
									for($count = 0; $count < count($comapany_list_data); $count++)
									{			
										$sl_no++;
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo $sl_no;; ?></td>
									<td style="word-wrap:break-word;"><?php echo $comapany_list_data[$count]["stock_company_master_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $comapany_list_data[$count]["stock_company_master_contact_person"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $comapany_list_data[$count]["stock_company_master_address"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $comapany_list_data[$count]["stock_company_master_tin_no"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $comapany_list_data[$count]["user_name"]; ?></td>	
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($comapany_list_data[$count]
									["stock_company_master_added_on"])); ?></td>	
									<td><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($comapany_list_data[$count]["stock_company_master_active"] == "1")){?><a href="#" 
									onclick="return delete_company(<?php echo $comapany_list_data[$count]["stock_company_master_id"]; ?>);">Delete</a><?php } ?><?php } ?></td> 
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="8">No company data added yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_company(company_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "stock_master_company_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/stock_master_delete_company.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("company_id=" + company_id + "&action=0");
		}
	}	
}

</script>


  </body>

</html>