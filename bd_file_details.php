<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 29th Dec 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */
/* DEFINES - START */define('BD_FILE_LIST_FUNC_ID','131');define('LAND_STATUS_LIST_FUNC_ID','327');define('DELAY_LIST_FUNC_ID','328');define('FILE_PAYMENT_LIST_FUNC_ID','329');define('FILE_DETAILS_LIST_FUNC_ID','330');define('LAWYER_REPORT_PRINT_FUNC_ID','331');
/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');

/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];	
	$loggedin_name = $_SESSION["loggedin_user_name"];		$land_status_view_perms_list    = i_get_user_perms($user,'',LAND_STATUS_LIST_FUNC_ID,'2','1');	$land_status_edit_perms_list    = i_get_user_perms($user,'',LAND_STATUS_LIST_FUNC_ID,'3','1');	$land_status_delete_perms_list  = i_get_user_perms($user,'',LAND_STATUS_LIST_FUNC_ID,'4','1');		$delay_list_view_perms_list    = i_get_user_perms($user,'',DELAY_LIST_FUNC_ID,'2','1');	$delay_list_edit_perms_list    = i_get_user_perms($user,'',DELAY_LIST_FUNC_ID,'3','1');	$delay_list_delete_perms_list  = i_get_user_perms($user,'',DELAY_LIST_FUNC_ID,'4','1');		$file_payment_list_view_perms_list    = i_get_user_perms($user,'',FILE_PAYMENT_LIST_FUNC_ID,'2','1');	$file_payment_list_edit_perms_list    = i_get_user_perms($user,'',FILE_PAYMENT_LIST_FUNC_ID,'3','1');	$file_payment_list_delete_perms_list  = i_get_user_perms($user,'',FILE_PAYMENT_LIST_FUNC_ID,'4','1');		$file_details_view_perms_list    = i_get_user_perms($user,'',FILE_DETAILS_LIST_FUNC_ID,'2','1');	$file_details_edit_perms_list    = i_get_user_perms($user,'',FILE_DETAILS_LIST_FUNC_ID,'3','1');	$file_details_delete_perms_list  = i_get_user_perms($user,'',FILE_DETAILS_LIST_FUNC_ID,'4','1');		$lawyer_report_view_perms_list    = i_get_user_perms($user,'',LAWYER_REPORT_PRINT_FUNC_ID,'2','1');	$lawyer_report_edit_perms_list    = i_get_user_perms($user,'',LAWYER_REPORT_PRINT_FUNC_ID,'3','1');	$lawyer_report_delete_perms_list  = i_get_user_perms($user,'',LAWYER_REPORT_PRINT_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}
	else
	{
		$file_id = "";
	}		
	/* QUERY STRING - END */
	
	// Temp data
	$alert = "";	
	
	// Get list of files
	$bd_file_list = i_get_bd_files_list($file_id,'','','','','','','');	

	if($bd_file_list["status"] == SUCCESS)
	{
		$bd_file_list_data = $bd_file_list["data"];
	}
	else
	{
		header('location:bd_file_list.php');
	}		// Delay details for the file	$delay_sresult = i_get_delay_details($bd_file_list_data[0]["bd_project_file_id"]);	if($delay_sresult['status'] == SUCCESS)	{		$delay_reason = $delay_sresult['data'][0]['bd_delay_reason_name'];	}	else	{		$delay_reason = 'NO DELAY';	}	// Corresponding Legal File Details	$legal_file_sresult = i_get_file_list('',$bd_file_list_data[0]["bd_project_file_id"],'','','','','','','','','','');	if($legal_file_sresult['status'] == SUCCESS)	{		$is_legal_file = true;		$legal_file_no = $legal_file_sresult['data'][0]['file_number']; 		$legal_file_id = $legal_file_sresult['data'][0]['file_id']; 				// Get list of file payments		$file_payment_list = i_get_file_payment_list($legal_file_id,'');		if($file_payment_list["status"] == SUCCESS)		{			// Get specific file payment details			$file_payment_list_data = $file_payment_list["data"];			for($pay_count = 0 ; $pay_count < count($file_payment_list_data) ; $pay_count++)			{				$paid = $paid + $file_payment_list_data[$pay_count]["file_payment_amount"];			}		}		else		{			$alert = "Invalid File!";			$paid = 0;		}	}	else	{		$is_legal_file = false;		$legal_file_no = 'NO LEGAL FILE';		$legal_file_id = '';		$paid = 0;	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>BD File Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>BD File Details</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">						
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">							
								<div class="tab-pane active" id="formcontrols">
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>File Details</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>	
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>											
											  <tr>
											  <td width="25%"><b>File No</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?> </td>
											  <td width="25%"><b>Survey No</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["bd_file_survey_no"]; ?> </td>
											  </tr>											 
											  <tr>
											  <td width="25%"><b>Project</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["bd_project_name"]; ?> </td>
											  <td width="25%"><b>Village</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["village_name"]; ?> </td>	
											  </tr>
											  <tr>
											  <td width="25%"><b>Land Owner</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["bd_file_owner"]; ?> </td>
											  <td width="25%"><b>Land Owner No</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["bd_file_owner_phone_no"]; ?> </td>
											  </tr>
											  <tr>
											  <td width="25%"><b>Land Owner Address</b></td>											  
											  <td colspan="3"><?php echo $bd_file_list_data[0]["bd_file_owner_address"]; ?> </td>
											  </tr>	
											  <tr>
											  <td width="25%"><b>Extent</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["bd_file_extent"]; ?> guntas</td>
											  <td width="25%"><b>KNS Account</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["bd_own_account_master_account_name"]; ?> </td>
											  </tr>
											  <tr>
											  <td width="25%"><b>Land Cost</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["bd_file_land_cost"]; ?> </td>
											  <td width="25%"><b>Brokerage Amount</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["bd_file_brokerage_amount"]; ?> </td>
											  </tr>
											  <tr>
											  <td width="25%"><b>JDA%</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["bd_project_file_jda_share_percent"]; ?> </td>
											  <td width="25%"><b>Added By</b></td>
											  <td width="25%"><?php echo $bd_file_list_data[0]["user_name"]; ?> </td>
											  </tr>													
											  </tbody>
											</table>
									<div>
										<table class="table table-bordered" style="table-layout: fixed;">
										  <tbody>												<div class="widget-header">												<h3>Actions</h3>												</div> <!-- /widget-header -->
												<tr>												
													<td><a href="bd_add_document_to_file.php?file=<?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?>">Upload Documents</a></td>
													<td><a href="bd_file_doc_list.php?file=<?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?>">View Documents</a>
													</td>
													<td><a href="bd_court_case_list.php?file=<?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?>">Court Case</a>
													</td>												
												</tr>	
												<?php
												if($role == '1')
												{
												?>
												<tr>												
													<td><a href="bd_edit_file.php?file=<?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?>">Edit</a></td>
													<td><a href="bd_remove_file.php?file=<?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?>">Remove</a>
													</td>
													<td><a href="#" onclick="return confirm_deletion(<?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?>);"><span style="color:black; text-decoration: underline;">Delete</span></a>
													</td>												
												</tr>																								<tr>												<td>												<a href="bd_delay_list.php?file=<?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?>" target="_blank"><?php echo $delay_reason; ?> </a></td>																								<td style="word-wrap:break-word;"><?php												if($is_legal_file == false)												{												?><a href="add_file.php?file=<?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?>">Start Process</a>												<?php												}												else												{													echo 'PROCESS STARTED';													?>													<br /><br /><?php													if($file_payment_list_view_perms_list['status'] == SUCCESS)													{													?><a href="file_payment_list.php?file=<?php echo $legal_file_id; ?>&file_id=<?php echo $bd_file_list_data[0]["bd_project_file_id"];?>" target="_blank">Payment Details</a>															<?php													}												}?>												</td>												<td style="word-wrap:break-word;"><?php if($is_legal_file != false) { ?><?php if($lawyer_report_view_perms_list['status'] == SUCCESS)												{													?> <a href="print_lawyer_report.php?file=<?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?>" target="_blank">Lawyer Report</a> <?php } }?></td>												</tr>												<tr><td style="word-wrap:break-word;"><?php echo $bd_file_list_data[$count]["bd_file_owner_status_name"]; ?><br /><?php												if($land_status_edit_perms_list['status'] == SUCCESS)												{												?><a href="bd_update_land_status.php?file=<?php echo $bd_file_list_data[0]["bd_project_file_id"]; ?>">Land Status Update</a><?php } ?><td></td>																										<td></td></tr>																						
												<?php
												}
												?>
										  </tbody>
										</table>	
									</div>
									</fieldset>									
								</div>																
								
							</div>
						  						  
						</div>
	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(bd_file)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "bd_file_list.php";
				}
			}

			xmlhttp.open("GET", "bd_file_delete.php?file=" + bd_file);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}
</script>


  </body>

</html>
