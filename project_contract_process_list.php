<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 05-May-2017
// LAST UPDATED BY: Ashwini
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */
$_SESSION['module'] = 'PM Masters';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	//Get Project Machine Vendor master List
	$project_contract_process_search_data = array("active"=>'1');
	$project_contract_process_list = i_get_project_contract_process($project_contract_process_search_data);
	if($project_contract_process_list["status"] == SUCCESS)
	{
		$project_contract_process_list_data = $project_contract_process_list["data"];
	}
	else
	{
		$alert = $project_contract_process_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Contract Process Master List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Contract Process Master List</h3><span style="float:right; padding-right:20px;"><a href="project_add_contract_process.php"> Add Contract Process</a>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>Name</th>
									<th>Remarks</th>		
									<th>Added By</th>		
									<th>Added On</th>
                                    <th colspan="2" style="text-align:center;">Action</th>									
								</tr>
								</thead>
								<tbody>							
								<?php
								if($project_contract_process_list["status"] == SUCCESS)
								{									
									for($count = 0; $count < count($project_contract_process_list_data); $count++)
									{																	
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo $project_contract_process_list_data[$count]["project_contract_process_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $project_contract_process_list_data[$count]["project_contract_process_remarks"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $project_contract_process_list_data[$count]["user_name"]; ?></td>	
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_contract_process_list_data[$count]
									["project_contract_process_added_on"])); ?></td>	
                                    <td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_contract_process('<?php echo $project_contract_process_list_data[$count]["project_contract_process_id"]; ?>');">Edit </a></div></td>
                                    <td><?php if(($project_contract_process_list_data[$count]["project_contract_process_active"] == "1")){?><a href="#" onclick="return delete_project_contract_process(<?php echo $project_contract_process_list_data[$count]["project_contract_process_id"]; ?>);">Delete</a><?php } ?></td>									
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="5">No Project Contract Process Master data added yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_project_contract_process(contract_process_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_contract_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_delete_contract_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("contract_process_id=" + contract_process_id + "&action=0");
		}
	}	
}

function go_to_project_edit_contract_process(contract_process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_contract_process.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","contract_process_id");
	hiddenField1.setAttribute("value",contract_process_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>