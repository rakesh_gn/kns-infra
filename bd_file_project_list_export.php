<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: Attendance Report

CREATED ON	: 26-Mar-2016

CREATED BY	: Nitin Kashyap

PURPOSE     : Attendance Report

*/



/*

TBD: 

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');


include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');


include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];





	// Temp data

	$alert      = "";

	$alert_type = "";		
	
	$is_not_started = "";
	// Query String
	
	if(isset($_GET["project_id"]))

	{

		$project_id = $_GET["project_id"];

	}

	else

	{

		$project_id = "";

	}

	if(isset($_GET["fund_source"]))

	{

		$fund_source = $_GET["fund_source"];

	}

	else

	{

		$fund_source = "";

	}
	
	if(isset($_GET["project_name"]))

	{

		$project_name = $_GET["project_name"];

	}

	else

	{

		$project_name = "";

	}
	if(isset($_GET["location"]))

	{

		$location = $_GET["location"];

	}

	else

	{

		$location = "";

	}
	
	

	// Get list of files
	$bd_project_list = i_get_bd_project_list($project_id,$project_name,$location,$fund_source,'1');

	

	/* Create excel sheet and write the column headers - START */

	// Instantiate a new PHPExcel object

	$objPHPExcel = new PHPExcel(); 

	// Set the active Excel worksheet to sheet 0

	$objPHPExcel->setActiveSheetIndex(0); 

	// Initialise the Excel row number

	$row_count = 1; 

	// Excel column identifier array

	$column_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S');

	

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "PROJECT NAME");

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "LOCATION"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "LAUNCH MONTH"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "EXTENT"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, "AVAILABLE AREA"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, "TOTAL REVENUE"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "TOTAL COST"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, "NET INCOME"); 
	

	$style_array = array('font' => array('bold' => true));

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[7].$row_count)->applyFromArray($style_array);

	$row_count++;

	if($bd_project_list["status"] == SUCCESS)

	{

		$bd_project_list_data = $bd_project_list['data'];

		$sl_no           = 0;

		

		for($bd_file_count = 0; $bd_file_count < count($bd_project_list_data); $bd_file_count++)

		{

			$sl_no++;
			$total_extent = 0;
			$total_jd_extent = 0;
			$total_extent_guntas = 0;
			$available_area = 0;
			$total_land_cost = 0;
			
			// Check if there is financial calculation settings

			$fin_cal_sresult = i_get_bd_prof_calc_settings('',$bd_project_list_data[$bd_file_count]["bd_project_id"]);

			if($fin_cal_sresult['status'] == SUCCESS)

			{

				$is_fin_calc_added = true;

			}

			else

			{

				$is_fin_calc_added = false;

			}
			
			// Get list of files for this project

			$bd_files_list = i_get_bd_files_list('',$bd_project_list_data[$bd_file_count]["bd_project_id"],'','','','','','1');
			if($bd_files_list["status"] == SUCCESS)

			{
				
				$bd_files_list_data = $bd_files_list["data"];
				for($extent_count = 0 ;$extent_count < count($bd_files_list_data) ; $extent_count++)
				{
					$extent = $bd_files_list_data[$extent_count]["bd_file_extent"];	
					$total_extent    = round(($total_extent + $extent),2);
					
					$jd_share_area = round(($bd_files_list_data[$extent_count]["bd_file_extent"]/GUNTAS_PER_ACRE)*SALEABLE_AREA*($bd_files_list_data[$extent_count]["bd_project_file_jda_share_percent"]/100),2);
					
					$total_jd_extent = round(($total_jd_extent + $jd_share_area),2);
					
					$land_cost = round($bd_files_list_data[0]["bd_file_land_cost"],2); 
				}
				$total_extent_guntas = ($total_extent/GUNTAS_PER_ACRE);
				$available_area = round((($total_extent_guntas*SALEABLE_AREA) - $total_jd_extent),2);$total_land_cost = round(($total_land_cost + $land_cost),2);
				

			}

			else

			{

				$alert = $alert."Alert: ".$bd_files_list["data"];
				$extent = 0;

			}
			
			if($fin_cal_sresult["status"] == SUCCESS)

			{
				$total_revenue = (($total_extent_guntas*SALEABLE_AREA) - $total_jd_extent)*$fin_cal_sresult["data"][0]["bd_profitability_calc_setting_expected_rate"];
				
				$total_dev_cost = ($total_extent_guntas*SALEABLE_AREA)*$fin_cal_sresult["data"][0]["bd_profitability_calc_setting_dev_cost_rate"];
				
				$admin_mktg_cost = ((($total_extent_guntas*SALEABLE_AREA) - $total_jd_extent)*$fin_cal_sresult["data"][0]["bd_profitability_calc_setting_expected_rate"])*$fin_cal_sresult["data"][0]["bd_profitability_calc_setting_admin_charges"]/100; 
				
				$finance_cost = ($total_land_cost + $total_dev_cost + $admin_mktg_cost + $fin_cal_sresult["data"][0]["bd_profitability_calc_setting_stamp_duty_charges"])*2*($fin_cal_sresult["data"][0]["bd_profitability_calc_setting_fianance_rate"]/100);
				
				$net_income = $total_revenue -($total_land_cost + $total_dev_cost + $admin_mktg_cost + $fin_cal_sresult["data"][0]["bd_profitability_calc_setting_stamp_duty_charges"] + $finance_cost); 
			}
			else

			{

				$total_revenue = "0";
				$total_dev_cost = "0";
				$admin_mktg_cost = "0";
				$finance_cost = "0";
				$net_income = "0";

			}
			if($bd_project_list_data[$bd_file_count]["bd_project_gfs_month"] != "0000-00-00")

			{ 

				$gfs_month =  date("M-Y",strtotime($bd_project_list_data[$bd_file_count]["bd_project_launch_date"]));

			}

			else

			{

				$gfs_month =  "Not set yet";

			}

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $bd_project_list_data[$bd_file_count]["bd_project_name"]); 
			
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $bd_project_list_data[$bd_file_count]["bd_project_location"]); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $gfs_month); 
			
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, round($total_extent_guntas)); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, round($available_area)); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, round($total_revenue)); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, round($total_land_cost)); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, round($net_income));

			$row_count++;

		}

	}

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'0'.':'.$column_array[7].$row_count)->getAlignment()->setWrapText(true); 	

	

	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[7].$row_count);

	$row_count++;

	

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'1'.':'.$column_array[7].($row_count - 1))->getAlignment()->setWrapText(true); 	

	/* Create excel sheet and write the column headers - END */

	

	

	header('Content-Type: application/vnd.ms-excel'); 

	header('Content-Disposition: attachment;filename="LandBank - Live.xls"');

	header('Cache-Control: max-age=0'); 

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 

	$objWriter->save('php://output');

}		

else

{

	header("location:login.php");

}	

?>