<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 7th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

define('CRM_ADD_SITE_TRAVEL_ACTUAL_FUNC_ID','110');

/* TBD - START */
/* TBD - END */
$_SESSION['module'] = 'CRM Transactions';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$add_perms_list     = i_get_user_perms($user,'',CRM_ADD_SITE_TRAVEL_ACTUAL_FUNC_ID,'1','1');
	$view_perms_list    = i_get_user_perms($user,'',CRM_ADD_SITE_TRAVEL_ACTUAL_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',CRM_ADD_SITE_TRAVEL_ACTUAL_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',CRM_ADD_SITE_TRAVEL_ACTUAL_FUNC_ID,'4','1');


	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query string data
	if(isset($_GET["enquiry"]))
	{
		$enquiry_id = $_GET["enquiry"];
	}
	else
	{
		$enquiry_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["add_sta_submit"]))
	{
		$enquiry_id       = $_POST["hd_enquiry_id"];
		$site_travel_date = $_POST["dt_site_travel_actual"];
		$project          = $_POST["ddl_project"];
		$vehicle          = $_POST["stxt_vehicle"];
		$opening_km       = $_POST["stxt_opening_km"];
		$closing_km       = $_POST["stxt_closing_km"];
		$driver           = $_POST["stxt_driver"];	
		$num_passengers   = $_POST["stxt_no_passengers"];
		$remarks          = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($site_travel_date !="") && ($project !="") && ($vehicle !="") && ($opening_km !="") && ($closing_km !="") && ($driver !=""))
		{
			$sta_result = i_add_site_travel_actual($site_travel_date,$enquiry_id,$project,$vehicle,$opening_km,$closing_km,$driver,$num_passengers,$remarks,$user);
			
			$alert = "Actual Site Travel entered successfully";
			$alert_type = 1;
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get project list
	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');
	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Cab List
	$cab_list = i_get_cab_list('','');
	if($cab_list["status"] == SUCCESS)
	{
		$cab_list_data = $cab_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$cab_list["data"];
		$alert_type = 0; // Failure
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Site Travel Actual</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Site Travel Actual</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_sta" class="form-horizontal" method="post" action="crm_add_site_travel_actual.php">
								<input type="hidden" name="hd_enquiry_id" value="<?php echo $enquiry_id; ?>" />
									<fieldset>																											
										<div class="control-group">											
											<label class="control-label" for="dt_site_visit">Site Travel Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_site_travel_actual" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
											
										<div class="control-group">											
											<label class="control-label" for="ddl_project">Project*</label>
											<div class="controls">
												<select name="ddl_project" required>
												<option value="">- - Select Project - -</option>
												<?php
												for($count = 0; $count < count($project_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_list_data[$count]["project_id"]; ?>"><?php echo $project_list_data[$count]["project_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
										
										<div class="control-group">											
											<label class="control-label" for="stxt_vehicle">Vehicle Number*</label>
											<div class="controls">
											<input type="text" name="stxt_vehicle" class="span6" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_opening_km">Opening Km*</label>
											<div class="controls">
											<input type="text" name="stxt_opening_km" class="span6" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_closing_km">Closing Km*</label>
											<div class="controls">
											<input type="text" name="stxt_closing_km" class="span6" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_driver">Driver*</label>
											<div class="controls">
											<input type="text" name="stxt_driver" class="span6" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_driver">Remarks</label>
											<div class="controls">
											<textarea name="txt_remarks"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_no_passengers">No. of Passengers</label>
											<div class="controls">
											<input type="text" name="stxt_no_passengers" class="span6" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_sta_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>

