<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 14th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	/* QUERY STRING - START */
	if(isset($_GET["travel_plan_id"]))
	{
		$travel_plan_id = $_GET["travel_plan_id"];
	}
	else
	{
		$travel_plan_id = "";
	}

	$enquiry_id = "";
	if(isset($_GET["enquiry"]))
	{
		$enquiry_id = $_GET["enquiry"];
	}

	$project_name = "";
	if(isset($_GET["project"]))
	{
		$project_name = $_GET["project"];
	}

	$cell="";
	if(isset($_GET["cell"]))
	{
		$cell = $_GET["cell"];
	}

	$name="";
	if(isset($_GET["name"]))
	{
		$name = $_GET["name"];
	}

	$planned_by="";
	if(isset($_GET["planned_by"]))
	{
		$planned_by = $_GET["planned_by"];
	}

	$travel_plan_svp="";
	if(isset($_GET["travel_plan_svp"]))
	{
		$travel_plan_svp = $_GET["travel_plan_svp"];
	}

	$plan_drive="";
	if(isset($_GET["plan_drive"]))
	{
		$plan_drive = $_GET["plan_drive"];
	}

	$travel_plan_cab="";
	if(isset($_GET["travel_plan_cab"]))
	{
		$travel_plan_cab = $_GET["travel_plan_cab"];
	}

	$assigned_to="";
	if(isset($_GET["assigned_to"]))
	{
		$assigned_to = $_GET["assigned_to"];
	}

	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["update_site_travel_submit"]))
	{
		$travel_plan = $_POST["hd_travel_plan_id"];
		$status      = $_POST["ddl_status"];
		$cab_number  = $_POST["stxt_cab_number"];
		$cab_id  = $_POST["ddl_cab"];
		$svp_id = $_POST["travel_plan_svp"];

		// Check for mandatory fields
		if(($travel_plan !="") && ($status !=""))
		{
			$travel_plan_uresult = i_update_site_travel_plan($travel_plan,$status,$cab_number,$cab_id);
			print_r($svp_id);
			$sv_data=array("site_visit_plan_drive"=>"0");
      $site_visit_plan_uresult = db_update_site_visit($svp_id,$sv_data);
			if($travel_plan_uresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				if($status == "2")
				{
					$site_travel_plan_list = i_get_site_travel_plan_list($travel_plan,'','','','','','','','');
					echo $site_travel_plan_list["data"][0]["enquiry_id"]; exit;
				}
				else
				{
					print_r(json_encode($travel_plan_uresult)); exit;
				}
		}
	}
 }
 // Get site travel plan details
 $travel_plan_list = i_get_site_travel_plan_list($travel_plan_id,'','','','','','','','');
 if($travel_plan_list["status"] == SUCCESS)
 {
	 $travel_plan_list_data = $travel_plan_list["data"];
 }
 else
 {
	 $alert = $travel_plan_list["data"];
	 $alert_type = 0;
 }

 $cab_list = i_get_cab_list('','');
  if($cab_list["status"] == SUCCESS)
   {
   	$cab_list_data = $cab_list["data"];
   }
  else{
       echo $cab_list;
   }
}
else
{
	header("location:login.php");
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Add Site Visit Plan</h4>
</div>
	<div class="widget-header" style="height:auto;">
		<div style="border-bottom: 1px solid #C0C0C0;">
			<span class="header-label project">Project Name: </span><span><?php echo $project_name; ?></span>
		</div>
		<div style="border-bottom: 1px solid #C0C0C0;">
			<span class="header-label">Enquiry Number: </span><span id="enquiry"><?php echo $enquiry_id; ?></span>
		</div>
		<div style="border-bottom: 1px solid #C0C0C0;">
			<span class="header-label">Name: </span><span id="name"><?php echo $name; ?></span>
		</div>
		<div style="border-bottom: 1px solid #C0C0C0;">
			<span class="header-label">Phone Number: </span><span id="phone"><?php echo $cell; ?></span>
		</div>
 </div>
 <div class="modal-body">
	<form class="form-group" id="update_travel_form">
		<input  class="form-control" type="hidden" id="plan_id" value="<?php echo $travel_plan_id; ?>" name="plan_id"/>
		<input  class="form-control" type="hidden" id="travel_plan_svp" value="<?php echo $travel_plan_svp; ?>" name="travel_plan_svp"/>
		<input  class="form-control" type="hidden" id="status" value="<?php echo $travel_plan_list_data[0]["crm_site_travel_plan_status"]; ?>" name="plan_id"/>
		<?php
		if(($role == 1) || ($role == 5) || (($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "1") && ($user == $assigned_to || $user==$planned_by))){
		 ?>
		<div class="form-group">
			<label class="control-label" for="ddl_status">Status*</label>
				<select class="form-control" id="ddl_status" name="ddl_status" required>
					<option value="1" <?php if($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "1"){ ?> selected="selected" <?php } ?>>Pending</option>
					<option value="2" <?php if($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "2"){ ?> selected="selected" <?php } ?>>Completed</option>
					<option value="3" <?php if($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "3"){ ?> selected="selected" <?php } ?>>Customer Cancelled</option>
					<option value="4" <?php if($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "4"){ ?> selected="selected" <?php } ?>>Driver Did not turn up</option>
					<option value="5" <?php if($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "5"){ ?> selected="selected" <?php } ?>>Company Cancelled</option>
				</select>
		</div>
		<div class="form-group">
			<label class="control-label" for="ddl_cab">Cab*</label>
				<select id="ddl_cab" name="ddl_cab" class="form-control">
				<option value="2">- - Select Cab - -</option>
				<?php
				foreach ($cab_list_data as $item) {?>
				<option  <?php if($plan_drive == "1" && $item["crm_cab_id"]==2){ ?>
					       selected="selected"
							<?php
						}
							else{
							 if($plan_drive !== "1" && $travel_plan_cab==$item["crm_cab_id"]){?>
								  selected="selected"
									<?php
								}
						 }
						 ?>
							 value="<?php echo  $item["crm_cab_id"]; ?>"><?php echo $item["crm_cab_travels"]; ?></option>
							<?php
							}
							?>
				</select>
		</div>
		<?php
	} ?>
		<div class="form-group">
			<label class="control-label" for="stxt_cab_number">Remarks*</label>
			<input type="text" class="form-control" id="stxt_cab_number" name="stxt_cab_number" placeholder="Vehicle Number of the cab if completed or remarks if any other status" required="required">
		</div>
	</form>
	</div>
<div class="modal-footer">
 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 <button type="button" class="btn btn-primary" onclick="submitPlan()">Submit</button>
</div>
