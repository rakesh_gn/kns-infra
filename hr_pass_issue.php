<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Data initialization
	$alert_type = -1;
	$alert      = "";
	
	$out_pass_filter_data = array('status'=>'1');
	$op_sresult = i_get_out_pass_list($out_pass_filter_data);
	
	for($count = 0; $count < count($op_sresult['data']); $count++)
	{
		// Get attendance for each of these dates
		$attendance_filter_data = array('employee_id'=>$op_sresult['data'][$count]["hr_out_pass_employee"],'attendance_date'=>$op_sresult['data'][$count]["hr_out_pass_date"]);
		$attendance_sresult = i_get_attendance_list($attendance_filter_data);
		
		for($att_count = 0; $att_count < count($attendance_sresult['data']); $att_count++)
		{
			$in_time = explode(':',$attendance_sresult['data'][$att_count]['hr_attendance_in_time']);
			$in_time = ($in_time[0]*60) + $in_time[1];
			echo $in_time.'&nbsp;&nbsp;&nbsp;';
			
			$out_time = explode(':',$attendance_sresult['data'][$att_count]['hr_attendance_out_time']);
			$out_time = ($out_time[0]*60) + $out_time[1];
			echo $out_time.'&nbsp;&nbsp;&nbsp;';
			
			$inp_time = explode(':',$op_sresult['data'][$count]['hr_out_pass_out_time']);
			$inp_time = ($inp_time[0]*60) + $inp_time[1];
			echo $inp_time.'&nbsp;&nbsp;&nbsp;';
			
			$outp_time = explode(':',$op_sresult['data'][$count]['hr_out_pass_finish_time']);
			$outp_time = ($outp_time[0]*60) + $outp_time[1];
			echo $outp_time.'&nbsp;&nbsp;&nbsp;';
			
			$total_time = explode(':',$attendance_sresult['data'][$att_count]['hr_attendance_total_duration']);
			$total_time = ($total_time[0]*60) + $total_time[1];
			echo $total_time.'&nbsp;&nbsp;&nbsp;<br />';
		
			if((($out_time - $in_time) + ($outp_time - $inp_time)) > $total_time)
			{
				// Employee Name
				echo $attendance_sresult['data'][$att_count]['hr_employee_name'].'&nbsp;&nbsp;&nbsp;';
				
				// Employee ID
				echo $attendance_sresult['data'][$att_count]['hr_employee_id'].'&nbsp;&nbsp;&nbsp;';
				
				// Date
				echo $op_sresult['data'][$count]['hr_out_pass_date'].'&nbsp;&nbsp;&nbsp;';
				
				// In Time
				echo $attendance_sresult['data'][$att_count]['hr_attendance_in_time'].'&nbsp;&nbsp;&nbsp;';
				
				// Out Time
				echo $attendance_sresult['data'][$att_count]['hr_attendance_out_time'].'&nbsp;&nbsp;&nbsp;';
				
				// Total Duration
				echo $attendance_sresult['data'][$att_count]['hr_attendance_total_duration'].'&nbsp;&nbsp;&nbsp;';
				
				// Status
				echo $attendance_sresult['data'][$att_count]['hr_attendance_type'].'&nbsp;&nbsp;&nbsp;';
				
				// OP Time START
				echo $op_sresult['data'][$count]['hr_out_pass_out_time'].'&nbsp;&nbsp;&nbsp;';
				
				// OP Time End
				echo $op_sresult['data'][$count]['hr_out_pass_finish_time'].'&nbsp;&nbsp;&nbsp;';
				
				echo '<br />';
			}
		}
	}
}
else
{
	header("location:login.php");
}	
?>