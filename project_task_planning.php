<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';
/* DEFINES - START */
define('PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID', '362');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '2', '1');
		$edit_perms_list   	= i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '1', '1');
		?>

  <script>
    window.permissions = {
      view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    }
  </script>

  <?php
    $project_id = '';
    if (isset($_GET["project_id"])) {
      $project_id   = $_GET["project_id"];
    }

    if (isset($_GET["search_task"])) {
      $search_task  = $_GET["search_task"];
    }
    else {
      $search_task = '';
    }

    $start_date = "";
    if (isset($_REQUEST["start_date"])) {
        $start_date = $_REQUEST["start_date"];
    }

    $end_date = "";
    if (isset($_REQUEST["end_date"])) {
        $end_date = $_REQUEST["end_date"];
    }
    $plan_start_date="";
    $plan_end_date="";
    // Get Project Management Master modes already added
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    $project_management_master_list_data = array();
    if ($project_management_master_list['status'] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list['data'];
    }
} else {
    header("location:login.php");
}
?>

    <html>

    <head>
      <meta charset="utf-8">
      <title>Project Management - Project Task Planning List</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
      <script src="datatable/project_task_scheduling_datatable.js?<?php echo time();?>"></script>
      <link href="./css/style.css?<?php echo time();?>" rel="stylesheet">
      <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
      <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
      <link href="./bootstrap_aku.min.css" rel="stylesheet">
    </head>

    <body>
      <?php
      include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'menu_header.php');?>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Task Planning</h4>
              </div>
              <div class="widget-header" style="height:auto;">
                <div style="border-bottom: 1px solid #C0C0C0;">
                  <span class="header-label">Project Name: </span><span id="project_name"></span>
                </div>
                <div style="border-bottom: 1px solid #C0C0C0;">
                  <span class="header-label">Process: </span><span id="process_name"></span>
                </div>

                <div style="border-bottom: 1px solid #FFFFFF;">
                  <span class="header-label">Task: </span><span id="task_name"></span>
                </div>
              </div>

              <div class="modal-body">
                <form id="edit_task_plan_form">
                  <input type="hidden" id="planning_id" name="planning_id" />
                  <input type="hidden" id="output" name="output" />
                  <input type="hidden" id="task_id" name="task_id" />
                  <input type="hidden" id="selected_row_id" name="selected_row_id" />
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="resource_type" class="control-label">Resource Type</label>
                        <select name="resource_type" required id="resource_type" class="form-control">
                    <option value="">- - -Select Resource Type - -</option>
                    <option value="CW">CW</option>
                    <option value="MC">MC</option>
                    </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="resource_name" class="control-label">Resource Name</label>
                        <select name="resource_name" required id="resource_name" class="form-control">
                  <option value="">- - -Select Resource Name- - -</option>
                  </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="quantity" class="control-label">Quantity</label>
                        <input type="number" class="form-control" id="quantity" name="quantity" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="remarks" class="control-label">Start date</label>
                        <input type="date" class="form-control" id="plan_start_date" name="plan_start_date" class="form-control">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="plan_end_date" class="control-label">End Date</label>
                        <input type="date" class="form-control" id="plan_end_date" name="plan_end_date" class="form-control">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="edit_task_plan_data()">Submit</button>
              </div>
            </div>
          </div>
        </div>
        <!-- MODAL -->
        <div class="main margin-top">
          <div class="main-inner">
            <div class="container">
              <div class="row">
                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3 style="padding-right:3px;">Project Management - Project Task Planning List</h3>
                    <?php if(isset($project_id) && !empty($project_id) && $edit_perms_list['status'] == SUCCESS){ ?>

                    <iframe src="about:blank" id="iframe_file_upload" name="iframe_file_upload" style="width:0px; height:0px;"></iframe>
                    <form id="formImport" style="float:right;margin-top: 5px; margin-right: 5px;" class="form-horizontal" target="iframe_file_upload" method="post" enctype="multipart/form-data">
                      <input type="hidden" id="hd_project_id" name="hd_project_id" class="form-control" />
                      <div class="btn-group btn-group-sm pull-right" role="group">
                        <button class="btn btn-primary" onclick="exportData()">
                                        <span class="glyphicon glyphicon-download"></span> Export
                                      </button>
                        <label class="btn btn-default" style="margin-top: 0px;" id="upload-button">
                                        <span class="glyphicon glyphicon-upload"></span>
                                        Import <input id="input-import_file" type="file" name="file" id="file" accept=".xls" onchange="checkFileAttached()" hidden>
                                    </label>
                        <label class="btn btn-default" style="margin-top: 0px; display:none;" id="processing-button">
                                        <img src="img/ajax-loader.gif">
                                        Wait....
                                    </label>
                      </div>
                    </form>
                    <?php } ?>
                    <!-- Export & Import buttons -->
                    <span class="header-label">Start date : </span><span id="plan_start_date"></span>
                    <span class="header-label">End date : </span><span id="plan_end_date"></span>
                    <span class="header-label">No of days : </span><span id="plan_date_diff"></span>
                  </div>
                  <div class="widget-header widget-toolbar" style="height:auto">
                    <form class="form-inline">
                      <select name="project_id" id="project_id" class="form-control input-sm">
                                 <option value="">- - Select Project - -</option>
                              <?php
                                for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) { ?>
                                 <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>"
                                   <?php if ($project_id == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                                    ?> selected="selected" <?php } ?>>
                                    <?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?>
                                 </option>
                              <?php } ?>
                              </select>
                      <select name="search_process" id="search_process" class="form-control input-sm" style="max-width: 250px;">
                                <opton value="0">------Select Process-----</option>
                              </select>
                      <input type="hidden" name="ddl_task_id" id="ddl_task_id" value=<?php echo $search_task ;?>>
                      <select name="search_task" id="search_task" class="form-control input-sm" style="max-width: 250px;">
                                <opton value="0">------Select Task-----</option>
                              </select>
                      <input type="date" id="start_date" name="start_date" value="<?php echo $start_date; ?>" class="form-control input-sm" />
                      <input type="date" id="end_date" name="end_date" value="<?php echo $end_date; ?>" class="form-control input-sm" />
                      <button id="submit_button" type="button" class="btn btn-primary" onclick="redrawTable()">Submit</button>
                    </form>
                  </div>
                  <?php if($view_perms_list['status'] == SUCCESS){ ?>
                  <div class="widget-content">
                    <table class="table table-striped table-bordered display nowrap" id="example" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Project</th>
                          <th>Process</th>
                          <th>Task Name</th>
                          <th>UOM</th>
                          <th>Road</th>
                          <th>Quantity</th>
                          <th>Actual</th>
                          <th>Resource Type</th>
                          <th>Resource Name</th>
                          <th>No of Objects</th>
                          <th>Per day output</th>
                          <th>Total Days</th>
                          <th>P Start Date</th>
                          <th>P End Date</th>
                          <th>Planning Id</th>
                          <th>Edit</th>
                        </tr>
                      </thead>
                      </tbody>
                    </table>
                  </div>
                <?php }
                else{ ?>
                  <div class="widget-content">
                   <h1>Access Denied</h1>
                   <h3>You dont have permissions to view the Document</h3>
                 </div>
               <?php } ?>
                  <!-- widget-content -->
                </div>
              </div>
            </div>
          </div>
    </body>

    </html>
