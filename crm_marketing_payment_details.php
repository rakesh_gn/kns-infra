<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_marketing'.DIRECTORY_SEPARATOR.'crm_marketing_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_POST['expense']))
	{
		$expenses_id = $_POST['expense'];
	}

	// Capture the form data
	if(isset($_POST["add_marketing_payment_submit"]))
	{
		$expenses_id		= $_POST["hd_expense_id"];
		$mode      			= $_POST["ddl_mode"];
		$instrument_details = $_POST["stxt_instrument"];
		$date               = $_POST["date_date"];
		$paid_to            = $_POST["stxt_paid_to"];
		$amount 			= $_POST["num_amount"];		
		$added_by			= $user;
		
		// Check for mandatory fields
		if(($mode != "") && ($instrument_details != "") && ($date != "") && ($paid_to != "") && ($amount != ""))
		{
			$marketing_iresult = i_add_marketing_expenses_payment($mode,$instrument_details,$date,$paid_to,$amount,$expenses_id,$added_by,'','');
			
			if($marketing_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $marketing_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get Payment Mode
	$payment_mode_type_list = i_get_payment_mode_list('',''); 
	if($payment_mode_type_list["status"] == SUCCESS)
	{
		$payment_mode_type_list_data = $payment_mode_type_list["data"];

	}
	else
	{
		$alert = $payment_mode_type_list["data"];
		$alert_type = 0;
	}
	
	// Get expense details
	$marketing_expenses_list =  i_get_marketing_expenses_list($expenses_id,'','','','','','','','','');
	if($marketing_expenses_list["status"] == SUCCESS)
	{
		$marketing_expenses_list_data = $marketing_expenses_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$marketing_expenses_list["data"];
	}
	
	// Get expense payment details
	$expense_payment_list = i_get_marketing_payment_list('','','','','','1',$expenses_id,'');
	if($expense_payment_list['status'] == SUCCESS)
	{
		$expense_payment_list_data = $expense_payment_list['data'];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Marketing Expense Related Payments</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Mktng Source: <?php echo $marketing_expenses_list_data[0]['enquiry_source_master_name']; ?>&nbsp;&nbsp;&nbsp;Amount: <?php echo $marketing_expenses_list_data[0]['crm_marketing_expenses_amount']; ?>&nbsp;&nbsp;&nbsp;Requested By: <?php echo $marketing_expenses_list_data[0]['user_name']; ?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Marketing Expenses</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_marketing_payment_form" class="form-horizontal" method="post" action="crm_marketing_payment_details.php">
								<input type="hidden" name="hd_expense_id" value="<?php echo $expenses_id; ?>" />
									<fieldset>					

										<div class="control-group">											
											<label class="control-label" for="ddl_mode">Mode</label>
											<div class="controls">
												<select name="ddl_mode" required>
												<?php
												for($count = 0; $count < count($payment_mode_type_list_data); $count++)
												{
												?>
												<option value="<?php echo $payment_mode_type_list_data[$count]["payment_mode_id"]; ?>"><?php echo $payment_mode_type_list_data[$count]["payment_mode_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->									
																				
										<div class="control-group">											
											<label class="control-label" for="stxt_instrument">Instrument Details</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_instrument" placeholder="Cheque No. / DD No. etc." required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="date_date">Date</label>
											<div class="controls">
												<input type="date" class="span6" name="date_date" placeholder="" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_paid_to">Paid to</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_paid_to" placeholder="The person to which the payment was handed over" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount</label>
											<div class="controls">
												<input type="text" class="span6" name="num_amount" placeholder="Amount" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																					
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_marketing_payment_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  						  
						</div>
						<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>SL No</th>
									<th>Payment Mode</th>					
									<th>Instrument</th>	
									<th>Instrument Date</th>
									<th>Paid To</th>
									<th>Amount</th>
									<th>Status</th>
									<th>Issued By</th>
									<th>Issued On</th>
								</tr>
								</thead>
								<tbody>							
								<?php
								$sl_no = 0;
								if($expense_payment_list["status"] == SUCCESS)
								{											
									for($count = 0; $count < count($expense_payment_list_data); $count++)
									{	
										$sl_no = $sl_no + 1;
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
									<td style="word-wrap:break-word;"><?php echo $expense_payment_list_data[$count]["payment_mode_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $expense_payment_list_data[$count]["crm_marketing_expenses_payment_details_instrument_details"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo date('d-M-Y',strtotime($expense_payment_list_data[$count]["crm_marketing_expenses_payment_details_date"])); ?></td>
									<td style="word-wrap:break-word;"><?php echo $expense_payment_list_data[$count]["crm_marketing_expenses_payment_details_paid_to"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $expense_payment_list_data[$count]["crm_marketing_expenses_payment_details_amount"]; ?></td>
									<td style="word-wrap:break-word;"><?php if($expense_payment_list_data[$count]["crm_marketing_expenses_payment_details_status"] == 1)
									{
										echo 'Cleared';
									}
									else
									{
										echo 'Not Cleared';
									}?></td>
									<td style="word-wrap:break-word;"><?php echo $expense_payment_list_data[$count]["user_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($expense_payment_list_data[$count]["crm_marketing_expenses_payment_details_added_on"])); ?></td>									
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="9">No payments done yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
									
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
