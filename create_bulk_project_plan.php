<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD: 
1. User drop down from user table
2. Session management
3. Edit form-actions css to have no background
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert = "";
	$alert_type = -1; // No alert

	// Query String
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}	
	
	if(isset($_POST["bd_file_search_submit"]))
	{		
		$project_id = $_POST["ddl_project"];
	}
	else
	{
		$project_id = '';
	}

	// Get list of process types
	$process_type_list = i_get_process_type_list('',LEGAL_LIAISONING,'1','1'); // Get process types for legal module	
	if($process_type_list["status"] == SUCCESS)
	{
		$process_type_list_data = $process_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$process_type_list["data"];
		$alert_type = 0; // Failure
	}

	// Get list of users
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of all files
	$file_list = i_get_file_list('','','','','','','','','','','',$project_id,'','');
	if($file_list["status"] == SUCCESS)
	{
		$file_list_data = $file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$file_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of BD projects
	$bd_project_list = i_get_bd_project_list('','','','');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_project_list_data = $bd_project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_project_list["data"];
		$alert_type = 0; // Failure
	}

	$selected_user    = '';
	$selected_process = '';
	
	if(!(isset($_POST["create_project_plan_submit"])) && (isset($_POST["process_type"])))
	{
		$process_type = $_POST["process_type"];
		$start_date   = $_POST["start_date"]; 
		
		$process_user_results = i_get_process_user_list($process_type,'','');
		
		if($process_user_results["status"] == SUCCESS)
		{
			$process_user_results_data = $process_user_results["data"];
			$selected_process		   = $process_user_results_data[0]["process_user_maping_process_type"];
			$selected_user			   = $process_user_results_data[0]["process_user_maping_user_id"];
		}
		else
		{
			$alert = $alert."Alert: ".$process_user_results["data"];
			$alert_type = 0; // Failure
		}
	}
	
	if(isset($_POST["create_bulk_project_plan_submit"]))
	{
		// Capture all form data
		$process_type = $_POST["process_type"];
		$start_date   = $_POST["start_date"];		
		$assigned_to  = $_POST["assigned_to"];
		$files        = $_POST["cb_files"];
		
		if(($process_type != "") && ($start_date != "") && (!(empty($files))))
		{						
			if(strtotime(date("Y-m-d")) <= strtotime($start_date))
			{
				$bulk_plan_legal_result = i_legal_add_bulk_process($process_type,$assigned_to,$start_date,$user);
				
				if($bulk_plan_legal_result["status"] != SUCCESS)
				{
					$alert = $bulk_plan_legal_result["data"];
					$alert_type = 0; // Failure
				}
				else
				{
					$file_add = 1;

					// Add files to this bulk process
					for($count = 0; $count < count($files); $count++)
					{
						$file_process_iresult = i_legal_add_bulk_files($bulk_plan_legal_result["data"],$files[$count],$assigned_to,$user);
						
						if($file_process_iresult['status'] != SUCCESS)
						{
							$file_add = $file_add & 0;
						}
					}
					
					if($file_add == 1)
					{
						$alert_type = 1;
						$alert      = "Bulk process successfully initiated";
						
						header("location:add_task.php?bprocess=".$bulk_plan_legal_result["data"]);
					}
					else
					{
						$alert_type = 0;
						$alert      = "Bulk process initiated, but not initiated for all files";
					}
				}
			}
			else
			{
				$alert = "Process Start Date cannot be earlier than today";
				$alert_type = 0; // Failure
			}			
		}
		else	
		{
			$alert = "Please fill all the mandatory fields. Mandatory fields are marked with *";
			$alert_type = 0; // Failure
		}
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Create Bulk Project Plan</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-header" style="height:50px; padding-top:10px;">               
					  <form method="post" id="file_search_form" action="create_bulk_project_plan.php">
					  <span style="padding-left:20px; padding-right:20px;">
					  <select name="ddl_project">
					  <option value="">- - Select Project - -</option>
					  <?php for($count = 0; $count < count($bd_project_list_data); $count++)
					  {?>
					  <option value="<?php echo $bd_project_list_data[$count]["bd_project_id"]; ?>" <?php if($bd_project_list_data[$count]["bd_project_id"] == $project_id){ ?> selected="selected" <?php } ?>><?php echo $bd_project_list_data[$count]["bd_project_name"]; ?></option>
					  <?php
					  }?>
					  </select>
					  </span>
					  <input type="submit" name="bd_file_search_submit" />
					  </form>			  
					</div>
					
					<div class="widget-content">
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Create Bulk Project Plan</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="create_bulk_project_plan" class="form-horizontal" method="post" action="create_bulk_project_plan.php">
								<fieldset>
										
										<div class="control-group">											
											<label class="control-label" for="process_type">Process Type</label>
											<div class="controls">
												<select name="process_type" onchange="this.form.submit();">
												<option value="">- - Select Process - -</option>
												<?php 
												for($count = 0; $count < count($process_type_list_data); $count++)
												{
												?>
												<option value="<?php echo $process_type_list_data[$count]["process_master_id"]; ?>" <?php if($selected_process == $process_type_list_data[$count]["process_master_id"]) { ?> selected <?php } ?>><?php echo $process_type_list_data[$count]["process_name"]; ?></option>												
												<?php
												}
												?>
												</select>
												<p class="help-block">The type of process</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->																														
										<div class="control-group">											
											<label class="control-label" for="start_date">Start Date</label>
											<div class="controls">
												<input type="date" class="span6" name="start_date" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="assigned_to">Assigned To</label>
											<div class="controls">
												<select name="assigned_to">
												<?php 
												for($count = 0; $count < count($user_list_data); $count++)
												{
												?>
												<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($selected_user == $user_list_data[$count]["user_id"]) { ?> selected <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
												<?php
												}
												?>
												</select>
												<p class="help-block">The user to whom this set of activities has to be assigned</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="cb_files">Files</label>
											<div class="controls">
												<table class="table table-bordered" style="table-layout: fixed;">
												<tr>
												<td>SL No</td>
												<td>File No</td>
												<td>Survey No</td>
												<td>Land Owner</td>
												<td>Project Name</td>
												<td>Extent</td>
												<td>&nbsp;</td>
												</tr>
												<?php 
												$sl_no = 0;
												for($count = 0; $count < count($file_list_data); $count++)
												{
													$file_handover_data = i_get_file_handover_details($file_list_data[$count]["file_id"]);
							
													if($file_handover_data["status"] != SUCCESS)
													{
														$sl_no = $sl_no + 1;
														?>			
														<tr>
														<td><?php echo $sl_no; ?></td>
														<td><?php echo $file_list_data[$count]['file_number']; ?></td>
														<td><?php echo $file_list_data[$count]['file_survey_number']; ?></td>
														<td><?php echo $file_list_data[$count]['file_land_owner']; ?></td>
														<td><?php echo $file_list_data[$count]["bd_project_name"]; ?></td>
														<td><?php echo $file_list_data[$count]['file_extent']; ?></td>
														<td><input type="checkbox" name="cb_files[]" value="<?php echo $file_list_data[$count]['file_id']; ?>" /></td>
														</tr>														
														<?php
													}
												}
												?>	
												</table>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="create_bulk_project_plan_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
