<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: reason_list.php
CREATED ON	: 28-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans for a particular process ID
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["request"]))
	{
		$request_id = $_GET["request"];
	}
	else
	{
		header("location:payment_request_list.php");
	}
	
	// Initialization
	$alert_type = -1;
	$alert      = "";

	// Get Payment History Details
	$pay_history_data = array("request"=>$request_id);
	$pay_history_list = i_get_pay_request_history_list($pay_history_data);
	if($pay_history_list["status"] == SUCCESS)
	{
		$pay_history_list_data = $pay_history_list["data"];
		
		$amount       = $pay_history_list_data[0]["legal_payment_request_amount"];
		$task_type    = $pay_history_list_data[0]["task_type_name"];
		if(($pay_history_list_data[0]["process_name"] == '0') || ($pay_history_list_data[0]["process_name"] == ''))
		{
			$process_type = $pay_history_list_data[0]["bulk_process_name"];
			$file_no      = '<a href="bulk_file_list.php?bprocess='.$pay_history_list_data[0]["legal_bulk_process_id"].'" target="_blank">Click</a>';
		}
		else
		{
			$process_type = $pay_history_list_data[0]["process_name"];
			$file_no      = $pay_history_list_data[0]["file_number"];
		}		
		$requested_by = $pay_history_list_data[0]["user_name"];
	}
	else
	{
		$alert = $alert."Alert: ".$pay_history_list["data"];
		
		$amount       = "<i>Invalid payment request</i>";
		$task_type    = "<i>Invalid payment request</i>";
		$process_type = "<i>Invalid payment request</i>";
		$file_no      = "<i>Invalid payment request</i>";
		$requested_by = "<i>Invalid payment request</i>";
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Payment Request Update List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Payment Request Update List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<form action="task_list.php" method="post" id="task_update_form">			
			<span style="padding-left:50px;">
			Amount Requested: <?php echo $amount; ?>&nbsp;&nbsp;&nbsp;&nbsp;Task: <?php echo $task_type; ?>&nbsp;&nbsp;&nbsp;&nbsp;Process: <?php echo $process_type; ?>&nbsp;&nbsp;&nbsp;&nbsp;File: <?php echo $file_no; ?>&nbsp;&nbsp;&nbsp;Requested By: <?php echo $requested_by; ?>
			</span>
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>Status</th>
					<th>Remarks</th>
					<th>Added By</th>
					<th>Added On</th>
				</tr>
				</thead>
				<tbody>
				 <?php
				 if($pay_history_list["status"] == SUCCESS)
				 {
					for($count = 0; $count < count($pay_history_list_data); $count++)
					{					
					?>				
					<tr>
						<td><?php 						
						switch($pay_history_list_data[$count]["pay_status"])
						{
						case "1":
						$pay_status = "HOD Approval Pending";
						break;
						
						case "2":
						$pay_status = "HOD Approved. Pending with Finance";
						break;
						
						case "3":
						$pay_status = "HOD Rejected";
						break;
						
						case "4":
						$pay_status = "Payment Released";
						break;
						
						case "5":
						$pay_status = "Finance Rejected";
						break;
						
						case "6":
						$pay_status = "Finance Held";
						break;
						
						default:
						$pay_status = "HOD Approval Pending";
						break;
						}
						echo $pay_status;
						?></td>
						<td><?php echo $pay_history_list_data[$count]["legal_payment_request_update_remarks"]; ?></td>
						<td><?php echo $pay_history_list_data[$count]["user_name"]; ?></td>
						<td><?php echo date("d-M-Y",strtotime($pay_history_list_data[$count]["legal_payment_request_updated_on"])); ?></td>					
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<td colspan=45">No update added</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <br />			
			</form>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>