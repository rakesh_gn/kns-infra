<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: asset_details_list.php
CREATED ON	: 21-Jan-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('ASSET_DETAILS_LIST_FUNC_ID','232');
/* DEFINES - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'asset'.DIRECTORY_SEPARATOR.'asset_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',ASSET_DETAILS_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',ASSET_DETAILS_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',ASSET_DETAILS_LIST_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',ASSET_DETAILS_LIST_FUNC_ID,'1','1');

	// Query String Data
	// Nothing
	
	$search_material   	 = "";
	
	if(isset($_POST["file_search_submit"]))
	{
		
		$search_material   = $_POST["hd_asset_master_id"];
	}
	
	// Get Asset Master modes already added
	$asset_master_search_data = array("active"=>'1');
	$asset_master_list = i_get_asset_master($asset_master_search_data);
	if($asset_master_list['status'] == SUCCESS)
	{
		$asset_master_list_data = $asset_master_list['data'];
	}	
	else
	{
		$alert = $asset_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Asset Details already added
	$asset_details_search_data = array("active"=>'1',"asset_master_id"=>$search_material);
	$asset_details_list = i_get_asset_details($asset_details_search_data);
	if($asset_details_list['status'] == SUCCESS)
	{
		$asset_details_list_data = $asset_details_list['data'];
	}
	else
	{
		
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Asset Details List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Asset Details List</h3><?php if($add_perms_list["status"] == SUCCESS) {?><span style="float:right; padding-right:20px;"><a href="asset_add_details.php">Asset Add Details</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
			<?php 
			if($view_perms_list["status"] == SUCCESS)
			{
		     ?>
			<div class="widget-header" style=" padding-top:10px;">               
			  <form method="post" id="file_search_form" action="asset_details_list.php">
			  <input type="hidden" name="hd_asset_master_id" id="hd_asset_master_id"  value="<?php echo $search_material; ?>" />		  
			    <div style="padding-left:20px; width:220px; float:left;">	
					<input type="text" name="stxt_material" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();"placeholder="Search by Material" />
					<div id="search_results" class="dropdown-content"></div>
					</div>
			  <input type="submit" name="file_search_submit" />
			  </form>			  
            </div>
			<?php
			} 
			?>
            <div class="widget-content">
			<?php 
			if($view_perms_list["status"] == SUCCESS)
			{
		     ?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="word-wrap:break-word;">SL No</th>
					<th style="word-wrap:break-word;">Asset</th>
					<th style="word-wrap:break-word;">Asset Type</th>
					<th style="word-wrap:break-word;">Vendor</th>
					<th style="word-wrap:break-word;">Location</th>
					<th style="word-wrap:break-word;">Invoice No</th>
					<th style="word-wrap:break-word;">Invoice Amount</th>
					<th style="word-wrap:break-word;">Invoice Date</th>
					<th style="word-wrap:break-word;">Other Charges</th>
					<th style="word-wrap:break-word;">Capitalized Amount</th>
					<th style="word-wrap:break-word;">Serial No</th>
					<th style="word-wrap:break-word;">Units</th>
					<th style="word-wrap:break-word;">Cost Of Asset</th>
					<th style="word-wrap:break-word;">On Date</th>
					<th style="word-wrap:break-word;">Life of Asset</th>
					<th style="word-wrap:break-word;">Asset Used Till</th>
					<th style="word-wrap:break-word;">Remaining Life Form</th>
					<th style="word-wrap:break-word;">Depreciation Rate</th>
					<th style="word-wrap:break-word;">Depreciation Amount</th>
					<th style="word-wrap:break-word;">Wdv as On</th>
					<th style="word-wrap:break-word;">Remarks</th>
					<th style="word-wrap:break-word;">Added By</th>					
					<th style="word-wrap:break-word;">Added On</th>									
					<th colspan="3" style="text-align:center;word-wrap:break-word;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($asset_details_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($asset_details_list_data); $count++)
					{
						$sl_no++;
						$invoice_amount = $asset_details_list_data[$count]["asset_details_invoice_amount"];
						$other_charges = $asset_details_list_data[$count]["asset_details_other_charges"]; 
						$capitalized_amount = $invoice_amount+$other_charges;
						
						$unit = $asset_details_list_data[$count]["asset_details_no_of_units"];
						$cost_of_asset = $capitalized_amount/$unit;
						
						$on_date    = $asset_details_list_data[$count]["asset_details_capitalization_on_date"];
						$start_date = date("Y-m-d");
						$date_diff = get_date_diff($on_date,$start_date);
						$date_diff1 = $date_diff["data"];
						$asset_used_till = $date_diff1/365;
						
						$life_of_asset = $asset_details_list_data[$count]["asset_details_life_of_asset"];
						$life_form = $life_of_asset-$asset_used_till;
						
					?>
					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["stock_material_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_type_master_asset_type"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["stock_vendor_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["stock_location_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_details_invoice_no"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_details_invoice_amount"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($asset_details_list_data[$count][
					"asset_details_invoice_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_details_other_charges"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $capitalized_amount; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_details_serial_no"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_details_no_of_units"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $cost_of_asset; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($asset_details_list_data[$count][
					"asset_details_capitalization_on_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_details_life_of_asset"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_used_till; ?></td>
					<td style="word-wrap:break-word;"><?php echo $life_form ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_details_depreciation_rate"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_details_depreciation_amount"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_details_wdv_as_on"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["asset_details_remarks"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $asset_details_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($asset_details_list_data[$count][
					"asset_details_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_edit_asset_details('<?php echo $asset_details_list_data[$count]["asset_details_id"]; ?>');">Edit</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if(($asset_details_list_data[$count]["asset_details_active"] == "1") && ($delete_perms_list["status"] == SUCCESS)){ ?><a href="#" onclick="return delete_asset_details(<?php echo $asset_details_list_data[$count]["asset_details_id"]; ?>);">Delete</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($add_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_asset_transfer('<?php echo $asset_details_list_data[$count]["asset_details_location"]; ?>','<?php echo $asset_details_list_data[$count]["asset_details_id"]; ?>');">Transfer</a><?php } ?></div></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <?php
			} 
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_asset_details(details_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "asset_details_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/asset_delete_details.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("details_id=" + details_id + "&action=0");
		}
	}	
}

function go_to_edit_asset_details(details_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "asset_edit_details.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","details_id");
	hiddenField1.setAttribute("value",details_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function get_material_list()
{ 
	var searchstring = document.getElementById('stxt_material').value;
	
	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = function()
		{				
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{		
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results').style.display = 'block';
					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}
			}
		}

		xmlhttp.open("POST", "ajax/asset_get_material.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);				
	}
	else	
	{
		document.getElementById('search_results').style.display = 'none';
	}
}

function select_material(asset_master_id,name)
{
	document.getElementById('hd_asset_master_id').value 	= asset_master_id;
	document.getElementById('stxt_material').value = name;
	
	document.getElementById('search_results').style.display = 'none';
}

function go_to_asset_transfer(location,details_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "asset_add_transfer.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","location");
	hiddenField1.setAttribute("value",location);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","details_id");
	hiddenField2.setAttribute("value",details_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>