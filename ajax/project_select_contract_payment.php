<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
  $rowdata = $_POST["val"];
	$vendor_id = $_POST["vendor_id"];
	$contract_length = count($rowdata);

  if($contract_length > 0){
		$start_date = "0000-00-00";
		$end_date = "0000-00-00";
		$length = "";
		$breadth = "";
		$depth = "";
		$measurement = "";
		$total_amount = "";
		$item_count = 0;
		$number = 0;
			$payment_contract_iresult = i_add_project_actual_contract_payment('','','','','','','','','','','','','','',$user);
			if($payment_contract_iresult["status"] == SUCCESS)
			{
				$contract_payment_id = $payment_contract_iresult["data"];
			}
			$total_amount = 0;
			for($item_count = 0 ; $item_count < $contract_length; $item_count++)
			{
					$length  			 = $length + $rowdata[$item_count]['project_task_actual_boq_length'];
					$breadth   			 = $breadth + $rowdata[$item_count]['project_task_actual_boq_breadth'];
					$depth    		 = $depth + $rowdata[$item_count]['project_task_actual_boq_depth'];
					$measurement    = $measurement + $rowdata[$item_count]['project_task_actual_boq_total_measurement'];
					$contract_rate    = $rowdata[$item_count]['project_task_actual_boq_amount'];
					$uom         =    $rowdata[$item_count]['project_task_actual_boq_uom'];
					$number       = $number + $rowdata[$item_count]['project_task_boq_actual_number'];
					$contrct_date     =  $rowdata[$item_count]['project_task_actual_boq_date'];
					$actual_amount    =  $rowdata[$item_count]['project_task_actual_boq_lumpsum'];
					$contract_vendor_id		= $vendor_id;
					$contract_id    = $rowdata[$item_count]['project_task_boq_actual_id'];


					if($start_date == "0000-00-00")
					{
						$start_date = $contrct_date;
					}
					else if(strtotime($contrct_date) < strtotime($start_date))
					{
						$start_date = $contrct_date;
					}

					if($start_date == "0000-00-00")
					{
						$end_date = $contrct_date;
					}
					elseif(strtotime($contrct_date) > strtotime($end_date))
					{
						$end_date = $contrct_date;
					}
					$payment_contract_mapping_iresult = i_add_project_payment_contract_mapping($contract_id,$contract_payment_id,'',$user);
					if ($payment_contract_iresult["status"] == SUCCESS) {
					$project_task_actual_boq_update_data = array("status1"=>"Bill Generated");
					$manpower_results = i_update_project_task_boq_actual($contract_id, $project_task_actual_boq_update_data);
					}
					$total_amount = $total_amount + $actual_amount;
				}

		$contract_start_date = date("Y-m-d",strtotime($start_date)).' '."00:00:00";
		$contract_end_date = date("Y-m-d",strtotime($end_date)) .' '."00:00:00";

		//Update Payment Contract
		$project_actual_contract_payment_update_data = array("vendor_id"=>$contract_vendor_id,"rate"=>$contract_rate,"amount"=>$total_amount,"uom"=>$uom,"number"=>$number,"total_measurement"=>$measurement,"length"=>$length,"breadth"=>$breadth,"depth"=>$depth,"uom"=>$uom,"from_date"=>$contract_start_date,"to_date"=>$contract_end_date);
		$payment_contract_uresults =  i_update_project_actual_contract_payment($contract_payment_id,$project_actual_contract_payment_update_data);

	 $output = array('data'=> $payment_contract_uresults['data'],'status'=> $payment_contract_uresults['status'],'id'=>$contract_payment_id);
		echo json_encode($output);
  }
 }
 else{
   echo "FAILURE";
 }
?>
