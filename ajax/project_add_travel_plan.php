<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_site_visit.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
// include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
  $project = "";
  $date    = "";

  // Capture the form data
  if((!(isset($_POST["add_stp_submit"]))) && (isset($_POST["ddl_project"])))
  {
    $project = $_POST["ddl_project"];
  }
  if(isset($_POST["add_stp_submit"]))
  {
    $date    = $_POST["dt_site_visit"];
    $project = $_POST["ddl_project"];
    $sv_plan = $_POST["cb_sv_plans"];
    $cab     = $_POST["ddl_cab"];

    // Check for mandatory fields
    if(($date !="") && ($cab !=""))
    {
      // Process all the checked site visits
        $stp_result = i_add_site_travel_plan($sv_plan,$cab,$project,$date,$user);
        $site_visit_update_data = array("status1"=>"Planned");
        $stp_update_result= db_update_site_visit($sv_plan,$site_visit_update_data);
        $alert = "Cab Plan entered successfully";
        $alert_type = 1;
    }
    else
    {
      $alert = "Please fill all the mandatory fields";
      $alert_type = 0;
    }
  }
		echo $alert_type;
  }
 else{
   echo "FAILURE";
 }
?>
