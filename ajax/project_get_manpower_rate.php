<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$vendor_id      = $_POST["vendor_id"];
	$men_rate = 0;
	$women_rate = 0;
	$mason_rate = 0;
	$project_man_power_rate_search_data = array("vendor_id"=>$vendor_id);
	$project_manpower_list = i_get_project_man_power_rate_master($project_man_power_rate_search_data);
	
	if($project_manpower_list['status'] == SUCCESS)
	{
		for($rate_count =0 ; $rate_count < count($project_manpower_list["data"]) ; $rate_count++ )
		{
			$project_manpower_list_data = $project_manpower_list['data'];
			if($project_manpower_list_data[$rate_count]["project_man_power_type_id"] == 2)
			{
				$women_rate = $project_manpower_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
			}
			if($project_manpower_list_data[$rate_count]["project_man_power_type_id"] == 1)
			{
				$men_rate = $project_manpower_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
			}
			if($project_manpower_list_data[$rate_count]["project_man_power_type_id"] == 3)
			{
				$mason_rate = $project_manpower_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
			}
			else
			{
				
			}
		}
	}
	else
	{
		
	}
	$result = array("men_rate"=>$men_rate,"women_rate"=>$women_rate,"mason_rate"=>$mason_rate);
	echo json_encode($result);
}
else
{
	header("location:login.php");
}
?>