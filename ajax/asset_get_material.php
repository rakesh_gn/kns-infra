<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'asset'.DIRECTORY_SEPARATOR.'asset_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$search  = $_POST["search"];
	$asset_master_search_data = array("name"=>$search);
	$asset_master_list_result = i_get_asset_master($asset_master_search_data);
	if($asset_master_list_result["status"] == SUCCESS)
	{		
		$asset_master_list = '';

		for($count = 0; $count < count($asset_master_list_result['data']); $count++)
		{
			$asset_master_list = $asset_master_list.'<a style="display:block; width:100%; border-bottom:1px solid #efefef; text-decoration:none; cursor:pointer; padding:5px;" onclick="return select_material(\''.$asset_master_list_result['data'][$count]['asset_master_id'].'\',\''.$asset_master_list_result['data'][$count]['stock_material_name'].'\');">'.$asset_master_list_result['data'][$count]['stock_material_name'].'</a>';
		}
	}
	else
	{
		$asset_master_list = 'FAILURE';
	}
	
	echo $asset_master_list;
}
else
{
	header("location:login.php");
}
?>