<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

  $start_date = $_GET["start_date"];
  $end_date = $_GET["end_date"];
  $user_id = $_GET["user_id"];

  $follow_up_data = i_get_enquiry_fup_list('','',$user_id,'','','',$start_date,$end_date,'assignee_only','','','');
	// print_r($follow_up_data); exit;
  if($follow_up_data["status"] == SUCCESS)
  {
		$startDate=intval(substr(explode(" ",$start_date)[1],0,2));
		$endDate=intval(substr(explode(" ",$end_date)[1],0,2));
		$answered_fup_count=0;
		$not_answered_fup_count=0;
		$follow_up_list_data=$follow_up_data["data"];
		for($count=0;$count<count($follow_up_list_data);$count++){
			$follow_up_start_datetime=intval(substr(explode(" ",$follow_up_list_data[$count]["enquiry_follow_up_added_on"])[1],0,2));
			if($follow_up_list_data[$count]["enquiry_follow_up_call_status"]==""){
				continue;
			}
			else if($follow_up_list_data[$count]["enquiry_follow_up_call_status"]=="answered"){
				if($follow_up_start_datetime>=$startDate && $follow_up_start_datetime<=$endDate){
					$answered_fup_count++;
				}
			}
			else{
				if($follow_up_start_datetime>=$startDate && $follow_up_start_datetime<=$endDate){
					$not_answered_fup_count++;
				}
			}
		}
  }
  else
  {
		$answered_fup_count=0;
		$not_answered_fup_count=0;
  }
$startDate=explode(" ",$start_date);
$endDate=explode(" ",$end_date);
$answered_status="answered_".substr($startDate[1],0,2)."_".substr($endDate[1],0,2);
$not_answered_status="not_answered_".substr($startDate[1],0,2)."_".substr($endDate[1],0,2);
  $output = array($answered_status=>$answered_fup_count,$not_answered_status=>$not_answered_fup_count);
	echo json_encode($output);
}
 else{
   echo "FAILURE";
 }
?>
