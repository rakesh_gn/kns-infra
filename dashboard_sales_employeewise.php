<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Search parameters
	if(isset($_POST["marketing_results_employeewise_filter_submit"]))
	{		
		$start_date = $_POST['dt_start_filter'];
		$end_date   = $_POST['dt_end_filter'];		
	}
	else
	{	
		$start_date = date('Y-m-d');
		$end_date   = date('Y-m-d');
	}
	
	// Get list of sales department employees
	$user_result = i_get_user_list('','','','','1','1');	
	if($user_result['status'] == SUCCESS)
	{
		$user_result_data = $user_result['data'];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Marketing Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:50px; padding-top:10px;"> <i class="icon-th-list"></i>
              <h3>Employeewise Marketing Report</h3>		  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="marketing_daywise_filter_form" action="dashboard_sales_employeewise.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_filter" value="<?php echo $start_date; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_filter" value="<?php echo $end_date; ?>" />
			  </span>			  			  
			  <input type="submit" name="marketing_results_employeewise_filter_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="word-wrap:break-word;">Employee</th>
					<th style="word-wrap:break-word;">Leads</th>
					<th style="word-wrap:break-word;">Follow up Leads</th>					
					<th style="word-wrap:break-word;">Planned Site Visit</th>
					<th style="word-wrap:break-word;">Actual Site Visit</th>
					<th style="word-wrap:break-word;">Sales</th>
					<th style="word-wrap:break-word;">Sales (sq. ft)</th>
					<th style="word-wrap:break-word;">Prospective Clients</th>
					<th style="word-wrap:break-word;">Unqualified</th>
				</tr>
				</thead>
				<tbody>
				<?php									
				if($user_result["status"] == SUCCESS)
				{
					for($count = 0; $count < count($user_result['data']); $count++)
					{
						// No of follow ups
						$fup_filter_array = array('fup_start_date'=>$start_date.' 00:00:00','fup_end_date'=>$end_date.' 23:59:59','assigned_to'=>$user_result['data'][$count]['user_id']);						
						$fup_count = i_get_enquiry_fup_count($fup_filter_array);						
						
						// No of followed up
						$fupped_filter_array = array('fupped_start_date'=>$start_date.' 00:00:00','fupped_end_date'=>$end_date.' 23:59:59','added_by'=>$user_result['data'][$count]['user_id']);						
						$fupped_count = i_get_enquiry_fup_count($fupped_filter_array);
						
						// No of site visits
						$site_visit_filter = array('start_date'=>$start_date.' 00:00:00','end_date'=>$end_date.' 23:59:59','added_by'=>$user_result['data'][$count]['user_id']);
						$site_visit_count = i_get_sv_count($site_visit_filter);
						
						// No of site visits completed
						$comp_sv_result = i_get_site_visit_completed_enquiries($user_result['data'][$count]['user_id'],$start_date,$end_date);
						if($comp_sv_result['status'] == SUCCESS)
						{
							$comp_sv_count = count($comp_sv_result['data']);
						}
						else
						{
							$comp_sv_count = 0;
						}

						// No of sales
						$booking_filter_data = array('booking_start_date'=>$start_date,'booking_end_date'=>$end_date,'added_by'=>$user_result['data'][$count]['user_id'],'status'=>'1');
						$sales_result = i_get_booking_summary_data($booking_filter_data);
						$sales_count  = $sales_result['data']['salecount'];
						$sales_area   = $sales_result['data']['salearea'];
						
						// No of prospective clients
						$prospective_filter_array = array('added_by'=>$user_result['data'][$count]['user_id']);
						$prosp_count = i_get_prospective_count($prospective_filter_array);		

						// No of leads unqualified
						$unq_list = i_get_unqualified_leads('','','','','',$start_date.' 00:00:00',$end_date.' 23:59:59','','','','',$user_result['data'][$count]['user_id']);
						if($unq_list['status'] == SUCCESS)
						{
							$unq_count = count($unq_list['data']);
						}
						else
						{
							$unq_count = 0;
						}
						?>
						<tr>
						<td style="word-wrap:break-word;"><?php echo $user_result['data'][$count]['user_name']; ?></th>
						<td style="word-wrap:break-word;"><?php echo $fup_count; ?></th>
						<td style="word-wrap:break-word;"><?php echo $fupped_count; ?></th>						
						<td style="word-wrap:break-word;"><?php echo $site_visit_count; ?></th>
						<td style="word-wrap:break-word;"><?php echo $comp_sv_count; ?></th>
						<td style="word-wrap:break-word;"><?php echo $sales_count; ?></th>
						<td style="word-wrap:break-word;"><?php echo $sales_area; ?></th>
						<td style="word-wrap:break-word;"><?php echo $prosp_count; ?></th>
						<td style="word-wrap:break-word;"><?php echo $unq_count; ?></th>
						</tr>
						<?php
					}
				}
				else
				{
				?>
				<td colspan="4">No marketing source type added!</td>
				<?php
				}
				?>	

                </tbody>
              </table>	
			  <script>
			  document.getElementById('total_leads').innerHTML = <?php echo $total_num_leads; ?>;
			  </script>
            </div>
			
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    
<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

  </body>

</html>
