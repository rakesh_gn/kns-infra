<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: Attendance Report
CREATED ON	: 26-Mar-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Attendance Report
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_payment_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];


	// Temp data
	$alert      = "";
	$alert_type = "";		
	
	// Query String
	if(isset($_GET["village"]))
	{
		$village = $_GET["village"];
	}
	else
	{
		$village = "";
	}
	
	if(isset($_GET["bank"]))
	{
		$bank = $_GET["bank"];
	}
	else
	{
		$bank = "";
	}
	
	if(isset($_GET["start_date"]))
	{
		$start_date = $_GET["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(isset($_GET["end_date"]))
	{
		$end_date = $_GET["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	// Get borrowing details
	$borrow_details = i_get_bd_borrow_list('','',$bank,'','','','','',$village,'','',$start_date,$end_date);
	
	/* Create excel sheet and write the column headers - START */
	// Instantiate a new PHPExcel object
	$objPHPExcel = new PHPExcel(); 
	// Set the active Excel worksheet to sheet 0
	$objPHPExcel->setActiveSheetIndex(0); 
	// Initialise the Excel row number
	$row_count = 1; 
	// Excel column identifier array
	$column_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N');
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "SURVEY NO");
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "LAND OWNER"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "KNS ACCOUNT"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "VILLAGE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, "EXTENT (GUNTAS)"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, "EXTENT (ACRES)"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "EXTENT (SQ. FT)"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, "BANK"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, "MORTGAGE DATE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "LOAN VALUE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, "RELEASED DATE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, "REPAID VALUE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, "REMARKS"); 
	
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[12].$row_count)->applyFromArray($style_array);
	$row_count++;
	if($borrow_details["status"] == SUCCESS)
	{
		$borrow_details_data = $borrow_details['data'];		$borrower 		     = $borrow_details_data["data"][0]["bd_file_borrower"];
		$sl_no           = 0;
		
		for($borrow_count = 0 ; $borrow_count < count($borrow_details_data); $borrow_count++)
		{
			$sl_no++;
			
			$repaid_value = 0;
			
			// Get Borrow Payment List
			$payment_request_data = array("borrow_id"=>$borrow_details_data[$borrow_count]['bd_file_borrow_id']);
			$bd_file_borrow_payment_list = i_get_borrow_bd_payment_list($payment_request_data);
			if($bd_file_borrow_payment_list["status"] == SUCCESS)
			{
				$bd_file_borrow_payment_list_data = $bd_file_borrow_payment_list["data"];				
				for($bpcount = 0; $bpcount < count($bd_file_borrow_payment_list_data); $bpcount++)
				{
					$repaid_value = $repaid_value + $bd_file_borrow_payment_list_data[$bpcount]["bd_file_borrow_payment_amount"];
				}
			}
			
			$extent_in_acres = (($borrow_details_data[$borrow_count]["bd_file_extent"])/GUNTAS_PER_ACRE);
			$extent_in_sq_ft = $extent_in_acres * SQUARE_FOOT_PER_ACRE;
			
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $borrow_details_data[$borrow_count]['bd_file_survey_no']); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $borrower); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $borrow_details_data[$borrow_count]['bd_own_account_master_account_name']); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $borrow_details_data[$borrow_count]['village_name']); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, $borrow_details_data[$borrow_count]['bd_file_extent']); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, $extent_in_acres); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $extent_in_sq_ft); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, $borrow_details_data[$borrow_count]['crm_bank_name']); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, date('d-M-Y',strtotime($borrow_details_data[$borrow_count]['bd_file_borrow_mortgage']))); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $borrow_details_data[$borrow_count]['bd_file_borrow_recieved_loan_value']); 			
			if($borrow_details_data[$borrow_count]['bd_file_borrow_released_date'] == '0000-00-00')
			{
				$release_date = '';
			}
			else 
			{
				$release_date = date('d-M-Y',strtotime($borrow_details_data[$borrow_count]['bd_file_borrow_released_date']));
			}
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, $release_date); 
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, $repaid_value);
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, $borrow_details_data[$borrow_count]['bd_file_borrow_remarks']); 			
			$row_count++;
		}
	}
	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'0'.':'.$column_array[12].$row_count)->getAlignment()->setWrapText(true); 	
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[12].$row_count);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'1'.':'.$column_array[12].($row_count - 1))->getAlignment()->setWrapText(true); 	
	/* Create excel sheet and write the column headers - END */
	
	
	header('Content-Type: application/vnd.ms-excel'); 
	header('Content-Disposition: attachment;filename="Borrow Details.xls"'); 
	header('Cache-Control: max-age=0'); 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
	$objWriter->save('php://output');
}		
else
{
	header("location:login.php");
}	
?>