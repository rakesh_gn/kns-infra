<?php
  session_start();
  $_SESSION['module'] = 'PM Masters';

  /* DEFINES - START */
  define('PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID', '359');
  /* DEFINES - END */

  // Includes
  $base = $_SERVER["DOCUMENT_ROOT"];
  require dirname(__FILE__) . '/utilities/PHPExcel-1.8/Classes/PHPExcel.php';
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

  if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '1', '1');

    // Query String Data
    // Nothing

    // Temp data
    $alert_type = -1;
    $alert 	    = "";

    if (isset($_REQUEST['hd_project_id'])) {
      $project_id = $_REQUEST['hd_project_id'];
    } else {
      $project_id = "";
    }

    // Get Already added Object Output

    $project_budget_planning_list = db_get_project_budget_history($project_id);
    if ($project_budget_planning_list["status"] == SUCCESS) {
      $project_budget_planning_list_data = $project_budget_planning_list["data"];
    } else {
      $alert = $alert."Alert: ".$project_budget_planning_list["data"];
      exit;
    }

    $project_name = $project_budget_planning_list["data"][0]['project_master_name'];
    $filename = $project_name."-budget-history-data_".date("m-d-Y-h-m-s").".xls";
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // PRINT HEADERS
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Project')
                ->setCellValue('B1', 'Process_name')
                ->setCellValue('C1', 'Task')
                ->setCellValue('D1', 'Road Name')
                ->setCellValue('E1', 'Manpower')
                ->setCellValue('F1', 'Machine')
                ->setCellValue('G1', 'Contract')
                ->setCellValue('H1', 'Material')
                ->setCellValue('I1', 'Remarks')
                ->setCellValue('J1', 'History File')
                ->setCellValue('K1', 'Changed By')
                ->setCellValue('L1', 'Changed On');

      for ($count = 0; $count < count($project_budget_planning_list_data); $count++) {
        if ($project_budget_planning_list_data[$count]["budget_history_road_id"] != "0") {
            $road_name = $project_budget_planning_list_data[$count]["project_site_location_mapping_master_name"];
        } else {
            $road_name = "No Roads";
        }

        $changed_on=date("d-m-Y",strtotime($project_budget_planning_list_data[$count]["budget_history_changed_on"]));

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($count+2), $project_budget_planning_list_data[$count]["project_master_name"])
                    ->setCellValue('B'.($count+2), $project_budget_planning_list_data[$count]["project_process_master_name"])
                    ->setCellValue('C'.($count+2), $project_budget_planning_list_data[$count]["project_task_master_name"])
                    ->setCellValue('D'.($count+2), $road_name)
                    ->setCellValue('E'.($count+2), $project_budget_planning_list_data[$count]["budget_history_manpower"])
                    ->setCellValue('F'.($count+2), $project_budget_planning_list_data[$count]["budget_history_machine"])
                    ->setCellValue('G'.($count+2), $project_budget_planning_list_data[$count]["budget_history_contract"])
                    ->setCellValue('H'.($count+2), $project_budget_planning_list_data[$count]["budget_history_material"])
                    ->setCellValue('I'.($count+2), $project_budget_planning_list_data[$count]["budget_history_remarks"])
                    ->setCellValue('J'.($count+2), $project_budget_planning_list_data[$count]["budget_history_file"])
                    ->setCellValue('K'.($count+2), $project_budget_planning_list_data[$count]["user_name"])
                    ->setCellValue('L'.($count+2), $changed_on);
}
    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle($project_name);

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
  } else {
    header("location:login.php");
  }
