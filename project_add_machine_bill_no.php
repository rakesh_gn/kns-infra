<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th oct 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Query String Data
	if(isset($_REQUEST['payment_machine_id']))
	{
		$payment_machine_id = $_REQUEST['payment_machine_id'];
	}
	else
	{
		$payment_machine_id = '';
	}

	// Capture the form data
	if(isset($_POST["add_bill_no_submit"]))
	{
		$payment_machine_id  		  = $_POST["machine_id"];
		$billing_address    = $_POST["billing_address"];
		$remarks      		  = $_POST["remarks"];
		$approved_on 		  	= date("Y-m-d H:i:s");
		$project_actual_machine_payment_update_data = array("billing_address"=>$billing_address,"approved_by"=>$user,"approved_on"=>$approved_on,"remarks"=>$remarks);
		$approve_payment_machine_result = i_update_project_payment_machine($payment_machine_id,$project_actual_machine_payment_update_data);
		if($approve_payment_machine_result["status"] == SUCCESS)
		{
			$project_payment_machine_search_data = array("payment_machine_id"=>$payment_machine_id);
			$payment_machine_list = i_get_project_payment_machine($project_payment_machine_search_data);

			if($payment_machine_list["status"] == SUCCESS)
			{
				$payment_machine_list_data = $payment_machine_list["data"];
				$machine_bill_no = $payment_machine_list_data[0]["project_payment_machine_bill_no"];
			}
			echo $machine_bill_no; exit;
		} else {
			print_r($approve_payment_machine_result); exit;
		}
	}
}
else
{
	header("location:login.php");
}
?>
