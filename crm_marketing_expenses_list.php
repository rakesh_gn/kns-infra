<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_enquiry_source_list.php
CREATED ON	: 06-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Enquiry Sources
*/
define('CRM_MARKETING_EXPENSES_LIST_FUNC_ID','116');
/*
TBD: 
*/$_SESSION['module'] = 'CRM Transactions';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_marketing'.DIRECTORY_SEPARATOR.'crm_marketing_functions.php');include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
    // Get permission settings for this user for this page	$add_perms_list     = i_get_user_perms($user,'',CRM_MARKETING_EXPENSES_LIST_FUNC_ID,'1','1');	$view_perms_list    = i_get_user_perms($user,'',CRM_MARKETING_EXPENSES_LIST_FUNC_ID,'2','1');	$edit_perms_list    = i_get_user_perms($user,'',CRM_MARKETING_EXPENSES_LIST_FUNC_ID,'3','1');	$delete_perms_list  = i_get_user_perms($user,'',CRM_MARKETING_EXPENSES_LIST_FUNC_ID,'4','1');
	// Query String Data
	// Nothing

	// Temp data
	$alert = "";

	$marketing_expenses_list =  i_get_marketing_expenses_list('','','','','','','','','','');
	if($marketing_expenses_list["status"] == SUCCESS)
	{
		$marketing_expenses_list_data = $marketing_expenses_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$marketing_expenses_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Marketing Expensess List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Marketing Expenses List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<span id="span_msg"></span>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th style="word-wrap:break-word;">SL No</th>					
					<th style="word-wrap:break-word;">Marketing Source</th>					
					<th style="word-wrap:break-word;">Marketing From Date</th>					
					<th style="word-wrap:break-word;">Marketing To Date</th>					
					<th style="word-wrap:break-word;">Amount</th>
					<th style="word-wrap:break-word;">Predicted Leads</th>
					<th style="word-wrap:break-word;">Contact Person</th>
					<th style="word-wrap:break-word;">Contact Details</th>
					<th style="word-wrap:break-word;">Remarks</th>
					<th style="word-wrap:break-word;">Added By</th>
					<th style="word-wrap:break-word;">Added On</th>
					<th style="word-wrap:break-word;">Status</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
					<th style="word-wrap:break-word;">&nbsp;</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($marketing_expenses_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($marketing_expenses_list_data); $count++)
					{
						$sl_no++;
				    if($marketing_expenses_list_data[$count]["crm_marketing_expenses_approval_status"]==0)
					{
						$text = "Pending";
					}
					else if($marketing_expenses_list_data[$count]["crm_marketing_expenses_approval_status"]==1)
					{
						 $text = "Approved";
					}
					else
					{
						 $text = "Rejected";
					}?>
					
					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $marketing_expenses_list_data[$count]["enquiry_source_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($marketing_expenses_list_data[$count]["crm_marketing_expenses_applicable_start_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($marketing_expenses_list_data[$count]["crm_marketing_expenses_applicable_end_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $marketing_expenses_list_data[$count]["crm_marketing_expenses_amount"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $marketing_expenses_list_data[$count]["crm_marketing_expenses_predicted_leads"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $marketing_expenses_list_data[$count]["crm_marketing_expenses_contact_person"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $marketing_expenses_list_data[$count]["crm_marketing_expenses_contact_no"]; ?>&nbsp;&nbsp;&nbsp;<?php echo $marketing_expenses_list_data[$count]["crm_marketing_expenses_email"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $marketing_expenses_list_data[$count]["crm_marketing_expenses_remarks"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $marketing_expenses_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($marketing_expenses_list_data[$count]["crm_marketing_expenses_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $text; ?></td>
					<?php if(($role == '1') || ($role == '5')) 
					{?>
					<td style="word-wrap:break-word;">
					<?php if(($marketing_expenses_list_data[$count]["crm_marketing_expenses_approval_status"] == 0) || ($marketing_expenses_list_data[$count]["crm_marketing_expenses_approval_status"] == 2)){?>
					<a href="#" onclick="return approve_marketing_expenses(<?php echo $marketing_expenses_list_data[$count]["crm_marketing_expenses_id"]; ?>);"><span style="color:black; text-decoration: underline;">Approve</span></a>
					<?php
					}?></td>
					<td style="word-wrap:break-word;">
					<?php if(($marketing_expenses_list_data[$count]["crm_marketing_expenses_approval_status"] == 0) || ($marketing_expenses_list_data[$count]["crm_marketing_expenses_approval_status"] == 1)){?>
					<a href="#" onclick="return reject_marketing_expenses(<?php echo $marketing_expenses_list_data[$count]["crm_marketing_expenses_id"]; ?>);"><span style="color:black; text-decoration: underline;">Reject</span></a>
					<?php
					}
					?></td>	
					<?php
					}
					else
					{
						?>
						<td style="word-wrap:break-word;"></td>
						<td style="word-wrap:break-word;"></td>
						<?php
					}
					?>
					<td>
					<?php
					if($role == '1')
					{
						?>
						<a href="#" onclick="return go_to_payments(<?php echo $marketing_expenses_list_data[$count]["crm_marketing_expenses_id"]; ?>);"><span style="color:black; text-decoration: underline;">Payments</span></a>
						<?php
					}
					?></td>									
					<td>
					<?php
					if(($role == '1') || ($role == '5'))
					{
						?>
						<a href="#" onclick="return go_to_edit_expense(<?php echo $marketing_expenses_list_data[$count]["crm_marketing_expenses_id"]; ?>);"><span style="color:black; text-decoration: underline;">Edit</span></a>
						<?php
					}
					?></td>				
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="16">No Marketing Expenses added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function go_to_payments(expense_id)
{		
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("action", "crm_marketing_payment_details.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","expense");
	hiddenField1.setAttribute("value",expense_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
	form.submit();
}

function go_to_edit_expense(expense_id)
{		
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("action", "crm_edit_marketing_expenses.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","hd_navigate_expense_id");
	hiddenField1.setAttribute("value",expense_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
	form.submit();
}

function approve_marketing_expenses(crm_marketing_expenses_id)
{
	var ok = confirm("Are you sure you want to approve?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "crm_marketing_expenses_list.php";
					}
				}
			}

			xmlhttp.open("POST","marketing_expenses_update.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("marketing_expenses_id=" + crm_marketing_expenses_id + "&status=1");
		}
	}	
}

function reject_marketing_expenses(crm_marketing_expenses_id)
{
	var ok = confirm("Are you sure you want to reject?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{				
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "crm_marketing_expenses_list.php";
					}
				}
			}

			xmlhttp.open("POST", "marketing_expenses_update.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("marketing_expenses_id=" + crm_marketing_expenses_id + "&status=2");
		}
	}	
}
</script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>